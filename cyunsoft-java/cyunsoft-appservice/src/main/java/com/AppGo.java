package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * WEB启动，主程序入口
 * @author lsq
 */
@EnableTransactionManagement // 开启事务管理
@EnableAsync(proxyTargetClass = true)
@SpringBootApplication
//@EnableAspectJAutoProxy //读写分离时打开
//@MapperScan("com.core136.mapper.**.mapper")
@MapperScan(basePackages = "com.core136.mapper")
public class AppGo extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AppGo.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(AppGo.class);
    }

	/**
	 * 解决TOMCAT8.5以上版本不能接收[]{}等字符参数
	 * @return ConfigurableServletWebServerFactory
	 */
	@Bean
    public ConfigurableServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(connector -> connector.setProperty("relaxedQueryChars", "|{}[]\\"));
        return factory;
    }
}
