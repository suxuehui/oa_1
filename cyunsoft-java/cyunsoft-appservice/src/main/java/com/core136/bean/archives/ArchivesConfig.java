package com.core136.bean.archives;

import java.io.Serializable;

public class ArchivesConfig implements Serializable {
	private String borrowingUserRole;
	private String borrowingDeptRole;
	private String borrowingLevelRole;

	private String borrowingFlowId;
	private String borrowingDataMap;
	private String codeRuleId;
	private String createTime;
	private String createUser;
	private String orgId;

	public String getBorrowingUserRole() {
		return borrowingUserRole;
	}

	public void setBorrowingUserRole(String borrowingUserRole) {
		this.borrowingUserRole = borrowingUserRole;
	}

	public String getBorrowingDeptRole() {
		return borrowingDeptRole;
	}

	public void setBorrowingDeptRole(String borrowingDeptRole) {
		this.borrowingDeptRole = borrowingDeptRole;
	}

	public String getBorrowingLevelRole() {
		return borrowingLevelRole;
	}

	public void setBorrowingLevelRole(String borrowingLevelRole) {
		this.borrowingLevelRole = borrowingLevelRole;
	}

	public String getBorrowingFlowId() {
		return borrowingFlowId;
	}

	public void setBorrowingFlowId(String borrowingFlowId) {
		this.borrowingFlowId = borrowingFlowId;
	}

	public String getBorrowingDataMap() {
		return borrowingDataMap;
	}

	public void setBorrowingDataMap(String borrowingDataMap) {
		this.borrowingDataMap = borrowingDataMap;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getCodeRuleId() {
		return codeRuleId;
	}

	public void setCodeRuleId(String codeRuleId) {
		this.codeRuleId = codeRuleId;
	}
}
