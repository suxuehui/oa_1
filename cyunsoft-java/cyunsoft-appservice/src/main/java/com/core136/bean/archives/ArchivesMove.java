package com.core136.bean.archives;

import java.io.Serializable;
public class ArchivesMove implements Serializable {
	private String recordId;
	private String title;

	private String sourceRoomId;
	private String targetRoomId;
	private String archivesId;
	private String moveTime;
	private String moveUser;
	private String moveType;
	private String remark;
	private String createTime;
	private String createUser;
	private String orgId;

	public String getArchivesId() {
		return archivesId;
	}

	public void setArchivesId(String archivesId) {
		this.archivesId = archivesId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMoveTime() {
		return moveTime;
	}

	public void setMoveTime(String moveTime) {
		this.moveTime = moveTime;
	}

	public String getMoveUser() {
		return moveUser;
	}

	public void setMoveUser(String moveUser) {
		this.moveUser = moveUser;
	}

	public String getMoveType() {
		return moveType;
	}

	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getSourceRoomId() {
		return sourceRoomId;
	}

	public void setSourceRoomId(String sourceRoomId) {
		this.sourceRoomId = sourceRoomId;
	}

	public String getTargetRoomId() {
		return targetRoomId;
	}

	public void setTargetRoomId(String targetRoomId) {
		this.targetRoomId = targetRoomId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
