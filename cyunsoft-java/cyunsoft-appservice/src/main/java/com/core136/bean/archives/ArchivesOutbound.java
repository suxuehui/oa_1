package com.core136.bean.archives;

import java.io.Serializable;
public class ArchivesOutbound implements Serializable {
	private String recordId;
	private String title;
	private String archivesId;
	private String outboundTime;
	private String outboundUser;
	private String outboundType;
	private String remark;
	private String createTime;
	private String createUser;
	private String orgId;

	public String getArchivesId() {
		return archivesId;
	}

	public void setArchivesId(String archivesId) {
		this.archivesId = archivesId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOutboundTime() {
		return outboundTime;
	}

	public void setOutboundTime(String outboundTime) {
		this.outboundTime = outboundTime;
	}

	public String getOutboundUser() {
		return outboundUser;
	}

	public void setOutboundUser(String outboundUser) {
		this.outboundUser = outboundUser;
	}

	public String getOutboundType() {
		return outboundType;
	}

	public void setOutboundType(String outboundType) {
		this.outboundType = outboundType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
