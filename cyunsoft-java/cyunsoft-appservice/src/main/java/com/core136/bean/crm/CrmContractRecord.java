package com.core136.bean.crm;

import java.io.Serializable;

/**
 * 客户联系记录
 * @author lsq
 */
public class CrmContractRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String recordId;
    private String customerId;
    private String linkManId;
    private String sendFile;
    private String remark;
    private String attachId;
    private String recordType;
    private String nextVisit;
    private String linkType;
    private String createTime;
    private String createUser;
    private String msgType;
    private String orgId;

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getLinkManId() {
		return linkManId;
	}

	public void setLinkManId(String linkManId) {
		this.linkManId = linkManId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getNextVisit() {
		return nextVisit;
	}

	public void setNextVisit(String nextVisit) {
		this.nextVisit = nextVisit;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getSendFile() {
		return sendFile;
	}

	public void setSendFile(String sendFile) {
		this.sendFile = sendFile;
	}
}
