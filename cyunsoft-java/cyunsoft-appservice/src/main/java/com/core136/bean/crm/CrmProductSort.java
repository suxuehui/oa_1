package com.core136.bean.crm;

import java.io.Serializable;
import java.util.List;


/**
 * @author lsq
 */
public class CrmProductSort implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String sortId;
    private Integer sortNo;
    private String sortName;
    private String parentId;
	private transient List<CrmProductSort> children;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getSortId() {
		return sortId;
	}

	public void setSortId(String sortId) {
		this.sortId = sortId;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public List<CrmProductSort> getChildren() {
		return children;
	}

	public void setChildren(List<CrmProductSort> children) {
		this.children = children;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
