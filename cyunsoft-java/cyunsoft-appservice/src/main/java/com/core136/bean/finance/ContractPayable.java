package com.core136.bean.finance;

import java.io.Serializable;

public class ContractPayable implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String payableId;
    private String contractId;
    private String contractJson;
    private Double payabled;
    private Double unPayabled;
    /**
     * 需付款时间
     */
    private String payableTime;
    /**
     * 真正付款时间
     */
    private String payabledTime;
    private String remark;
    /**
     * 付款凭证
     */
    private String attachId;
    private String accountId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getPayableId() {
        return payableId;
    }

    public void setPayableId(String payableId) {
        this.payableId = payableId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Double getPayabled() {
        return payabled;
    }

    public void setPayabled(Double payabled) {
        this.payabled = payabled;
    }

    public Double getUnPayabled() {
        return unPayabled;
    }

    public void setUnPayabled(Double unPayabled) {
        this.unPayabled = unPayabled;
    }

    public String getPayableTime() {
        return payableTime;
    }

    public void setPayableTime(String payableTime) {
        this.payableTime = payableTime;
    }

    public String getPayabledTime() {
        return payabledTime;
    }

    public void setPayabledTime(String payabledTime) {
        this.payabledTime = payabledTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getContractJson() {
        return contractJson;
    }

    public void setContractJson(String contractJson) {
        this.contractJson = contractJson;
    }
}
