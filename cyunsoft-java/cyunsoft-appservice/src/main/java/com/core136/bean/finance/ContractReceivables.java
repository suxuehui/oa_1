package com.core136.bean.finance;

import java.io.Serializable;

/**
 * 合同应收款
 * @author lsq
 */
public class ContractReceivables implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String receivablesId;
    private String contractId;
    private String contractJson;
    private Double UnReceived;
    private String accountId;
    /**
     * 应收款日期
     */
    private String receivablesTime;
    private String attachId;
    private String remark;

    private Double received;
    /**
     * 真正收款日期
     */
    private String receivedTime;
    private String createTime;
    private String createUser;
    private String orgId;

    public Double getReceived() {
        return received;
    }

    public void setReceived(Double received) {
        this.received = received;
    }

    public Double getUnReceived() {
        return UnReceived;
    }

    public void setUnReceived(Double unReceived) {
        UnReceived = unReceived;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getReceivablesId() {
        return receivablesId;
    }

    public void setReceivablesId(String receivablesId) {
        this.receivablesId = receivablesId;
    }

    public String getReceivablesTime() {
        return receivablesTime;
    }

    public void setReceivablesTime(String receivablesTime) {
        this.receivablesTime = receivablesTime;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getAttachId() {
        return attachId;
    }

    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getContractJson() {
        return contractJson;
    }

    public void setContractJson(String contractJson) {
        this.contractJson = contractJson;
    }
}
