package com.core136.bean.info;

import java.io.Serializable;

/**
 * @author lsq
 */
public class NoticeConfig implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String configId;
    private String noticeType;
    private String approver;
    private String notApprover;
	private String createUser;
	private String createTime;
    private String orgId;

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public String getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public String getNotApprover() {
		return notApprover;
	}

	public void setNotApprover(String notApprover) {
		this.notApprover = notApprover;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
