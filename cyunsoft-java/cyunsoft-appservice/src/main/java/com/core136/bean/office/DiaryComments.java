package com.core136.bean.office;

import java.io.Serializable;

/**
 * 日志评论
 * @author lsq
 */
public class DiaryComments implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String commId;
    private String diaryId;
    private String content;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getCommId() {
        return commId;
    }

    public void setCommId(String commId) {
        this.commId = commId;
    }

    public String getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(String diaryId) {
        this.diaryId = diaryId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
