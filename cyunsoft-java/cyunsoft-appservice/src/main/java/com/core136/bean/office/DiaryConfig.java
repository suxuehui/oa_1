package com.core136.bean.office;

import java.io.Serializable;

public class DiaryConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    private String configId;
    private Integer lockDay;
    private Integer shareStatus;
    private Integer commStatus;
    private String template;
    private String orgId;
    private String createTime;
    private String createUser;

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public Integer getLockDay() {
        return lockDay;
    }

    public void setLockDay(Integer lockDay) {
        this.lockDay = lockDay;
    }

    public Integer getShareStatus() {
        return shareStatus;
    }

    public void setShareStatus(Integer shareStatus) {
        this.shareStatus = shareStatus;
    }

    public Integer getCommStatus() {
        return commStatus;
    }

    public void setCommStatus(Integer commStatus) {
        this.commStatus = commStatus;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

}
