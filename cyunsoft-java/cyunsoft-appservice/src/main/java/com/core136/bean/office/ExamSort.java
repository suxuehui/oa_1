package com.core136.bean.office;

import java.io.Serializable;
import java.util.List;

/**
 * 试题分类
 * @author lsq
 */
public class ExamSort implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String sortId;
    private Integer sortNo;
    private String name;
    private String parentId;
    private String remark;
	private transient List<ExamSort> children;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getSortId() {
		return sortId;
	}

	public void setSortId(String sortId) {
		this.sortId = sortId;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public List<ExamSort> getChildren() {
		return children;
	}

	public void setChildren(List<ExamSort> children) {
		this.children = children;
	}
}
