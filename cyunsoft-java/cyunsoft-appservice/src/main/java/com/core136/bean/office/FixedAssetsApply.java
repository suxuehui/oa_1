package com.core136.bean.office;

import java.io.Serializable;

/**
 * @author lsq
 *
 */
public class FixedAssetsApply implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String applyId;
    private String assetsId;
    private String title;
    private String usedUser;
    private String usedDept;
    private String remark;
    private String status;
    private String approvalUser;
    private String approvalTime;
    private String ideaText;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    public String getAssetsId() {
        return assetsId;
    }

    public void setAssetsId(String assetsId) {
        this.assetsId = assetsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsedUser() {
        return usedUser;
    }

    public void setUsedUser(String usedUser) {
        this.usedUser = usedUser;
    }

    public String getUsedDept() {
        return usedDept;
    }

    public void setUsedDept(String usedDept) {
        this.usedDept = usedDept;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovalUser() {
        return approvalUser;
    }

    public void setApprovalUser(String approvalUser) {
        this.approvalUser = approvalUser;
    }

    public String getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(String approvalTime) {
        this.approvalTime = approvalTime;
    }

    public String getIdeaText() {
        return ideaText;
    }

    public void setIdeaText(String ideaText) {
        this.ideaText = ideaText;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
