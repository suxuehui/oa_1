package com.core136.bean.office;

import java.io.Serializable;

/**
 * @author lsq
 */
public class Meeting implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String meetingId;
    private String meetingType;
    private String msgType;
    private String subject;
    private String roomId;
    private String meetingDay;
    private Integer meetingBegin;
    private Integer meetingEnd;
    private String beginTime;
    private String endTime;
    //参会人员
    private String userJoin;
    private String deptJoin;
    private String levelJoin;
    private String otherJoin;
    private String content;
    private String attachId;
    private String notesUser;
    private String chair;
    private String status;
    private String deviceId;
    private String joinCal;
    private String tel;
    private String attachRole;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getMeetingId() {
		return meetingId;
	}

	public void setMeetingId(String meetingId) {
		this.meetingId = meetingId;
	}

	public String getMeetingType() {
		return meetingType;
	}

	public void setMeetingType(String meetingType) {
		this.meetingType = meetingType;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getMeetingDay() {
		return meetingDay;
	}

	public void setMeetingDay(String meetingDay) {
		this.meetingDay = meetingDay;
	}

	public Integer getMeetingBegin() {
		return meetingBegin;
	}

	public void setMeetingBegin(Integer meetingBegin) {
		this.meetingBegin = meetingBegin;
	}

	public Integer getMeetingEnd() {
		return meetingEnd;
	}

	public void setMeetingEnd(Integer meetingEnd) {
		this.meetingEnd = meetingEnd;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getUserJoin() {
		return userJoin;
	}

	public void setUserJoin(String userJoin) {
		this.userJoin = userJoin;
	}

	public String getDeptJoin() {
		return deptJoin;
	}

	public void setDeptJoin(String deptJoin) {
		this.deptJoin = deptJoin;
	}

	public String getLevelJoin() {
		return levelJoin;
	}

	public void setLevelJoin(String levelJoin) {
		this.levelJoin = levelJoin;
	}

	public String getOtherJoin() {
		return otherJoin;
	}

	public void setOtherJoin(String otherJoin) {
		this.otherJoin = otherJoin;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getNotesUser() {
		return notesUser;
	}

	public void setNotesUser(String notesUser) {
		this.notesUser = notesUser;
	}

	public String getChair() {
		return chair;
	}

	public void setChair(String chair) {
		this.chair = chair;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getJoinCal() {
		return joinCal;
	}

	public void setJoinCal(String joinCal) {
		this.joinCal = joinCal;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAttachRole() {
		return attachRole;
	}

	public void setAttachRole(String attachRole) {
		this.attachRole = attachRole;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
