package com.core136.bean.office;

import java.io.Serializable;

public class TaskProcess implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String processId;
    private String taskDataId;
    private String content;
    private String attachId;
    private Double progress;
    private String completeTime;
    private String createUser;
    private String createTime;
    private String orgId;

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getTaskDataId() {
		return taskDataId;
	}

	public void setTaskDataId(String taskDataId) {
		this.taskDataId = taskDataId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public Double getProgress() {
		return progress;
	}

	public void setProgress(Double progress) {
		this.progress = progress;
	}

	public String getCompleteTime() {
		return completeTime;
	}

	public void setCompleteTime(String completeTime) {
		this.completeTime = completeTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
