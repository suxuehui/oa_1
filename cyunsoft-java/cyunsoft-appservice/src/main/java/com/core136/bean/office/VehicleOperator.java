package com.core136.bean.office;

import java.io.Serializable;

/**
 * 车辆调度员设置
 * @author lsq
 */
public class VehicleOperator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String configId;
    private String optUser;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getOptUser() {
        return optUser;
    }

    public void setOptUser(String optUser) {
        this.optUser = optUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
