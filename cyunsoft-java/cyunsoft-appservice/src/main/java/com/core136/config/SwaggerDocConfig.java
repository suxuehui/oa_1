package com.core136.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerDocConfig {
	@Value("${knife4j.enable}")
	private boolean knife4jEnable;
	@Bean
	public Docket createRestApi() {
		/*添加接口请求头参数配置 没有的话 可以忽略*/
		return new Docket(DocumentationType.OAS_30)
				.enable(knife4jEnable)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.core136.controller"))
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo apiInfo() {
		Contact contact= new Contact("江苏稠云信息技术有限公司","www.cyoasoft.com","68311718@qq.com");
		return new ApiInfoBuilder()
			.title("稠云OA系统接口简介")
			.description("此功能主要用于开发人员的进行开发调试,生产环境中请勿开启使用。切记切记！</br>")
			.contact(contact)
			.version("V3.1")
			.build();
	}



}
