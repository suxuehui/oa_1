package com.core136.controller.archives;

import com.alibaba.fastjson.JSON;
import com.core136.bean.account.UserInfo;
import com.core136.bean.archives.ArchivesConfig;
import com.core136.bean.archives.ArchivesSearch;
import com.core136.bean.system.PageParam;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.archives.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 档案管理Controller
 * @author lsq
 */
@RestController
@RequestMapping("/get/archives")
@CrossOrigin
@Api(value="档案管理GetController",tags={"档案管理获取数据接口"})
public class ApiGetArchivesController {
	private UserInfoService userInfoService;
	private ArchivesDicService archivesDicService;
	private ArchivesConfigService archivesConfigService;
	private ArchivesRoomService archivesRoomService;
	private ArchivesFrameService archivesFrameService;
	private ArchivesSortService archivesSortService;
	private ArchivesLibraryService archivesLibraryService;
	private ArchivesEnterService archivesEnterService;
	private ArchivesAccessService archivesAccessService;
	private ArchivesTempHumService archivesTempHumService;
	private ArchivesDestroyService archivesDestroyService;
	private ArchivesRecordService archivesRecordService;
	private ArchivesPatrolService archivesPatrolService;
	private ArchivesTransferService archivesTransferService;
	private ArchivesInventoryService archivesInventoryService;
	private ArchivesMoveService archivesMoveService;
	private ArchivesOutboundService archivesOutboundService;
	private ArchivesSearchService archivesSearchService;
	private ArchivesBorrowingService archivesBorrowingService;
	private EchartsArchivesService echartsArchivesService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setArchivesDicService(ArchivesDicService archivesDicService) {
		this.archivesDicService = archivesDicService;
	}
	@Autowired
	public void setArchivesConfigService(ArchivesConfigService archivesConfigService) {
		this.archivesConfigService = archivesConfigService;
	}
	@Autowired
	public void setArchivesRoomService(ArchivesRoomService archivesRoomService) {
		this.archivesRoomService = archivesRoomService;
	}
	@Autowired
	public void setArchivesFrameService(ArchivesFrameService archivesFrameService) {
		this.archivesFrameService = archivesFrameService;
	}
	@Autowired
	public void setArchivesSortService(ArchivesSortService archivesSortService) {
		this.archivesSortService = archivesSortService;
	}
	@Autowired
	public void setArchivesLibraryService(ArchivesLibraryService archivesLibraryService) {
		this.archivesLibraryService = archivesLibraryService;
	}
	@Autowired
	public void setArchivesEnterService(ArchivesEnterService archivesEnterService) {
		this.archivesEnterService = archivesEnterService;
	}
	@Autowired
	public void setArchivesAccessService(ArchivesAccessService archivesAccessService) {
		this.archivesAccessService = archivesAccessService;
	}
	@Autowired
	public void setArchivesTempHumService(ArchivesTempHumService archivesTempHumService) {
		this.archivesTempHumService = archivesTempHumService;
	}
	@Autowired
	public void setArchivesDestroyService(ArchivesDestroyService archivesDestroyService) {
		this.archivesDestroyService = archivesDestroyService;
	}
	@Autowired
	public void setArchivesRecordService(ArchivesRecordService archivesRecordService) {
		this.archivesRecordService = archivesRecordService;
	}
	@Autowired
	public void setArchivesPatrolService(ArchivesPatrolService archivesPatrolService) {
		this.archivesPatrolService = archivesPatrolService;
	}
	@Autowired
	public void setArchivesTransferService(ArchivesTransferService archivesTransferService) {
		this.archivesTransferService = archivesTransferService;
	}
	@Autowired
	public void setArchivesInventoryService(ArchivesInventoryService archivesInventoryService) {
		this.archivesInventoryService = archivesInventoryService;
	}
	@Autowired
	public void setArchivesMoveService(ArchivesMoveService archivesMoveService) {
		this.archivesMoveService = archivesMoveService;
	}
	@Autowired
	public void setArchivesOutboundService(ArchivesOutboundService archivesOutboundService) {
		this.archivesOutboundService = archivesOutboundService;
	}
	@Autowired
	public void setArchivesSearchService(ArchivesSearchService archivesSearchService) {
		this.archivesSearchService = archivesSearchService;
	}
	@Autowired
	public void setArchivesBorrowingService(ArchivesBorrowingService archivesBorrowingService) {
		this.archivesBorrowingService = archivesBorrowingService;
	}
	@Autowired
	public void setEchartsArchivesService(EchartsArchivesService echartsArchivesService) {
		this.echartsArchivesService = echartsArchivesService;
	}

	/**
	 * 获取档案借阅列表
	 * @param pageParam 分页信息
	 * @param toOut 是否外借
	 * @param status 借阅状态
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesBorrowingList")
	public RetDataBean getArchivesBorrowingList(PageParam pageParam, String toOut,String status, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.destroy_time");
			} else {
				pageParam.setProp("b."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesBorrowingService.getArchivesBorrowingList(pageParam,date,timeArr[0],timeArr[1],toOut,status));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 档案架档案数量对比
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesFrameForBar")
	public RetDataBean getArchivesFrameForBar() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsArchivesService.getArchivesFrameForBar(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 档案分类排行
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesSortForPie")
	public RetDataBean getArchivesSortForPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsArchivesService.getArchivesSortForPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按涉密等级饼状图
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesSmForPie")
	public RetDataBean getArchivesSmForPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsArchivesService.getArchivesSmForPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取一段时间内按月份统计档案数据上架走势
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesByMonthLine")
	public RetDataBean getArchivesByMonthLine() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsArchivesService.getArchivesByMonthLine(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 全文检索
	 * @param keywords 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/searchIndex")
	public RetDataBean searchIndex(String keywords) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ArchivesSearch archivesSearch = new ArchivesSearch();
			archivesSearch.setKeyword(keywords);
			archivesSearch.setOrgId(userInfo.getOrgId());
			archivesSearchService.addArchivesSearch(userInfo, archivesSearch);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRecordService.searchIndex(userInfo.getOrgId(), keywords));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取热门关键字
	 * @return 消息结构
	 */
	@GetMapping(value = "/getHostKeywords")
	public RetDataBean getHostKeywords() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesSearchService.getHostKeywords(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案出库记录
	 * @param pageParam 分页信息
	 * @param outboundType 出库类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesOutboundList")
	public RetDataBean getArchivesOutboundList(PageParam pageParam,String outboundType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.outbound_time");
			} else {
				pageParam.setProp("t."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesOutboundService.getArchivesOutboundList(pageParam,date,timeArr[0],timeArr[1],outboundType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取档案移库记录列表
	 * @param pageParam 分页信息
	 * @param moveType 移库类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesMoveList")
	public RetDataBean getArchivesMoveList(PageParam pageParam,String moveType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("m.move_time");
			} else {
				pageParam.setProp("m."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesMoveService.getArchivesMoveList(pageParam,date,timeArr[0],timeArr[1],moveType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案盘点记录
	 * @param pageParam 分页信息
	 * @param inventoryType 盘点类型
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesInventoryList")
	public RetDataBean getArchivesInventoryList(PageParam pageParam,String inventoryType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("i.inventory_time");
			} else {
				pageParam.setProp("i."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesInventoryService.getArchivesInventoryList(pageParam,date,timeArr[0],timeArr[1],inventoryType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取待归档档案列表
	 * @param pageParam 分页信息
	 * @param isKind 是否为实物
	 * @param classifiedLevel 涉密级别
	 * @param status 状态
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesRecordListForFile")
	public RetDataBean getArchivesRecordListForFile(PageParam pageParam, String isKind,String classifiedLevel,String status, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.identify_time");
			} else {
				pageParam.setProp("r."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRecordService.getArchivesRecordListForFile(pageParam,date,timeArr[0],timeArr[1],status,isKind,classifiedLevel));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按分类获取档案列表
	 * @param pageParam 分页信息
	 * @param classifiedLevel 涉密级别
	 * @param sortId 分类Id
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesRecordListBySort")
	public RetDataBean getArchivesRecordListBySort(PageParam pageParam,String classifiedLevel,String sortId, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.identify_time");
			} else {
				pageParam.setProp("r."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRecordService.getArchivesRecordListBySort(pageParam,date,timeArr[0],timeArr[1],classifiedLevel,sortId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取待上架档案列表
	 * @param pageParam 分页信息
	 * @param classifiedLevel 涉密级别
	 * @param frameStatus 档案架状态
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesRecordListForFrame")
	public RetDataBean getArchivesRecordListForFrame(PageParam pageParam,String classifiedLevel,String frameStatus, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.identify_time");
			} else {
				pageParam.setProp("r."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRecordService.getArchivesRecordListForFrame(pageParam,date,timeArr[0],timeArr[1],frameStatus,classifiedLevel));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案转移列表
	 * @param pageParam 分页信息
	 * @param transferType 转移类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesTransferList")
	public RetDataBean getArchivesTransferList(PageParam pageParam, String transferType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.transfer_type");
			} else {
				pageParam.setProp("t."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesTransferService.getArchivesTransferList(pageParam,date,timeArr[0],timeArr[1],transferType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取档案巡检记录列表
	 * @param pageParam 分页信息
	 * @param patrolType 巡检类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesPatrolList")
	public RetDataBean getArchivesPatrolList(PageParam pageParam, String patrolType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("p.patrol_time");
			} else {
				pageParam.setProp("p."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesPatrolService.getArchivesPatrolList(pageParam,patrolType,date,timeArr[0],timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案下接列表
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesRecordListForSelect")
	public RetDataBean getArchivesRecordListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRecordService.getArchivesRecordListForSelect(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取档案销毁记录
	 * @param pageParam 分页信息
	 * @param destroyType 销毁类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesDestroyList")
	public RetDataBean getArchivesDestroyList(PageParam pageParam, String destroyType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.destroy_time");
			} else {
				pageParam.setProp("d."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesDestroyService.getArchivesDestroyList(pageParam,date,timeArr[0],timeArr[1],destroyType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取档案仓库进出记录
	 * @param pageParam 分页信息
	 * @param isOut 人员类型
	 * @param accessType 进出类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesAccessList")
	public RetDataBean getArchivesAccessList(PageParam pageParam,String isOut, String accessType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("a.create_time");
			} else {
				pageParam.setProp("a."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesAccessService.getArchivesAccessList(pageParam,date,timeArr[0],timeArr[1],isOut,accessType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案库房温湿度检查列表
	 * @param pageParam 分页信息
	 * @param checkType 检查类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesTempHumList")
	public RetDataBean getArchivesTempHumList(PageParam pageParam,String checkType, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.create_time");
			} else {
				pageParam.setProp("t."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesTempHumService.getArchivesTempHumList(pageParam,date,timeArr[0],timeArr[1],checkType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案登记记录列表
	 * @param pageParam 分页信息
	 * @param status 状态
	 * @param archivesSource 档案登记记录来源
	 * @param archivesEnterType 档案登记记录类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesEnterList")
	public RetDataBean getArchivesEnterList(PageParam pageParam,String status, String archivesSource, String archivesEnterType,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("e.create_time");
			} else {
				pageParam.setProp("e."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesEnterService.getArchivesEnterList(pageParam,status,archivesSource,archivesEnterType,date,timeArr[0],timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案库列表
	 * @param pageParam 分页信息
	 * @param status 状态
	 * @param storagePeriod 保存年限
	 * @param classifiedLevel 涉密等级
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesLibraryList")
	public RetDataBean getArchivesLibraryList(PageParam pageParam,String status, Integer storagePeriod, String classifiedLevel) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.create_time");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesLibraryService.getArchivesLibraryList(pageParam,status,storagePeriod,classifiedLevel));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有档案分类树型结构
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesSortTree")
	public RetDataBean getArchivesSortTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesSortService.getArchivesSortTree(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取档案分类树形结构
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllArchivesSortForSelectTree")
	public RetDataBean getAllArchivesSortForSelectTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesSortService.getAllArchivesSortForSelectTree(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取档案架号列表
	 * @param pageParam 分页信息
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesFrameListForManage")
	public RetDataBean getArchivesFrameListForManage(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("f.sort_no");
			} else {
				pageParam.setProp("f."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesFrameService.getArchivesFrameListForManage(pageParam));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取档案架号列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesFrameListForSelect")
	public RetDataBean getArchivesFrameListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesFrameService.getArchivesFrameListForSelect(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取所有档案库房列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllArchivesRoomList")
	public RetDataBean getAllArchivesRoomList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRoomService.getAllArchivesRoomList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取档案库房列表
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesRoomListForSelect")
	public RetDataBean getArchivesRoomListForSelect(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRoomService.getArchivesRoomListForSelect(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取字典分类列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesDicParentList")
	public RetDataBean getArchivesDicParentList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesDicService.getArchivesDicParentList(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典树
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesDicTree")
	public RetDataBean getArchivesDicTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesDicService.getArchivesDicTree(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按模块获取分类列表
	 *
	 * @param code 字典分类码
	 * @return 分类列表
	 */
	@GetMapping(value = "/getArchivesDicByCode")
	public RetDataBean getArchivesDicByCode(String code) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesDicService.getArchivesDicByCode(userInfo.getOrgId(), code));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典列表
	 *
	 * @param pageParam 分页信息
	 * @param parentId 字典分类父级Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesDicList")
	public RetDataBean getArchivesDicList(PageParam pageParam, String parentId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.sort_no");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesDicService.getArchivesDicList(pageParam, parentId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取档案参数详情
	 * @return 消息结构
	 */
	@GetMapping(value = "/getArchivesConfig")
	public RetDataBean getArchivesConfig() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ArchivesConfig archivesConfig = new ArchivesConfig();
			archivesConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesConfigService.selectOneArchivesConfig(archivesConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


}
