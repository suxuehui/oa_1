package com.core136.controller.archives;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UserInfo;
import com.core136.bean.archives.*;
import com.core136.bean.bpm.BpmList;
import com.core136.bean.bpm.BpmStepRun;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.archives.*;
import com.core136.service.bpm.BpmFormDataService;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
/**
 * 档案管理Controller
 * @author lsq
 */
@RestController
@RequestMapping("/set/archives")
@CrossOrigin
@Api(value="档案管理SetController",tags={"档案管理更新数据接口"})
public class ApiSetArchivesController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private BpmFormDataService bpmFormDataService;
	private UserInfoService userInfoService;
	private ArchivesDicService archivesDicService;
	private ArchivesConfigService archivesConfigService;
	private ArchivesRoomService archivesRoomService;
	private ArchivesFrameService archivesFrameService;
	private ArchivesSortService archivesSortService;
	private ArchivesLibraryService archivesLibraryService;
	private ArchivesEnterService archivesEnterService;
	private ArchivesAccessService archivesAccessService;
	private ArchivesTempHumService archivesTempHumService;
	private ArchivesDestroyService archivesDestroyService;
	private ArchivesRecordService archivesRecordService;
	private ArchivesPatrolService archivesPatrolService;
	private ArchivesTransferService archivesTransferService;
	private ArchivesInventoryService archivesInventoryService;
	private ArchivesMoveService archivesMoveService;
	private ArchivesOutboundService archivesOutboundService;
	private ArchivesBorrowingService archivesBorrowingService;
	@Autowired
	public void setBpmFormDataService(BpmFormDataService bpmFormDataService) {
		this.bpmFormDataService = bpmFormDataService;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setArchivesDicService(ArchivesDicService archivesDicService) {
		this.archivesDicService = archivesDicService;
	}
	@Autowired
	public void setArchivesConfigService(ArchivesConfigService archivesConfigService) {
		this.archivesConfigService = archivesConfigService;
	}
	@Autowired
	public void setArchivesRoomService(ArchivesRoomService archivesRoomService) {
		this.archivesRoomService = archivesRoomService;
	}
	@Autowired
	public void setArchivesFrameService(ArchivesFrameService archivesFrameService) {
		this.archivesFrameService = archivesFrameService;
	}
	@Autowired
	public void setArchivesSortService(ArchivesSortService archivesSortService) {
		this.archivesSortService = archivesSortService;
	}
	@Autowired
	public void setArchivesLibraryService(ArchivesLibraryService archivesLibraryService) {
		this.archivesLibraryService = archivesLibraryService;
	}
	@Autowired
	public void setArchivesEnterService(ArchivesEnterService archivesEnterService) {
		this.archivesEnterService = archivesEnterService;
	}
	@Autowired
	public void setArchivesAccessService(ArchivesAccessService archivesAccessService) {
		this.archivesAccessService = archivesAccessService;
	}
	@Autowired
	public void setArchivesTempHumService(ArchivesTempHumService archivesTempHumService) {
		this.archivesTempHumService = archivesTempHumService;
	}
	@Autowired
	public void setArchivesDestroyService(ArchivesDestroyService archivesDestroyService) {
		this.archivesDestroyService = archivesDestroyService;
	}
	@Autowired
	public void setArchivesRecordService(ArchivesRecordService archivesRecordService) {
		this.archivesRecordService = archivesRecordService;
	}
	@Autowired
	public void setArchivesPatrolService(ArchivesPatrolService archivesPatrolService) {
		this.archivesPatrolService = archivesPatrolService;
	}
	@Autowired
	public void setArchivesTransferService(ArchivesTransferService archivesTransferService) {
		this.archivesTransferService = archivesTransferService;
	}
	@Autowired
	public void setArchivesInventoryService(ArchivesInventoryService archivesInventoryService) {
		this.archivesInventoryService = archivesInventoryService;
	}
	@Autowired
	public void setArchivesMoveService(ArchivesMoveService archivesMoveService) {
		this.archivesMoveService = archivesMoveService;
	}
	@Autowired
	public void setArchivesOutboundService(ArchivesOutboundService archivesOutboundService) {
		this.archivesOutboundService = archivesOutboundService;
	}
	@Autowired
	public void setArchivesBorrowingService(ArchivesBorrowingService archivesBorrowingService) {
		this.archivesBorrowingService = archivesBorrowingService;
	}

	/**
	 * 发起档案借阅申请
	 * @param archivesBorrowing 档案借阅对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/createArchivesBorrowingRule")
	public RetDataBean createArchivesBorrowingRule(ArchivesBorrowing archivesBorrowing) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesBorrowing.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			archivesBorrowing.setCreateUser(userInfo.getAccountId());
			archivesBorrowing.setOrgId(userInfo.getOrgId());
			ArchivesRecord archivesRecord = new ArchivesRecord();
			archivesRecord.setRecordId(archivesBorrowing.getArchivesId());
			archivesRecord.setOrgId(userInfo.getOrgId());
			archivesRecord = archivesRecordService.selectOneArchivesRecord(archivesRecord);
			ArchivesConfig archivesConfig = new ArchivesConfig();
			archivesConfig.setOrgId(userInfo.getOrgId());
			archivesConfig = archivesConfigService.selectOneArchivesConfig(archivesConfig);
			BpmList bpmList = new BpmList();
			bpmList.setFlowTitle("《"+archivesRecord.getTitle()+"》的档案借阅审批");
			//bpmList.setRunAttach(archivesRecord.getAttachId());
			bpmList.setUrgency("0");
			bpmList.setFlowId(archivesConfig.getBorrowingFlowId());
			String[] dataMapList = archivesConfig.getBorrowingDataMap().split(",");
			JSONObject formDataJson = new JSONObject();
			JSONObject vJson = (JSONObject) JSON.toJSON(archivesBorrowing);
			//JSONObject vJson = (JSONObject) JSONObject.toJSON(htRecord);
			for(String vkStr:dataMapList)
			{
				String[] vkArr = vkStr.split(":");
				if(vkArr[0].equals("beginToEnd"))
				{
					String [] vArr = vJson.getString(vkArr[0]).split(",");
					formDataJson.put(vkArr[1], "[\""+vArr[0]+"\",\""+vArr[1]+"\"]");
				}else {
					formDataJson.put(vkArr[1], vJson.get(vkArr[0]));
				}
			}
			RetDataBean retdataBean = bpmFormDataService.createBpmFormData(userInfo, bpmList, new BpmStepRun(), formDataJson);
			JSONObject resJson = JSON.parseObject(retdataBean.getData().toString());
			if(StringUtils.isNotBlank(archivesBorrowing.getBeginToEnd())) {
				String[] timeArr = archivesBorrowing.getBeginToEnd().split(",");
				archivesBorrowing.setBeginTime(timeArr[0]);
				archivesBorrowing.setEndTime(timeArr[1]);
			}
			archivesBorrowing.setRunId(resJson.getString("runId"));
			archivesBorrowing.setStatus("0");
			archivesBorrowing.setCreateUser(userInfo.getAccountId());
			archivesBorrowing.setRecordId(SysTools.getGUID());
			archivesBorrowingService.insertArchivesBorrowing(archivesBorrowing);
			return retdataBean;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加档案出库记录
	 * @param archivesOutbound 出库记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesOutbound")
	public RetDataBean insertArchivesMove(ArchivesOutbound archivesOutbound) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesOutbound.setRecordId(SysTools.getGUID());
			archivesOutbound.setOrgId(userInfo.getOrgId());
			archivesOutbound.setCreateUser(userInfo.getCreateUser());
			archivesOutbound.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesOutboundService.insertArchivesOutbound(archivesOutbound));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案出库记录
	 * @param archivesOutbound 出库记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesOutbound")
	public RetDataBean updateArchivesMove(ArchivesOutbound archivesOutbound) {
		try {
			if (StringUtils.isBlank(archivesOutbound.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesOutbound.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesOutbound.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesOutboundService.updateArchivesOutbound(example,archivesOutbound));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案出库记录
	 * @param archivesOutbound 出库记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesOutbound")
	public RetDataBean deleteArchivesOutbound(ArchivesOutbound archivesOutbound) {
		try {
			if (StringUtils.isBlank(archivesOutbound.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesOutbound.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesOutboundService.deleteArchivesOutbound(archivesOutbound));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案出库记录
	 * @param ids 出库记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesOutboundByIds")
	public RetDataBean deleteArchivesOutboundByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesOutboundService.deleteArchivesOutboundByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加档案移库记录
	 * @param archivesMove 移库记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesMove")
	public RetDataBean insertArchivesMove(ArchivesMove archivesMove) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesMove.setRecordId(SysTools.getGUID());
			archivesMove.setOrgId(userInfo.getOrgId());
			archivesMove.setCreateUser(userInfo.getCreateUser());
			archivesMove.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesMoveService.insertArchivesMove(archivesMove));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案移库记录
	 * @param archivesMove 移库记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesMove")
	public RetDataBean updateArchivesMove(ArchivesMove archivesMove) {
		try {
			if (StringUtils.isBlank(archivesMove.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesMove.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesMove.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesMoveService.updateArchivesMove(example,archivesMove));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案移库记录
	 * @param archivesmove 移库记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesMove")
	public RetDataBean deleteArchivesMove(ArchivesMove archivesmove) {
		try {
			if (StringUtils.isBlank(archivesmove.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesmove.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesMoveService.deleteArchivesMove(archivesmove));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案移库记录
	 * @param ids 移库记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesMoveByIds")
	public RetDataBean deleteArchivesMoveByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesMoveService.deleteArchivesMoveByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加档案盘点记录
	 * @param archivesInventory 档案盘点对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesInventory")
	public RetDataBean insertArchivesInventory(ArchivesInventory archivesInventory) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesInventory.setRecordId(SysTools.getGUID());
			archivesInventory.setOrgId(userInfo.getOrgId());
			archivesInventory.setCreateUser(userInfo.getCreateUser());
			archivesInventory.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesInventoryService.insertArchivesInventory(archivesInventory));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案盘点记录
	 * @param archivesInventory 档案盘点对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesInventory")
	public RetDataBean updateArchivesInventory(ArchivesInventory archivesInventory) {
		try {
			if (StringUtils.isBlank(archivesInventory.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesInventory.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesInventory.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesInventoryService.updateArchivesInventory(example,archivesInventory));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案盘点记录
	 * @param archivesInventory 档案盘点对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesInventory")
	public RetDataBean deleteArchivesInventory(ArchivesInventory archivesInventory) {
		try {
			if (StringUtils.isBlank(archivesInventory.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesInventory.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesInventoryService.deleteArchivesInventory(archivesInventory));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案盘点记录
	 * @param ids 盘点记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesInventoryByIds")
	public RetDataBean deleteArchivesInventoryByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesInventoryService.deleteArchivesInventoryByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加档案转移记录
	 * @param archivesTransfer 转移记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesTransfer")
	public RetDataBean insertArchivesTransfer(ArchivesTransfer archivesTransfer) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesTransfer.setRecordId(SysTools.getGUID());
			archivesTransfer.setOrgId(userInfo.getOrgId());
			archivesTransfer.setCreateUser(userInfo.getCreateUser());
			archivesTransfer.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesTransferService.insertArchivesTransfer(archivesTransfer));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案转移记录
	 * @param archivesTransfer 转移记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesTransfer")
	public RetDataBean updateArchivesTransfer(ArchivesTransfer archivesTransfer) {
		try {
			if (StringUtils.isBlank(archivesTransfer.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesTransfer.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesTransfer.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesTransferService.updateArchivesTransfer(example,archivesTransfer));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案转移记录
	 * @param archivesTransfer 转移记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesTransfer")
	public RetDataBean deleteArchivesTransfer(ArchivesTransfer archivesTransfer) {
		try {
			if (StringUtils.isBlank(archivesTransfer.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesTransfer.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesTransferService.deleteArchivesTransfer(archivesTransfer));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案转移记录
	 * @param ids 转移记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesTransferByIds")
	public RetDataBean deleteArchivesTransferByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesTransferService.deleteArchivesTransferByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 档案鉴定
	 * @param archivesRecord 档案鉴定对象
	 * @param enterId 档案登记记录Id
	 * @return 消息结构
	 */
	@PostMapping(value = "/setArchivesIdentify")
	public RetDataBean setArchivesIdentify(ArchivesRecord archivesRecord,String enterId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesRecord.setRecordId(SysTools.getGUID());
			archivesRecord.setOrgId(userInfo.getOrgId());
			archivesRecord.setCreateUser(userInfo.getCreateUser());
			archivesRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesRecordService.setArchivesIdentify(archivesRecord,enterId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加巡检记录
	 * @param archivesPatrol 巡检记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesPatrol")
	public RetDataBean insertArchivesPatrol(ArchivesPatrol archivesPatrol) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesPatrol.setRecordId(SysTools.getGUID());
			archivesPatrol.setOrgId(userInfo.getOrgId());
			archivesPatrol.setCreateUser(userInfo.getCreateUser());
			archivesPatrol.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesPatrolService.insertArchivesPatrol(archivesPatrol));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新巡检记录
	 * @param archivesPatrol 巡检记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesPatrol")
	public RetDataBean updateArchivesPatrol(ArchivesPatrol archivesPatrol) {
		try {
			if (StringUtils.isBlank(archivesPatrol.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesPatrol.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesPatrol.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesPatrolService.updateArchivesPatrol(example,archivesPatrol));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除巡检记录
	 * @param archivesPatrol 巡检记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesPatrol")
	public RetDataBean deleteArchivesPatrol(ArchivesPatrol archivesPatrol) {
		try {
			if (StringUtils.isBlank(archivesPatrol.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesPatrol.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesPatrolService.deleteArchivesPatrol(archivesPatrol));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除巡检记录
	 * @param ids 巡检记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesPatrolByIds")
	public RetDataBean deleteArchivesPatrolByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesPatrolService.deleteArchivesPatrolByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加档案记录
	 * @param archivesRecord 档案记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesRecord")
	public RetDataBean insertArchivesRecord(ArchivesRecord archivesRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesRecord.setRecordId(SysTools.getGUID());
			archivesRecord.setOrgId(userInfo.getOrgId());
			archivesRecord.setCreateUser(userInfo.getCreateUser());
			archivesRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesRecordService.insertArchivesRecord(archivesRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案记录
	 * @param archivesRecord 档案记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesRecord")
	public RetDataBean updateArchivesRecord(ArchivesRecord archivesRecord) {
		try {
			if (StringUtils.isBlank(archivesRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesRecordService.updateArchivesRecord(example,archivesRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案记录
	 * @param archivesRecord 档案记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesRecord")
	public RetDataBean deleteArchivesRecord(ArchivesRecord archivesRecord) {
		try {
			if (StringUtils.isBlank(archivesRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesRecordService.deleteArchivesRecord(archivesRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案记录
	 * @param ids 档案记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesRecordByIds")
	public RetDataBean deleteArchivesRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesRecordService.deleteArchivesRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加档案销毁记录
	 * @param archivesDestroy 销毁记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesDestroy")
	public RetDataBean insertArchivesDestroy(ArchivesDestroy archivesDestroy) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesDestroy.setRecordId(SysTools.getGUID());
			archivesDestroy.setOrgId(userInfo.getOrgId());
			archivesDestroy.setCreateUser(userInfo.getCreateUser());
			archivesDestroy.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesDestroyService.insertArchivesDestroy(archivesDestroy));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案销毁记录
	 * @param archivesDestroy 销毁记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesDestroy")
	public RetDataBean updateArchivesDestroy(ArchivesDestroy archivesDestroy) {
		try {
			if (StringUtils.isBlank(archivesDestroy.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesDestroy.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesDestroy.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesDestroyService.updateArchivesDestroy(example,archivesDestroy));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案销毁记录
	 * @param archivesDestroy 销毁记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesDestroy")
	public RetDataBean deleteArchivesDestroy(ArchivesDestroy archivesDestroy) {
		try {
			if (StringUtils.isBlank(archivesDestroy.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesDestroy.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesDestroyService.deleteArchivesDestroy(archivesDestroy));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案销毁记录
	 * @param ids 销毁记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesDestroyIds")
	public RetDataBean deleteArchivesDestroyIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesDestroyService.deleteArchivesDestroyIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加温湿度记录
	 * @param archivesTempHum 温湿度记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesTempHum")
	public RetDataBean insertArchivesTempHum(ArchivesTempHum archivesTempHum) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesTempHum.setRecordId(SysTools.getGUID());
			archivesTempHum.setOrgId(userInfo.getOrgId());
			archivesTempHum.setCreateUser(userInfo.getCreateUser());
			archivesTempHum.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesTempHumService.insertArchivesTempHum(archivesTempHum));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新温湿度记录
	 * @param archivesTempHum 温湿度记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesTempHum")
	public RetDataBean updateArchivesTempHum(ArchivesTempHum archivesTempHum) {
		try {
			if (StringUtils.isBlank(archivesTempHum.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesTempHum.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesTempHum.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesTempHumService.updateArchivesTempHum(example,archivesTempHum));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除温湿度记录
	 * @param archivesTempHum 温湿度记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesTempHum")
	public RetDataBean deleteArchivesTempHum(ArchivesTempHum archivesTempHum) {
		try {
			if (StringUtils.isBlank(archivesTempHum.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesTempHum.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesTempHumService.deleteArchivesTempHum(archivesTempHum));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除温湿度记录
	 * @param ids 温湿度记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesTempHumIds")
	public RetDataBean deleteArchivesTempHumIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesTempHumService.deleteArchivesTempHumIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加进出管理记录
	 * @param archivesAccess 进出管理记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesAccess")
	public RetDataBean insertArchivesAccess(ArchivesAccess archivesAccess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesAccess.setRecordId(SysTools.getGUID());
			archivesAccess.setOrgId(userInfo.getOrgId());
			archivesAccess.setCreateUser(userInfo.getCreateUser());
			archivesAccess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesAccessService.insertArchivesAccess(archivesAccess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新进出管理记录
	 * @param archivesAccess 进出管理记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesAccess")
	public RetDataBean updateArchivesAccess(ArchivesAccess archivesAccess) {
		try {
			if (StringUtils.isBlank(archivesAccess.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesAccess.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesAccess.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesAccessService.updateArchivesAccess(example,archivesAccess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除进出管理记录
	 * @param archivesAccess 进出管理记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesAccess")
	public RetDataBean deleteArchivesAccess(ArchivesAccess archivesAccess) {
		try {
			if (StringUtils.isBlank(archivesAccess.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesAccess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesAccessService.deleteArchivesAccess(archivesAccess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案登记记录
	 * @param ids 进出管理记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesAccessIds")
	public RetDataBean deleteArchivesAccessIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesAccessService.deleteArchivesAccessIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 档案登记
	 * @param archivesEnter 档案登记对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesEnter")
	public RetDataBean insertArchivesEnter(ArchivesEnter archivesEnter) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesEnter.setRecordId(SysTools.getGUID());
			archivesEnter.setOrgId(userInfo.getOrgId());
			archivesEnter.setCreateUser(userInfo.getCreateUser());
			archivesEnter.setStatus("0");
			archivesEnter.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesEnterService.insertArchivesEnter(archivesEnter));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案登记记录
	 * @param archivesEnter 档案登记对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesEnter")
	public RetDataBean updateArchivesEnter(ArchivesEnter archivesEnter) {
		try {
			if (StringUtils.isBlank(archivesEnter.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesEnter.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesEnter.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesEnterService.updateArchivesEnter(example,archivesEnter));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案登记记录
	 * @param archivesEnter 档案登记对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesEnter")
	public RetDataBean deleteArchivesEnter(ArchivesEnter archivesEnter) {
		try {
			if (StringUtils.isBlank(archivesEnter.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesEnter.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesEnterService.deleteArchivesEnter(archivesEnter));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案登记记录
	 * @param ids 档案登记Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesEnterIds")
	public RetDataBean deleteArchivesEnterIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesEnterService.deleteArchivesEnterIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建档案卷库
	 * @param archivesLibrary 档案卷库对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesLibrary")
	public RetDataBean insertArchivesLibrary(ArchivesLibrary archivesLibrary) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesLibrary.setRecordId(SysTools.getGUID());
			archivesLibrary.setOrgId(userInfo.getOrgId());
			archivesLibrary.setCreateUser(userInfo.getCreateUser());
			archivesLibrary.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesLibraryService.insertArchivesLibrary(archivesLibrary));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案卷库
	 * @param archivesLibrary 档案卷库对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesLibrary")
	public RetDataBean updateArchivesLibrary(ArchivesLibrary archivesLibrary) {
		try {
			if (StringUtils.isBlank(archivesLibrary.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesLibrary.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", archivesLibrary.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesLibraryService.updateArchivesLibrary(example,archivesLibrary));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案卷库
	 * @param archivesLibrary 档案卷库对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesLibrary")
	public RetDataBean deleteArchivesLibrary(ArchivesLibrary archivesLibrary) {
		try {
			if (StringUtils.isBlank(archivesLibrary.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesLibrary.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesLibraryService.deleteArchivesLibrary(archivesLibrary));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案卷库
	 * @param ids 档案卷库Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesLibraryIds")
	public RetDataBean deleteArchivesLibraryIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesLibraryService.deleteArchivesLibraryIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建档案分类
	 * @param archivesSort 档案分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesSort")
	public RetDataBean insertArchivesSort(ArchivesSort archivesSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesSort.setSortId(SysTools.getGUID());
			archivesSort.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(archivesSort.getParentId())) {
				archivesSort.setParentId("0");
			}
			archivesSort.setCreateUser(userInfo.getCreateUser());
			archivesSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesSortService.insertArchivesSort(archivesSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案分类
	 * @param archivesSort 档案分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesSort")
	public RetDataBean updateArchivesFrame(ArchivesSort archivesSort) {
		try {
			if (StringUtils.isBlank(archivesSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", archivesSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesSortService.updateArchivesSort(example,archivesSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案分类
	 * @param archivesSort 档案分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesSort")
	public RetDataBean deleteArchivesSort(ArchivesSort archivesSort) {
		try {
			if (StringUtils.isBlank(archivesSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesSort.setOrgId(userInfo.getOrgId());
			if(archivesRecordService.getCountBySortId(userInfo.getOrgId(),archivesSort.getSortId())<1) {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesSortService.deleteArchivesSort(archivesSort));
			}else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案分类
	 * @param ids 档案分类Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesSortIds")
	public RetDataBean deleteArchivesSortIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesSortService.deleteArchivesSortIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建档案架
	 * @param archivesFrame 档案架对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesFrame")
	public RetDataBean insertArchivesFrame(ArchivesFrame archivesFrame) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesFrame.setFrameId(SysTools.getGUID());
			archivesFrame.setOrgId(userInfo.getOrgId());
			archivesFrame.setStatus("1");
			archivesFrame.setCreateUser(userInfo.getCreateUser());
			archivesFrame.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesFrameService.insertArchivesFrame(archivesFrame));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案架信息
	 * @param archivesFrame 档案架对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesFrame")
	public RetDataBean updateArchivesFrame(ArchivesFrame archivesFrame) {
		try {
			if (StringUtils.isBlank(archivesFrame.getFrameId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesFrame.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("frameId", archivesFrame.getFrameId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesFrameService.updateArchivesFrame(example,archivesFrame));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案架信息
	 * @param archivesFrame 档案架对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesFrame")
	public RetDataBean deleteArchivesFrame(ArchivesFrame archivesFrame) {
		try {
			if (StringUtils.isBlank(archivesFrame.getFrameId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesFrame.setOrgId(userInfo.getOrgId());
			if(archivesRecordService.getCountByFrameId(userInfo.getOrgId(),archivesFrame.getFrameId())<1) {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesFrameService.deleteArchivesFrame(archivesFrame));
			}else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除档案号
	 * @param ids 档案架Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesFrameByIds")
	public RetDataBean deleteArchivesFrameByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return archivesFrameService.deleteArchivesFrameByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建档案库房
	 * @param archivesRoom 档案库房对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesRoom")
	public RetDataBean insertArchivesRoom(ArchivesRoom archivesRoom) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesRoom.setRoomId(SysTools.getGUID());
			archivesRoom.setOrgId(userInfo.getOrgId());
			archivesRoom.setStatus("1");
			archivesRoom.setCreateUser(userInfo.getCreateUser());
			archivesRoom.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesRoomService.insertArchivesRoom(archivesRoom));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新档案库房
	 * @param archivesRoom 档案库房对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesRoom")
	public RetDataBean updateArchivesRoom(ArchivesRoom archivesRoom) {
		try {
			if (StringUtils.isBlank(archivesRoom.getRoomId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesRoom.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("roomId", archivesRoom.getRoomId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesRoomService.updateArchivesRoom(example,archivesRoom));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除档案库房
	 * @param archivesRoom 档案库房对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesRoom")
	public RetDataBean deleteArchivesRoom(ArchivesRoom archivesRoom) {
		try {
			if (StringUtils.isBlank(archivesRoom.getRoomId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (archivesRecordService.getCountByRoomId(userInfo.getOrgId(),archivesRoom.getRoomId())>0) {//要判断库房下是否有档案
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
			archivesRoom.setOrgId(userInfo.getOrgId());
			if(archivesFrameService.getCountByRoomId(userInfo.getOrgId(),archivesRoom.getRoomId())<1) {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesRoomService.deleteArchivesRoom(archivesRoom));
			}else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建字典
	 *
	 * @param archivesDic 档案字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertArchivesDic")
	public RetDataBean insertArchivesDic(ArchivesDic archivesDic) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(archivesDic.getParentId())) {
				archivesDic.setParentId("0");
			}
			archivesDic.setDicId(SysTools.getGUID());
			archivesDic.setOrgId(userInfo.getOrgId());
			archivesDic.setStatus("1");
			archivesDic.setCreateUser(userInfo.getCreateUser());
			archivesDic.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesDicService.insertArchivesDic(archivesDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新字典
	 *
	 * @param archivesDic 档案字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateArchivesDic")
	public RetDataBean updateArchivesDic(ArchivesDic archivesDic) {
		try {
			if (StringUtils.isBlank(archivesDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ArchivesDic.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("dicId", archivesDic.getDicId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesDicService.updateArchivesDic(archivesDic, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除字典
	 *
	 * @param archivesDic 档案字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesDic")
	public RetDataBean deleteArchivesDic(ArchivesDic archivesDic) {
		try {
			if (StringUtils.isBlank(archivesDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (archivesDicService.isExistChild(userInfo.getOrgId(), archivesDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
			archivesDic.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesDicService.deleteArchivesDic(archivesDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除字典
	 *
	 * @param ids 档案字典Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteArchivesDicByIds")
	public RetDataBean deleteArchivesDicByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return archivesDicService.deleteArchivesDicByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 字典排序
	 *
	 * @param oldDicId 原字典Id
	 * @param newDicId 新字典Id
	 * @param oldSortNo 原字典排序号
	 * @param newSortNo 新字典排序号
	 * @return 消息结构
	 */
	@PostMapping(value = "/dropArchivesDic")
	public RetDataBean dropArchivesDic(String oldDicId, String newDicId, Integer oldSortNo, Integer newSortNo) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesDicService.dropArchivesDic(userInfo.getOrgId(), oldDicId, newDicId, oldSortNo, newSortNo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置档案参数
	 * @param archivesConfig 档案参数对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setArchivesConfig")
	public RetDataBean setArchivesConfig(ArchivesConfig archivesConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			archivesConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesConfigService.setArchivesConfig(userInfo,archivesConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
}
