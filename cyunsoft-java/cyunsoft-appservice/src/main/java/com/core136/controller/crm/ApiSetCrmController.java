package com.core136.controller.crm;

import com.alibaba.fastjson.JSON;
import com.core136.bean.account.UserInfo;
import com.core136.bean.crm.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.crm.*;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/crm")
@CrossOrigin
@Api(value="客户管理SetController",tags={"Crm更新数据接口"})
public class ApiSetCrmController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private UserInfoService userInfoService;
	private CrmRoleService crmRoleService;
	private CrmProductSortService crmProductSortService;
	private CrmCustomerService crmCustomerService;
	private CrmProductService crmProductService;
	private CrmDicService crmDicService;
	private CrmLinkManService crmLinkManService;
	private CrmContractRecordService crmContractRecordService;
	private CrmInquiryService crmInquiryService;
	private CrmQuotationService crmQuotationService;
	private CrmCareService crmCareService;
	private CrmOrderService crmOrderService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setCrmRoleService(CrmRoleService crmRoleService) {
		this.crmRoleService = crmRoleService;
	}
	@Autowired
	public void setCrmProductSortService(CrmProductSortService crmProductSortService) {
		this.crmProductSortService = crmProductSortService;
	}
	@Autowired
	public void setCrmCustomerService(CrmCustomerService crmCustomerService) {
		this.crmCustomerService = crmCustomerService;
	}
	@Autowired
	public void setCrmProductService(CrmProductService crmProductService) {
		this.crmProductService = crmProductService;
	}
	@Autowired
	public void setCrmDicService(CrmDicService crmDicService) {
		this.crmDicService = crmDicService;
	}
	@Autowired
	public void setCrmLinkManService(CrmLinkManService crmLinkManService) {
		this.crmLinkManService = crmLinkManService;
	}
	@Autowired
	public void setCrmContractRecordService(CrmContractRecordService crmContractRecordService) {
		this.crmContractRecordService = crmContractRecordService;
	}
	@Autowired
	public void setCrmInquiryService(CrmInquiryService crmInquiryService) {
		this.crmInquiryService = crmInquiryService;
	}
	@Autowired
	public void setCrmQuotationService(CrmQuotationService crmQuotationService) {
		this.crmQuotationService = crmQuotationService;
	}
	@Autowired
	public void setCrmCareService(CrmCareService crmCareService) {
		this.crmCareService = crmCareService;
	}
	@Autowired
	public void setCrmOrderService(CrmOrderService crmOrderService) {
		this.crmOrderService = crmOrderService;
	}

	/**
	 * 创建订单记录
	 * @param crmOrder 订单记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmOrder")
	public RetDataBean insertCrmOrder(CrmOrder crmOrder,String mx) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmOrder.setOrderId(SysTools.getGUID());
			crmOrder.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmOrder.setCreateUser(userInfo.getAccountId());
			crmOrder.setStatus("0");
			crmOrder.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmOrderService.addCrmOrder(crmOrder,mx));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除订单记录
	 * @param crmOrder 订单记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmOrderAndMx")
	public RetDataBean deleteCrmOrderAndMx(CrmOrder crmOrder) {
		try {
			if (StringUtils.isBlank(crmOrder.getOrderId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmOrder.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmOrderService.deleteCrmOrderAndMx(crmOrder));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新订单
	 * @param crmOrder 订单记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmOrder")
	public RetDataBean updateCrmOrder(CrmOrder crmOrder) {
		try {
			if (StringUtils.isBlank(crmOrder.getOrderId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmOrder.setOrgId(userInfo.getOrgId());
				Example example = new Example(CrmOrder.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("orderId", crmOrder.getOrderId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmOrderService.updateCrmOrder(crmOrder,example));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新订单记录
	 * @param crmOrder 订单记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmOrderAndMx")
	public RetDataBean updateCrmOrderAndMx(CrmOrder crmOrder,String mx) {
		try {
			if (StringUtils.isBlank(crmOrder.getOrderId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmOrder.setOrgId(userInfo.getOrgId());
				crmOrder.setCreateUser(userInfo.getAccountId());
				return crmOrderService.updateCrmOrderAndMx(crmOrder,mx);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 客户关怀审批
	 * @param crmCare 客户关怀记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/approvedCrmCare")
	public RetDataBean approvedCrmCare(CrmCare crmCare) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmCare.setApprovedTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmCare.setOrgId(userInfo.getOrgId());
			Example example = new Example(CrmCare.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", crmCare.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, crmCareService.updateCrmCare(example,crmCare));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建客户关怀记录
	 * @param crmCare 客户关怀记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmCare")
	public RetDataBean insertCrmCare(CrmCare crmCare) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmCare.setRecordId(SysTools.getGUID());
			crmCare.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmCare.setCreateUser(userInfo.getAccountId());
			crmCare.setStatus("0");
			crmCare.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmCareService.insertCrmCare(crmCare));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除客户关怀记录
	 * @param crmCare 客户关怀记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmCare")
	public RetDataBean deleteCrmCare(CrmCare crmCare) {
		try {
			if (StringUtils.isBlank(crmCare.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmCare.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmCareService.deleteCrmCare(crmCare));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除客户关怀记录
	 * @param ids 客户关怀记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmCareByIds")
	public RetDataBean deleteCrmCareByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return crmCareService.deleteCrmCareByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新客户关怀记录
	 * @param crmCare 客户关怀记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmCare")
	public RetDataBean updateCrmCare(CrmCare crmCare) {
		try {
			if (StringUtils.isBlank(crmCare.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmCare.setOrgId(userInfo.getOrgId());
				Example example = new Example(CrmCare.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", crmCare.getRecordId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmCareService.updateCrmCare(example,crmCare));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 报价单审批
	 * @param crmQuotation 报价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/approvedCrmQuotation")
	public RetDataBean approvedCrmQuotation(CrmQuotation crmQuotation) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmQuotation.setApprovedTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmQuotation.setOrgId(userInfo.getOrgId());
			Example example = new Example(CrmQuotation.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("quotationId", crmQuotation.getQuotationId());
			return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, crmQuotationService.updateCrmQuotation(example, crmQuotation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建报价单
	 * @param crmQuotation 报价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmQuotation")
	public RetDataBean insertCrmQuotation(CrmQuotation crmQuotation) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmQuotation.setQuotationId(SysTools.getGUID());
			crmQuotation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmQuotation.setCreateUser(userInfo.getAccountId());
			crmQuotation.setStatus("0");
			crmQuotation.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmQuotationService.insertCrmQuotation(crmQuotation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除报价单
	 * @param crmQuotation 报价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmQuotation")
	public RetDataBean deleteCrmQuotation(CrmQuotation crmQuotation) {
		try {
			if (StringUtils.isBlank(crmQuotation.getQuotationId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmQuotation.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmQuotationService.deleteCrmQuotation(crmQuotation));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 更新报价单
	 * @param crmQuotation 报价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmQuotation")
	public RetDataBean updateCrmQuotation(CrmQuotation crmQuotation) {
		try {
			if (StringUtils.isBlank(crmQuotation.getQuotationId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmQuotation.setOrgId(userInfo.getOrgId());
				Example example = new Example(CrmQuotation.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("quotationId", crmQuotation.getQuotationId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmQuotationService.updateCrmQuotation(example,crmQuotation));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建询价单
	 * @param crmInquiry 询价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmInquiry")
	public RetDataBean insertCrmInquiry(CrmInquiry crmInquiry) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmInquiry.setInquiryId(SysTools.getGUID());
			crmInquiry.setCreateUser(userInfo.getAccountId());
			crmInquiry.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmInquiry.setOrgId(userInfo.getOrgId());
			crmInquiry.setStatus("0");
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmInquiryService.insertCrmInquiry(crmInquiry));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  删除询价单
	 * @param crmInquiry 询价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmInquiry")
	public RetDataBean deleteCrmInquiry(CrmInquiry crmInquiry) {
		try {
			if (StringUtils.isBlank(crmInquiry.getInquiryId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmInquiry.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmInquiryService.deleteCrmInquiry(crmInquiry));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 *  批量删除询价单
	 * @param ids 询价单Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmInquiryByIds")
	public RetDataBean deleteCrmInquiryByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return crmInquiryService.deleteDiscussNoticeIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更改询价单状态
	 * @param crmInquiry 询价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInquiry")
	public RetDataBean updateInquiry(CrmInquiry crmInquiry) {
		try {
			if (StringUtils.isBlank(crmInquiry.getInquiryId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmInquiry.setOrgId(userInfo.getOrgId());
				Example example = new Example(CrmInquiry.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("inquiryId", crmInquiry.getInquiryId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmInquiryService.updateCrmInquiry(crmInquiry, example));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 更新询价单
	 * @param crmInquiry 询价单对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmInquiry")
	public RetDataBean updateCrmInquiry(CrmInquiry crmInquiry) {
		try {
			if (StringUtils.isBlank(crmInquiry.getInquiryId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmInquiry.setOrgId(userInfo.getOrgId());
				Example example = new Example(CrmInquiry.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("inquiryId", crmInquiry.getInquiryId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmInquiryService.updateCrmInquiry(crmInquiry, example));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建联系记录
	 *
	 * @param crmContractRecord 联系记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmContractRecord")
	public RetDataBean insertCrmContractRecord(CrmContractRecord crmContractRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmContractRecord.setRecordId(SysTools.getGUID());
			crmContractRecord.setCreateUser(userInfo.getAccountId());
			crmContractRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmContractRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmContractRecordService.addRecordAndCalendar(crmContractRecord));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加客户联系人
	 *
	 * @param crmLinkMan 客户联系人对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmLinkMan")
	public RetDataBean insertCrmLinkMan(CrmLinkMan crmLinkMan) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmLinkMan.setLinkManId(SysTools.getGUID());
			crmLinkMan.setCreateUser(userInfo.getAccountId());
			crmLinkMan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmLinkMan.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmLinkManService.insertCrmLinkMan(crmLinkMan));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除联系人
	 *
	 * @param crmLinkMan 客户联系人对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmLinkMan")
	public RetDataBean deleteCrmLinkMan(CrmLinkMan crmLinkMan) {
		try {
			if (StringUtils.isBlank(crmLinkMan.getLinkManId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			crmLinkMan.setOrgId(userInfo.getOrgId());
			if (userInfo.getOpFlag().equals("1")) {
				Example example = new Example(CrmLinkMan.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmLinkManService.deleteCrmLinkMan(crmLinkMan));
			} else {
				CrmRole crmRole = new CrmRole();
				crmRole.setOrgId(userInfo.getOrgId());
				crmRole = crmRoleService.selectOneCrmRole(crmRole);
				if (("," + crmRole.getManager() + ",").contains("," + userInfo.getAccountId() + ",")) {
					return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmLinkManService.deleteCrmLinkMan(crmLinkMan));
				} else {
					CrmLinkMan newCrmLinkMan = new CrmLinkMan();
					newCrmLinkMan.setOrgId(userInfo.getOrgId());
					newCrmLinkMan.setLinkManId(crmLinkMan.getLinkManId());
					newCrmLinkMan = crmLinkManService.selectOne(newCrmLinkMan);
					CrmCustomer crmCustomer = new CrmCustomer();
					crmCustomer.setOrgId(userInfo.getOrgId());
					crmCustomer.setCustomerId(newCrmLinkMan.getCustomerId());
					crmCustomer = crmCustomerService.selectOne(crmCustomer);
					if (crmCustomer.getKeepUser().equals(userInfo.getAccountId())) {
						return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmLinkManService.deleteCrmLinkMan(crmLinkMan));
					} else {
						return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
					}
				}
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新联系人
	 *
	 * @param crmLinkMan 客户联系人对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmLinkMan")
	public RetDataBean updateCrmLinkMan(CrmLinkMan crmLinkMan) {
		try {
			if (StringUtils.isBlank(crmLinkMan.getLinkManId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			crmLinkMan.setOrgId(userInfo.getOrgId());
			if (userInfo.getOpFlag().equals("1")) {
				Example example = new Example(CrmLinkMan.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmLinkManService.updateCrmLinkMan(crmLinkMan, example));
			} else {
				CrmRole crmRole = new CrmRole();
				crmRole.setOrgId(userInfo.getOrgId());
				crmRole = crmRoleService.selectOneCrmRole(crmRole);
				if (("," + crmRole.getManager() + ",").contains("," + userInfo.getAccountId() + ",")) {
					Example example = new Example(CrmLinkMan.class);
					example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
					return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmLinkManService.updateCrmLinkMan(crmLinkMan, example));
				} else {
					CrmLinkMan newCrmLinkMan = new CrmLinkMan();
					newCrmLinkMan.setOrgId(userInfo.getOrgId());
					newCrmLinkMan.setLinkManId(crmLinkMan.getLinkManId());
					newCrmLinkMan = crmLinkManService.selectOne(newCrmLinkMan);
					CrmCustomer crmCustomer = new CrmCustomer();
					crmCustomer.setOrgId(userInfo.getOrgId());
					crmCustomer.setCustomerId(newCrmLinkMan.getCustomerId());
					crmCustomer = crmCustomerService.selectOne(crmCustomer);
					if (crmCustomer.getKeepUser().equals(userInfo.getAccountId())) {
						Example example = new Example(CrmLinkMan.class);
						example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
						return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmLinkManService.updateCrmLinkMan(crmLinkMan, example));
					} else {
						return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
					}
				}
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 客户报备
	 *
	 * @param crmCustomer 客户信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmCustomer")
	public RetDataBean insertCrmCustomer(CrmCustomer crmCustomer) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmCustomer.setCustomerId(SysTools.getGUID());
			crmCustomer.setCreateUser(userInfo.getAccountId());
			crmCustomer.setKeepUser(userInfo.getAccountId());
			crmCustomer.setStatus("1");
			crmCustomer.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmCustomer.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmCustomerService.insertCrmCustomer(crmCustomer));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建客户
	 *
	 * @param crmCustomer 客户信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmCustomerToPool")
	public RetDataBean insertCrmCustomerToPool(CrmCustomer crmCustomer) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmCustomer.setCustomerId(SysTools.getGUID());
			crmCustomer.setCreateUser(userInfo.getAccountId());
			crmCustomer.setStatus("1");
			crmCustomer.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmCustomer.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmCustomerService.insertCrmCustomer(crmCustomer));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除客户
	 *
	 * @param crmCustomer 客户信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmCustomer")
	public RetDataBean deleteCrmCustomer(CrmCustomer crmCustomer) {
		try {
			if (StringUtils.isBlank(crmCustomer.getCustomerId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmCustomerService.deleteCrmCustomer(crmCustomer));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 批量删除客户信息
	 *
	 * @param ids 客户Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmCustomerByIds")
	public RetDataBean deleteCrmCustomerByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return crmCustomerService.deleteCrmCustomerByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除客户联系记录
	 *
	 * @param ids 客户联系记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmContractRecordByIds")
	public RetDataBean deleteCrmContractRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return crmContractRecordService.deleteCrmContractRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除客户联系记录
	 * @param crmContractRecord 客户联系记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmContractRecord")
	public RetDataBean deleteCrmContractRecord(CrmContractRecord crmContractRecord) {
		try {
			if (StringUtils.isBlank(crmContractRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmContractRecordService.deleteCrmContractRecord(crmContractRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新客户联系记录
	 * @param crmContractRecord 客户联系记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmContractRecord")
	public RetDataBean updateCrmContractRecord(CrmContractRecord crmContractRecord) {
		try {
			if (StringUtils.isBlank(crmContractRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			} else {
				UserInfo userInfo = userInfoService.getRedisUser();
				crmContractRecord.setOrgId(userInfo.getOrgId());
				Example example = new Example(CrmContractRecord.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", crmContractRecord.getRecordId());
				return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmContractRecordService.updateCrmContractRecord(crmContractRecord, example));
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新客户信息
	 *
	 * @param crmCustomer 客户信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmCustomer")
	public RetDataBean updateCrmCustomer(CrmCustomer crmCustomer) {
		try {
			if (StringUtils.isBlank(crmCustomer.getCustomerId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			crmCustomer.setOrgId(userInfo.getOrgId());
			Example example = new Example(CrmCustomer.class);
			example.createCriteria().andEqualTo("customerId", crmCustomer.getCustomerId()).andEqualTo("orgId", crmCustomer.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmCustomerService.UpdateCrmCustomer(crmCustomer, example));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建字典
	 *
	 * @param crmDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmDic")
	public RetDataBean insertCrmDic(CrmDic crmDic) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(crmDic.getParentId())) {
				crmDic.setParentId("0");
			}
			crmDic.setDicId(SysTools.getGUID());
			crmDic.setOrgId(userInfo.getOrgId());
			crmDic.setStatus("1");
			crmDic.setCreateUser(userInfo.getCreateUser());
			crmDic.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, crmDicService.insertCrmDic(crmDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新字典
	 *
	 * @param crmDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmDic")
	public RetDataBean updateCrmDic(CrmDic crmDic) {
		try {
			if (StringUtils.isBlank(crmDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(CrmDic.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("dicId", crmDic.getDicId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, crmDicService.updateCrmDic(crmDic, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除字典
	 *
	 * @param crmDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmDic")
	public RetDataBean deleteCrmDic(CrmDic crmDic) {
		try {
			if (StringUtils.isBlank(crmDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (crmDicService.isExistChild(userInfo.getOrgId(), crmDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
			crmDic.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmDicService.deleteCrmDic(crmDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除字典
	 *
	 * @param ids 字典Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmDicByIds")
	public RetDataBean deleteCrmDicByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return crmDicService.deleteCrmDicByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 字典排序
	 *
	 * @param oldDicId 原字典Id
	 * @param newDicId 新籽典Id
	 * @param oldSortNo 原排序号
	 * @param newSortNo 新排序号
	 * @return 消息结构
	 */
	@PostMapping(value = "/dropCrmDic")
	public RetDataBean dropCrmDic(String oldDicId, String newDicId, Integer oldSortNo, Integer newSortNo) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmDicService.dropCrmDic(userInfo.getOrgId(), oldDicId, newDicId, oldSortNo, newSortNo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加产品分类
	 *
	 * @param crmProductSort 产品分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmProductSort")
	public RetDataBean insertCrmProductSort(CrmProductSort crmProductSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmProductSort.setSortId(SysTools.getGUID());
			crmProductSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmProductSort.setCreateUser(userInfo.getAccountId());
			crmProductSort.setOrgId(userInfo.getOrgId());
			if (StringUtils.isBlank(crmProductSort.getParentId())) {
				crmProductSort.setParentId("0");
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmProductSortService.insertCrmProductSort(crmProductSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除产品分类
	 *
	 * @param crmProductSort 产品分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmProductSort")
	public RetDataBean deleteCrmProductSort(CrmProductSort crmProductSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(crmProductSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			crmProductSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmProductSortService.deleteCrmProductSort(crmProductSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新产品分类
	 *
	 * @param crmProductSort 产品分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmProductSort")
	public RetDataBean updateCrmProductSort(CrmProductSort crmProductSort) {
		try {
			if (StringUtils.isBlank(crmProductSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (crmProductSort.getSortId().equals(crmProductSort.getParentId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(crmProductSort.getParentId())) {
				crmProductSort.setParentId("0");
			}
			Example example = new Example(CrmProductSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", crmProductSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmProductSortService.updateCrmProductSort(example, crmProductSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加销售产品
	 *
	 * @param crmProduct 产品对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertCrmProduct")
	public RetDataBean insertCrmProduct(CrmProduct crmProduct) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			crmProduct.setProductId(SysTools.getGUID());
			crmProduct.setStatus("0");
			crmProduct.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmProduct.setCreateUser(userInfo.getAccountId());
			crmProduct.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmProductService.insertCrmProduct(crmProduct));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除销售产品
	 *
	 * @param crmProduct 产品对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmProduct")
	public RetDataBean deleteCrmProduct(CrmProduct crmProduct) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(crmProduct.getProductId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			crmProduct.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmProductService.deleteCrmProduct(crmProduct));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删销售产品
	 *
	 * @param ids 产品Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteCrmProductByIds")
	public RetDataBean deleteCrmProductByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return crmProductService.deleteCrmProductByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新销售产品
	 *
	 * @param crmProduct 产品对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateCrmProduct")
	public RetDataBean updateCrmProduct(CrmProduct crmProduct) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(CrmProduct.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("productId", crmProduct.getProductId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmProductService.updateCrmProduct(example, crmProduct));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 销售产品导入
	 *
	 * @param response HttpServletResponse
	 * @param file 文件
	 */
	@PostMapping(value = "/importCrmProductInfo")
	public void importCrmProductInfo(HttpServletResponse response, MultipartFile file) {
		PrintWriter out = null;
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			RetDataBean retDataBean = crmProductService.importCrmProductInfo(userInfo, file);
			String s = JSON.toJSONString(retDataBean);
			response.setContentType("text/html; charset=utf-8");
			out = response.getWriter().append(s);
			out.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * 设置CRM权限
	 *
	 * @param crmRole CRM权限对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setCrmRole")
	public RetDataBean setCrmRole(CrmRole crmRole) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (userInfo.getOpFlag().equals("1")) {
				CrmRole newCrmRole = new CrmRole();
				newCrmRole.setOrgId(userInfo.getOrgId());
				crmRole.setOrgId(userInfo.getOrgId());
				int count = crmRoleService.selectCount(newCrmRole);
				if (count <= 0) {
					crmRole.setRoleId(SysTools.getGUID());
					return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmRoleService.insertCrmRole(crmRole));
				} else {
					Example example = new Example(CrmRole.class);
					example.createCriteria().andEqualTo("orgId", userInfo.getOrgId());
					return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmRoleService.updateCrmRole(crmRole, example));
				}
			} else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}


}
