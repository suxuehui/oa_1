package com.core136.controller.info;

import com.core136.bean.account.UserInfo;
import com.core136.bean.info.*;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.info.*;
import com.core136.service.system.SysWebSiteService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/get/info")
@CrossOrigin
@Api(value="信息GetController",tags={"信息获取数据接口"})
public class ApiGetInfoController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
    private NewsService newsService;
	private UserInfoService userInfoService;
    private NoticeTemplateService noticeTemplateService;
	private NoticeService noticeService;
	private NoticeConfigService noticeConfigService;
	private SysWebSiteService sysWebSiteService;
	private VoteService voteService;
	private LeadActivityService leadActivityService;
	private LeaderMailboxConfigService leaderMailboxConfigService;
	private LeaderMailboxService leaderMailboxService;
	private DataUploadInfoService dataUploadInfoService;
	private DataUploadHandleService dataUploadHandleService;
	private AddressBookService addressBookService;
	private VoteResultService voteResultService;
	private BigStoryService bigStoryService;
	private DiscussService discussService;
	private DiscussNoticeService discussNoticeService;
	private DiscussRecordService discussRecordService;
	private DynamicsRecordService dynamicsRecordService;
	private DynamicsCommentService dynamicsCommentService;
	private EmergencyService emergencyService;
	private BookMarkService bookMarkService;
	private OpenInfoService openInfoService;
	@Autowired
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setNoticeTemplateService(NoticeTemplateService noticeTemplateService) {
		this.noticeTemplateService = noticeTemplateService;
	}
	@Autowired
	public void setNoticeService(NoticeService noticeService) {
		this.noticeService = noticeService;
	}
	@Autowired
	public void setNoticeConfigService(NoticeConfigService noticeConfigService) {
		this.noticeConfigService = noticeConfigService;
	}
	@Autowired
	public void setSysWebSiteService(SysWebSiteService sysWebSiteService) {
		this.sysWebSiteService = sysWebSiteService;
	}
	@Autowired
	public void setVoteService(VoteService voteService) {
		this.voteService = voteService;
	}
	@Autowired
	public void setLeadActivityService(LeadActivityService leadActivityService) {
		this.leadActivityService = leadActivityService;
	}
	@Autowired
	public void setLeaderMailboxConfigService(LeaderMailboxConfigService leaderMailboxConfigService) {
		this.leaderMailboxConfigService = leaderMailboxConfigService;
	}
	@Autowired
	public void setLeaderMailboxService(LeaderMailboxService leaderMailboxService) {
		this.leaderMailboxService = leaderMailboxService;
	}
	@Autowired
	public void setDataUploadInfoService(DataUploadInfoService dataUploadInfoService) {
		this.dataUploadInfoService = dataUploadInfoService;
	}
	@Autowired
	public void setDataUploadHandleService(DataUploadHandleService dataUploadHandleService) {
		this.dataUploadHandleService = dataUploadHandleService;
	}
	@Autowired
	public void setAddressBookService(AddressBookService addressBookService) {
		this.addressBookService = addressBookService;
	}
	@Autowired
	public void setVoteResultService(VoteResultService voteResultService) {
		this.voteResultService = voteResultService;
	}
	@Autowired
	public void setBigStoryService(BigStoryService bigStoryService) {
		this.bigStoryService = bigStoryService;
	}
	@Autowired
	public void setDiscussService(DiscussService discussService) {
		this.discussService = discussService;
	}
	@Autowired
	public void setDiscussNoticeService(DiscussNoticeService discussNoticeService) {
		this.discussNoticeService = discussNoticeService;
	}
	@Autowired
	public void setDiscussRecordService(DiscussRecordService discussRecordService) {
		this.discussRecordService = discussRecordService;
	}
	@Autowired
	public void setDynamicsRecordService(DynamicsRecordService dynamicsRecordService) {
		this.dynamicsRecordService = dynamicsRecordService;
	}
	@Autowired
	public void setDynamicsCommentService(DynamicsCommentService dynamicsCommentService) {
		this.dynamicsCommentService = dynamicsCommentService;
	}
	@Autowired
	public void setEmergencyService(EmergencyService emergencyService) {
		this.emergencyService = emergencyService;
	}
	@Autowired
	public void setBookMarkService(BookMarkService bookMarkService) {
		this.bookMarkService = bookMarkService;
	}
	@Autowired
	public void setOpenInfoService(OpenInfoService openInfoService) {
		this.openInfoService = openInfoService;
	}

	/**
	 * 获取工作便签信息
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyBookMark")
	public RetDataBean getMyBookMark() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			BookMark bookMark = new BookMark();
			bookMark.setOrgId(userInfo.getOrgId());
			bookMark.setCreateUser(userInfo.getAccountId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, bookMarkService.selectOneBookMark(bookMark));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取紧急事件列表
	 * @param pageParam 分页参数
	 * @param emergencyType 事件类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getEmergencyListForManage")
	public RetDataBean getEmergencyListForManage(PageParam pageParam, String emergencyType,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.create_time");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, emergencyService.getEmergencyListForManage(pageParam, emergencyType,date, timeArr[0], timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取当前紧急事件列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getEmergencyListForDesk")
	public RetDataBean getEmergencyListForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, emergencyService.getEmergencyListForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取动态评论列表
	 * @param parentId 评论父级Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDynamicsCommentList")
	public RetDataBean getDynamicsCommentList(String parentId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dynamicsCommentService.getDynamicsCommentList(userInfo.getOrgId(),parentId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取动态列表
	 * @param page 页码
	 * @param myDate 日期
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDynamicsRecordList")
	public RetDataBean getDynamicsRecordList(Integer page,String myDate,String keyword) {
		try {
			String beginTime="";
			String endTime="";
			if(StringUtils.isNotBlank(myDate))
			{
				beginTime = myDate+" 00:00:00";
				endTime = myDate+" 23:59:59";
			}
			if(StringUtils.isBlank(keyword))
			{
				keyword = "";
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dynamicsRecordService.getDynamicsRecordList(userInfo.getOrgId(),beginTime,endTime,page,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面信息动态列表
	 * @return 信息动态列表
	 */
	@GetMapping(value = "/getDynamicsRecordForDesk")
	public RetDataBean getDynamicsRecordForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dynamicsRecordService.getDynamicsRecordForDesk(userInfo.getOrgId()));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取移动端领导行程
	 * @param page 页码
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyLeadActivityForApp")
	public RetDataBean getMobileMyLeadActivity(Integer page) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leadActivityService.getMyLeadActivityForApp(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(), page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取移动端通知公告
	 * @param page 页码
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyNoticeListForApp")
	public RetDataBean getMyNoticeListForApp(Integer page,String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getMyNoticeListForApp(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(), page,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取移动端企业新闻的列表
	 * @param page 页码
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyNewsListForApp")
	public RetDataBean getMyNewsListForApp(Integer page,String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<Map<String, String>> pageInfo = newsService.getMyNewsListForApp(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(), page,keyword);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取讨论区参与人列表
	 * @param discussId 讨论区Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiscussUserList")
	public RetDataBean getDiscussUserList(String discussId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussRecordService.getDiscussUserList(userInfo.getOrgId(),discussId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取子帖
	 *
	 * @param recordId 记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiscussRecordListById")
	public RetDataBean getDiscussRecordListById(String recordId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussRecordService.getDiscussRecordListById(userInfo.getOrgId(), userInfo.getAccountId(), recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取讨论主题列表
	 * @param discussId 讨论区Id
	 * @param page 页码
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTopDiscussRecordList")
	public RetDataBean getTopDiscussRecordList(String discussId,Integer page,String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussRecordService.getTopDiscussRecordList(userInfo.getOrgId(),userInfo.getAccountId(),discussId,keyword,page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取通知公告列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiscussNoticeForManage")
	public RetDataBean getDiscussNoticeForManage(PageParam pageParam,String date,String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = discussNoticeService.getDiscussNoticeForManage(pageParam,date,timeArr[0],timeArr[1],status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取通知公告列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiscussNoticeList")
	public RetDataBean getDiscussNoticeList() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussNoticeService.getDiscussNoticeList(userInfo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  获取讨论区通知公告
	 * @param discussNotice 讨论区通知公告对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiscussNoticeById")
	public RetDataBean getDiscussNoticeById(DiscussNotice discussNotice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			discussNotice.setOrgId(userInfo.getOrgId());
			discussNotice = discussNoticeService.selectOneDiscussNotice(discussNotice);
			String reader = discussNotice.getReader();
			DiscussNotice discussNoticeTemp = new DiscussNotice();
			if (("," + reader + ",").indexOf("," + userInfo.getAccountId() + ",") <= 0) {
				if (StringUtils.isBlank(reader)) {
					reader = userInfo.getAccountId();
				} else {
					reader = reader + "," + userInfo.getAccountId();
				}
			}
			discussNoticeTemp.setReader(reader);
			Example example = new Example(DiscussNotice.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", discussNotice.getRecordId());
			discussNoticeService.updateDiscussNotice(example, discussNoticeTemp);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussNotice);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取讨论区列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyDiscussList")
	public RetDataBean getMyDiscussList() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussService.getMyDiscussList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 版块列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDiscussList")
	public RetDataBean getDiscussList() {
		UserInfo userInfo = userInfoService.getRedisUser();
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("discuss:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,discussService.getDiscussList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有大记事
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBigStoryList")
	public RetDataBean getBigStoryList() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("bigstory:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, bigStoryService.getBigStoryList(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取大纪事管理列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param eventType 类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getBigStoryListForManage")
	public RetDataBean getBigStoryListForManage(PageParam pageParam, String date,String eventType) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("bigstory:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("b.happen_time");
			} else {
				pageParam.setProp("b."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			PageInfo<Map<String, String>> pageInfo = bigStoryService.getBigStoryListForManage(pageParam, eventType, date,timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取投票人员列表
	 * @param voteId 投票记录Id
	 * @param itemId 设票结果Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/voteDetailsForUser")
	public RetDataBean voteDetailsForUser(String voteId, String itemId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteResultService.voteDetailsForUser(userInfo.getOrgId(), voteId, itemId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取投票人员查看状态
	 *
	 * @param vote 投票对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVoteStatus")
	public RetDataBean getVoteStatus(Vote vote) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			vote.setOrgId(userInfo.getOrgId());
			vote = voteService.selectOneVote(vote);
			List<Map<String, String>> accountIdList = userInfoService.getAccountIdInRole(vote.getOrgId(), vote.getUserRole(), vote.getDeptRole(), vote.getLevelRole());
			List<String> list = new ArrayList<>();
			for (Map<String, String> map : accountIdList) {
				list.add(map.get("accountId"));
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteService.getVoteStatus(userInfo.getOrgId(), vote.getVoteId(), list));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取投票结果
	 * @param vote 投票对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVoteResult")
	public RetDataBean getVoteResult(Vote vote) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			vote.setOrgId(userInfo.getOrgId());
			vote = voteService.selectOneVote(vote);
			if(vote.getReadRes().equals("1") && !vote.getCreateUser().equals(userInfo.getAccountId())&& !userInfo.getOpFlag().equals("1")){
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteService.getVoteResultByVoteId(vote));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取历史投票列表
	 * @param pageParam 分页参数
	 * @param voteType 投票类型
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 投票列表
	 */
	@GetMapping(value = "/getMyOldVoteListForVote")
	public RetDataBean getMyOldVoteListForVote(PageParam pageParam,String voteType,String date,String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("v.create_time");
			} else {
				pageParam.setProp("v."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = voteService.getMyOldVoteListForVote(pageParam, voteType,date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取移动端个人投票列表
	 * @param page 页码
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyVoteListForApp")
	public RetDataBean getMyVoteListForApp(Integer page,String keyword) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<Map<String, String>> pageInfo = voteService.getMyVoteListForApp(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(),keyword,page);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取待我投票列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyVoteListForVote")
	public RetDataBean getMyVoteListForVote() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteService.getMyVoteListForVote(userInfo.getOrgId(),userInfo.getAccountId(),userInfo.getDeptId(),userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	@GetMapping(value = "/getMyVoteListForVoteForDesk")
	public RetDataBean getMyVoteListForVoteForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteService.getMyVoteListForVoteForDesk(userInfo.getOrgId(),userInfo.getAccountId(),userInfo.getDeptId(),userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取投票管理列表
	 * @param pageParam 分页参数
	 * @param voteType 投票类型
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getVoteListForManage")
	public RetDataBean getVoteListForManage(PageParam pageParam,String voteType,String date,String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("v.create_time");
			} else {
				pageParam.setProp("v."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			String orderBy = pageParam.getProp() + " " + pageParam.getSortOrder();
			pageParam.setOrderBy(orderBy);
			pageParam.setOrgId(userInfo.getOrgId());
			PageInfo<Map<String, String>> pageInfo = voteService.getVoteListForManage(pageParam, voteType,date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取他人共享的人员列表
	 * @param bookType 通讯录类型
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyShareAddressBookList")
	public RetDataBean getMyShareAddressBookList(String bookType,String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getMyShareAddressBookList(userInfo.getOrgId(), userInfo.getAccountId(), bookType,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人通讯录
	 * @param bookType 通讯录类型
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyAddressBookList")
	public RetDataBean getMyAddressBookList(String bookType,String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getAddressBook(userInfo.getOrgId(),userInfo.getAccountId(),bookType,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取同事列表
	 * @param levelId 层级Id
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyFriendsByLevel")
	public RetDataBean getMyFriendsByLevel(String levelId,String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, addressBookService.getMyFriendsByLevel(userInfo.getOrgId(), levelId,keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取信息处理详情
	 * @param dataUploadHandle 信息处理对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDataUploadHandleById")
	public RetDataBean getDataUploadHandleById(DataUploadHandle dataUploadHandle) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			dataUploadHandle.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dataUploadHandleService.getDataUploadHandleList(dataUploadHandle));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面上报信息列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyDataInfoForDesk")
	public RetDataBean getMyDataInfoForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dataUploadInfoService.getMyDataInfoForDesk(userInfo.getOrgId(),userInfo.getAccountId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取信息上报详情
	 * @param recordId 上报数据Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDataUploadInfoById")
	public RetDataBean getDataUploadInfoById(String recordId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(recordId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dataUploadInfoService.getDataUploadInfoById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(), userInfo.getDeptId(),recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取持处理的信息列表
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param fromAccountId 上报人
	 * @param status 处理状诚
	 * @param date 日期范围类型
	 * @param dataType 上报数据类型
	 * @param approvedType 审批状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getToProcessInfoList")
	public RetDataBean getToProcessInfoList(PageParam pageParam, String deptId, String fromAccountId,String status,String date,String dataType, String approvedType) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = dataUploadInfoService.getToProcessInfoList(pageParam, deptId, fromAccountId,status,date, timeArr[0], timeArr[1], dataType, approvedType);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取上报信息列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param deptId 部门Id
	 * @param fromAccountId 上报人
	 * @param dataType 上报数据类型
	 * @param approvedType 审批状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDataUploadInfoList")
	public RetDataBean getDataUploadInfoList(PageParam pageParam,String date, String deptId, String fromAccountId, String dataType, String approvedType) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("dataupload:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dataUploadInfoService.getDataUploadInfoList(pageParam, deptId, fromAccountId, date,timeArr[0], timeArr[1], dataType, approvedType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取领导信箱信息列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param mailType 信箱类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyLeaderMailBoxList")
	public RetDataBean getMyLeaderMailBoxList(PageParam pageParam,String date ,String mailType,String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadbox:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.create_time");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = leaderMailboxService.getMyLeaderMailBoxList(pageParam,status,mailType,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取领导信箱记录列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param status 状态
	 * @param mailType 信箱类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeaderMailBoxList")
	public RetDataBean getLeaderMailBoxList(PageParam pageParam,String date,String status,String mailType) {
		Subject subject = SecurityUtils.getSubject();
		if (!subject.isPermitted("leadbox:query")) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
		}
		UserInfo userInfo = userInfoService.getRedisUser();
		LeaderMailboxConfig leaderMailboxConfig = new LeaderMailboxConfig();
		leaderMailboxConfig.setOrgId(userInfo.getOrgId());
		leaderMailboxConfig = leaderMailboxConfigService.selectOneLeaderMailboxConfig(leaderMailboxConfig);
		if(leaderMailboxConfig==null)
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
		}
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.create_time");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			if (leaderMailboxConfig.getLeader().equals(userInfo.getAccountId())) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leaderMailboxService.getLeaderMailBoxList(pageParam,status,mailType,date,timeArr[0],timeArr[1]));
			} else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 领导信箱设置详情
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeaderMailboxConfig")
	public RetDataBean getLeaderMailboxConfig() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadbox:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			LeaderMailboxConfig leaderMailboxConfig = new LeaderMailboxConfig();
			leaderMailboxConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leaderMailboxConfigService.selectOneLeaderMailboxConfig(leaderMailboxConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 查询程领导日程
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param ledActType 日程类型
	 * @param leader 领导
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeadActivityQueryList")
	public RetDataBean getLeadActivityQueryList(PageParam pageParam,String date,String ledActType, String leader) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.begin_time");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTimeAfter(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = leadActivityService.getLeadActivityQueryList(pageParam,ledActType,date, timeArr[0], timeArr[1], leader);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面领导日程
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeadActivityListForDesk")
	public RetDataBean getLeadActivityListForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leadActivityService.getLeadActivityListForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *获取新闻详情
	 * @param newsId 新闻Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyNewsById")
	public RetDataBean getMyNewsById(String newsId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(newsId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getMyNewsById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(),newsId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取领导行程详情
	 * @param recordId 领导日程Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeadActivityById")
	public RetDataBean getLeadActivityById(String recordId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(recordId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, leadActivityService.getLeadActivityById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(),recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取投票详情
	 * @param voteId 投票记录Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyVoteById")
	public RetDataBean getMyVoteById(String voteId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("vote:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(voteId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, voteService.getMyVoteById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(),voteId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取公告详情
	 * @param noticeId 通知公告Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyNoticeById")
	public RetDataBean getMyNoticeById(String noticeId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(noticeId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getMyNoticeById(userInfo.getOrgId(),userInfo.getOpFlag(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel(),noticeId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取领导行程列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param ledActType 日程类型
	 * @param leader 领导
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeadActivityList")
	public RetDataBean getLeadActivityList(PageParam pageParam,String date,String ledActType,String leader,String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("leadactivity:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("l.begin_time");
			} else {
				pageParam.setProp("l."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDayAfter(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			PageInfo<Map<String, String>> pageInfo = leadActivityService.getLeadActivityList(pageParam,ledActType,date, timeArr[0], timeArr[1], leader,status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取新闻管理列表
	 * @param pageParam 分页参数
	 * @param newsType 新闻类型
	 * @param status 状态
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
    @GetMapping(value = "/getNewsManageList")
    public RetDataBean getNewsManageList(PageParam pageParam, String newsType,String status,String date) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getNewsManageList(pageParam, newsType, status,date, timeArr[0], timeArr[1]));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 获取个人权限内的新闻
	 * @param pageParam 分页参数
	 * @param newsType 新闻类型
	 * @param status 装诚
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
    @GetMapping(value = "/getMyNewsList")
    public RetDataBean getMyNewsList(PageParam pageParam,String newsType,String status,String date) {
        try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getMyNewsList(pageParam, userInfo.getDeptId(), userInfo.getUserLevel(), newsType, status,date, timeArr[0], timeArr[1]));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 获取审批列表
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param noticeType 通知公告类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNoticeListForApproval")
	public RetDataBean getNoticeListForApproval(PageParam pageParam,String date,String noticeType) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getNoticeListForApproval(pageParam,noticeType,date,timeArr[0],timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取通知公告维护列表
	 * @param pageParam 分类参数
	 * @param noticeType 通知公告类型
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNoticeManageList")
	public RetDataBean getNoticeManageList(PageParam pageParam,String noticeType,String date) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.create_time");
			} else {
				pageParam.setProp("n."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getNoticeManageList(pageParam, noticeType, date,timeArr[0], timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取个人的通知公告
	 * @param pageParam 分页参数
	 * @param noticeType 通知公告类型
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyNoticeList")
	public RetDataBean getMyNoticeList(PageParam pageParam,String noticeType,String date,String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.send_time");
			} else {
				pageParam.setProp("n."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = noticeService.getMyNoticeList(pageParam, noticeType,date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按分类获取红头列表
	 * @param noticeType 通知公告类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNoticeTemplateListByType")
	public RetDataBean getNoticeTemplateListByType(String noticeType) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeTemplateService.getNoticeTemplateListByType(userInfo.getOrgId(), noticeType));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取新闻图片
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyPicNewsListForDesk")
	public RetDataBean getMyPicNewsListForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getMyPicNewsListForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面新闻
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyNewsListForDesk")
	public RetDataBean getMyNewsListForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getMyNewsListForDesk(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}



	/**
	 * 获取新闻人员查看状态
	 *
	 * @param news 单位新闻对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNewsReadStatus")
	public RetDataBean getNewsReadStatus(News news) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("news:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			news.setOrgId(userInfo.getOrgId());
			news = newsService.selectOneNews(news);
			List<Map<String, String>> accountIdList = userInfoService.getAccountIdInRole(news.getOrgId(), news.getUserRole(), news.getDeptRole(), news.getLevelRole());
			List<String> list = new ArrayList<>();
			for (Map<String, String> map : accountIdList) {
				list.add(map.get("accountId"));
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getNewsReadStatus(userInfo.getOrgId(), news.getNewsId(), list));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取通知公告模版
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNoticeTemplateList")
	public RetDataBean getNoticeTemplateList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("n.sort_no");
			} else {
				pageParam.setProp("n."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeTemplateService.getNoticeTemplateList(pageParam));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取通知公告配置列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNoticeConfigList")
	public RetDataBean getNoticeConfigList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeConfigService.getNoticeConfigList(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取常用网址列表
	 * @param pageParam 分页参数
	 * @param status 状态
	 * @param webType 网址类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSysWebSite")
	public RetDataBean getSysWebSite(PageParam pageParam,String status,String webType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("sort_no");
			} else {
				pageParam.setProp(StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysWebSiteService.getSysWebSite(pageParam,status,webType));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取我的常用网址
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMySysWebSiteList")
	public RetDataBean getMySysWebSiteList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysWebSiteService.getMySysWebSiteList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取桌面的通知消息
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyNoticeListForDesk")
	public RetDataBean getMyNoticeListForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getMyNoticeListForDesk(userInfo.getOrgId(), SysTools.getTime("yyyy-MM-dd"), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取通知公告人员查看状态
	 *
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getNoticeReadStatus")
	public RetDataBean getNoticeReadStatus(Notice notice) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("notice:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			notice.setOrgId(userInfo.getOrgId());
			notice = noticeService.selectOneNotice(notice);
			List<Map<String, String>> accountIdList = userInfoService.getAccountIdInRole(notice.getOrgId(), notice.getUserRole(), notice.getDeptRole(), notice.getLevelRole());
			List<String> list = new ArrayList<>();
			for (Map<String, String> map : accountIdList) {
				list.add(map.get("accountId"));
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getNoticeReadStatus(userInfo.getOrgId(), notice.getNoticeId(), list));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取政务公开信息列表
	 * @param pageParam 分页参数
	 * @param infoType 信息类型
	 * @param status 信息状态
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getOpenInfoListForManage")
	public RetDataBean getOpenInfoListForManage(PageParam pageParam, String infoType,String status,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("o.sort_no");
			} else {
				pageParam.setProp("o."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, openInfoService.getOpenInfoListForManage(pageParam, infoType,status,date, timeArr[0], timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有政务公开信息列表
	 * @param pageParam 分页参数
	 * @param infoType 信息类型
	 * @param readStatus 查阅状态
	 * @param date 日期范围
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyOpenInfoList")
	public RetDataBean getMyOpenInfoList(PageParam pageParam, String infoType,String readStatus,String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("o.sort_no");
			} else {
				pageParam.setProp("o."+StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, openInfoService.getMyOpenInfoList(pageParam, infoType,readStatus,date, timeArr[0], timeArr[1]));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}



	/**
	 * 获取桌面模块政务公开例表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getOpenInfoListForDesk")
	public RetDataBean getOpenInfoListForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, openInfoService.getOpenInfoListForDesk(userInfo.getOrgId(),userInfo.getAccountId(),SysTools.getTime("yyyy-MM-dd")));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取政务公开详情
	 * @param recordId 信息Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getOpenInfoById")
	public RetDataBean getOpenInfoById(String recordId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, openInfoService.getOpenInfoById(userInfo.getOrgId(),recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}



}
