package com.core136.controller.invite;

import com.core136.bean.account.UserInfo;
import com.core136.bean.invite.*;
import com.core136.bean.system.SysDic;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.invite.*;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/invite")
@CrossOrigin
@Api(value="招标SetController",tags={"招标更新数据接口"})
public class ApiSetInviteController {
	private UserInfoService userInfoService;
	private InviteTeamService inviteTeamService;
	private InviteDicService inviteDicService;
	private InviteEntService inviteEntService;
	private InviteRecordService inviteRecordService;
	private InviteRoleService inviteRoleService;
	private InviteEntLicService inviteEntLicService;
	private InviteScoreService inviteScoreService;
	private InviteFileService inviteFileService;
	private InviteDepositService inviteDepositService;
	private InviteFileSellService inviteFileSellService;
	private InviteBiddingService inviteBiddingService;
	private InviteOpenService inviteOpenService;
	private InviteEvaluationService inviteEvaluationService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setInviteTeamService(InviteTeamService inviteTeamService) {
		this.inviteTeamService = inviteTeamService;
	}
	@Autowired
	public void setInviteDicService(InviteDicService inviteDicService) {
		this.inviteDicService = inviteDicService;
	}
	@Autowired
	public void setInviteEntService(InviteEntService inviteEntService) {
		this.inviteEntService = inviteEntService;
	}
	@Autowired
	public void setInviteRecordService(InviteRecordService inviteRecordService) {
		this.inviteRecordService = inviteRecordService;
	}
	@Autowired
	public void setInviteRoleService(InviteRoleService inviteRoleService) {
		this.inviteRoleService = inviteRoleService;
	}
	@Autowired
	public void setInviteEntLicService(InviteEntLicService inviteEntLicService) {
		this.inviteEntLicService = inviteEntLicService;
	}
	@Autowired
	public void setInviteScoreService(InviteScoreService inviteScoreService) {
		this.inviteScoreService = inviteScoreService;
	}
	@Autowired
	public void setInviteFileService(InviteFileService inviteFileService) {
		this.inviteFileService = inviteFileService;
	}
	@Autowired
	public void setInviteDepositService(InviteDepositService inviteDepositService) {
		this.inviteDepositService = inviteDepositService;
	}
	@Autowired
	public void setInviteFileSellService(InviteFileSellService inviteFileSellService) {
		this.inviteFileSellService = inviteFileSellService;
	}
	@Autowired
	public void setInviteBiddingService(InviteBiddingService inviteBiddingService) {
		this.inviteBiddingService = inviteBiddingService;
	}
	@Autowired
	public void setInviteOpenService(InviteOpenService inviteOpenService) {
		this.inviteOpenService = inviteOpenService;
	}
	@Autowired
	public void setInviteEvaluationService(InviteEvaluationService inviteEvaluationService) {
		this.inviteEvaluationService = inviteEvaluationService;
	}

	/**
	 * 录入评标记录
	 * @param inviteEvaluation 评标记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteEvaluation")
	public RetDataBean insertInviteEvaluation(InviteEvaluation inviteEvaluation) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteEvaluation.setRecordId(SysTools.getGUID());
			inviteEvaluation.setAccountId(userInfo.getAccountId());
			inviteEvaluation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteEvaluation.setCreateUser(userInfo.getAccountId());
			inviteEvaluation.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteEvaluationService.insertInviteEvaluation(inviteEvaluation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除评标记录
	 * @param inviteEvaluation 评标记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteEvaluation")
	public RetDataBean deleteInviteEvaluation(InviteEvaluation inviteEvaluation) {
		try {
			if (StringUtils.isBlank(inviteEvaluation.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteEvaluation.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteEvaluationService.deleteInviteEvaluation(inviteEvaluation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除评标记录
	 * @param ids 评标记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteEvaluationByIds")
	public RetDataBean deleteInviteEvaluationByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteEvaluationService.deleteInviteEvaluationByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新评标记录
	 * @param inviteEvaluation 评标记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteEvaluation")
	public RetDataBean updateInviteEvaluation(InviteEvaluation inviteEvaluation) {
		try {
			if (StringUtils.isBlank(inviteEvaluation.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteOpen.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", inviteEvaluation.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteEvaluationService.updateInviteEvaluation(example,inviteEvaluation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 录入开标记录
	 * @param inviteOpen 开标记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteOpen")
	public RetDataBean insertInviteOpen(InviteOpen inviteOpen) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteOpen.setRecordId(SysTools.getGUID());
			inviteOpen.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteOpen.setCreateUser(userInfo.getAccountId());
			inviteOpen.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteOpenService.insertInviteOpen(inviteOpen));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除开标记录
	 * @param inviteOpen 开标记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteOpen")
	public RetDataBean deleteInviteOpen(InviteOpen inviteOpen) {
		try {
			if (StringUtils.isBlank(inviteOpen.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteOpen.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteOpenService.deleteInviteOpen(inviteOpen));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除开标记录
	 * @param ids 开标记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteOpenByIds")
	public RetDataBean deleteInviteOpenByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteOpenService.deleteInviteOpenByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新开标记录
	 * @param inviteOpen 开标记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteOpen")
	public RetDataBean updateInviteOpen(InviteOpen inviteOpen) {
		try {
			if (StringUtils.isBlank(inviteOpen.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteOpen.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", inviteOpen.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteOpenService.updateInviteOpen(example,inviteOpen));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 录入投标文件记录
	 * @param inviteBidding 投标文件记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteBidding")
	public RetDataBean insertInviteBidding(InviteBidding inviteBidding) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteBidding.setRecordId(SysTools.getGUID());
			inviteBidding.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteBidding.setCreateUser(userInfo.getAccountId());
			inviteBidding.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteBiddingService.insertInviteBidding(inviteBidding));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除投标文件记录
	 * @param inviteBidding 投标文件记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteBidding")
	public RetDataBean deleteInviteBidding(InviteBidding inviteBidding) {
		try {
			if (StringUtils.isBlank(inviteBidding.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteBidding.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteBiddingService.deleteInviteBidding(inviteBidding));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除投标文件记录
	 * @param ids 投标文件记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteBiddingByIds")
	public RetDataBean deleteInviteBiddingByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteBiddingService.deleteInviteBiddingByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新投标文件记录
	 * @param inviteBidding 投标文件记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteBidding")
	public RetDataBean updateInviteBidding(InviteBidding inviteBidding) {
		try {
			if (StringUtils.isBlank(inviteBidding.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteBidding.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", inviteBidding.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteBiddingService.updateInviteBidding(example,inviteBidding));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 录入招标文件出售记录
	 * @param inviteFileSell 标文件出售记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteFileSell")
	public RetDataBean insertInviteFileSell(InviteFileSell inviteFileSell) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteFileSell.setRecordId(SysTools.getGUID());
			inviteFileSell.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteFileSell.setCreateUser(userInfo.getAccountId());
			inviteFileSell.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteFileSellService.insertInviteFileSell(inviteFileSell));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招标文件出售记录
	 * @param inviteFileSell 标文件出售记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteFileSell")
	public RetDataBean deleteInviteFileSell(InviteFileSell inviteFileSell) {
		try {
			if (StringUtils.isBlank(inviteFileSell.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteFileSell.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteFileSellService.deleteInviteFileSell(inviteFileSell));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除标书收售记录
	 * @param ids 标文件出售记录对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteFileSellByIds")
	public RetDataBean deleteInviteFileSellByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteFileSellService.deleteInviteFileSellByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新招标出售记录
	 * @param inviteFileSell 标文件出售记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteFileSell")
	public RetDataBean updateInviteFileSell(InviteFileSell inviteFileSell) {
		try {
			if (StringUtils.isBlank(inviteFileSell.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteFileSell.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", inviteFileSell.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteFileSellService.updateInviteFileSell(example,inviteFileSell));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}



	/**
	 * 录入招标保证金
	 * @param inviteDeposit 招标保证金对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteDeposit")
	public RetDataBean insertInviteDeposit(InviteDeposit inviteDeposit) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteDeposit.setRecordId(SysTools.getGUID());
			inviteDeposit.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteDeposit.setCreateUser(userInfo.getAccountId());
			inviteDeposit.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteDepositService.insertInviteDeposit(inviteDeposit));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招标保证金
	 * @param inviteDeposit 招标保证金对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteDeposit")
	public RetDataBean deleteInviteDeposit(InviteDeposit inviteDeposit) {
		try {
			if (StringUtils.isBlank(inviteDeposit.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteDeposit.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteDepositService.deleteInviteDeposit(inviteDeposit));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除招标保证金
	 * @param ids 招标保证金对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteDepositByIds")
	public RetDataBean deleteInviteDepositByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteDepositService.deleteInviteDepositByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新招标保证金
	 * @param inviteDeposit 招标保证金对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteDeposit")
	public RetDataBean updateInviteDeposit(InviteDeposit inviteDeposit) {
		try {
			if (StringUtils.isBlank(inviteDeposit.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteDeposit.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", inviteDeposit.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteDepositService.updateInviteDeposit(example,inviteDeposit));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建招标文件
	 * @param inviteFile 招标文件对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteFile")
	public RetDataBean insertInviteFile(InviteFile inviteFile) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteFile.setFileId(SysTools.getGUID());
			inviteFile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteFile.setCreateUser(userInfo.getAccountId());
			inviteFile.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteFileService.insertInviteFile(inviteFile));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招标文件
	 * @param inviteFile 招标文件对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteFile")
	public RetDataBean deleteInviteFile(InviteFile inviteFile) {
		try {
			if (StringUtils.isBlank(inviteFile.getFileId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteFile.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteFileService.deleteInviteFile(inviteFile));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除招标文件
	 * @param ids 招标文件对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteFileByIds")
	public RetDataBean deleteInviteFileByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteFileService.deleteInviteFileByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新招标文件
	 * @param inviteFile 招标文件对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteFile")
	public RetDataBean updateInviteFile(InviteFile inviteFile) {
		try {
			if (StringUtils.isBlank(inviteFile.getFileId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteFile.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("fileId", inviteFile.getFileId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteFileService.updateInviteFile(example,inviteFile));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}



	/**
	 * 供应商打分
	 * @param inviteScore 供应商打分记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteScore")
	public RetDataBean insertInviteScore(InviteScore inviteScore) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteScore.setRecordId(SysTools.getGUID());
			inviteScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteScore.setAccountId(userInfo.getAccountId());
			inviteScore.setCreateUser(userInfo.getAccountId());
			inviteScore.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteScoreService.insertInviteScore(inviteScore));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 供应商评分
	 * @param inviteScore 供应商打分记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setInviteScore")
	public RetDataBean setInviteScore(InviteScore inviteScore) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteScore.setRecordId(SysTools.getGUID());
			inviteScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteScore.setAccountId(userInfo.getAccountId());
			inviteScore.setCreateUser(userInfo.getAccountId());
			inviteScore.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteScoreService.setInviteScore(inviteScore));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 删除供应商打分记录
	 * @param inviteScore 供应商打分记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteScore")
	public RetDataBean deleteInviteScore(InviteScore inviteScore) {
		try {
			if (StringUtils.isBlank(inviteScore.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteScore.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteScoreService.deleteInviteScore(inviteScore));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除供应商打分记录
	 * @param ids 供应商打分记录对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteScoreByIds")
	public RetDataBean deleteInviteScoreByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteScoreService.deleteInviteScoreByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新供应商打分记录
	 * @param inviteScore 供应商打分记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteScore")
	public RetDataBean updateInviteScore(InviteScore inviteScore) {
		try {
			if (StringUtils.isBlank(inviteScore.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteScore.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", inviteScore.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteScoreService.updateInviteScore(example,inviteScore));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 录入供应商证照
	 * @param inviteEntLic 供应商证照对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteEntLic")
	public RetDataBean insertInviteEntLic(InviteEntLic inviteEntLic) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteEntLic.setLicId(SysTools.getGUID());
			inviteEntLic.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteEntLic.setCreateUser(userInfo.getAccountId());
			inviteEntLic.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteEntLicService.insertInviteEntLic(inviteEntLic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除供应商证照
	 * @param inviteEntLic 供应商证照对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteEntLic")
	public RetDataBean deleteInviteEntLic(InviteEntLic inviteEntLic) {
		try {
			if (StringUtils.isBlank(inviteEntLic.getLicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteEntLic.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteEntLicService.deleteInviteEntLic(inviteEntLic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除供应商证照
	 * @param ids 供应商证照对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteEntLicByIds")
	public RetDataBean deleteInviteEntLicByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteEntLicService.deleteInviteEntLicByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新供应商证照
	 * @param inviteEntLic 供应商证照对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteEntLic")
	public RetDataBean updateInviteEntLic(InviteEntLic inviteEntLic) {
		try {
			if (StringUtils.isBlank(inviteEntLic.getEntId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteEntLic.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("licId", inviteEntLic.getLicId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteEntLicService.updateInviteEntLic(example,inviteEntLic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 发布招标信息
	 * @param inviteRecord 招标信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteRecord")
	public RetDataBean insertInviteRecord(InviteRecord inviteRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteRecord.setRecordId(SysTools.getGUID());
			inviteRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteRecord.setCreateUser(userInfo.getAccountId());
			inviteRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteRecordService.insertInviteRecord(inviteRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招标信息
	 * @param inviteRecord 招标信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteRecord")
	public RetDataBean deleteInviteRecord(InviteRecord inviteRecord) {
		try {
			if (StringUtils.isBlank(inviteRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteRecordService.deleteInviteRecord(inviteRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除招标信息
	 * @param ids 招标信息对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteRecordByIds")
	public RetDataBean deleteInviteRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteRecordService.deleteInviteRecordByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新招标信息
	 * @param inviteRecord 招标信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteRecord")
	public RetDataBean updateInviteRecord(InviteRecord inviteRecord) {
		try {
			if (StringUtils.isBlank(inviteRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", inviteRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteRecordService.updateInviteRecord(example,inviteRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加供应商
	 * @param inviteEnt 供应商对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteEnt")
	public RetDataBean insertInviteEnt(InviteEnt inviteEnt) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteEnt.setEntId(SysTools.getGUID());
			inviteEnt.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteEnt.setCreateUser(userInfo.getAccountId());
			inviteEnt.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteEntService.insertInviteEnt(inviteEnt));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除供应商
	 * @param inviteEnt 供应商对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteEnt")
	public RetDataBean deleteInviteEnt(InviteEnt inviteEnt) {
		try {
			if (StringUtils.isBlank(inviteEnt.getEntId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteEnt.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteEntService.deleteInviteEnt(inviteEnt));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除供应商
	 * @param ids 供应商对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteEntByIds")
	public RetDataBean deleteInviteEntByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteEntService.deleteInviteEntByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新供应商
	 * @param inviteEnt 供应商对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteEnt")
	public RetDataBean updateInviteEnt(InviteEnt inviteEnt) {
		try {
			if (StringUtils.isBlank(inviteEnt.getEntId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteEnt.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("entId", inviteEnt.getEntId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteEntService.updateInviteEnt(example,inviteEnt));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 添加评标小组
	 * @param inviteTeam 评标小组对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteTeam")
	public RetDataBean insertInviteTeam(InviteTeam inviteTeam) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteTeam.setTeamId(SysTools.getGUID());
			inviteTeam.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteTeam.setCreateUser(userInfo.getAccountId());
			inviteTeam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteTeamService.insertInviteTeam(inviteTeam));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除评标小组
	 * @param inviteTeam 评标小组对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteTeam")
	public RetDataBean deleteInviteTeam(InviteTeam inviteTeam) {
		try {
			if (StringUtils.isBlank(inviteTeam.getTeamId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteTeam.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteTeamService.deleteInviteTeam(inviteTeam));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除评标小组
	 * @param ids 评标小组对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteTeamByIds")
	public RetDataBean deleteInviteTeamByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteTeamService.deleteInviteTeamByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新评标小组
	 * @param inviteTeam 评标小组对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteTeam")
	public RetDataBean updateInviteTeam(InviteTeam inviteTeam) {
		try {
			if (StringUtils.isBlank(inviteTeam.getTeamId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(InviteTeam.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("teamId", inviteTeam.getTeamId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, inviteTeamService.updateInviteTeam(inviteTeam,example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建字典
	 *
	 * @param inviteDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertInviteDic")
	public RetDataBean insertInviteDic(InviteDic inviteDic) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(inviteDic.getParentId())) {
				inviteDic.setParentId("0");
			}
			inviteDic.setDicId(SysTools.getGUID());
			inviteDic.setOrgId(userInfo.getOrgId());
			inviteDic.setStatus("1");
			inviteDic.setCreateUser(userInfo.getCreateUser());
			inviteDic.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteDicService.insertInviteDic(inviteDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新字典
	 *
	 * @param inviteDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateInviteDic")
	public RetDataBean updateInviteDic(InviteDic inviteDic) {
		try {
			if (StringUtils.isBlank(inviteDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SysDic.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("dicId", inviteDic.getDicId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteDicService.updateInviteDic(example,inviteDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除字典
	 *
	 * @param inviteDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteDic")
	public RetDataBean deleteInviteDic(InviteDic inviteDic) {
		try {
			if (StringUtils.isBlank(inviteDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (inviteDicService.isExistChild(userInfo.getOrgId(), inviteDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
			inviteDic.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, inviteDicService.deleteInviteDic(inviteDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除字典
	 * @param ids 删除字典Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteInviteDicByIds")
	public RetDataBean deleteInviteDicByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return inviteDicService.deleteInviteDicByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 字典排序
	 *
	 * @param oldDicId 源字典记录Id
	 * @param newDicId 新字典记录Id
	 * @param oldSortNo 原字典记录排序号
	 * @param newSortNo 新字内记录排序号
	 * @return 消息结构
	 */
	@PostMapping(value = "/dropSysDic")
	public RetDataBean dropSysDic(String oldDicId, String newDicId, Integer oldSortNo, Integer newSortNo) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return inviteDicService.dropInviteDic(userInfo.getOrgId(), oldDicId, newDicId, oldSortNo, newSortNo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置招标权限
	 * @param inviteRole 招标权限对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setInviteRole")
	public RetDataBean setInviteRole(InviteRole inviteRole) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			inviteRole.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			inviteRole.setCreateUser(userInfo.getAccountId());
			inviteRole.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, inviteRoleService.setInviteRole(inviteRole));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
}
