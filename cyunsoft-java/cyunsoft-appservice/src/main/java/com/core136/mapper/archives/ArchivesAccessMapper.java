package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesAccess;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesAccessMapper extends MyMapper<ArchivesAccess> {
	/**
	 * 获取档案仓库进出记录
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param isOut 人员类型
	 * @param accessType 进出类型
	 * @param keyword 查询关键词
	 * @return 进出记录列表
	 */
	List<Map<String,String>> getArchivesAccessList(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
												   @Param(value = "accountId")String accountId,@Param(value = "dateQueryType")String dateQueryType,
												   @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
												   @Param(value="isOut")String isOut,@Param(value="accessType")String accessType,
												   @Param(value = "keyword")String keyword);
}
