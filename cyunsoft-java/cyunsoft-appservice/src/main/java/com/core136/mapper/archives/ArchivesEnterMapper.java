package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesEnter;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 登记记录接口
 * @author lsq
 */
public interface ArchivesEnterMapper extends MyMapper<ArchivesEnter> {
	/**
	 * 获取档案登记记录列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status 状态
	 * @param archivesSource 档案来源
	 * @param archivesEnterType 登记类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 登记记录列表
	 */
	List<Map<String,String>> getArchivesEnterList(@Param(value = "orgId")String orgId,@Param(value="opFlag")String opFlag,
												  @Param(value="accountId")String accountId,@Param(value="status")String status,
												  @Param(value="archivesSource")String archivesSource,@Param(value="archivesEnterType")String archivesEnterType,
												  @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime")String beginTime,
												  @Param(value="endTime")String endTime, @Param(value="keyword")String keyword);
}
