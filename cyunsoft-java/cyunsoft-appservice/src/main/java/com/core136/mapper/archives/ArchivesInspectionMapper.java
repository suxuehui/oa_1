package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesInspection;
import com.core136.common.dbutils.MyMapper;

public interface ArchivesInspectionMapper extends MyMapper<ArchivesInspection> {
}
