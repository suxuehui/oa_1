package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesInventory;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 档案盘点接口
 * @author lsq
 */
public interface ArchivesInventoryMapper extends MyMapper<ArchivesInventory> {
	/**
	 * 获取档案盘点记录
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param inventoryType 盘点类型
	 * @param keyword 查询关键词
	 * @return 盘点记录列表
	 */
	List<Map<String,String>> getArchivesInventoryList(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
													  @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
													  @Param(value="inventoryType")String inventoryType,@Param(value="keyword")String keyword);
}
