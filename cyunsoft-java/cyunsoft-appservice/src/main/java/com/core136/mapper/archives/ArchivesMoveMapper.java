package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesMove;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesMoveMapper extends MyMapper<ArchivesMove> {
    /**
     * 获取档案移库记录列表
     * @param orgId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param moveType
     * @param keyword
     * @return
     */
    List<Map<String,String>> getArchivesMoveList(@Param(value="orgId")String orgId, @Param(value="dateQueryType")String dateQueryType,
                                                 @Param(value="beginTime")String beginTime, @Param(value = "endTime")String endTime,
                                                 @Param(value="moveType")String moveType, @Param(value="keyword")String keyword);
}
