package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesOutbound;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesOutboundMapper extends MyMapper<ArchivesOutbound> {

    /**
     * 获取档案出库记录
     * @param orgId
     * @param dateQueryTime
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param outboundType
     * @param keyword
     * @return
     */
    List<Map<String,String>> getArchivesOutboundList(@Param(value="orgId")String orgId, @Param(value="dateQueryType")String dateQueryTime,
                                                     @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime,
                                                     @Param(value="outboundType")String outboundType, @Param(value="keyword")String keyword);

}
