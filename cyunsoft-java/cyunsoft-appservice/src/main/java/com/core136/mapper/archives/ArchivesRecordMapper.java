package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ArchivesRecordMapper extends MyMapper<ArchivesRecord> {

	/**
	 * 获取待归档档案列表
	 * @param orgId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param isKind
	 * @param classifiedLevel
	 * @param keyword
	 * @return
	 */
    List<Map<String,String>>getArchivesRecordListForFile(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
                                                          @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime,@Param(value="status")String status,
                                                          @Param(value = "isKind")String isKind,@Param(value="classifiedLevel")String classifiedLevel,
                                                          @Param(value="keyword")String keyword);

	/**
	 * 获取待上架档案列表
	 * @param orgId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param frameStatus
	 * @param classifiedLevel
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>>getArchivesRecordListForFrame(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
														 @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime,@Param(value="frameStatus")String frameStatus,
														 @Param(value="classifiedLevel")String classifiedLevel,@Param(value="keyword")String keyword);

    /**
     * 获取档案下拉列表
     * @param orgId
     * @param keyword
     * @return
     */
    List<Map<String,String>> getArchivesRecordListForSelect(@Param(value="orgId")String orgId, @Param(value="keyword")String keyword);

	/**
	 * 按分类获取档案列表
	 * @param orgId
	 * @param dateQueryType  日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param classifiedLevel
	 * @param sortId
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>>getArchivesRecordListBySort(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
														  @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime,
														  @Param(value="classifiedLevel")String classifiedLevel,@Param(value="sortId")String sortId,
														@Param(value="keyword")String keyword);

}
