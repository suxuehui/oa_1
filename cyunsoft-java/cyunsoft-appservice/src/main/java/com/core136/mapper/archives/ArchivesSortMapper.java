package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArchivesSortMapper extends MyMapper<ArchivesSort> {
    public List<ArchivesSort> getAllArchivesSort(@Param(value="orgId")String orgId);

    /**
     * 获取档案分类树形结构
     * @param orgId
     * @return
     */
    public List<ArchivesSort> getAllArchivesSortForSelect(@Param(value="orgId")String orgId);

}
