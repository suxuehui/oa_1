package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesTransfer;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface ArchivesTransferMapper extends MyMapper<ArchivesTransfer> {
	/**
	 * 获取档案转移列表
	* @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param transferType
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getArchivesTransferList(@Param(value="orgId")String orgId,@Param(value="dateQueryType")String dateQueryType,
													 @Param(value = "beginTime")String beginTime,@Param(value = "endTime")String endTime,
													 @Param(value="transferType")String transferType,@Param(value = "keyword")String keyword);
}
