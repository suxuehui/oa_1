package com.core136.mapper.archives;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EchartsArchivesMapper {
	/**
	 * 按分类获取饼状图
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>>getArchivesSortForPie(@Param(value = "orgId")String orgId);

	/**
	 * 按涉密等级饼状图
	 * @param orgId
	 * @return
	 */
	List<Map<String,String>>getArchivesSmForPie(@Param(value="orgId")String orgId);

	/**
	 * 档案架档案数量对比
	 * @param orgId
	 * @return
	 */
	List<Map<String,String>>getArchivesFrameForBar(@Param(value="orgId")String orgId);

	/**
	 * 获取一段时间内按月份统计档案数据上架走势
	* @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	List<Map<String,String>>getArchivesByMonthLine(@Param(value = "orgId")String orgId,@Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime);

}
