package com.core136.mapper.crm;

import com.core136.bean.crm.CrmCare;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CrmCareMapper extends MyMapper<CrmCare> {

	/**
	 * 获取客户关怀记录列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param careType
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getCrmCareList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,@Param(value="accountId")String accountId,
											 @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
											 @Param(value = "status") String status, @Param(value = "careType") String careType,@Param(value = "keyword") String keyword);

	/**
	 * 获取客户关怀审批列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param careType
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getApprovedCrmCareList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,@Param(value="accountId")String accountId,
											 @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
											 @Param(value = "status") String status, @Param(value = "careType") String careType,@Param(value = "keyword") String keyword);


}
