package com.core136.mapper.crm;

import com.core136.bean.crm.CrmContractRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CrmContractRecordMapper extends MyMapper<CrmContractRecord> {
	/**
	 *  获取客户所有联系记录
	 * @param orgId 机构码
	 * @param customerId
	 * @return
	 */
    List<Map<String, String>> getRecordListByCustomerId(@Param(value = "orgId") String orgId, @Param(value = "customerId") String customerId);

	/**
	 * 获取业务员所有联系记录
	 * @param orgId 机构码
	 * @param linkType
	 * @param recordType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getContractRecordList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,@Param(value="accountId")String accountId,
													@Param(value = "linkType") String linkType,@Param(value="recordType")String recordType,
													@Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime,@Param(value = "endTime") String endTime,
													@Param(value="keyword")String keyword);

}
