package com.core136.mapper.crm;

import com.core136.bean.crm.CrmDic;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CrmDicMapper extends MyMapper<CrmDic> {
	List<Map<String,String>>getCrmDicParentList(@Param(value = "orgId") String orgId);

	List<Map<String,String>>getCrmDicList(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId, @Param(value = "keyword") String keyword);
	/**
	 * 按标识获取分类码列表
	 * @param orgId 机构码
	 * @param code
	 * @return
	 */
	List<Map<String, Object>> getCrmDicByCode(@Param(value = "orgId") String orgId, @Param(value = "code") String code);
}
