package com.core136.mapper.crm;

import com.core136.bean.crm.CrmInquiry;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
public interface CrmInquiryMapper extends MyMapper<CrmInquiry> {
	/**
	 * 获限权限内的询价单列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param customerNature
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getCrmInquiryList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value="dateQueryType")String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "customerNature") String customerNature,
            @Param(value = "status") String status,
            @Param(value = "keyword") String keyword);

	/**
	 * 获取询价单列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getCrmInquiryListForSelect(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value="keyword")String keyword);

	/**
	 * 获取询价单明细
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param inquiryId
	 * @return
	 */
    Map<String,String>getCrmInquiryFormJsonById(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId,@Param(value="inquiryId")String inquiryId);

}
