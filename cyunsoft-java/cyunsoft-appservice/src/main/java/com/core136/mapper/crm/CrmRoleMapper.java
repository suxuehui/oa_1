package com.core136.mapper.crm;

import com.core136.bean.crm.CrmRole;
import com.core136.common.dbutils.MyMapper;

public interface CrmRoleMapper extends MyMapper<CrmRole> {

}
