package com.core136.mapper.crm;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EchartsCrmMapper {
	/**
	 * 获取CRM的行业占比
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiCustomerIndustryPie(@Param(value = "orgId") String orgId);

	/**
	 * 获取CRM销售人员的占比
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiCustomerKeepUserPie(@Param(value = "orgId") String orgId);

	/**
	 * 获取CRM客户等级占比
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiCustomerLevelPie(@Param(value = "orgId") String orgId);


	List<Map<String, String>> getBiCustomerSourcePie(@Param(value = "orgId") String orgId);

	/**
	 * 获取询价单人员占比
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiInquiryByAccountPie(@Param(value = "orgId") String orgId);

	/**
	 * 按月份统计工作量
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	List<Map<String, Object>> getBiInquiryByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 部门询价单占比前10的占比
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiInquiryByDeptPie(@Param(value = "orgId") String orgId);


	/**
	 * 获取报价单人员占比
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiQuotationByAccountPie(@Param(value = "orgId") String orgId);

	/**
	 * 按月份统计报价单
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	List<Map<String, Object>> getBiQuotationByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 获取支付类型分类
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiInquiryByPayTypePie(@Param(value = "orgId") String orgId);

	/**
	 * 部门报价单占比前10的占比
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getBiQuotationByDeptPie(@Param(value = "orgId") String orgId);
}
