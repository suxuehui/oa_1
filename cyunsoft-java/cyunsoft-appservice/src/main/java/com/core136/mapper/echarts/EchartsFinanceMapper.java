package com.core136.mapper.echarts;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EchartsFinanceMapper {

	/**
	 * 获取合同应付款列表
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结构时间
	 * @return 应付款列表
	 */
	List<Map<String, Double>> getPayableListData(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 获取合同应收款
	* @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	List<Map<String, Double>> getReceviablesListData(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 获取应收应付总数
	 * @param orgId
	 * @return
	 */
	Map<String, String> getPayReceivTotalData(@Param(value = "orgId") String orgId);
}
