package com.core136.mapper.finance;

import com.core136.bean.finance.BudgetAdjustment;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BudgetAdjustmentMapper extends MyMapper<BudgetAdjustment> {

	/**
	 * 获取申请列表
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status
	 * @param projectId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getAdjustmentApplyList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                      @Param(value = "accountId") String accountId, @Param(value = "status") String status,
                                                      @Param(value = "projectId") String projectId,@Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                      @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

	/**
	 * 获取等审批列表
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param projectId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getAdjustmentApprovalList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,@Param(value = "accountId") String accountId,@Param(value = "status") String status,
                                                        @Param(value = "projectId") String projectId,@Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                        @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


}
