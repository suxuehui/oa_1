package com.core136.mapper.finance;

import com.core136.bean.finance.BudgetConfig;
import com.core136.common.dbutils.MyMapper;

public interface BudgetConfigMapper extends MyMapper<BudgetConfig> {

}
