package com.core136.mapper.finance;

import com.core136.bean.finance.BudgetProject;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BudgetProjectMapper extends MyMapper<BudgetProject> {
	/**
	 *
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getAllParentBudgetProject(@Param(value = "orgId") String orgId);

	/**
	 *
	* @param orgId 机构码
	 * @param keyword
	 * @return
	 */
	List<BudgetProject> getAllBudgetProjectList(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);
	/**
	 * 获取项目列表
	* @param orgId 机构码
	 * @param projectType
	 * @param budgetAccount
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getBudgetProjectList(@Param(value = "orgId") String orgId, @Param(value = "projectType") String projectType,
												   @Param(value = "budgetAccount") String budgetAccount, @Param(value = "dateQueryType") String dateQueryType,
												   @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,@Param(value = "keyword") String keyword
	);



}
