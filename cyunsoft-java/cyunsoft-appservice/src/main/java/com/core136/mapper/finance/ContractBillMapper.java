package com.core136.mapper.finance;

import com.core136.bean.finance.ContractBill;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContractBillMapper extends MyMapper<ContractBill> {

	/**
	 * 获取发票列表
	* @param orgId 机构码
	 * @param isOpen
	 * @param status
	 * @param billType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getContractBillList(
            @Param(value = "orgId") String orgId,
            @Param(value = "isOpen") String isOpen,
            @Param(value = "status") String status,
            @Param(value = "billType") String billType,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );

    /**
     *  获取近期发票列表
     * @param orgId
     * @return
     */
    List<Map<String, String>> getContractBillTop(@Param(value = "orgId") String orgId);
}
