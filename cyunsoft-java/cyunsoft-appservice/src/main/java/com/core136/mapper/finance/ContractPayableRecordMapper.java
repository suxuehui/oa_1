package com.core136.mapper.finance;

import com.core136.bean.finance.ContractPayableRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContractPayableRecordMapper extends MyMapper<ContractPayableRecord> {
	/**
	 * 获取付款记录列表
	 * @param orgId 机构码
	 * @param payableId 应付款记录Id
	 * @return 付款记录列表
	 */
    List<Map<String, String>> getContractPayableRecordList(@Param(value = "orgId") String orgId, @Param(value = "payableId") String payableId);

	/**
	 *获取最近前10条付款记录
	 * @param orgId 机构码
	 * @return 付款记录列表
	 */
    List<Map<String, String>> getPayableRecordTop(@Param(value = "orgId") String orgId);

	/**
	 * 查询历史付款记录列表
	 * @param orgId 机构码
	 * @param payReceivedType 付款种类
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 付款记录列表
	 */
	List<Map<String,String>>getAllContractPayableRecordList(@Param(value = "orgId") String orgId, @Param(value = "payReceivedType") String payReceivedType,
															@Param(value="dateQueryType")String dateQueryType,@Param(value="beginTime")String beginTime,
															@Param(value="endTime")String endTime,@Param(value="keyword")String keyword);
}
