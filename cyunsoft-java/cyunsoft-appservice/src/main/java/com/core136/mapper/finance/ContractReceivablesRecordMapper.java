package com.core136.mapper.finance;

import com.core136.bean.finance.ContractReceivablesRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContractReceivablesRecordMapper extends MyMapper<ContractReceivablesRecord> {

    /**
     * 获取收款记录
     * @param orgId 机构码
     * @param receivablesId 应收款记录Id
     * @return
     */
    List<Map<String, String>> getContractReceivablesRecordList(@Param(value = "orgId") String orgId, @Param(value = "receivablesId") String receivablesId);

    /**
     * 收款记录
     * @param orgId 机构码
     * @return 收款记录列表
     */
    List<Map<String, String>> getReceivablesRecordTop(@Param(value = "orgId") String orgId);


    /**
     * 查询历史收款记录列表
     * @param orgId 机构码
     * @param payReceivedType 付款种类
     * @param dateQueryType 日期范围
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 收款记录列表
     */
    List<Map<String,String>>getAllContractReceivablesRecordList(@Param(value = "orgId") String orgId, @Param(value = "payReceivedType") String payReceivedType,
                                                            @Param(value="dateQueryType")String dateQueryType,@Param(value="beginTime")String beginTime,
                                                            @Param(value="endTime")String endTime,@Param(value="keyword")String keyword);
}
