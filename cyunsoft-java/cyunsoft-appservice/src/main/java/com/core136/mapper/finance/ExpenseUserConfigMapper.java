package com.core136.mapper.finance;

import com.core136.bean.finance.ExpenseUserConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExpenseUserConfigMapper extends MyMapper<ExpenseUserConfig> {
	List<Map<String,String>> getExpenseUserConfigList(@Param(value="orgId")String orgId, @Param(value="deptId")String deptId,
													  @Param(value="accountId")String accountId, @Param(value="expenseYear")String expenseYear);
}
