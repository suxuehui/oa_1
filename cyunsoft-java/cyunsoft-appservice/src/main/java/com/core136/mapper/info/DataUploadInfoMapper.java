package com.core136.mapper.info;

import com.core136.bean.info.DataUploadInfo;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DataUploadInfoMapper extends MyMapper<DataUploadInfo> {

	/**
	 * 获取信息上报详情
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId
	 * @param recordId
	 * @return
	 */
	Map<String, String>getDataUploadInfoById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
									 @Param(value="accountId")String accountId,@Param(value="deptId")String deptId,@Param(value="recordId")String recordId);
    /**
     * 获取上报信息列表
     * @param orgId
     * @param deptId
     * @param fromAccountId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param dateType
     * @param approvedType
     * @param keyword
     * @return
     */
    List<Map<String, String>> getDataUploadInfoList(@Param(value = "orgId") String orgId,
                                                    @Param(value = "deptId") String deptId, @Param(value = "fromAccountId") String fromAccountId,
                                                    @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                    @Param(value = "dataType") String dateType, @Param(value = "approvedType") String approvedType,
                                                    @Param(value = "keyword") String keyword
    );
    List<Map<String, String>> getMyDataInfoForDesk(@Param(value = "orgId") String orgId,@Param(value = "accountId") String accountId);

    /**
     * 获取持处理的信息列表
     * @param orgId
     * @param accountId
     * @param deptId
     * @param fromAccountId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param dateType
     * @param approvedType
     * @param keyword
     * @return
     */
    List<Map<String, String>> getToProcessInfoList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                   @Param(value = "deptId") String deptId, @Param(value = "fromAccountId") String fromAccountId,@Param(value = "status")String status,
                                                   @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                   @Param(value = "dataType") String dateType, @Param(value = "approvedType") String approvedType,
                                                   @Param(value = "keyword") String keyword
    );
}
