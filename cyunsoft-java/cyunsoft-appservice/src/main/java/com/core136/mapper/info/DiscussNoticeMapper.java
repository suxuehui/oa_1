package com.core136.mapper.info;

import com.core136.bean.info.DiscussNotice;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DiscussNoticeMapper extends MyMapper<DiscussNotice> {
	/**
	 * 获取讨论区通知列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 通知列表
	 */
    List<Map<String, String>> getDiscussNoticeList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 获取讨论区通知管理列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 通知列表
	 */
	List<Map<String, String>> getDiscussNoticeForManage(@Param(value = "orgId") String orgId, @Param(value="dateQueryType")String dateQueryType,
													   @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
													   @Param(value = "status")String status,@Param(value="keyword")String keyword);

}
