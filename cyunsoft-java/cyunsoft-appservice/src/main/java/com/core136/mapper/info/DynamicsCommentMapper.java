package com.core136.mapper.info;

import com.core136.bean.info.DynamicsComment;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DynamicsCommentMapper extends MyMapper<DynamicsComment> {
	/**
	 * 获取动态评论列表
	* @param orgId 机构码
	 * @param parentId
	 * @return
	 */
	List<Map<String,String>> getDynamicsCommentList(@Param(value="orgId")String orgId, @Param(value="parentId")String parentId);
}
