package com.core136.mapper.info;

import com.core136.bean.info.LeadActivity;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LeadActivityMapper extends MyMapper<LeadActivity> {
	/**
	 * 获取领导行程详情
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param recordId
	 * @return
	 */
	Map<String, String>getLeadActivityById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
									 @Param(value="accountId")String accountId,@Param(value="deptId")String deptId,
									 @Param(value="userLevel")String userLevel,@Param(value="recordId")String recordId);
	/**
	 * 获取领导行程列表
	* @param orgId 机构码
	 * @param ledActType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param leader
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getLeadActivityList(
            @Param(value = "orgId") String orgId,@Param(value="ledActType") String ledActType, @Param(value = "dateQueryType") String dateQueryType,
			@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,@Param(value = "leader") String leader,@Param(value="status") String status, @Param(value = "keyword") String keyword);

	/**
	 * 查询程领导日程
	* @param orgId 机构码
	 * @param ledActType
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param leader
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getLeadActivityQueryList(
            @Param(value = "orgId") String orgId,@Param(value="ledActType") String ledActType, @Param(value = "dateQueryType") String dateQueryType, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "leader") String leader,
            @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel,
            @Param(value = "keyword") String keyword);

	/**
	 * 桌面领导日程
	 * @param orgId 机构码
	 * @param nowTime 当前时间
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别
	 * @return 日程列表
	 */
    List<Map<String, String>> getLeadActivityListForDesk(
            @Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime, @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel);

	/**
	 * App领导日程
	 * @param orgId 机构码
	 * @param nowTime 当前时间
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别
	 * @param page 页码
	 * @return 日程列表
	 */
    List<Map<String, String>> getMyLeadActivityForApp(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime, @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel, @Param(value = "page") Integer page);
}
