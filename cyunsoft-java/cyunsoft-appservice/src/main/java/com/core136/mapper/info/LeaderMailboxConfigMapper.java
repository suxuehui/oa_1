package com.core136.mapper.info;

import com.core136.bean.info.LeaderMailboxConfig;
import com.core136.common.dbutils.MyMapper;

public interface LeaderMailboxConfigMapper extends MyMapper<LeaderMailboxConfig> {

}
