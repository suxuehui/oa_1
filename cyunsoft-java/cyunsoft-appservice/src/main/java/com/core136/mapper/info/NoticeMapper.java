package com.core136.mapper.info;

import com.core136.bean.info.Notice;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface NoticeMapper extends MyMapper<Notice> {
	/**
	 * 获取公告详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param noticeId 通知公告Id
	 * @return 公告详情
	 */
	Map<String, Object>getMyNoticeById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
									 @Param(value="accountId")String accountId,@Param(value="deptId")String deptId,
									 @Param(value="userLevel")String userLevel,@Param(value="noticeId")String noticeId);
	/**
	 * 获取通知公告的维护列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param noticeType 通知公告类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键间
	 * @return 维护列表
	 */
    List<Map<String, Object>> getNoticeManageList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "noticeType") String noticeType,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );
	/**
	 * 获取审批列表
	 * @param orgId 机构码
	 * @param accountId 用户对象
	 * @param noticeType 通知公告类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询查询对象
	 * @return 审批列表
	 */
    List<Map<String, Object>> getNoticeListForApproval(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "noticeType") String noticeType,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );


	/**
	 * 判断是否有效
	* @param orgId 机构码
	 * @param noticeId
	 * @param endTime
	 * @return
	 */
	int isNoSendStatus(@Param(value = "orgId") String orgId,
                       @Param(value = "noticeId") String noticeId,
                       @Param(value = "endTime") String endTime);

	/**
	 * 获取个人的通知公告
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param noticeType 通知公告类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 通知公告列表
	 */
    List<Map<String, String>> getMyNoticeList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "userLevel") String userLevel,
            @Param(value = "noticeType") String noticeType,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "status") String status,
            @Param(value = "keyword") String keyword
    );

	/**
	 * 获取桌面的通知消息
	 * @param orgId 机构码
	 * @param endTime 结束时间
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别Id
	 * @return 通知消息列表
	 */
    List<Map<String, String>> getMyNoticeListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "endTime") String endTime,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "userLevel") String userLevel);

	/**
	 * 获取移动端通知公告
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param page
	 * @return
	 */
    List<Map<String, String>> getMyNoticeListForApp(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "userLevel") String userLevel,
            @Param(value = "page") Integer page,
			@Param(value = "keyword") String keyword);

	/**
	 * 通知公告人员查看状态
	 * @param orgId 机构码
	 * @param noticeId 新闻Id
	 * @param list 用户账号Id 列表
	 * @return 人员查看状态列表
	 */
    List<Map<String, String>> getNoticeReadStatus(@Param(value = "orgId") String orgId, @Param(value = "noticeId") String noticeId, @Param(value = "list") List<String> list);

}
