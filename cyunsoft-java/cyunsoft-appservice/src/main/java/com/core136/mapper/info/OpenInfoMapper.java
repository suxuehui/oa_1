package com.core136.mapper.info;

import com.core136.bean.info.OpenInfo;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OpenInfoMapper extends MyMapper<OpenInfo> {
	/**
	 * 获取政务公开信息列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标记
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param infoType 信息公开类型
	 * @param status 信息状态
	 * @param keyword 查询关键词
	 * @return 信息列表
	 */
	List<Map<String,String>> getOpenInfoListForManage(@Param(value="orgId")String orgId, @Param(value="opFlag")String opFlag,
													  @Param(value="accountId")String accountId, @Param(value = "dateQueryType")String dateQueryType,
													  @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime,
													  @Param(value="infoType")String infoType,@Param(value="status")String status,
													  @Param(value="keyword")String keyword);

	/**
	 * 获取桌面模块政务公开例表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 政务公开信息列表
	 */
	List<Map<String,String>> getOpenInfoListForDesk(@Param(value="orgId")String orgId,@Param(value="accountId")String accountId,@Param("endTime")String endTime);

	/**
	 * 获取所有政务公开信息列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标记
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param infoType 信息公开类型
	 * @param readStatus 查阅状态
	 * @param keyword 查询关键词
	 * @return 信息列表
	 */
	List<Map<String,String>> getMyOpenInfoList(@Param(value="orgId")String orgId, @Param(value="opFlag")String opFlag,
													  @Param(value="accountId")String accountId, @Param(value = "dateQueryType")String dateQueryType,
													  @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime,
													  @Param(value="infoType")String infoType,@Param(value="readStatus")String readStatus,
													  @Param(value="keyword")String keyword);

	/**
	 * 获取政务公开详情
	 * @param orgId 机构码
	 * @param recordId 信息Id
	 * @return 公开信息详情
	 */
	Map<String,String>getOpenInfoById(@Param(value="orgId")String orgId,@Param(value="recordId")String recordId);

}
