package com.core136.mapper.invite;

import com.core136.bean.invite.InviteDic;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteDicMapper extends MyMapper<InviteDic> {
	List<Map<String,String>> getInviteDicParentList(@Param(value = "orgId") String orgId);

	List<Map<String,String>>getInviteDicList(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId, @Param(value = "keyword") String keyword);


	/**
	 * 按标识获取分类码列表
	* @param orgId 机构码
	 * @param code
	 * @return
	 */
	List<Map<String, Object>> getInviteDicByCode(@Param(value = "orgId") String orgId, @Param(value = "code") String code);
}
