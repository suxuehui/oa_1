package com.core136.mapper.invite;

import com.core136.bean.invite.InviteEntLic;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteEntLicMapper extends MyMapper<InviteEntLic> {
	/**
	 * 获取供应商证照列表
	* @param orgId 机构码
	 * @param entId
	 * @param licType
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String,String>> getInviteEntLicList(@Param(value="orgId")String orgId,@Param(value="entId")String entId,@Param(value="licType")String licType,@Param(value="status")String status,@Param(value="keyword")String keyword);

	/**
	 * 按企业获取证照
	* @param orgId 机构码
	 * @param entId
	 * @return
	 */
	List<Map<String,String>> getInviteEntLicListByEntId(@Param(value="orgId")String orgId,@Param(value="entId")String entId);

}
