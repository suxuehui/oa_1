package com.core136.mapper.invite;

import com.core136.bean.invite.InviteEvaluation;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteEvaluationMapper extends MyMapper<InviteEvaluation> {
    /**
     * 获取评标记录列表
     * @param orgId
     * @param inviteId
     * @return
     */
    List<Map<String,String>> getInviteEvaluationByInviteIdList(@Param(value="orgId")String orgId, @Param(value="inviteId") String inviteId);
}
