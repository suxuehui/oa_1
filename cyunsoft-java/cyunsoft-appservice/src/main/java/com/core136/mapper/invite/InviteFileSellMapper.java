package com.core136.mapper.invite;

import com.core136.bean.invite.InviteFileSell;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteFileSellMapper extends MyMapper<InviteFileSell> {
	/**
	 * 获取招标文件出售记录
	* @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param entId
	 * @param keyword
	 * @return
	 */
    List<Map<String,String>> getInviteFileSellList(@Param(value = "orgId")String orgId, @Param(value="dateQueryType") String dateQueryType,
												   @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
												   @Param(value="entId")String entId,@Param(value="keyword")String keyword);
}
