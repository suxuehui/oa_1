package com.core136.mapper.invite;

import com.core136.bean.invite.InviteRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteRecordMapper extends MyMapper<InviteRecord> {
	/**
	 * 获取招标信息列表
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param inviteType
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getInviteRecordList(@Param(value="orgId")String orgId, @Param(value="opFlag")String opFlag,
												 @Param(value="accountId")String accountId,@Param(value="dateQueryType")String dateQueryType,
												 @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
												 @Param(value="status")String status,@Param(value="inviteType")String inviteType,
												 @Param(value="keyword")String keyword);

	/**
	 * 获取招标信息选择列表
	* @param orgId 机构码
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getInviteRecordListForSelect(@Param(value="orgId")String orgId,@Param(value="keyword")String keyword);
}
