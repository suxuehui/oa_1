package com.core136.mapper.invite;

import com.core136.bean.invite.InviteScore;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteScoreMapper extends MyMapper<InviteScore> {
    /**
     * 获取企业评分列表
     * @param orgId
     * @param entId
     * @return
     */
    List<Map<String,String>> getEntScoreListByEntId(@Param(value="orgId")String orgId, @Param(value="entId")String entId);
}
