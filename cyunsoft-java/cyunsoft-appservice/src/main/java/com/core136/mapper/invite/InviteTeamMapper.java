package com.core136.mapper.invite;

import com.core136.bean.invite.InviteTeam;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InviteTeamMapper extends MyMapper<InviteTeam> {

    List<Map<String,String>> getInviteTeamList(@Param(value="orgId")String orgId);

	/**
	 * 获取评标小组列表
	 * @param orgId
	 * @return
	 */
	List<Map<String,String>>getInviteTeamListForSelect(@Param(value="orgId")String orgId);


}
