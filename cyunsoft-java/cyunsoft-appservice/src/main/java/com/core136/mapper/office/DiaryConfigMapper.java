package com.core136.mapper.office;

import com.core136.bean.office.DiaryConfig;
import com.core136.common.dbutils.MyMapper;

public interface DiaryConfigMapper extends MyMapper<DiaryConfig> {

}
