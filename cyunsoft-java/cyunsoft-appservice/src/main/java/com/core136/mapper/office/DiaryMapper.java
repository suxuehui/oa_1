package com.core136.mapper.office;

import com.core136.bean.office.Diary;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 工作日志接口
 * @author lsq
 */
public interface DiaryMapper extends MyMapper<Diary> {

	/**
	 * 获取工作日志详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param diaryId 日志Id
	 * @return 日志详情
	 */
	Map<String,String>getMyDiaryById(@Param(value = "orgId") String orgId,@Param(value = "opFlag") String opFlag,
										@Param(value = "accountId") String accountId,@Param(value = "deptId") String deptId,
										@Param(value = "userLevel") String userLevel,@Param(value = "diaryId") String diaryId);
	/**
	 * 获取当前用户历史工作日志
	* @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param diaryDay
	 * @param keyword
	 * @return
	 */
    List<Map<String, Object>> getMyDiaryList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "diaryDay") String diaryDay,
            @Param(value = "keyword") String keyword
    );


	/**
	 * 获取我下属的工作晶志列表
	* @param orgId 机构码
	 * @param createUser
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getMySubordinatesDiaryList(@Param(value = "orgId") String orgId, @Param(value = "createUser") String createUser, @Param(value = "dateQueryType") String dateQueryType,
                                                         @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


	/**
	 * 获取他人的工作日志
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param endTime
	 * @return
	 */
    List<Map<String, Object>> getOtherDiaryList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "keyword") String keyword,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime
    );

	/**
	 * 获取个人日志总数
	* @param orgId 机构码
	 * @param accountId
	 * @return
	 */
	Integer getMyDiaryCount(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 被他人评论数
	* @param orgId 机构码
	 * @param accountId
	 * @return
	 */
	Integer getDiaryCommentCount(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 获取他人分享的工作日志
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param createUser
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getShareDiaryList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel, @Param(value = "dateQueryType") String dateQueryType,
                                                @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                @Param(value = "createUser") String createUser, @Param(value = "keyword") String keyword);

	/**
	 * 获取共享人员列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @return
	 */
    List<Map<String,String>>getShareDiaryUserList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                  @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel);

	/**
	 * 移动端获取个人工作日志列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param page
	 * @return
	 */
    List<Map<String, String>> getMyDiaryListForApp(@Param(value = "orgId") String orgId,
                                                   @Param(value = "accountId") String accountId,
                                                   @Param(value = "page") Integer page);
}
