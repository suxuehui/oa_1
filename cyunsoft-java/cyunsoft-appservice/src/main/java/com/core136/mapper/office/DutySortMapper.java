package com.core136.mapper.office;

import com.core136.bean.office.DutySort;
import com.core136.common.dbutils.MyMapper;

public interface DutySortMapper extends MyMapper<DutySort> {
}
