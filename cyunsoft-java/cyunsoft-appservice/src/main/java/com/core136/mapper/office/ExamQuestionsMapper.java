package com.core136.mapper.office;

import com.core136.bean.office.ExamQuestions;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExamQuestionsMapper extends MyMapper<ExamQuestions> {

    /**
     * 按分类获取试题分类
     *
     * @param orgId
     * @param sortId
     * @return
     */
    List<Map<String, String>> getExamQuestionsListBySortId(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId,
														   @Param(value = "examType") String examType, @Param(value = "keyword") String keyword);

    /**
     * 按分类获取试题列表
     *
     * @param orgId
     * @param sortId
     * @return
     */
    List<Map<String, String>> getExamQuestListForSelectBySortId(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);
}
