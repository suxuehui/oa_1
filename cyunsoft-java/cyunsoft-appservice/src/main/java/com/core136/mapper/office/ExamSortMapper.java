package com.core136.mapper.office;

import com.core136.bean.office.ExamSort;
import com.core136.common.dbutils.MyMapper;

public interface ExamSortMapper extends MyMapper<ExamSort> {

}
