package com.core136.mapper.office;

import com.core136.bean.office.ExamTest;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExamTestMapper extends MyMapper<ExamTest> {
	/**
	 * 获取考试详情
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param recordId
	 * @return
	 */
	Map<String, String>getMyExamTestById(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
									 @Param(value="accountId")String accountId,@Param(value="deptId")String deptId,
									 @Param(value="userLevel")String userLevel,@Param(value="recordId")String recordId,@Param(value="nowTime")String nowTime);
    /**
     * 获取试卷管理列表
     *
    * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword
     * @return
     */
    List<Map<String, String>> getExamTestManageList(@Param(value = "orgId") String orgId,@Param(value = "dateQueryType")String dateQueryType,
													@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

    /**
     * 获取个人考试表表
     *
     * @param orgId
     * @param accountId
     * @param deptId
     * @param userLevel
     * @param nowTime
     * @return
     */
    List<Map<String, String>> getMyExamTestList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                @Param(value = "deptId") String deptId, @Param(value = "userLevel") String userLevel, @Param(value = "nowTime") String nowTime);

    List<Map<String, String>> getExamTestListForSelect(@Param(value = "orgId") String orgId);

}
