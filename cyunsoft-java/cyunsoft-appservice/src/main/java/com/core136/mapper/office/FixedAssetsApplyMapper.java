package com.core136.mapper.office;

import com.core136.bean.office.FixedAssetsApply;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface FixedAssetsApplyMapper extends MyMapper<FixedAssetsApply> {

    /**
     * 获取申请列表
     * @param orgId
     * @param status
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param sortId
     * @param keyword
     * @return
     */
    List<Map<String, String>> getFixedAssetsApplyList(
            @Param(value = "orgId") String orgId,
            @Param(value = "status") String status,
            @Param(value="dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "sortId") String sortId,
            @Param(value = "keyword") String keyword
    );

    List<Map<String, String>> getFixedAssetsApplyOldList(
            @Param(value = "orgId") String orgId,
            @Param(value = "status") String status,
            @Param(value="dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "sortId") String sortId,
            @Param(value = "keyword") String keyword
    );


}
