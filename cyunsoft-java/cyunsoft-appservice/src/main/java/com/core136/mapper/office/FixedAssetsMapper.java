package com.core136.mapper.office;

import com.core136.bean.office.FixedAssets;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */

public interface FixedAssetsMapper extends MyMapper<FixedAssets> {


	Map<String, String> getFixedAssetsById(@Param(value = "orgId") String orgId,@Param(value = "assetsId") String assetsId);
	/**
	 * 固定资产列表
	* @param orgId 机构码
	 * @param sortId
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getFixedAssetsList(@Param(value = "orgId") String orgId,@Param(value = "sortId") String sortId, @Param(value = "keyword") String keyword);

	/**
	 * 查询固定资产列表
	* @param orgId 机构码
	 * @param sortId
	 * @param ownDept
	 * @param status
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> queryFixedAssetsList(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId, @Param(value = "ownDept") String ownDept,
                                                   @Param(value = "status") String status, @Param(value = "keyword") String keyword);

	/**
	 * 获取可申请的固定资产的列表
	* @param orgId 机构码
	 * @param sortI
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getApplyFixedAssetsList(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortI, @Param(value = "keyword") String keyword);

	/**
	 * 获取调拨列表
	* @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param sortId
	 * @param applyUser
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getAllocationList(@Param(value = "orgId") String orgId,@Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                @Param(value = "endTime") String endTime, @Param(value = "sortId") String sortId, @Param(value = "applyUser") String applyUser, @Param(value = "keyword") String keyword);

}
