package com.core136.mapper.office;

import com.core136.bean.office.FixedAssetsRepair;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FixedAssetsRepairMapper extends MyMapper<FixedAssetsRepair> {
    /**
     * 获取维修列表
    * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param sortId
     * @param status
     * @param keyword
     * @return
     */
    List<Map<String, String>> getFixedAssetsRepairList(
            @Param(value = "orgId") String orgId,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "sortId") String sortId,
            @Param(value = "status") String status,
            @Param(value = "keyword") String keyword
    );
}
