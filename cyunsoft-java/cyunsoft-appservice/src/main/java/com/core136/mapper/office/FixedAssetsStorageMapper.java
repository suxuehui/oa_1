package com.core136.mapper.office;

import com.core136.bean.office.FixedAssetsStorage;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface FixedAssetsStorageMapper extends MyMapper<FixedAssetsStorage> {

	/**
	 * 获取仓库列表
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getFixedAssetsStorageList(@Param(value = "orgId") String orgId);

	/**
	 * 获取仓库列表
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getAllFixedAssetsStorageList(@Param(value = "orgId") String orgId);

}
