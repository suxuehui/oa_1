package com.core136.mapper.office;

import com.core136.bean.office.LicenceBorrowing;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LicenceBorrowingMapper extends MyMapper<LicenceBorrowing> {
    /**
     * 获取证照出借记录
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status
     * @param licType
     * @param keyword
     * @return
     */
    List<Map<String,String>> getLicenceBorrowingList(@Param(value="orgId")String orgId,@Param(value="opFlag")String opFlag,
                                                     @Param(value="accountId")String accountId,@Param(value="dateQueryType")String dateQueryType,
                                                     @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
                                                     @Param(value="status")String status,@Param(value="licType")String licType,@Param(value="keyword")String keyword);

}
