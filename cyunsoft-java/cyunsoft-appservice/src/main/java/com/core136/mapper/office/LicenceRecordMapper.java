package com.core136.mapper.office;

import com.core136.bean.office.LicenceRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LicenceRecordMapper extends MyMapper<LicenceRecord> {
	/**
	 * 获取单位证照列表
	* @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param licType
	 * @param status
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>> getLicenceRecordList(@Param(value="orgId")String orgId, @Param(value="dateQueryType")String dateQueryType,
												  @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,
												  @Param(value="licType")String licType,@Param(value="status")String status,
												  @Param(value="keyword")String keyword);

	/**
	 * 获取单位证照备选列表
	* @param orgId 机构码
	 * @param keyword
	 * @return
	 */
	List<Map<String,String>>getLicenceRecordListForSelect(@Param(value="orgId")String orgId,@Param(value = "keyword")String keyword);

}
