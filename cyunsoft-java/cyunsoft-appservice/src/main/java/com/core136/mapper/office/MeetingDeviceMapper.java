package com.core136.mapper.office;

import com.core136.bean.office.MeetingDevice;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface MeetingDeviceMapper extends MyMapper<MeetingDevice> {

    /**
     * 获取会议设备列表
     * @param orgId
     * @return
     */
    List<Map<String, String>> getMeetingDeviceList(@Param(value = "orgId") String orgId);

	/**
	 * 获取权限内可用的会议室设备
	* @param orgId 机构码
	 * @param deptId
	 * @return
	 */
	List<Map<String, String>> getCanUseDeviceList(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId);

	/**
	 * 获取设备名称列表
	* @param orgId 机构码
	 * @param list
	 * @return
	 */
	List<Map<String, String>> getDeviceListName(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);
}
