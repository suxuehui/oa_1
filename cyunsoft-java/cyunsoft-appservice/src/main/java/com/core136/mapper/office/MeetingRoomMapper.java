package com.core136.mapper.office;

import com.core136.bean.office.MeetingRoom;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface MeetingRoomMapper extends MyMapper<MeetingRoom> {
	/**
	 * 获取我历史申请过的会议室列表
	* @param orgId 机构码
	 * @param deptId
	 * @param accountId
	 * @return
	 */
	List<Map<String, String>> getMyApplyMeetingRoomList(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId,@Param(value="accountId")String accountId);
    /**
     * 获取会议室列表
     * @param orgId
     * @return
     */
    List<Map<String, String>> getMeetingRoomList(@Param(value = "orgId") String orgId);
    /**
     * 获取当前用户可用的会议室
     * @param orgId
     * @param deptId
     * @return
     */
    List<Map<String, Object>> getCanUseMeetingRoomList(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId);
}
