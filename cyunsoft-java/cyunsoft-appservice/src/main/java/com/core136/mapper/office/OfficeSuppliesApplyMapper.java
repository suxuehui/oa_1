package com.core136.mapper.office;

import com.core136.bean.office.OfficeSuppliesApply;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface OfficeSuppliesApplyMapper extends MyMapper<OfficeSuppliesApply> {
	/**
	 * 获取个人历史申请记录
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getMyApplyOfficeSuppliesList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "dateQueryType")String dateQueryType,
														   @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "keyword") String keyword);

	/**
	 * 获取审批列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getOfficeSuppliesApplyRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "dateQueryType")String dateQueryType,
															   @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "keyword") String keyword);


	/**
	 * 待发放用品列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	List<Map<String, String>> getGrantOfficeSuppliesList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "dateQueryType")String dateQueryType,
														 @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);

}
