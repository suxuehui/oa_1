package com.core136.mapper.office;

import com.core136.bean.office.OfficeSuppliesGrant;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OfficeSuppliesGrantMapper extends MyMapper<OfficeSuppliesGrant> {
	/**
	 * 统计已发放了多少办公用品
	* @param orgId 机构码
	 * @param applyId
	 * @return
	 */
	int getGrantCount(@Param(value = "orgId") String orgId, @Param(value = "applyId") String applyId);

	/**
	 * 获取办公用品发放列表
	* @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
    List<Map<String, String>> getGrantOfficeList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "dateQueryType")String dateQueryType,
                                                 @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

}
