package com.core136.mapper.office;

import com.core136.bean.office.OfficeSupplies;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface OfficeSuppliesMapper extends MyMapper<OfficeSupplies> {

	/**
	 * 获取办公用品列表
	* @param orgId 机构码
	 * @param sortId
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getOfficeSuppliesListBySortId(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId, @Param(value = "keyword") String keyword);

	/**
	 * 获取可以领用的办公用品列表
	* @param orgId 机构码
	 * @param sortId
	 * @param deptId
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getApplyOfficeSuppliesList(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId,
                                                         @Param(value = "deptId") String deptId, @Param(value = "keyword") String keyword);
}
