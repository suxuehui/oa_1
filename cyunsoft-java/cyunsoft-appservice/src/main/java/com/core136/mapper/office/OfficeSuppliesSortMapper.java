package com.core136.mapper.office;

import com.core136.bean.office.OfficeSuppliesSort;
import com.core136.common.dbutils.MyMapper;

/**
 * @author lsq
 *
 */
public interface OfficeSuppliesSortMapper extends MyMapper<OfficeSuppliesSort> {


}
