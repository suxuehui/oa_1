package com.core136.mapper.office;

import com.core136.bean.office.VehicleApply;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface VehicleApplyMapper extends MyMapper<VehicleApply> {
	/**
	 * 获取申请记录
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param status
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getVehicleApplyList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "status") String status,
            @Param(value = "accountId") String accountId,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
			@Param(value = "carType") String carType,
            @Param(value = "keyword") String keyword
    );


	/**
	 * 获取待审批列表
	* @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param status
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param carType
	 * @return
	 */
    List<Map<String, String>> getVehicleApprovedList(
		@Param(value = "orgId") String orgId,
		@Param(value = "opFlag") String opFlag,
		@Param(value = "status") String status,
		@Param(value = "accountId") String accountId,
		@Param(value = "dateQueryType") String dateQueryType,
		@Param(value = "beginTime") String beginTime,
		@Param(value = "endTime") String endTime,
		@Param(value = "carType") String carType,
		@Param(value = "keyword") String keyword
	);
	Map<String, String> getVehicleApprovalResult(@Param(value = "orgId") String orgId,@Param(value = "applyId") String applyId);

	/**
	 * 获取待还车记录
	 * @param orgId 机构码
	 * @param carType
	 * @param returnFlag
	 * @param dateQueryType  日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getVehicleReturnList(
            @Param(value = "orgId") String orgId,
            @Param(value = "carType") String carType,
			@Param(value = "returnFlag") String returnFlag,
            @Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "keyword") String keyword
    );


}
