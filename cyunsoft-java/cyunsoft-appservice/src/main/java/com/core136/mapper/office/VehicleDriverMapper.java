package com.core136.mapper.office;

import com.core136.bean.office.VehicleDriver;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface VehicleDriverMapper extends MyMapper<VehicleDriver> {

    List<Map<String, String>> getVehicleDriverList(@Param(value = "orgId") String orgId);

    List<Map<String, String>> getVehicleDriverListForSelect(@Param(value = "orgId") String orgId);


}
