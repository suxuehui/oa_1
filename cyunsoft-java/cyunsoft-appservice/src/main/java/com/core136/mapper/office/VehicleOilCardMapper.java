package com.core136.mapper.office;

import com.core136.bean.office.VehicleOilCard;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface VehicleOilCardMapper extends MyMapper<VehicleOilCard> {

	/**
	 * 获取油卡列表
	* @param orgId 机构码
	 * @param oilType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    List<Map<String, String>> getVehicleOilCardList(@Param(value = "orgId") String orgId,
                                                    @Param(value = "oilType") String oilType,
                                                    @Param(value="dateQueryType")String dateQueryType,
                                                    @Param(value = "beginTime") String beginTime,
                                                    @Param(value = "endTime") String endTime,
                                                    @Param(value = "keyword") String keyword
    );

	/**
	 * 获取可表油卡列表
	 * @param orgId
	 * @return
	 */
	List<Map<String, String>> getCanUsedOilCardList(@Param(value = "orgId") String orgId);
}
