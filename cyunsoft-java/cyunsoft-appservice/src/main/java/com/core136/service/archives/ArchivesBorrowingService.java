package com.core136.service.archives;

import com.core136.bean.archives.ArchivesBorrowing;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesBorrowingMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 档案借阅操作服务类
 * @author lsq
 */
@Service
public class ArchivesBorrowingService {
    private ArchivesBorrowingMapper archivesBorrowingMapper;
	@Autowired
	public void setArchivesBorrowingMapper(ArchivesBorrowingMapper archivesBorrowingMapper) {
		this.archivesBorrowingMapper = archivesBorrowingMapper;
	}

	/**
	 * 创建档案借阅记录
	 * @param archivesBorrowing 档案借阅对象
	 * @return 成功创建记录数
	 */
    public int insertArchivesBorrowing(ArchivesBorrowing archivesBorrowing)
    {
        return archivesBorrowingMapper.insert(archivesBorrowing);
    }

	/**
	 * 删除档案借阅记录
	 * @param archivesBorrowing 档案借阅对象
	 * @return 成功删除记录数
	 */
    public int deleteArchivesBorrowing(ArchivesBorrowing archivesBorrowing)
    {
        return archivesBorrowingMapper.delete(archivesBorrowing);
    }

	/**
	 * 更新档案借阅记录
	 * @param example 更新条件
	 * @param archivesBorrowing 档案借阅对象
	 * @return 成功更新记录数
	 */
    public int updateArchivesBorrowing(Example example,ArchivesBorrowing archivesBorrowing)
    {
        return archivesBorrowingMapper.updateByExampleSelective(archivesBorrowing,example);
    }

	/**
	 * 查询单个档案借阅记录
	 * @param archivesBorrowing 档案借阅对象
	 * @return 档案借阅对象
	 */
    public ArchivesBorrowing selectOneArchivesBorrowing(ArchivesBorrowing archivesBorrowing)
    {
        return archivesBorrowingMapper.selectOne(archivesBorrowing);
    }

	/**
	 * 获取档案借阅列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param toOut 是不借出
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 借阅列表
	 */
    public List<Map<String,String>> getArchivesBorrowingList(String orgId, String opFlag, String accountId, String dateQueryType, String beginTime, String endTime, String toOut, String status, String keyword)
    {
        return archivesBorrowingMapper.getArchivesBorrowingList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,toOut,status,"%"+keyword+"%");
    }

    /**
     * 获取档案借阅列表
     * @param pageParam 分页参数
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param toOut 是不借出
     * @param status 状态
     * @return 借阅列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getArchivesBorrowingList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String toOut,String status) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesBorrowingList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType,beginTime,endTime,toOut,status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }



}
