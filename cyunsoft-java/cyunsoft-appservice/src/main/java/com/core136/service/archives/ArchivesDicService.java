package com.core136.service.archives;

import com.core136.bean.archives.ArchivesDic;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesDicMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ArchivesDicService {
    private ArchivesDicMapper archivesDicMapper;
	@Autowired
	public void setArchivesDicMapper(ArchivesDicMapper archivesDicMapper) {
		this.archivesDicMapper = archivesDicMapper;
	}

	/**
	 * 添加档案字典
	 * @param archivesDic 档案字典对象
	 * @return 成功创建记录数
	 */
	public int insertArchivesDic(ArchivesDic archivesDic) {
        return archivesDicMapper.insert(archivesDic);
    }

	/**
	 * 删除档案字典
	 * @param archivesDic 档案字典对象
	 * @return 成功删除记录数
	 */
	public int deleteArchivesDic(ArchivesDic archivesDic) {
        return archivesDicMapper.delete(archivesDic);
    }

    /**
     * 批量删除字典
     * @param orgId 机构码
     * @param list 字典Id 列表
     * @return 成功删除记录数
     */
    public RetDataBean deleteArchivesDicByIds(String orgId, List<String> list) {
        if(list.size()>0) {
            Example example = new Example(ArchivesDic.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("dicId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS,archivesDicMapper.deleteByExample(example));
        }else
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
        }
    }


	/**
	 * 按条件删除
	 * @param example 删除条件
	 * @return 成功删除记录数
	 */
	public int deleteArchivesDic(Example example) {
        return archivesDicMapper.deleteByExample(example);
    }

	/**
	 * 按条件更新
	 * @param archivesDic 档案字典对象
	 * @param example 更新条件
	 * @return 成功更新记录数
	 */
	public int updateArchivesDic(ArchivesDic archivesDic, Example example) {
        return archivesDicMapper.updateByExampleSelective(archivesDic, example);
    }

    /**
     * 字典排序
     * @param orgId 机构码
     * @param oldDicId 原字典Id
     * @param newDicId 新字典Id
     * @param oldSortNo 原排序号
     * @param newSortNo 新排序号
     * @return 消息结构
     */
    @Transactional(value = "generalTM")
    public RetDataBean dropArchivesDic(String orgId,String oldDicId, String newDicId,Integer oldSortNo,Integer newSortNo) {
        try {
            ArchivesDic oldArchivesDic = new ArchivesDic();
            oldArchivesDic.setSortNo(newSortNo);
            ArchivesDic newArchivesDic = new ArchivesDic();
            newArchivesDic.setSortNo(oldSortNo);
            Example example = new Example(ArchivesDic.class);
            example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",oldDicId);
            updateArchivesDic(oldArchivesDic,example);
            Example example1 = new Example(ArchivesDic.class);
            example1.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",newDicId);
            updateArchivesDic(newArchivesDic,example1);
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 判断字典分类下是否有子集
     * @param orgId 机构码
     * @param parentId 父级字典Id
     * @return 是否有子集
     */
    public boolean isExistChild(String orgId,String parentId)
    {
        ArchivesDic archivesDic = new ArchivesDic();
        archivesDic.setOrgId(orgId);
        archivesDic.setParentId(parentId);
        return archivesDicMapper.selectCount(archivesDic) > 0;
    }

	/**
	 * 查询单个字典分类
	 * @param archivesDic 字典分类对象
	 * @return 字典分类对象
	 */
	public ArchivesDic selectOneArchivesDic(ArchivesDic archivesDic) {
        return archivesDicMapper.selectOne(archivesDic);
    }

    /**
     * 获取字典分类列表
     * @param orgId 机构码
     * @return 字典分类列表
     */
    public List<Map<String,String>>getArchivesDicParentList(String orgId)
    {
        return archivesDicMapper.getArchivesDicParentList(orgId);
    }

	/**
	 * 获取所有字典列表
	 * @param orgId 机构码
	 * @return 字典列表
	 */
    List<ArchivesDic> getAllArchivesDic(String orgId)
    {
        Example example = new Example(ArchivesDic.class);
        example.createCriteria().andEqualTo("orgId",orgId);
        example.setOrderByClause("sort_no asc");
        return archivesDicMapper.selectByExample(example);
    }

	/**
	 * 获取树型字典数据列表
	 * @param orgId 机构码
	 * @return 字典列表
	 */
    public List<ArchivesDic> getArchivesDicTree(String orgId) {
        List<ArchivesDic> list =getAllArchivesDic(orgId);
        List<ArchivesDic> archivesDicList = new ArrayList<ArchivesDic>();
		for (ArchivesDic dic : list) {
			if (dic.getParentId().equals("0")) {
				archivesDicList.add(dic);
			}
		}
        for (ArchivesDic archivesDic : archivesDicList) {
            archivesDic.setChildren(getChild(archivesDic.getDicId(), list));
        }
        return archivesDicList;
    }

    /**
     * 获取字典子集
     * @param dicId 字典Id
     * @param rootArchivesDic 字典列表
     * @return 字典列表
     */
    private List<ArchivesDic> getChild(String dicId, List<ArchivesDic> rootArchivesDic) {
        List<ArchivesDic> childList = new ArrayList<ArchivesDic>();
        for (ArchivesDic archivesDic : rootArchivesDic) {
            if (archivesDic.getParentId().equals(dicId)) {
                childList.add(archivesDic);
            }
        }
        for (ArchivesDic archivesDic : childList) {
            archivesDic.setChildren(getChild(archivesDic.getDicId(), rootArchivesDic));
        }
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }

    /**
     * 获取字典列表
     * @param pageParam 分页参数
     * @return 字典列表
     */
    public PageInfo<Map<String,String>> getArchivesDicList(PageParam pageParam, String parentId) throws Exception{
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesDicList(pageParam.getOrgId(),parentId,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


	/**
	 * 获取字典列表
	 * @param orgId 机构码
	 * @param parentId 父级字典Id
	 * @param keyword 查询关键词
	 * @return 字典列表
	 */
    public List<Map<String,String>> getArchivesDicList(String orgId,String parentId,String keyword) {
        return archivesDicMapper.getArchivesDicList(orgId,parentId,"%"+keyword+"%");
    }


	/**
	 * 按标识获取分类码列表
	 * @param orgId 机构码
	 * @param code 字典标识码
	 * @return 分类码列表
	 */

    public List<Map<String, Object>> getArchivesDicByCode(String orgId, String code) {
        return archivesDicMapper.getArchivesDicByCode(orgId, code);
    }

	/**
	 * 获取分类列表
	 * @param archivesDic 档案字典对象
	 * @return 档案字典列表
	 */
	public List<ArchivesDic> getArchivesDicList(ArchivesDic archivesDic) {
        return archivesDicMapper.select(archivesDic);
    }

}
