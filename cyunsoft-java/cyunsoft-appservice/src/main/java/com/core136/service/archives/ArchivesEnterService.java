package com.core136.service.archives;

import com.core136.bean.archives.ArchivesEnter;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesEnterMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 档案登记操作服务类
 * @author lsq
 */
@Service
public class ArchivesEnterService {
    private ArchivesEnterMapper archivesEnterMapper;
	@Autowired
	public void setArchivesEnterMapper(ArchivesEnterMapper archivesEnterMapper) {
		this.archivesEnterMapper = archivesEnterMapper;
	}

	/**
	 * 创建档案登记
	 * @param archivesEnter 档案登记对象
	 * @return 成功创建记录数
	 */
    public int insertArchivesEnter(ArchivesEnter archivesEnter)
    {
        return  archivesEnterMapper.insert(archivesEnter);
    }

	/**
	 * 删除档案登记
	 * @param archivesEnter 档案登记对象
	 * @return 成功删除记录数
	 */
    public int deleteArchivesEnter(ArchivesEnter archivesEnter)
    {
        return archivesEnterMapper.delete(archivesEnter);
    }

	/**
	 * 批量删除档案登记记录
	 * @param orgId 机构码
	 * @param list 档案登记Id 列表
	 * @return 成功删除记录数
	 */
	public RetDataBean deleteArchivesEnterIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesEnter.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesEnterMapper.deleteByExample(example));
		}
	}

	/**
	 * 更新档案登记
	 * @param example 更新条件
	 * @param archivesEnter 档案登记对象
	 * @return 成功更新记录数
	 */
    public int updateArchivesEnter(Example example,ArchivesEnter archivesEnter)
    {
        return archivesEnterMapper.updateByExampleSelective(archivesEnter,example);
    }

	/**
	 * 查询单个档案登记
	 * @param archivesEnter 档案登记对象
	 * @return 档案登记对象
	 */
    public ArchivesEnter selectOneArchivesEnter(ArchivesEnter archivesEnter)
    {
        return archivesEnterMapper.selectOne(archivesEnter);
    }

	/**
	 * 获取档案登记记录列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status 状态
	 * @param archivesSource 档案来源
	 * @param archivesEnterType 登记类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 登记记录列表
	 */
	public List<Map<String,String>> getArchivesEnterList(String orgId, String opFlag, String accountId, String status,String archivesSource, String archivesEnterType,String dateQueryType,String beginTime,String endTime, String keyword) {
		return archivesEnterMapper.getArchivesEnterList(orgId,opFlag,accountId,status,archivesSource,archivesEnterType,dateQueryType,beginTime,endTime,"%"+keyword+"%");
	}

	/**
	 * 获取档案登记记录列表
	 * @param pageParam 分页参数
	 * @param status 状态
	 * @param archivesSource 档案来源
	 * @param archivesEnterType 登记类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 登记记录列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getArchivesEnterList(PageParam pageParam, String status,String archivesSource, String archivesEnterType,String dateQueryType,String beginTime,String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesEnterList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),status,archivesSource,archivesEnterType,dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
