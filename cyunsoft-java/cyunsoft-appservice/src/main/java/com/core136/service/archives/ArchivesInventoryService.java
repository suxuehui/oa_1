package com.core136.service.archives;

import com.core136.bean.archives.ArchivesInventory;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesInventoryMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesInventoryService {
	private ArchivesInventoryMapper archivesInventoryMapper;
	@Autowired
	public void setArchivesInventoryMapper(ArchivesInventoryMapper archivesInventoryMapper) {
		this.archivesInventoryMapper = archivesInventoryMapper;
	}

	public int insertArchivesInventory(ArchivesInventory archivesInventory) {
		return archivesInventoryMapper.insert(archivesInventory);
	}

	public int deleteArchivesInventory(ArchivesInventory archivesInventory) {
		return archivesInventoryMapper.delete(archivesInventory);
	}

	public int updateArchivesInventory(Example example,ArchivesInventory archivesInventory) {
		return archivesInventoryMapper.updateByExampleSelective(archivesInventory,example);
	}

	public ArchivesInventory selectOneArchivesInventory(ArchivesInventory archivesInventory) {
		return archivesInventoryMapper.selectOne(archivesInventory);
	}

	public RetDataBean deleteArchivesInventoryByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesInventory.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesInventoryMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取档案盘点记录
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param inventoryType 盘点类型
	 * @param keyword 查询关键词
	 * @return 盘点记录列表
	 */
	public List<Map<String,String>>getArchivesInventoryList(String orgId,String dateQueryType,String beginTime,String endTime,String inventoryType,String keyword) {
		return archivesInventoryMapper.getArchivesInventoryList(orgId,dateQueryType,beginTime,endTime,inventoryType,"%"+keyword+"%");
	}

	/**
	 * 获取档案盘点记录
	 * @param pageParam 分页参数
	 * @param date 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param inventoryType 盘点类型
	 * @return 盘点记录列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getArchivesInventoryList(PageParam pageParam, String date, String beginTime, String endTime, String inventoryType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesInventoryList(pageParam.getOrgId(),date,beginTime,endTime, inventoryType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
