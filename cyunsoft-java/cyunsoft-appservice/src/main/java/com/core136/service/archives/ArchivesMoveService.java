package com.core136.service.archives;

import com.core136.bean.archives.ArchivesMove;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesMoveMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesMoveService {
	private ArchivesMoveMapper archivesMoveMapper;
	@Autowired
	public void setArchivesMoveMapper(ArchivesMoveMapper archivesMoveMapper) {
		this.archivesMoveMapper = archivesMoveMapper;
	}

	public int insertArchivesMove(ArchivesMove archivesMove) {
		return archivesMoveMapper.insert(archivesMove);
	}

	public int deleteArchivesMove(ArchivesMove archivesMove) {
		return archivesMoveMapper.delete(archivesMove);
	}

	public int updateArchivesMove(Example example,ArchivesMove archivesMove) {
		return archivesMoveMapper.updateByExampleSelective(archivesMove,example);
	}

	public ArchivesMove selectOneArchivesMove(ArchivesMove archivesMove) {
		return archivesMoveMapper.selectOne(archivesMove);
	}

	/**
	 * 批量删除档案移库记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteArchivesMoveByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesMove.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesMoveMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取档案移库记录列表
	 * @param orgId
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param moveType
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>>getArchivesMoveList(String orgId, String dateQueryType, String beginTime, String endTime, String moveType, String keyword) {
		return archivesMoveMapper.getArchivesMoveList(orgId,dateQueryType,beginTime,endTime,moveType,"%"+keyword+"%");
	}

	/**
	 * 获取档案移库记录列表
	 * @param pageParam
	 * @param date
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param moveType
	 * @return
	 * @throws Exception
	 */
	public PageInfo<Map<String, String>> getArchivesMoveList(PageParam pageParam, String date, String beginTime, String endTime, String moveType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesMoveList(pageParam.getOrgId(),date,beginTime,endTime, moveType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


}
