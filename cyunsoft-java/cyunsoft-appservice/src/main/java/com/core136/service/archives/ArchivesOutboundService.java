package com.core136.service.archives;

import com.core136.bean.archives.ArchivesOutbound;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesOutboundMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesOutboundService {
	private ArchivesOutboundMapper archivesOutboundMapper;
	@Autowired
	public void setArchivesOutboundMapper(ArchivesOutboundMapper archivesOutboundMapper) {
		this.archivesOutboundMapper = archivesOutboundMapper;
	}

	public int insertArchivesOutbound(ArchivesOutbound archivesOutbound) {
		return archivesOutboundMapper.insert(archivesOutbound);
	}

	public int deleteArchivesOutbound(ArchivesOutbound archivesOutbound) {
		return archivesOutboundMapper.delete(archivesOutbound);
	}

	public int updateArchivesOutbound(Example example,ArchivesOutbound archivesOutbound) {
		return archivesOutboundMapper.updateByExampleSelective(archivesOutbound,example);
	}

	public ArchivesOutbound selectOneArchivesOutbound(ArchivesOutbound archivesOutbound) {
		return archivesOutboundMapper.selectOne(archivesOutbound);
	}

	/**
	 * 批量删除档案出库记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteArchivesOutboundByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesOutbound.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesOutboundMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取档案出库记录
	 * @param orgId
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param outboundType
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>>getArchivesOutboundList(String orgId, String dateQueryType, String beginTime, String endTime, String outboundType, String keyword) {
		return archivesOutboundMapper.getArchivesOutboundList(orgId,dateQueryType,beginTime,endTime,outboundType,"%"+keyword+"%");
	}

	/**
	 * 获取档案出库记录
	 * @param pageParam
	 * @param date
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param outboundType
	 * @return
	 * @throws Exception
	 */
	public PageInfo<Map<String, String>> getArchivesOutboundList(PageParam pageParam, String date, String beginTime, String endTime, String outboundType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesOutboundList(pageParam.getOrgId(),date,beginTime,endTime, outboundType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
