package com.core136.service.archives;

import com.core136.bean.archives.ArchivesRoom;
import com.core136.mapper.archives.ArchivesRoomMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ArchivesRoomService {
	private ArchivesRoomMapper archivesRoomMapper;
	private UserInfoService userInfoService;
	@Autowired
	public void setArchivesRoomMapper(ArchivesRoomMapper archivesRoomMapper) {
		this.archivesRoomMapper = archivesRoomMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertArchivesRoom(ArchivesRoom archivesRoom) {
		return archivesRoomMapper.insert(archivesRoom);
	}

	public int deleteArchivesRoom(ArchivesRoom archivesRoom) {
		return archivesRoomMapper.delete(archivesRoom);
	}

	public int updateArchivesRoom(Example example,ArchivesRoom archivesRoom) {
		return archivesRoomMapper.updateByExampleSelective(archivesRoom,example);
	}

	public ArchivesRoom selectOneArchivesRoom(ArchivesRoom archivesRoom) {
		return archivesRoomMapper.selectOne(archivesRoom);
	}

	/**
	 * 获取档案库房列表
	 * @param orgId
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>>getArchivesRoomListForSelect(String orgId, String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword="";
		}
		return archivesRoomMapper.getArchivesRoomListForSelect(orgId,"%"+keyword+"%");
	}

	/**
	 * 获取所有档案库房列表
	 * @param orgId
	 * @return
	 */
	public List<ArchivesRoom> getAllArchivesRoomList(String orgId) {
		Example example = new Example(ArchivesRoom.class);
		example.createCriteria().andEqualTo("orgId",orgId);
		example.setOrderByClause("sort_no desc");
		List<ArchivesRoom> res = archivesRoomMapper.selectByExample(example);
		List<ArchivesRoom> resList = new ArrayList<>();
		for(ArchivesRoom archivesRoom:res) {
			if(StringUtils.isNotBlank(archivesRoom.getManager())) {
				List<String> accountList = Arrays.asList(archivesRoom.getManager().split(","));
				archivesRoom.setManagerUserName(userInfoService.getUserNamesByAccountIds(orgId, accountList));
			}
			resList.add(archivesRoom);
		}
		return resList;
	}
}
