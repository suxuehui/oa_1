package com.core136.service.archives;

import com.core136.bean.archives.ArchivesSort;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.archives.ArchivesSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ArchivesSortService {
	private ArchivesSortMapper archivesSortMapper;
	private ArchivesRecordService archivesRecordService;
	@Autowired
	public void setArchivesSortMapper(ArchivesSortMapper archivesSortMapper) {
		this.archivesSortMapper = archivesSortMapper;
	}
	@Autowired
	public void setArchivesRecordService(ArchivesRecordService archivesRecordService) {
		this.archivesRecordService = archivesRecordService;
	}

	public int insertArchivesSort(ArchivesSort archivesSort) {
		return archivesSortMapper.insert(archivesSort);
	}

	public int deleteArchivesSort(ArchivesSort archivesSort) {
		return archivesSortMapper.delete(archivesSort);
	}

	public int updateArchivesSort(Example example,ArchivesSort archivesSort) {
		return archivesSortMapper.updateByExampleSelective(archivesSort,example);
	}

	public ArchivesSort selectOneArchivesSort(ArchivesSort archivesSort) {
		return archivesSortMapper.selectOne(archivesSort);
	}

	/**
	 * 批量删除档案分类
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteArchivesSortIds(String orgId, List<String> list) {
		List<String> sortList = new ArrayList<>();
		for(String sortId:list) {
			if(archivesRecordService.getCountBySortId(orgId,sortId)<1) {
				sortList.add(sortId);
			}
		}
		if (sortList.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesSort.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("sortId", sortList);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesSortMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取所有档案分类树型结构
	 * @param orgId
	 * @return
	 */
	public List<ArchivesSort> getArchivesSortTree(String orgId) {
		List<ArchivesSort> list = getAllArchivesSort(orgId);
		List<ArchivesSort> archivesSortList = new ArrayList<>();
		for (ArchivesSort sort : list) {
			if (sort.getParentId().equals("0")) {
				archivesSortList.add(sort);
			}
		}
		for (ArchivesSort archivesSort : archivesSortList) {
			archivesSort.setChildren(getChildArchivesSortList(archivesSort.getSortId(), list));
		}
		return archivesSortList;
	}


	/**
	 * 获取分类子表
	 * @param sortId
	 * @param rootArchivesSort
	 * @return
	 */
	public List<ArchivesSort> getChildArchivesSortList(String sortId, List<ArchivesSort> rootArchivesSort) {
		List<ArchivesSort> childList = new ArrayList<>();
		for (ArchivesSort archivesSort : rootArchivesSort) {
			if (archivesSort.getParentId().equals(sortId)) {
				childList.add(archivesSort);
			}
		}
		for (ArchivesSort archivesSort : childList) {
			archivesSort.setChildren(getChildArchivesSortList(archivesSort.getSortId(), rootArchivesSort));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	/**
	 * 获取所有档案分类列表
	 * @param orgId
	 * @return
	 */
	public List<ArchivesSort> getAllArchivesSort(String orgId) {
		return archivesSortMapper.getAllArchivesSort(orgId);
	}

	/**
	 * 获取档案分类树形结构
	 * @param orgId
	 * @return
	 */
	public List<ArchivesSort> getAllArchivesSortForSelect(String orgId) {
		return archivesSortMapper.getAllArchivesSortForSelect(orgId);
	}

	/**
	 * 获取档案分类树形结构
	 * @param orgId
	 * @return
	 */
	public List<ArchivesSort> getAllArchivesSortForSelectTree(String orgId) {
		List<ArchivesSort> list = getAllArchivesSortForSelect(orgId);
		List<ArchivesSort> archivesSortList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getParentId().equals("0")) {
				archivesSortList.add(list.get(i));
			}
		}
		for (ArchivesSort archivesSort : archivesSortList) {
			archivesSort.setChildren(getChildArchivesSortList(archivesSort.getSortId(), list));
		}
		return archivesSortList;
	}



}
