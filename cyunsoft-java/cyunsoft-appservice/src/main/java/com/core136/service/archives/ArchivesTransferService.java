package com.core136.service.archives;

import com.core136.bean.archives.ArchivesTransfer;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesTransferMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesTransferService {
	private ArchivesTransferMapper archivesTransferMapper;
	@Autowired
	public void setArchivesTransferMapper(ArchivesTransferMapper archivesTransferMapper) {
		this.archivesTransferMapper = archivesTransferMapper;
	}

	public int insertArchivesTransfer(ArchivesTransfer archivesTransfer) {
		return archivesTransferMapper.insert(archivesTransfer);
	}

	public int deleteArchivesTransfer(ArchivesTransfer archivesTransfer) {
		return archivesTransferMapper.delete(archivesTransfer);
	}

	public int updateArchivesTransfer(Example example, ArchivesTransfer archivesTransfer) {
		return archivesTransferMapper.updateByExampleSelective(archivesTransfer,example);
	}

	public ArchivesTransfer selectOneArchivesTransfer(ArchivesTransfer archivesTransfer) {
		return archivesTransferMapper.selectOne(archivesTransfer);
	}

	/**
	 * 批量删除档案转移记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteArchivesTransferByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ArchivesTransfer.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, archivesTransferMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取档案转移列表
	 * @param orgId
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param transferType
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>>getArchivesTransferList(String orgId, String dateQueryType, String beginTime, String endTime, String transferType, String keyword) {
		return archivesTransferMapper.getArchivesTransferList(orgId,dateQueryType,beginTime,endTime,transferType,"%"+keyword+"%");
	}

	/**
	 * 获取档案转移列表
	 * @param pageParam 分页参数
	 * @param date
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param transferType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getArchivesTransferList(PageParam pageParam, String date, String beginTime, String endTime, String transferType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getArchivesTransferList(pageParam.getOrgId(),date,beginTime,endTime, transferType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


}
