package com.core136.service.archives;

import com.core136.bean.account.UserInfo;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.bi.option.property.*;
import com.core136.bi.option.resdata.LegendData;
import com.core136.bi.option.resdata.SeriesData;
import com.core136.bi.option.style.AxisPointer;
import com.core136.bi.option.style.AxisTick;
import com.core136.bi.option.style.Emphasis;
import com.core136.bi.option.style.ItemStyle;
import com.core136.bi.option.units.BarOption;
import com.core136.bi.option.units.LineOption;
import com.core136.bi.option.units.PieOption;
import com.core136.mapper.archives.EchartsArchivesMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EchartsArchivesService {
	private final PieOption pieOption = new PieOption();
	private final LineOption lineOption = new LineOption();
	private final BarOption barOption = new BarOption();
	private EchartsArchivesMapper echartsArchivesMapper;
	@Autowired
	public void setEchartsArchivesMapper(EchartsArchivesMapper echartsArchivesMapper) {
		this.echartsArchivesMapper = echartsArchivesMapper;
	}

	/**
	 * 按分类获取饼状图
	 * @param orgId 机构码
	 * @return 饼状图数据列表
	 */
	public List<Map<String,String>> getArchivesSortForPie(String orgId)
	{
		return echartsArchivesMapper.getArchivesSortForPie(orgId);
	}

	/**
	 * 按涉密等级获取饼状图
	 * @param orgId 机构码
	 * @return 饼状图数据列表
	 */
	public List<Map<String,String>> getArchivesSmForPie(String orgId)
	{
		return echartsArchivesMapper.getArchivesSmForPie(orgId);
	}

	/**
	 * 获取一段时间内按月份统计档案数据上架走势
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 获取一段时间内按月份统计档案数据上架走势数据列表
	 */
	public List<Map<String,String>> getArchivesByMonthLine(String orgId,String beginTime,String endTime)
	{
		return echartsArchivesMapper.getArchivesByMonthLine(orgId,beginTime,endTime);
	}

	/**
	 * 档案架档案数量对比
	 * @param orgId 机构码
	 * @return
	 */
	public List<Map<String,String>> getArchivesFrameForBar(String orgId)
	{
		return echartsArchivesMapper.getArchivesFrameForBar(orgId);
	}

	/**
	 * 档案架档案数量对比
	 * @param userInfo 当前用户
	 * @return
	 */
	public OptionConfig getArchivesFrameForBar(UserInfo userInfo) {
		List<Map<String, String>> resdataList = getArchivesFrameForBar(userInfo.getOrgId());
		String[] xdata = new String[resdataList.size()];
		Double[] ydata = new Double[resdataList.size()];
		for (int i = 0; i < resdataList.size(); i++) {
			if (StringUtils.isNotBlank(resdataList.get(i).get("name"))) {
				xdata[i] = resdataList.get(i).get("name");
			} else {
				xdata[i] = "other" + i;
			}
			ydata[i] = Double.valueOf(String.valueOf(resdataList.get(i).get("value")));
		}
		OptionSeries optionSeries = new OptionSeries();
		optionSeries.setName("档案架档案数量对比");
		optionSeries.setType("bar");
		optionSeries.setBarWidth("60%");
		optionSeries.setData(ydata);
		OptionXAxis xAxis = new OptionXAxis();
		xAxis.setType("category");
		xAxis.setData(xdata);
		AxisTick axisTick = new AxisTick();
		axisTick.setAlignWithLabel(true);
		xAxis.setAxisTick(axisTick);
		OptionConfig optionConfig = barOption.getBarTickAlignChartOption(new OptionXAxis[]{xAxis}, new OptionSeries[]{optionSeries});
		OptionTooltip optionTooltip = new OptionTooltip();
		optionTooltip.setTrigger("axis");
		AxisPointer axisPointer = new AxisPointer();
		axisPointer.setType("shadow");
		optionTooltip.setAxisPointer(axisPointer);
		optionConfig.setTooltip(optionTooltip);
		OptionGrid optionGrid = new OptionGrid();
		optionGrid.setLeft("3%");
		optionGrid.setRight("4%");
		optionGrid.setBottom("3%");
		optionGrid.setContainLabel(true);
		optionConfig.setGrid(optionGrid);
		return optionConfig;
	}




	/**
	 * 档案分类排行
	 * @param userInfo 当前用户
	 * @return
	 */
	public OptionConfig getArchivesSortForPie(UserInfo userInfo) {
		OptionConfig optionConfig = new OptionConfig();
		List<Map<String, String>> resdataList = getArchivesSortForPie(userInfo.getOrgId());
		OptionSeries[] optionSeriesArr = new OptionSeries[1];
		SeriesData[] dataArr = new SeriesData[resdataList.size()];
		int selectedLeng = 0;
		if (dataArr.length >= 10) {
			selectedLeng = 10;
		} else {
			selectedLeng = dataArr.length;
		}
		String[] selected = new String[selectedLeng];
		LegendData[] legendDatas = new LegendData[dataArr.length];
		for (int i = 0; i < dataArr.length; i++) {
			if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
				resdataList.get(i).put("name", "other" + i);
			}
			if (i < selectedLeng) {
				selected[i] = resdataList.get(i).get("name");
			}
			LegendData legendData = new LegendData();
			legendData.setName(resdataList.get(i).get("name"));
			legendDatas[i] = legendData;
			SeriesData seriesData = new SeriesData();
			seriesData.setName(resdataList.get(i).get("name"));
			seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
			dataArr[i] = seriesData;
		}
		OptionSeries optionSeries = new OptionSeries();
		optionSeries.setName("档案分类统计");
		optionSeries.setType("pie");
		optionSeries.setRadius("55%");
		optionSeries.setCenter(new String[]{"40%", "50%"});
		Emphasis emphasis = new Emphasis();
		ItemStyle itemStyle = new ItemStyle();
		itemStyle.setShadowBlur(10);
		itemStyle.setShadowOffsetX(0);
		itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
		emphasis.setItemStyle(itemStyle);
		optionSeries.setData(dataArr);
		optionSeriesArr[0] = optionSeries;
		optionConfig.setSeries(optionSeriesArr);
		optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
		OptionTitle optionTitle = new OptionTitle();
		optionTitle.setText("档案分类统计");
		optionTitle.setSubtext("档案分类占比");
		optionTitle.setLeft("left");
		optionConfig.setTitle(optionTitle);
		return optionConfig;
	}

	/**
	 * 按涉密等级饼状图
	 * @param userInfo 当前用户
	 * @return
	 */
	public OptionConfig getArchivesSmForPie(UserInfo userInfo) {
		OptionConfig optionConfig = new OptionConfig();
		List<Map<String, String>> resdataList = getArchivesSmForPie(userInfo.getOrgId());
		OptionSeries[] optionSeriesArr = new OptionSeries[1];
		SeriesData[] dataArr = new SeriesData[resdataList.size()];
		int selectedLeng = 0;
		if (dataArr.length >= 10) {
			selectedLeng = 10;
		} else {
			selectedLeng = dataArr.length;
		}
		String[] selected = new String[selectedLeng];
		LegendData[] legendDatas = new LegendData[dataArr.length];
		for (int i = 0; i < dataArr.length; i++) {
			if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
				resdataList.get(i).put("name", "other" + i);
			}
			if (i < selectedLeng) {
				selected[i] = resdataList.get(i).get("name");
			}
			LegendData legendData = new LegendData();
			legendData.setName(resdataList.get(i).get("name"));
			legendDatas[i] = legendData;
			SeriesData seriesData = new SeriesData();
			seriesData.setName(resdataList.get(i).get("name"));
			seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
			dataArr[i] = seriesData;
		}
		OptionSeries optionSeries = new OptionSeries();
		optionSeries.setName("档案涉密等级统计");
		optionSeries.setType("pie");
		optionSeries.setRadius("55%");
		optionSeries.setCenter(new String[]{"40%", "50%"});
		Emphasis emphasis = new Emphasis();
		ItemStyle itemStyle = new ItemStyle();
		itemStyle.setShadowBlur(10);
		itemStyle.setShadowOffsetX(0);
		itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
		emphasis.setItemStyle(itemStyle);
		optionSeries.setData(dataArr);
		optionSeriesArr[0] = optionSeries;
		optionConfig.setSeries(optionSeriesArr);
		optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
		OptionTitle optionTitle = new OptionTitle();
		optionTitle.setText("档案涉密等级统计");
		optionTitle.setSubtext("涉密等级占比");
		optionTitle.setLeft("left");
		optionConfig.setTitle(optionTitle);
		return optionConfig;
	}

	/**
	 * 获取一段时间内按月份统计档案数据上架走势
	 * @param userInfo 当前用户
	 * @return
	 */
	public OptionConfig getArchivesByMonthLine(UserInfo userInfo) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		Date y = c.getTime();
		String endTime = format.format(y);
		c.add(Calendar.YEAR, -1);
		y = c.getTime();
		String beginTime = format.format(y);
		List<Map<String, String>> resList = getArchivesByMonthLine(userInfo.getOrgId(), beginTime, endTime);
		String[] xAxisData = new String[resList.size()];
		Double[] resData = new Double[resList.size()];
		for (int i = 0; i < resList.size(); i++) {
			xAxisData[i] = resList.get(i).get("frameTime");
			resData[i] = Double.valueOf(String.valueOf(resList.get(i).get("total")));
		}
		return lineOption.getBasicLineChartOption(xAxisData, resData);
	}
}
