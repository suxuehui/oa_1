package com.core136.service.crm;

import com.core136.bean.crm.CrmCare;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmCareMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CrmCareService {
	private CrmCareMapper crmCareMapper;
	@Autowired
	public void setCrmCareMapper(CrmCareMapper crmCareMapper) {
		this.crmCareMapper = crmCareMapper;
	}

	public int insertCrmCare(CrmCare crmCare)
	{
		return crmCareMapper.insert(crmCare);
	}

	public int deleteCrmCare(CrmCare crmCare)
	{
		return crmCareMapper.delete(crmCare);
	}
	public int updateCrmCare(Example example,CrmCare crmCare)
	{
		return crmCareMapper.updateByExampleSelective(crmCare,example);
	}
	public CrmCare selectOneCrmCare(CrmCare crmCare)
	{
		return crmCareMapper.selectOne(crmCare);
	}

	/**
	 * 批量删除客户关怀记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteCrmCareByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(CrmCare.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, crmCareMapper.deleteByExample(example));
		}
	}
	/**
	 * 获取客户关怀记录列表
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param careType
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getCrmCareList(String orgId, String opFlag,String accountId,String dateQueryType, String beginTime, String endTime,String status,String careType, String keyword) {
		return crmCareMapper.getCrmCareList(orgId,opFlag, accountId, dateQueryType,beginTime, endTime, status, careType,"%" + keyword + "%");
	}

	/**
	 * 获取客户关怀记录列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param careType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getCrmCareList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String status,String careType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getCrmCareList(pageParam.getOrgId(), pageParam.getOpFlag(),pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, careType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取客户关怀审批列表
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param careType
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getApprovedCrmCareList(String orgId, String opFlag,String accountId,String dateQueryType, String beginTime, String endTime,String status,String careType, String keyword) {
		return crmCareMapper.getApprovedCrmCareList(orgId,opFlag, accountId, dateQueryType,beginTime, endTime, status, careType,"%" + keyword + "%");
	}

	/**
	 * 获取客户关怀审批列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param careType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getApprovedCrmCareList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String status,String careType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getApprovedCrmCareList(pageParam.getOrgId(), pageParam.getOpFlag(),pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, careType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
