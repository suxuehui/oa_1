package com.core136.service.crm;

import com.core136.bean.calendar.Calendar;
import com.core136.bean.crm.CrmContractRecord;
import com.core136.bean.crm.CrmCustomer;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.calendar.CalendarMapper;
import com.core136.mapper.crm.CrmContractRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 客户联系记录
 * @author lsq
 */
@Service
public class CrmContractRecordService {
    private CrmContractRecordMapper crmContractRecordMapper;
    private CalendarMapper calendarMapper;
    private CrmCustomerService crmCustomerService;
	@Autowired
	public void setCrmContractRecordMapper(CrmContractRecordMapper crmContractRecordMapper) {
		this.crmContractRecordMapper = crmContractRecordMapper;
	}
	@Autowired
	public void setCalendarMapper(CalendarMapper calendarMapper) {
		this.calendarMapper = calendarMapper;
	}
	@Autowired
	public void setCrmCustomerService(CrmCustomerService crmCustomerService) {
		this.crmCustomerService = crmCustomerService;
	}

	public CrmContractRecord selectOneCrmContractRecord(CrmContractRecord crmContractRecord) {
        return crmContractRecordMapper.selectOne(crmContractRecord);
    }

    public int deleteCrmContractRecord(CrmContractRecord crmContractRecord) {
        return crmContractRecordMapper.delete(crmContractRecord);
    }

    public int updateCrmContractRecord(CrmContractRecord crmContractRecord, Example example) {
        return crmContractRecordMapper.updateByExampleSelective(crmContractRecord, example);
    }

    public int insertCrmContractRecord(CrmContractRecord crmContractRecord) {
        return crmContractRecordMapper.insert(crmContractRecord);
    }

	/**
	 * 批量删除联系记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteCrmContractRecordByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(CrmContractRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, crmContractRecordMapper.deleteByExample(example));
		}
	}

	/**
	 * 添加联系记录并处理下次联系加入个人日程
	 * @param crmContractRecord
	 * @return
	 */
	@Transactional(value = "generalTM")
    public int addRecordAndCalendar(CrmContractRecord crmContractRecord) {
        if (StringUtils.isNotBlank(crmContractRecord.getNextVisit())) {
            CrmCustomer crmCustomer = new CrmCustomer();
            crmCustomer.setCustomerId(crmContractRecord.getCustomerId());
            crmCustomer.setOrgId(crmContractRecord.getOrgId());
            crmCustomer = crmCustomerService.selectOne(crmCustomer);
            Calendar calendar = new Calendar();
            calendar.setCalendarId(SysTools.getGUID());
            if (StringUtils.isNotBlank(crmCustomer.getCnName())) {
                calendar.setTitle("回访" + crmCustomer.getEnName());
            } else {
                calendar.setTitle("回访" + crmCustomer.getCnName());
            }
            calendar.setStartTime(crmContractRecord.getNextVisit() + " 08:00:00");
            calendar.setEndTime(crmContractRecord.getNextVisit() + " 23:59:59");
            calendar.setCalendarType("1");
            calendar.setEventLevel("1");
            calendar.setStatus("0");
            calendar.setShareStatus("0");
            calendar.setAccountId(crmContractRecord.getCreateUser());
            calendar.setModule("CRM_CUSTOMER");
            calendar.setRecordId(crmContractRecord.getCustomerId());
            calendar.setOrgId(crmContractRecord.getOrgId());
            calendarMapper.insert(calendar);
        }
        return crmContractRecordMapper.insert(crmContractRecord);
    }


	/**
	 * 获把指定企业的联系记录
	 * @param example
	 * @return
	 */
	public List<CrmContractRecord> getRecordByCustomerId(Example example) {
        return crmContractRecordMapper.selectByExample(example);
    }

	/**
	 * 获取业务员所有联系记录
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param linkType
	 * @param recordType
	 * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getContractRecordList(String orgId,String opFlag,String accountId,String linkType, String recordType, String dateQueryType, String beginTime, String endTime,String keyword) {
		return crmContractRecordMapper.getContractRecordList(orgId, opFlag,accountId,linkType,recordType,dateQueryType,beginTime,endTime,"%"+keyword+"%");
	}
	public PageInfo<Map<String, String>> getContractRecordList(PageParam pageParam,String linkType, String recordType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getContractRecordList(pageParam.getOrgId(), pageParam.getOpFlag(),pageParam.getAccountId(),linkType,recordType,dateQueryType,beginTime,endTime,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}
    /**
     * 获把指定企业的联系记录
     */

    public List<Map<String, String>> getRecordListByCustomerId(String orgId, String customerId) {
        return crmContractRecordMapper.getRecordListByCustomerId(orgId, customerId);
    }

    public PageInfo<Map<String, String>> getRecordListByCustomerId(PageParam pageParam, String customerId) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getRecordListByCustomerId(pageParam.getOrgId(), customerId);
		return new PageInfo<>(datalist);
    }

}
