package com.core136.service.crm;

import com.core136.bean.crm.CrmCustomer;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmCustomerMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 基本信息操作类
 * @author lsq
 */
@Service
public class CrmCustomerService {
    private CrmCustomerMapper crmCustomerMapper;
	@Autowired
	public void setCrmCustomerMapper(CrmCustomerMapper crmCustomerMapper) {
		this.crmCustomerMapper = crmCustomerMapper;
	}

	/**
	 * 查询客户基本信息
	 * @param crmCustomer
	 * @return
	 */
	public CrmCustomer selectOne(CrmCustomer crmCustomer) {
        return crmCustomerMapper.selectOne(crmCustomer);
    }

	/**
	 * 更新客户基本信息
	 * @param crmCustomer
	 * @param example
	 * @return
	 */
	public int UpdateCrmCustomer(CrmCustomer crmCustomer, Example example) {
        return crmCustomerMapper.updateByExampleSelective(crmCustomer, example);
    }

	/**
	 * 删除客户基本信息
	 * @param crmCustomer
	 * @return
	 */
	public int deleteCrmCustomer(CrmCustomer crmCustomer) {
        return crmCustomerMapper.delete(crmCustomer);
    }

	/**
	 * 批量删除个人客户列表
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteCrmCustomerByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(CrmCustomer.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("customerId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, crmCustomerMapper.deleteByExample(example));
		}
	}

	/**
	 * 添加客户基本信息
	 * @param crmCustomer
	 * @return
	 */
	public int insertCrmCustomer(CrmCustomer crmCustomer) {
        return crmCustomerMapper.insert(crmCustomer);
    }

	/**
	 * 获取客户列表
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String,String>> getCrmCustomerList(PageParam pageParam,String industry,String customerType,String crmLevel, String intention) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String,String>> datalist = crmCustomerMapper.getCrmCustomerList(pageParam.getOrgId(),pageParam.getAccountId(),industry,customerType,crmLevel,intention,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public List<Map<String,String>>getCrmCustomerList(String orgId,String accountId,String industry,String model,String crmLevel, String intention,String keyword)
	{
		return crmCustomerMapper.getCrmCustomerList(orgId,accountId,industry,model,crmLevel,intention,"%"+keyword+"%");
	}


	/**
	 * 获取权限内所有客户
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status 状态
	 * @param industry 企业类型
	 * @param customerType 客户类型
	 * @param crmLevel 等级
	 * @param intention 意向
	 * @param keyword 查询关键词
	 * @return 客户列表
	 */
    public List<Map<String, String>> getAllCrmCustomerList(String orgId, String opFlag, String accountId,String status,String industry, String customerType,String crmLevel, String intention,String keyword) {
        return crmCustomerMapper.getAllCrmCustomerList(orgId, opFlag, accountId,status, industry, customerType, crmLevel, intention,"%"+keyword+"%");
    }


	/**
	 * 获取权限内所有客户
	 * @param pageParam 分页参数
	 * @param industry 企业类型
	 * @param customerType 客户类型
	 * @param crmLevel 等级
	 * @param intention 意向
	 * @return 客户列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getAllCrmCustomerList(PageParam pageParam, String status,String industry,String customerType,String crmLevel, String intention) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAllCrmCustomerList(pageParam.getOrgId(), pageParam.getOpFlag(),pageParam.getAccountId(),status,industry,customerType,crmLevel,intention,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取SELECT2客户列表
     */

    public List<Map<String, String>> getMyCustomerListForSelect(String orgId, String keepUser,String customerId,String keyword) {
		if(StringUtils.isBlank(keyword))
		{
			keyword = "%%";
		}else
		{
			keyword = "%"+keyword+"%";
		}
        return crmCustomerMapper.getMyCustomerListForSelect(orgId, keepUser,customerId,keyword);
    }

	public List<Map<String, String>> getAllCustomerListForSelect(String orgId, String customerId,String keyword) {
    	if(StringUtils.isBlank(keyword))
		{
			keyword = "%%";
		}else
		{
			keyword = "%"+keyword+"%";
		}
		return crmCustomerMapper.getAllCustomerListForSelect(orgId, customerId,keyword);
	}

	/**
	 * 获取客户详情
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param customerId
	 * @return
	 */
	public Map<String,String>getCrmCustomerById(String orgId,String opFlag,String accountId,String customerId)
	{
		return crmCustomerMapper.getCrmCustomerById(orgId, opFlag,accountId,customerId);
	}

}
