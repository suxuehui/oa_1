package com.core136.service.crm;

import com.core136.bean.crm.CrmDic;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmDicMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Crm 字典服务类
 * @author lsq
 */
@Service
public class CrmDicService {
    private CrmDicMapper crmDicMapper;
	@Autowired
	public void setCrmDicMapper(CrmDicMapper crmDicMapper) {
		this.crmDicMapper = crmDicMapper;
	}

	public int insertCrmDic(CrmDic crmDic) {
        return crmDicMapper.insert(crmDic);
    }

    public int deleteCrmDic(CrmDic crmDic) {
        return crmDicMapper.delete(crmDic);
    }

    public int updateCrmDic( CrmDic crmDic,Example example) {
        return crmDicMapper.updateByExampleSelective(crmDic, example);
    }

    public CrmDic selectOneCrmDic(CrmDic crmDic) {
        return crmDicMapper.selectOne(crmDic);
    }

    public List<CrmDic> getCrmDicList(Example example) {
        return crmDicMapper.selectByExample(example);
    }

    public int deleteCrmDic(Example example) {
        return crmDicMapper.deleteByExample(example);
    }

	/**
	 * 批量删除字典
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteCrmDicByIds(String orgId, List<String> list) {
		if(!list.isEmpty()) {
			Example example = new Example(CrmDic.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("dicId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS,crmDicMapper.deleteByExample(example));
		}else {
			return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
		}
	}

	/**
	 * 判断字典分类下是否有子集
	 * @param orgId
	 * @param parentId
	 * @return
	 */
	public boolean isExistChild(String orgId,String parentId) {
		CrmDic crmDic = new CrmDic();
		crmDic.setOrgId(orgId);
		crmDic.setParentId(parentId);
		return crmDicMapper.selectCount(crmDic) > 0;
	}

	/**
	 * 按标识获取分类码列表
	 */

	public List<Map<String, Object>> getCrmDicByCode(String orgId, String code) {
		return crmDicMapper.getCrmDicByCode(orgId, code);
	}

	/**
	 * 字典排序
	 * @param orgId
	 * @param oldDicId
	 * @param newDicId
	 * @param oldSortNo
	 * @param newSortNo
	 * @return
	 */
	@Transactional(value = "generalTM")
	public RetDataBean dropCrmDic(String orgId,String oldDicId, String newDicId,Integer oldSortNo,Integer newSortNo) {
		try {
			CrmDic oldCrmDic = new CrmDic();
			oldCrmDic.setSortNo(newSortNo);
			CrmDic newCrmDic = new CrmDic();
			newCrmDic.setSortNo(oldSortNo);
			Example example = new Example(CrmDic.class);
			example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",oldDicId);
			updateCrmDic(oldCrmDic,example);
			Example example1 = new Example(CrmDic.class);
			example1.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",newDicId);
			updateCrmDic(newCrmDic,example1);
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典列表
	 * @param pageParam 分页参数
	 * @return
	 */
	public PageInfo<Map<String,String>> getCrmDicList(PageParam pageParam, String parentId) throws Exception{
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getCrmDicList(pageParam.getOrgId(),parentId,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取字典列表
	 * @param orgId
	 * @param parentId
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>> getCrmDicList(String orgId,String parentId,String keyword) {
		return crmDicMapper.getCrmDicList(orgId,parentId,"%"+keyword+"%");
	}


	public List<CrmDic> getCrmDicTree(String orgId) {
		List<CrmDic> list =getAllCrmDic(orgId);
		List<CrmDic> crmDicList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getParentId().equals("0")) {
				crmDicList.add(list.get(i));
			}
		}
		for (CrmDic crmDic : crmDicList) {
			crmDic.setChildren(getChild(crmDic.getDicId(), list));
		}
		return crmDicList;
	}

	/**
	 * 获取字典子集
	 * @param dicId
	 * @param rootCrmDic
	 * @return
	 */
	private List<CrmDic> getChild(String dicId, List<CrmDic> rootCrmDic) {
		List<CrmDic> childList = new ArrayList<>();
		for (CrmDic crmDic : rootCrmDic) {
			if (crmDic.getParentId().equals(dicId)) {
				childList.add(crmDic);
			}
		}
		for (CrmDic crmDic : childList) {
			crmDic.setChildren(getChild(crmDic.getDicId(), rootCrmDic));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	List<CrmDic> getAllCrmDic(String orgId) {
		Example example = new Example(CrmDic.class);
		example.createCriteria().andEqualTo("orgId",orgId);
		example.setOrderByClause("sort_no asc");
		return crmDicMapper.selectByExample(example);
	}

	/**
	 * 获取字典分类列表
	 * @param orgId
	 * @return
	 */
	public List<Map<String,String>>getCrmDicParentList(String orgId) {
		return crmDicMapper.getCrmDicParentList(orgId);
	}
}
