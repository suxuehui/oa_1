package com.core136.service.crm;

import com.core136.bean.crm.CrmInquiry;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmInquiryMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 询价单操作服务类
 * @author lsq
 */
@Service
public class CrmInquiryService {
    private CrmInquiryMapper crmInquiryMapper;
	@Autowired
	public void setCrmInquiryMapper(CrmInquiryMapper crmInquiryMapper) {
		this.crmInquiryMapper = crmInquiryMapper;
	}

	public CrmInquiry selectOneCrmInquiry(CrmInquiry crmInquiry) {
        return crmInquiryMapper.selectOne(crmInquiry);
    }

    public int insertCrmInquiry(CrmInquiry crmInquiry) {
        return crmInquiryMapper.insert(crmInquiry);
    }

    public int deleteCrmInquiry(CrmInquiry crmInquiry) {
        return crmInquiryMapper.delete(crmInquiry);
    }

    public int updateCrmInquiry(CrmInquiry crmInquiry, Example example) {
        return crmInquiryMapper.updateByExampleSelective(crmInquiry, example);
    }

	/**
	 * 批量删除询价单
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteDiscussNoticeIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(CrmInquiry.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("inquiryId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, crmInquiryMapper.deleteByExample(example));
		}
	}

	/**
	 * 获限权限内的询价单列表
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param customerNature
	 * @param status
	 * @param keyword
	 * @return
	 */

    public List<Map<String, String>> getCrmInquiryList(String orgId, String opFlag, String accountId, String dateQueryType,String beginTime, String endTime, String customerNature, String status, String keyword) {
        return crmInquiryMapper.getCrmInquiryList(orgId, opFlag, accountId,dateQueryType, beginTime, endTime, customerNature, status, "%" + keyword + "%");
    }

	/**
	 * 获限权限内的询价单列表
	 * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param customerNature
	 * @param status
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getCrmInquiryList(PageParam pageParam,String dateQueryType, String beginTime, String endTime, String customerNature, String status) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCrmInquiryList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, customerNature, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取询价单列表
	 * @param orgId
	 * @param accountId
	 * @return
	 */
    public List<Map<String, String>> getCrmInquiryListForSelect(String orgId, String accountId,String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword = "%%";
		}else {
			keyword = "%"+keyword+"%";
		}
        return crmInquiryMapper.getCrmInquiryListForSelect(orgId, accountId,keyword);
    }

	/**
	 * 获取询价单明细
	 * @param orgId
	 * @param accountId 用户账号
	 * @param inquiryId
	 * @return
	 */
	public Map<String,String>getCrmInquiryFormJsonById(String orgId,String accountId,String inquiryId) {
		return crmInquiryMapper.getCrmInquiryFormJsonById(orgId,accountId,inquiryId);
	}

}
