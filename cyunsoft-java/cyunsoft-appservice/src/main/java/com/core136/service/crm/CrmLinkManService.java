package com.core136.service.crm;


import com.core136.bean.account.UserInfo;
import com.core136.bean.crm.CrmLinkMan;
import com.core136.bean.email.EmailBody;
import com.core136.bean.email.EmailConfig;
import com.core136.bean.file.Attach;
import com.core136.bean.system.PageParam;
import com.core136.bean.system.SysConfigInit;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmLinkManMapper;
import com.core136.service.email.EmailBodyService;
import com.core136.service.email.EmailConfigService;
import com.core136.service.file.AttachService;
import com.core136.service.system.SysConfigInitService;
import com.core136.service.system.WebEmailService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 客户联系人管理
 * @author lsq
 */
@Service
public class CrmLinkManService {
    private CrmLinkManMapper crmLinkManMapper;
    private AttachService attachService;
    private EmailConfigService emailConfigService;
    private SysConfigInitService sysConfigService;
    private EmailBodyService emailBodyService;
	@Autowired
	public void setCrmLinkManMapper(CrmLinkManMapper crmLinkManMapper) {
		this.crmLinkManMapper = crmLinkManMapper;
	}
	@Autowired
	public void setAttachService(AttachService attachService) {
		this.attachService = attachService;
	}
	@Autowired
	public void setEmailConfigService(EmailConfigService emailConfigService) {
		this.emailConfigService = emailConfigService;
	}
	@Autowired
	public void setSysConfigService(SysConfigInitService sysConfigService) {
		this.sysConfigService = sysConfigService;
	}
	@Autowired
	public void setEmailBodyService(EmailBodyService emailBodyService) {
		this.emailBodyService = emailBodyService;
	}

	public int insertCrmLinkMan(CrmLinkMan crmLinkMan) {
        return crmLinkManMapper.insert(crmLinkMan);
    }

    public CrmLinkMan selectOne(CrmLinkMan crmLinkMan) {
        return crmLinkManMapper.selectOne(crmLinkMan);
    }

    public int updateCrmLinkMan(CrmLinkMan crmLinkMan, Example example) {
        return crmLinkManMapper.updateByExampleSelective(crmLinkMan, example);
    }

    public int deleteCrmLinkMan(CrmLinkMan crmLinkMan) {
        return crmLinkManMapper.delete(crmLinkMan);
    }

    /**
     * 获取客户联系人列表
     */

    public List<Map<String, String>> getCrmLinkManList(String orgId, String customerId) {
        return crmLinkManMapper.getCrmLinkManList(orgId, customerId);
    }

	public List<Map<String, String>> getCrmLinkManListForSelect(String orgId, String customerId) {
		return crmLinkManMapper.getCrmLinkManListForSelect(orgId, customerId);
	}


	/**
	 * 获取企业所有联系人
	 * @param example
	 * @return
	 */
	public List<CrmLinkMan> getCrmLinkManByCustomerId(Example example) {
        return crmLinkManMapper.selectByExample(example);
    }

    /**
     * 获取CRM联系人列表
     */

    public List<Map<String, String>> getCrmLinkManAllList(String orgId, String keyword) {
        return crmLinkManMapper.getCrmLinkManAllList(orgId, "%" + keyword + "%");
    }


    public PageInfo<Map<String, String>> getCrmLinkManAllList(PageParam pageParam) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCrmLinkManAllList(pageParam.getOrgId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 联系人基本信息
     */

    public Map<String, Object> getCrmLinkManInfo(String orgId, String linkManId) {
        return crmLinkManMapper.getCrmLinkManInfo(orgId, linkManId);
    }

    /**
     * 邮件服务
     * @param userInfo
     * @param to
     * @param subject
     * @param content
     * @param attachId
     * @param sendServiceType
     * @return
     */
    public RetDataBean sendWebMail(UserInfo userInfo, String to, String subject, String content, String attachId, String sendServiceType) {
        try {
            EmailBody emailBody = new EmailBody();
            emailBody.setBodyId(SysTools.getGUID());
            emailBody.setAttachId(attachId);
            emailBody.setFromId(userInfo.getAccountId());
            emailBody.setSubject(subject);
            emailBody.setWebEmailTo(to);
            emailBody.setContent(content);
            emailBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            emailBody.setOrgId(userInfo.getOrgId());
            List<String> paths = new ArrayList<>();
            if (StringUtils.isNotBlank(attachId)) {
                if (attachId.endsWith(",")) {
                    attachId = attachId.substring(0, attachId.length() - 1);
                }
                String[] attachIds;
                if (attachId.contains(",")) {
                    attachIds = attachId.split(",");
                } else {
                    attachIds = new String[]{attachId};
                }
                List<String> list = Arrays.asList(attachIds);
                List<Attach> attachList = attachService.getAttachList(list);

                for (int i = 0; i < attachList.size(); i++) {
                    paths.add(attachList.get(i).getPath());
                }
            }
            if (sendServiceType.equals("0")) {
                WebEmailService webEmailService = SpringUtils.getBean(WebEmailService.class);
                if (StringUtils.isNotBlank(attachId)) {
                    webEmailService.sendAttachmentsWebEmail(to, subject, content, paths);
                } else {
                    webEmailService.sendSimpleWebEmail(to, subject, content);
                }
                emailBody.setWebEmailFlag("0");
            } else if (sendServiceType.equals("1")) {
                SysConfigInit sysConfigInit = new SysConfigInit();
                sysConfigInit.setOrgId(userInfo.getOrgId());
                sysConfigInit = sysConfigService.selectOneSysConfigInit(sysConfigInit);
                WebEmailService webEmailService = SpringUtils.getBean(WebEmailService.class);
                webEmailService.sendWebMailPersonByOrg(sysConfigInit, to, subject, content, paths);
                emailBody.setWebEmailFlag("1");
            } else if (sendServiceType.equals("2")) {
                EmailConfig emailConfig = new EmailConfig();
                emailConfig.setAccountId(userInfo.getAccountId());
                emailConfig.setOrgId(userInfo.getOrgId());
                emailConfig = emailConfigService.selectOneEmailConfig(emailConfig);
                WebEmailService webEmailService = SpringUtils.getBean(WebEmailService.class);
                webEmailService.sendWebMailPerson(emailConfig, to, subject, content, paths);
                emailBody.setWebEmailFlag("2");
            }
            emailBodyService.sendEmail(emailBody, userInfo);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

	/**
	 * 业务员客户联系人
	 * @param orgId
	 * @param keepUser
	 * @param keyword
	 * @return
	 */

    public List<Map<String, Object>> getMyCrmLinkManAllList(String orgId, String keepUser, String keyword) {
        return crmLinkManMapper.getMyCrmLinkManAllList(orgId, keepUser, "%" + keyword + "%");
    }

	/**
	 * 业务员客户联系人
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, Object>> getMyCrmLinkManAllList(PageParam pageParam) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, Object>> datalist = getMyCrmLinkManAllList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
