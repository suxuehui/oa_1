package com.core136.service.crm;

import com.core136.bean.crm.CrmOrderMx;
import com.core136.mapper.crm.CrmOrderMxMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class CrmOrderMxService {
	private CrmOrderMxMapper crmOrderMxMapper;
	@Autowired
	public void setCrmOrderMxMapper(CrmOrderMxMapper crmOrderMxMapper) {
		this.crmOrderMxMapper = crmOrderMxMapper;
	}

	public int insertCrmOrderMx(CrmOrderMx crmOrderMx)
	{
		return crmOrderMxMapper.insert(crmOrderMx);
	}

	public int deleteCrmOrderMx(CrmOrderMx crmOrderMx)
	{
		return crmOrderMxMapper.delete(crmOrderMx);
	}
	public int updateCrmOrderMx(Example example,CrmOrderMx crmOrderMx)
	{
		return crmOrderMxMapper.updateByExampleSelective(crmOrderMx,example);
	}
	public CrmOrderMx selectOneCrmOrderMx(CrmOrderMx crmOrderMx)
	{
		return crmOrderMxMapper.selectOne(crmOrderMx);
	}

	/**
	 * 获取订单明细
	 * @param orgId
	 * @param orderId
	 * @return
	 */
	public List<CrmOrderMx>getCrmOrderMxList(String orgId,String orderId)
	{
		CrmOrderMx crmOrderMx = new CrmOrderMx();
		crmOrderMx.setOrderId(orderId);
		crmOrderMx.setOrgId(orgId);
		return crmOrderMxMapper.select(crmOrderMx);
	}
}
