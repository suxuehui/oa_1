package com.core136.service.crm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.crm.CrmOrder;
import com.core136.bean.crm.CrmOrderMx;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmOrderMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 客户订单管理
 * @author lsq
 */
@Service
public class CrmOrderService {
    private CrmOrderMapper crmOrderMapper;
    private CrmOrderMxService crmOrderMxService;
	@Autowired
	public void setCrmOrderMapper(CrmOrderMapper crmOrderMapper) {
		this.crmOrderMapper = crmOrderMapper;
	}
	@Autowired
	public void setCrmOrderMxService(CrmOrderMxService crmOrderMxService) {
		this.crmOrderMxService = crmOrderMxService;
	}

	/**
	 * 获取订单详情
	 * @param crmOrder
	 * @return
	 */
	public CrmOrder selectOne(CrmOrder crmOrder) {
        return crmOrderMapper.selectOne(crmOrder);
    }

	/**
	 * 修改订单
	 * @param crmOrder
	 * @param example
	 * @return
	 */
	public int updateCrmOrder(CrmOrder crmOrder, Example example) {
        return crmOrderMapper.updateByExampleSelective(crmOrder, example);
    }
	public int deleteCrmOrder(CrmOrder crmOrder) {
		return crmOrderMapper.delete(crmOrder);
	}

	/**
	 * 删除订单
	 * @param crmOrder
	 * @return
	 */
	public int deleteCrmOrderAndMx(CrmOrder crmOrder) {
		CrmOrderMx crmOrderMx = new CrmOrderMx();
		crmOrderMx.setOrgId(crmOrder.getOrgId());
		crmOrderMx.setOrderId(crmOrder.getOrderId());
		crmOrderMxService.deleteCrmOrderMx(crmOrderMx);
        return crmOrderMapper.delete(crmOrder);
    }

	/**
	 * 创建订单
	 * @param crmOrder
	 * @return
	 */
	public int insertCrmOrder(CrmOrder crmOrder) {
        return crmOrderMapper.insert(crmOrder);
    }


	/**
	 * 更新订单记录
	 * @param crmOrder
	 * @param mx
	 * @return
	 */
	@Transactional(value = "generalTM")
	public RetDataBean updateCrmOrderAndMx(CrmOrder crmOrder,String mx)
	{
		CrmOrderMx tempCrmOrderMx = new CrmOrderMx();
		tempCrmOrderMx.setOrgId(crmOrder.getOrgId());
		tempCrmOrderMx.setOrderId(crmOrder.getOrderId());
		crmOrderMxService.deleteCrmOrderMx(tempCrmOrderMx);
		JSONArray jsonArray = JSON.parseArray(mx);
		for(int i=0;i<jsonArray.size();i++)
		{
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			CrmOrderMx crmOrderMx = new CrmOrderMx();
			crmOrderMx.setRecordId(SysTools.getGUID());
			crmOrderMx.setOrderId(crmOrder.getOrderId());
			crmOrderMx.setProductId(jsonObject.getString("productId"));
			crmOrderMx.setCount(jsonObject.getDouble("count"));
			crmOrderMx.setGuidePrice(jsonObject.getDouble("guidePrice"));
			crmOrderMx.setDelivery(jsonObject.getString("delivery"));
			crmOrderMx.setTotal(jsonObject.getDouble("total"));
			crmOrderMx.setUnit(jsonObject.getString("unit"));
			crmOrderMx.setMaterialCode(jsonObject.getString("materialCode"));
			crmOrderMx.setModel(jsonObject.getString("model"));
			crmOrderMx.setProductName(jsonObject.getString("productName"));
			crmOrderMx.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmOrderMx.setCreateUser(crmOrder.getCreateUser());
			crmOrderMx.setOrgId(crmOrder.getOrgId());
			crmOrderMxService.insertCrmOrderMx(crmOrderMx);
		}
		Example example = new Example(CrmOrder.class);
		example.createCriteria().andEqualTo("orgId", crmOrder.getOrgId()).andEqualTo("orderId", crmOrder.getOrderId());
		return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS,updateCrmOrder(crmOrder,example));
	}
	/**
	 * 设置订单产品明细
	 * @param crmOrder
	 * @param mx
	 * @return
	 */
	@Transactional(value = "generalTM")
    public RetDataBean addCrmOrder(CrmOrder crmOrder,String mx)
	{
		JSONArray jsonArray = JSON.parseArray(mx);
		for(int i=0;i<jsonArray.size();i++)
		{
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			CrmOrderMx crmOrderMx = new CrmOrderMx();
			crmOrderMx.setRecordId(SysTools.getGUID());
			crmOrderMx.setOrderId(crmOrder.getOrderId());
			crmOrderMx.setProductId(jsonObject.getString("productId"));
			crmOrderMx.setCount(jsonObject.getDouble("count"));
			crmOrderMx.setGuidePrice(jsonObject.getDouble("guidePrice"));
			crmOrderMx.setDelivery(jsonObject.getString("delivery"));
			crmOrderMx.setTotal(jsonObject.getDouble("total"));
			crmOrderMx.setUnit(jsonObject.getString("unit"));
			crmOrderMx.setMaterialCode(jsonObject.getString("materialCode"));
			crmOrderMx.setModel(jsonObject.getString("model"));
			crmOrderMx.setProductName(jsonObject.getString("productName"));
			crmOrderMx.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			crmOrderMx.setCreateUser(crmOrder.getCreateUser());
			crmOrderMxService.insertCrmOrderMx(crmOrderMx);
		}
		return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS,insertCrmOrder(crmOrder));
	}

	/**
	 * 获取订单列表
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param payType
	 * @param keyword
	 * @return
	 */
	public List<Map<String, String>> getCrmOrderList(String orgId, String opFlag, String accountId, String dateQueryType, String beginTime, String endTime, String status, String payType, String keyword) {
		return crmOrderMapper.getCrmOrderList(orgId,opFlag, accountId, dateQueryType,beginTime, endTime, status, payType,"%" + keyword + "%");
	}

	/**
	 * 获取订单列表 分页参数
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param payType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getCrmOrderList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String status, String payType) {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getCrmOrderList(pageParam.getOrgId(), pageParam.getOpFlag(),pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, payType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

}
