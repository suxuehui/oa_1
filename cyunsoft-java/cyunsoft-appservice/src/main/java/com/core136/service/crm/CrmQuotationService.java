package com.core136.service.crm;

import com.core136.bean.crm.CrmQuotation;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmQuotationMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CrmQuotationService {
    private CrmQuotationMapper crmQuotationMapper;
	@Autowired
	public void setCrmQuotationMapper(CrmQuotationMapper crmQuotationMapper) {
		this.crmQuotationMapper = crmQuotationMapper;
	}

	public int insertCrmQuotation(CrmQuotation crmQuotation) {
        return crmQuotationMapper.insert(crmQuotation);
    }

    public int deleteCrmQuotation(CrmQuotation crmQuotation) {
        return crmQuotationMapper.delete(crmQuotation);
    }

    public int updateCrmQuotation(Example example, CrmQuotation crmQuotation) {
        return crmQuotationMapper.updateByExampleSelective(crmQuotation, example);
    }

    public CrmQuotation selectOneCrmQuotation(CrmQuotation crmQuotation) {
        return crmQuotationMapper.selectOne(crmQuotation);
    }


	/**
	 * 获取报价列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 报价状态
	 * @param keyword 查询关键词
	 * @return 报价列表
	 */
    public List<Map<String, String>> getMyCrmQuotationList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
        return crmQuotationMapper.getMyCrmQuotationList(orgId, accountId, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

	/**
	 * 获取报价列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @return 报价列表
	 */
    public PageInfo<Map<String, String>> getMyCrmQuotationList(PageParam pageParam,String dateQueryType, String beginTime, String endTime, String status) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyCrmQuotationList(pageParam.getOrgId(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取审批列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param opFlag 管理员标识
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 审批列表
	 */
    public List<Map<String, String>> getMyApprovedList(String orgId, String opFlag,String accountId,String status, String keyword) {
        return crmQuotationMapper.getMyApprovedList(orgId, opFlag,accountId,status,"%" + keyword + "%");
    }

	/**
	 * 获取审批列表
	 * @param pageParam 分页参数
	 * @param status 审批状态
	 * @return 审批列表
	 */
    public PageInfo<Map<String, String>> getMyApprovedList(PageParam pageParam,String status) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyApprovedList(pageParam.getOrgId(),pageParam.getOpFlag(), pageParam.getAccountId(),status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }
	/**
	 * 获取报价单明细
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param quotationId 询价Id
	 * @return 报价单明细
	 */
	public Map<String,String>getCrmQuotationFormJsonById(String orgId,String accountId,String quotationId)
	{
		return crmQuotationMapper.getCrmQuotationFormJsonById(orgId,accountId,quotationId);
	}

	/**
	 * 获取报价单列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 报价单列表
	 */
	public List<Map<String, String>> getCrmQuotationListForSelect(String orgId, String accountId,String keyword) {
		if(StringUtils.isBlank(keyword))
		{
			keyword = "%%";
		}else
		{
			keyword = "%"+keyword+"%";
		}
		return crmQuotationMapper.getCrmQuotationListForSelect(orgId, accountId,keyword);
	}
}
