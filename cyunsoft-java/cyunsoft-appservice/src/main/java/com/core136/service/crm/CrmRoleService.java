package com.core136.service.crm;

import com.core136.bean.crm.CrmRole;
import com.core136.mapper.crm.CrmRoleMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * CRM权限服务类
 * @author lsq
 */
@Service
public class CrmRoleService {
    private CrmRoleMapper crmRoleMapper;
    private UserInfoService userInfoService;
	@Autowired
	public void setCrmRoleMapper(CrmRoleMapper crmRoleMapper) {
		this.crmRoleMapper = crmRoleMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertCrmRole(CrmRole crmRole) {
        return crmRoleMapper.insert(crmRole);
    }


    public int updateCrmRole(CrmRole crmRole, Example example) {
        return crmRoleMapper.updateByExampleSelective(crmRole, example);
    }

    public CrmRole selectOneCrmRole(CrmRole crmRole) {
        return crmRoleMapper.selectOne(crmRole);
    }

    public int selectCount(CrmRole crmRole) {
        return crmRoleMapper.selectCount(crmRole);
    }

    public int deleteCrmRole(CrmRole crmRole) {
        return crmRoleMapper.delete(crmRole);
    }

    /**
     * 获取所有销售业务人员,包含经理
     * @param orgId
     * @param keyword
     * @return
     */
    public List<Map<String, String>> getAllSender(String orgId, String keyword) {
        CrmRole crmRole = new CrmRole();
        crmRole.setOrgId(orgId);
        crmRole = this.selectOneCrmRole(crmRole);
        String accountS = "";
        if (StringUtils.isNotBlank(crmRole.getManager())) {
            accountS += "," + crmRole.getManager();
        }
        if (StringUtils.isNotBlank(crmRole.getSender())) {
            accountS += "," + crmRole.getSender();
        }
        if (StringUtils.isNotBlank(crmRole.getSalesman())) {
            accountS += "," + crmRole.getSalesman();
        }
        String[] accountId = null;
        if (StringUtils.isNotBlank(accountS)) {
            if (accountS.startsWith(",")) {
                accountS = accountS.substring(1);
            }
            if (accountS.endsWith(",")) {
                accountS = accountS.substring(0, accountS.length() - 1);
            }
            if (accountS.indexOf(",") > -1) {
                accountId = accountS.split(",");
            } else {
                accountId = new String[]{accountS};
            }
        }
        List<String> list = new ArrayList<>();
        for (String s : accountId) {
            if (StringUtils.isNotBlank(s)) {
                list.add(s);
            }
        }
        return userInfoService.getUserNamesByAccountIds(list, orgId);
    }

}
