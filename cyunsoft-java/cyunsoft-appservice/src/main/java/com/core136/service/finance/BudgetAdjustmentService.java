package com.core136.service.finance;

import com.core136.bean.finance.BudgetAdjustment;
import com.core136.bean.finance.BudgetProject;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.BudgetAdjustmentMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetAdjustmentService {
    private BudgetAdjustmentMapper budgetAdjustmentMapper;
    private BudgetProjectService budgetProjectService;
	@Autowired
	public void setBudgetAdjustmentMapper(BudgetAdjustmentMapper budgetAdjustmentMapper) {
		this.budgetAdjustmentMapper = budgetAdjustmentMapper;
	}
	@Autowired
	public void setBudgetProjectService(BudgetProjectService budgetProjectService) {
		this.budgetProjectService = budgetProjectService;
	}

	public int insertBudgetAdjustment(BudgetAdjustment BudgetAdjustment) {
        return budgetAdjustmentMapper.insert(BudgetAdjustment);
    }

    public int deleteBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
        return budgetAdjustmentMapper.delete(budgetAdjustment);
    }

    public int updateBudgetAdjustment(Example example, BudgetAdjustment budgetAdjustment) {
        return budgetAdjustmentMapper.updateByExampleSelective(budgetAdjustment, example);
    }


    public BudgetAdjustment selectOneBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
        return budgetAdjustmentMapper.selectOne(budgetAdjustment);
    }

	/**
	 * 审批时更新项目预算
	 * @param budgetAdjustment
	 * @return
	 */
	@Transactional(value = "generalTM")
    public int setBudgetAdjustmentApprovalStatus(BudgetAdjustment budgetAdjustment) {
        if (budgetAdjustment.getStatus().equals("1")) {
            BudgetAdjustment budgetAdjustment1 = new BudgetAdjustment();
            budgetAdjustment1.setRecordId(budgetAdjustment.getRecordId());
            budgetAdjustment1.setOrgId(budgetAdjustment.getOrgId());
            budgetAdjustment1 = selectOneBudgetAdjustment(budgetAdjustment1);
            BudgetProject budgetProject = new BudgetProject();
            budgetProject.setTotalCost(budgetAdjustment1.getNewTotalCost());
            Example example1 = new Example(BudgetProject.class);
            example1.createCriteria().andEqualTo("orgId", budgetAdjustment1.getOrgId()).andEqualTo("projectId", budgetAdjustment1.getProjectId());
            budgetProjectService.updateBudgetProject(example1, budgetProject);
        }
        budgetAdjustment.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
        Example example = new Example(BudgetAdjustment.class);
        example.createCriteria().andEqualTo("orgId", budgetAdjustment.getOrgId()).andEqualTo("recordId", budgetAdjustment.getRecordId());
        return updateBudgetAdjustment(example, budgetAdjustment);
    }


	/**
	 * 获取申请列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param status 申请状态
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getAdjustmentApplyList(String orgId, String opFlag, String accountId, String status, String projectId,String dateQueryType, String beginTime, String endTime, String keyword) {
        return budgetAdjustmentMapper.getAdjustmentApplyList(orgId, opFlag, accountId, status, projectId,dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取申请列表
	 * @param pageParam 分页参数
	 * @param status 申请状态
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getAdjustmentApplyList(PageParam pageParam, String status, String projectId, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAdjustmentApplyList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), status, projectId,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


	/**
	 * 获取待审批列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 待审批列表
	 */
    public List<Map<String, String>> getAdjustmentApprovalList(String orgId,String opFlag,String accountId,String status,String projectId, String dateQueryType, String beginTime, String endTime,String keyword) {
        return budgetAdjustmentMapper.getAdjustmentApprovalList(orgId, opFlag,accountId,status, projectId, dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 *  获取待审批列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 待审批列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getAdjustmentApprovalList(PageParam pageParam,String status, String projectId, String dateQueryType, String beginTime, String endTime) throws Exception {
       	PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAdjustmentApprovalList(pageParam.getOrgId(),pageParam.getOpFlag(), pageParam.getAccountId(), status,projectId, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
