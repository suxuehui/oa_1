package com.core136.service.finance;

import com.core136.bean.account.UserInfo;
import com.core136.bean.finance.BudgetConfig;
import com.core136.mapper.finance.BudgetConfigMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class BudgetConfigService {
    private BudgetConfigMapper budgetConfigMapper;
    private UserInfoService userInfoService;
	@Autowired
	public void setBudgetConfigMapper(BudgetConfigMapper budgetConfigMapper) {
		this.budgetConfigMapper = budgetConfigMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertBudgetConfig(BudgetConfig budgetConfig) {
        return budgetConfigMapper.insert(budgetConfig);
    }

    public int deleteBudgetConfig(BudgetConfig budgetConfig) {
        return budgetConfigMapper.delete(budgetConfig);
    }

    public int updateBudgetConfig(Example example, BudgetConfig budgetConfig) {
        return budgetConfigMapper.updateByExampleSelective(budgetConfig, example);
    }

    public BudgetConfig selectOneBudgetConfig(BudgetConfig budgetConfig) {
        return budgetConfigMapper.selectOne(budgetConfig);
    }

    public int getConfigCount(BudgetConfig budgetConfig) {
        return budgetConfigMapper.selectCount(budgetConfig);
    }

	/**
	 * 设置预算配置
	 * @param budgetConfig 预算配置对象
	 * @return 成功更新记录数
	 */
	public int setBudgetConfig(BudgetConfig budgetConfig) {
        BudgetConfig budgetConfig1 = new BudgetConfig();
        budgetConfig1.setOrgId(budgetConfig.getOrgId());
        if (getConfigCount(budgetConfig1) > 0) {
            Example example = new Example(BudgetConfig.class);
            example.createCriteria().andEqualTo("orgId", budgetConfig.getOrgId());
            return updateBudgetConfig(example, budgetConfig);
        } else {
            return insertBudgetConfig(budgetConfig);
        }
    }

	/**
	 * 获取预算调整审批人员列表
	 * @param userInfo 用户对象
	 * @return 审批人员列表
	 */
	public List<Map<String, String>> getAdjustmentApprovalUser(UserInfo userInfo) {
        List<Map<String, String>> returnList = new ArrayList<>();
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(userInfo.getOrgId());
        budgetConfig = selectOneBudgetConfig(budgetConfig);
        if (budgetConfig != null) {
            String accountStr = budgetConfig.getAdjustmentApprovalUser();
            if (StringUtils.isNotBlank(accountStr)) {
                String[] accountArr = accountStr.split(",");
                List<String> accounList = Arrays.asList(accountArr);
                returnList = userInfoService.getAllUserByAccountList(userInfo.getOrgId(), accounList);
            }
        }
        return returnList;
    }


	/**
	 * 获取费用预算申请审批人
	 * @param userInfo 用户对象
	 * @return 预算申请审批人列表
	 */
	public List<Map<String, String>> getCostApprovalUser(UserInfo userInfo) {
        List<Map<String, String>> returnList = new ArrayList<>();
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(userInfo.getOrgId());
        budgetConfig = selectOneBudgetConfig(budgetConfig);
        if (budgetConfig != null) {
            String accountStr = budgetConfig.getCostApprovalUser();
            if (StringUtils.isNotBlank(accountStr)) {
                String[] accountArr = accountStr.split(",");
                List<String> accounrList = Arrays.asList(accountArr);
                returnList = userInfoService.getAllUserByAccountList(userInfo.getOrgId(), accounrList);
            }
        }
        return returnList;
    }

}
