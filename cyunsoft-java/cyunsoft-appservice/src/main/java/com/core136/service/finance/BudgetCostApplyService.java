package com.core136.service.finance;


import com.core136.bean.finance.BudgetConfig;
import com.core136.bean.finance.BudgetCostApply;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.BudgetCostApplyMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetCostApplyService {
    private BudgetCostApplyMapper budgetCostApplyMapper;
    private BudgetConfigService budgetConfigService;
	@Autowired
	public void setBudgetCostApplyMapper(BudgetCostApplyMapper budgetCostApplyMapper) {
		this.budgetCostApplyMapper = budgetCostApplyMapper;
	}
	@Autowired
	public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
		this.budgetConfigService = budgetConfigService;
	}

	public int insertBudgetCostApply(BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.insert(budgetCostApply);
    }


    public int deleteBudgetCostApply(BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.delete(budgetCostApply);
    }

    public int updateBudgetCostApply(Example example, BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.updateByExampleSelective(budgetCostApply, example);
    }

    public BudgetCostApply selectOneBudgetCostApply(BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.selectOne(budgetCostApply);
    }

	/**
	 * 预算费用申请
	 * @param budgetCostApply 预算费用申请对象
	 * @return 消息结构
	 */
	public RetDataBean addBudgetCostApply(BudgetCostApply budgetCostApply) {
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(budgetConfig.getOrgId());
        budgetConfig = budgetConfigService.selectOneBudgetConfig(budgetConfig);
        if (budgetConfig.getCostApplayType().equals("1")) {
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertBudgetCostApply(budgetCostApply));
        }
        return RetDataTools.NotOk(MessageCode.MSG_00016);

    }

	/**
	 * 获取申请记录列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getBudgetCostApplyList(String orgId, String opFlag, String accountId, String projectId, String dateQueryType,String beginTime, String endTime, String status, String keyword) {
        return budgetCostApplyMapper.getBudgetCostApplyList(orgId, opFlag, accountId, projectId, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

	/**
	 * 获取申请记录列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @return 申请记录列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getBudgetCostApplyList(PageParam pageParam, String projectId, String dateQueryType,String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetCostApplyList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), projectId, dateQueryType,beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取预算费用审批记录
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 预算费用审批记录
	 */
    public List<Map<String, String>> getBudgetCostApprovalList(String orgId, String opFlag, String accountId, String projectId, String dateQueryType,String beginTime, String endTime, String status, String keyword) {
        return budgetCostApplyMapper.getBudgetCostApprovalList(orgId, opFlag, accountId, projectId, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

	/**
	 * 获取预算费用审批记录
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @return 预算费用审批记录
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getBudgetCostApprovalList(PageParam pageParam, String projectId, String dateQueryType,String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetCostApprovalList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), projectId, dateQueryType,beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 预算费用申请审批
	 * @param budgetCostApply
	 * @return
	 */
	public int setCostApprovalStatus(BudgetCostApply budgetCostApply) {
        budgetCostApply.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
        Example example = new Example(BudgetCostApply.class);
        example.createCriteria().andEqualTo("orgId", budgetCostApply.getOrgId()).andEqualTo("recordId", budgetCostApply.getRecordId());
        return updateBudgetCostApply(example, budgetCostApply);
    }

}
