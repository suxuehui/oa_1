package com.core136.service.finance;

import com.core136.bean.finance.BudgetConfig;
import com.core136.bean.finance.BudgetCost;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.BudgetCostMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetCostService {
    private BudgetConfigService budgetConfigService;
    private BudgetCostMapper budgetCostMapper;
	@Autowired
	public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
		this.budgetConfigService = budgetConfigService;
	}
	@Autowired
	public void setBudgetCostMapper(BudgetCostMapper budgetCostMapper) {
		this.budgetCostMapper = budgetCostMapper;
	}

	public int insertBudgetCost(BudgetCost budgetCost) {
        return budgetCostMapper.insert(budgetCost);
    }

    public int deleteBudgetCost(BudgetCost budgetCost) {
        return budgetCostMapper.delete(budgetCost);
    }

    public int updateBudgetCost(Example example, BudgetCost budgetCost) {
        return budgetCostMapper.updateByExampleSelective(budgetCost, example);
    }

    public BudgetCost selectOneBudgetCost(BudgetCost budgetCost) {
        return budgetCostMapper.selectOne(budgetCost);
    }

	/**
	 * 添加费用支出记录
	 * @param budgetCost 预算对象
	 * @return 消息结构
	 */
	public RetDataBean addBudgetCost(BudgetCost budgetCost) {
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(budgetCost.getOrgId());
        budgetConfig = budgetConfigService.selectOneBudgetConfig(budgetConfig);
        if (budgetConfig.getCostUpdateType().equals("1")) {
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertBudgetCost(budgetCost));
        }
        return RetDataTools.NotOk(MessageCode.MSG_00016);
    }


	/**
	 * 获取子项目费用支出列表
	 * @param orgId 机构码
	 * @param projectId 项目Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 关键词
	 * @return 费用支出列表
	 */
    public List<Map<String, String>> getChildBudgetCostList(String orgId, String projectId, String dateQueryType,String beginTime, String endTime, String keyword) {
        return budgetCostMapper.getChildBudgetCostList(orgId, projectId, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取子项目费用支出列表
	 * @param pageParam 分页参数
	 * @param projectId 项目Id
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 费用支出列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getChildBudgetCostList(PageParam pageParam, String projectId, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getChildBudgetCostList(pageParam.getOrgId(), projectId, dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取项目预算费用支出列表
	 * @param orgId 机构码
	 * @param projectId 项目Id
	 * @return 费用支出列表
	 */
    public List<Map<String, String>> getBudgetCostList(String orgId, String projectId) {
        return budgetCostMapper.getBudgetCostList(orgId, projectId);
    }

	/**
	 * 获取项目预算费用支出列表
	 * @param pageParam 分页参数
	 * @param parentId 父级Id
	 * @return 支出列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getBudgetCostList(PageParam pageParam, String parentId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetCostList(pageParam.getOrgId(), parentId);
		return new PageInfo<>(datalist);
    }


}
