package com.core136.service.finance;

import com.core136.bean.finance.ContractBill;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ContractBillMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractBillService {
    private ContractBillMapper contractBillMapper;
	@Autowired
	public void setContractBillMapper(ContractBillMapper contractBillMapper) {
		this.contractBillMapper = contractBillMapper;
	}

	public int insertContractBill(ContractBill contractBill) {
        return contractBillMapper.insert(contractBill);
    }

    public int deleteContractBill(ContractBill contractBill) {
        return contractBillMapper.delete(contractBill);
    }
    /**
     * 批量删除发票
     * @param orgId
     * @param list
     * @return
     */
    public RetDataBean deleteContractBillByIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(ContractBill.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("billId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, contractBillMapper.deleteByExample(example));
        }
    }
    public int updateContractBill(ContractBill contractBill, Example example) {
        return contractBillMapper.updateByExampleSelective(contractBill, example);
    }

    public ContractBill selectOneContractBill(ContractBill contractBill) {
        return contractBillMapper.selectOne(contractBill);
    }

	/**
	 * 获取发票列表
	 * @param orgId
	 * @param isOpen
	 * @param status
	 * @param billType
	 * @param dateQueryType
     * @param beginTime 开始时间
     * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getContractBillList(String orgId, String isOpen, String status, String billType,String dateQueryType,String beginTime, String endTime, String keyword) {
        return contractBillMapper.getContractBillList(orgId,isOpen, status, billType,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取发票列表
     * @param pageParam 分页参数
     * @param isOpen
     * @param status
     * @param billType
     * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getContractBillList(PageParam pageParam, String isOpen, String status, String billType,String dateQueryType, String beginTime, String endTime) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractBillList(pageParam.getOrgId(), isOpen, status, billType,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取近期发票列表
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getContractBillTop(String orgId) {
        return contractBillMapper.getContractBillTop(orgId);
    }
}
