package com.core136.service.finance;

import com.core136.bean.finance.ContractPayable;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ContractPayableMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractPayableService {
    private ContractPayableMapper contractPayableMapper;
	@Autowired
	public void setContractPayableMapper(ContractPayableMapper contractPayableMapper) {
		this.contractPayableMapper = contractPayableMapper;
	}

	public int insertContractPayable(ContractPayable contractPayable) {
        return contractPayableMapper.insert(contractPayable);
    }

    public int deleteContractPayable(ContractPayable contractPayable) {
        return contractPayableMapper.delete(contractPayable);
    }
    /**
     * 批量删除应付款
     * @param orgId 机构码
     * @param list 应付款Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteContractPayableByIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(ContractPayable.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("payableId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, contractPayableMapper.deleteByExample(example));
        }
    }
    public int updateContractPayable(ContractPayable contractPayable, Example example) {
        return contractPayableMapper.updateByExampleSelective(contractPayable, example);
    }

    public ContractPayable selectOneContractPayable(ContractPayable contractPayable) {
        return contractPayableMapper.selectOne(contractPayable);
    }

    /**
     * 获取应付款列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @param keyword 查询关键词
     * @return 应付款列表
     */
    public List<Map<String, String>> getContractPayableList(String orgId,  String accountId, String dateQueryType,String beginTime, String endTime, String status, String keyword) {
        return contractPayableMapper.getContractPayableList(orgId, accountId, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

    /**
     * 获取应付款列表
     * @param pageParam 分页参数
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status 状态
     * @return 应付款列表
     */
    public PageInfo<Map<String, String>> getContractPayableList(PageParam pageParam,  String dateQueryType,String beginTime, String endTime, String status) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractPayableList(pageParam.getOrgId(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
