package com.core136.service.finance;

import com.core136.bean.finance.ContractReceivables;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ContractReceivablesMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractReceivablesService {
    private ContractReceivablesMapper contractReceivablesMapper;
	@Autowired
	public void setContractReceivablesMapper(ContractReceivablesMapper contractReceivablesMapper) {
		this.contractReceivablesMapper = contractReceivablesMapper;
	}

	public int insertContractReceivables(ContractReceivables contractReceivables) {
        return contractReceivablesMapper.insert(contractReceivables);
    }

    public int deleteContractReceivables(ContractReceivables contractReceivables) {
        return contractReceivablesMapper.delete(contractReceivables);
    }

    public int updateContractReceivables(ContractReceivables contractReceivables, Example example) {
        return contractReceivablesMapper.updateByExampleSelective(contractReceivables, example);
    }

    public ContractReceivables selectOneContractReceivables(ContractReceivables contractReceivables) {
        return contractReceivablesMapper.selectOne(contractReceivables);
    }
    /**
     * 批量删除应收款
     * @param orgId 机构码
     * @param list 应收款Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteContractReceivablesByIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(ContractReceivables.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("receivablesId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, contractReceivablesMapper.deleteByExample(example));
        }
    }
    /**
     * 获取应收款列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @param keyword 关键词
     * @return 应收款列表
     */
    public List<Map<String, Object>> getContractReceivablesList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
        return contractReceivablesMapper.getContractReceivablesList(orgId, accountId,dateQueryType, beginTime, endTime, status, "%" + keyword + "%");
    }

	/**
	 * 获取应收款列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日蜞有范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @return 应收款列表
	 */
    public PageInfo<Map<String, Object>> getContractReceivablesList(PageParam pageParam,String dateQueryType, String beginTime, String endTime, String status) {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, Object>> datalist = getContractReceivablesList(pageParam.getOrgId(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
