package com.core136.service.finance;

import com.core136.bean.finance.ExpenseComConfig;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ExpenseComConfigMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExpenseComConfigService {
	private ExpenseComConfigMapper expenseComConfigMapper;
	@Autowired
	public void setExpenseComConfigMapper(ExpenseComConfigMapper expenseComConfigMapper) {
		this.expenseComConfigMapper = expenseComConfigMapper;
	}

	public int insertExpenseComConfig(ExpenseComConfig expenseComConfig) {
		return expenseComConfigMapper.insert(expenseComConfig);
	}

	public int deleteExpenseComConfig(ExpenseComConfig expenseComConfig) {
		return expenseComConfigMapper.delete(expenseComConfig);
	}

	public int updateExpenseComConfig(ExpenseComConfig expenseComConfig, Example example) {
		return expenseComConfigMapper.updateByExampleSelective(expenseComConfig,example);
	}

	public ExpenseComConfig selectOneExpenseComConfig(ExpenseComConfig expenseComConfig) {
		return expenseComConfigMapper.selectOne(expenseComConfig);
	}

	/**
	 * 批量删除公司年度费用预算
	 * @param orgId 机构码
	 * @param list 公司费用预算记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteExpenseComConfigByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ExpenseComConfig.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, expenseComConfigMapper.deleteByExample(example));
		}
	}

	/**
	 * 设置公司年度费用预算
	 * @param expenseComConfig 公司年度费用预算对象
	 * @return 消息结构
	 */
	public RetDataBean setExpenseComConfig(ExpenseComConfig expenseComConfig){
		ExpenseComConfig ecc = new ExpenseComConfig();
		ecc.setOrgId(expenseComConfig.getOrgId());
		ecc.setExpenseYear(expenseComConfig.getExpenseYear());
		if(expenseComConfigMapper.selectCount(ecc)>0) {
			Example example = new Example(ExpenseComConfig.class);
			example.createCriteria().andEqualTo("orgId",expenseComConfig.getOrgId()).andEqualTo("expenseYear",expenseComConfig.getExpenseYear());
			updateExpenseComConfig(expenseComConfig,example);
		}else{
			expenseComConfig.setRecordId(SysTools.getGUID());
			insertExpenseComConfig(expenseComConfig);
		}
		return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
	}

	/**
	 * 获取公司年度费用预算列表
	 * @param orgId 机构码
	 * @param expenseYear 年度
	 * @return 费用预算列表
	 */
	public List<Map<String,String>> getExpenseComConfigList(String orgId,String expenseYear) {
		return expenseComConfigMapper.getExpenseComConfigList(orgId,expenseYear);
	}

	/**
	 * 获取公司年度费用预算列表
	 * @param pageParam 分页参数
	 * @param expenseYear 年度
	 * @return 费用预算列表
	 */
	public PageInfo<Map<String, String>> getExpenseComConfigList(PageParam pageParam,String expenseYear){
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getExpenseComConfigList(pageParam.getOrgId(),expenseYear);
		return new PageInfo<>(datalist);
	}

}
