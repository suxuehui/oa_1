package com.core136.service.finance;

import com.core136.bean.finance.ExpenseDeptConfig;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.finance.ExpenseDeptConfigMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExpenseDeptConfigService {
	private ExpenseDeptConfigMapper expenseDeptConfigMapper;
	@Autowired
	public void setExpenseDeptConfigMapper(ExpenseDeptConfigMapper expenseDeptConfigMapper) {
		this.expenseDeptConfigMapper = expenseDeptConfigMapper;
	}

	public int insertExpenseDeptConfig(ExpenseDeptConfig expenseDeptConfig) {
		return expenseDeptConfigMapper.insert(expenseDeptConfig);
	}

	public int deleteExpenseDeptConfig(ExpenseDeptConfig expenseDeptConfig) {
		return expenseDeptConfigMapper.delete(expenseDeptConfig);
	}

	public int updateExpenseDeptConfig(Example example,ExpenseDeptConfig expenseDeptConfig) {
		return expenseDeptConfigMapper.updateByExampleSelective(expenseDeptConfig,example);
	}

	public ExpenseDeptConfig selectOneExpenseDeptConfig(ExpenseDeptConfig expenseDeptConfig) {
		return expenseDeptConfigMapper.selectOne(expenseDeptConfig);
	}

	/**
	 * 设置部门预算
	 * @param expenseDeptConfig 部门预算对象
	 * @return 消息结构
	 */
	public RetDataBean setExpenseDeptConfig(ExpenseDeptConfig expenseDeptConfig) {
		ExpenseDeptConfig td = new ExpenseDeptConfig();
		td.setOrgId(expenseDeptConfig.getOrgId());
		td.setDeptId(expenseDeptConfig.getDeptId());
		td.setExpenseYear(expenseDeptConfig.getExpenseYear());
		if(expenseDeptConfigMapper.selectCount(td)>0) {
			Example example = new Example(ExpenseDeptConfig.class);
			example.createCriteria().andEqualTo("orgId",expenseDeptConfig.getOrgId()).andEqualTo("deptId",expenseDeptConfig.getDeptId()).andEqualTo("expenseYear",expenseDeptConfig.getExpenseYear());
			updateExpenseDeptConfig(example,expenseDeptConfig);
		}else{
			expenseDeptConfig.setRecordId(SysTools.getGUID());
			expenseDeptConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			insertExpenseDeptConfig(expenseDeptConfig);
		}
		return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
	}

	/**
	 * 批量删除部门年底预算
	 * @param orgId 机构码
	 * @param list 部门年度预算记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteExpenseDeptConfigByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(ExpenseDeptConfig.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, expenseDeptConfigMapper.deleteByExample(example));
		}
	}

	public List<Map<String,String>> getExpenseDeptConfigList(String orgId, String deptId, String expenseYear) {
		return expenseDeptConfigMapper.getExpenseDeptConfigList(orgId,deptId,expenseYear);
	}

	public PageInfo<Map<String, String>> getExpenseDeptConfigList(PageParam pageParam, String deptId, String expenseYear){
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getExpenseDeptConfigList(pageParam.getOrgId(),deptId,expenseYear);
		return new PageInfo<>(datalist);
	}

}
