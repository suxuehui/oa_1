package com.core136.service.info;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UserInfo;
import com.core136.bean.info.AddressBook;
import com.core136.bean.system.SysDic;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.AddressBookMapper;
import com.core136.service.system.SysDicService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.*;

@Service
public class AddressBookService {
    private AddressBookMapper addressBookMapper;
	private SysDicService sysDicService;
	@Autowired
	public void setAddressBookMapper(AddressBookMapper addressBookMapper) {
		this.addressBookMapper = addressBookMapper;
	}
	@Autowired
	public void setSysDicService(SysDicService sysDicService) {
		this.sysDicService = sysDicService;
	}

	public int insertAddressBook(AddressBook addressBook) {
        return addressBookMapper.insert(addressBook);
    }

    public int deleteAddressBook(AddressBook addressBook) {
        return addressBookMapper.delete(addressBook);
    }

    public int updateAddressBook(Example example, AddressBook addressBook) {
        return addressBookMapper.updateByExampleSelective(addressBook, example);
    }

    public AddressBook selectOneAddressBook(AddressBook addressBook) {
        return addressBookMapper.selectOne(addressBook);
    }

    /**
     * 获取通讯录列表
     * @param orgId 机构码
     * @param accountId 用户对象
     * @return 通讯录列表
     */
	public List<JSONObject> getAddressBook(String orgId,String accountId,String bookType,String keyword) {
        Map<String, JSONArray> retMap = new HashMap<>();
        List<Map<String,String>> resUserList = addressBookMapper.getMyAddressBook(orgId,accountId,bookType,"%"+keyword+"%");
        for(Map<String,String>map:resUserList)
        {
            if(StringUtils.isNotBlank(map.get("pinYin"))) {
                String py = (map.get("pinYin").toUpperCase(Locale.ROOT)).substring(0, 1);
                if (retMap.get(py) == null) {
                    retMap.put(py, new JSONArray());
                }
                retMap.get(py).add(map);
            }else
            {
                if (retMap.get("#") == null) {
                    retMap.put("#", new JSONArray());
                }
                retMap.get("#").add(map);
            }
        }
        List<JSONObject> list = new ArrayList<>();
        for (String key : retMap.keySet()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("letter",key);
            jsonObject.put("list",retMap.get(key));
            list.add(jsonObject);
        }
        list.sort(new pinYinComparator());
        return list;
    }

    /**
     * 获取他人共享的人员列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param bookType 通讯录类型
     * @return 人员列表
     */
    public List<JSONObject> getMyShareAddressBookList(String orgId, String accountId, String bookType,String keyword) {

        Map<String, JSONArray> retMap = new HashMap<>();
        List<Map<String,String>> resUserList = addressBookMapper.getMyShareAddressBookList(orgId, accountId, bookType,"%"+keyword+"%");;
        for(Map<String,String>map:resUserList)
        {
            if(StringUtils.isNotBlank(map.get("pinYin"))) {
                String py = (map.get("pinYin").toUpperCase(Locale.ROOT)).substring(0, 1);
                if (retMap.get(py) == null) {
                    retMap.put(py, new JSONArray());
                }
                retMap.get(py).add(map);
            }else
            {
                if (retMap.get("#") == null) {
                    retMap.put("#", new JSONArray());
                }
                retMap.get("#").add(map);
            }
        }
        List<JSONObject> list = new ArrayList<>();
        for (String key : retMap.keySet()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("letter",key);
            jsonObject.put("list",retMap.get(key));
            list.add(jsonObject);
        }
        Collections.sort(list,new AddressBookService.pinYinComparator());
        return list;
    }

	/**
	 * 获取同事列表
	 * @param orgId 机构码
	 * @param levelId
	 * @return
	 */
    public List<JSONObject> getMyFriendsByLevel(String orgId, String levelId,String keyword) {
		Map<String, JSONArray> retMap = new HashMap<>();
		List<Map<String,String>> resUserList = addressBookMapper.getMyFriendsByLevel(orgId, levelId,"%"+keyword+"%");
		for(Map<String,String>map:resUserList) {
			if(StringUtils.isNotBlank(map.get("pinYin"))) {
				String py = (map.get("pinYin").toUpperCase(Locale.ROOT)).substring(0, 1);
				if (retMap.get(py) == null) {
					retMap.put(py, new JSONArray());
				}
				retMap.get(py).add(map);
			}else {
				if (retMap.get("#") == null) {
					retMap.put("#", new JSONArray());
				}
				retMap.get("#").add(map);
			}
		}
		List<JSONObject> list = new ArrayList<>();
		for (String key : retMap.keySet()) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("letter",key);
			jsonObject.put("list",retMap.get(key));
			list.add(jsonObject);
		}
		Collections.sort(list,new AddressBookService.pinYinComparator());
        return list;
    }

	/**
	 * 自定义流程列表排序，ASC
	 */
	class pinYinComparator implements Comparator<JSONObject> {
		@Override
		public int compare(JSONObject p1, JSONObject p2) {
			String pStr1 = p1.getString("letter");
			String pStr2 = p2.getString("letter");
			if(pStr1.equals("#")) {
				pStr1="a";
			}
			if(pStr2.equals("#")) {
				pStr2="a";
			}
			return pStr1.compareTo(pStr2);
		}
	}

	/**
	 * 获取同事联系方式
	 * @param orgId
	 * @param accountId
	 * @return
	 */
    public Map<String, String> getMyFriendsInfo(String orgId, String accountId) {
        return addressBookMapper.getMyFriendsInfo(orgId, accountId);
    }


    /**
     * 通讯录信息导入
     * @param user 用户对象
     * @param file 文件
     * @return 消息结构
     * @throws IOException Sql异常
     */
    @Transactional(value = "generalTM")
    public RetDataBean importAddressBookForExcel(UserInfo user, MultipartFile file) throws IOException {
		List<AddressBook> addressBookList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("姓名");
		titleList.add("性别");
		titleList.add("部门");
		titleList.add("职务");
		titleList.add("电话");
		titleList.add("手机号");
		titleList.add("邮件");
		titleList.add("地址");
		titleList.add("类型");
		titleList.add("是否共享");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			AddressBook addressBook = new AddressBook();
			addressBook.setRecordId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("姓名")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						addressBook.setUserName(tempMap.get(s));
						addressBook.setPinYin(StrTools.getPinYin(tempMap.get(s)));
					}else{
						resList.add("姓名不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("性别")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						if (tempMap.get(s).equals("男")) {
							addressBook.setSex("1");
						} else {
							addressBook.setSex("0");
						}
					} else {
						addressBook.setSex("2");
					}
				}
				if (s.equals("部门")) {
					addressBook.setDeptName(tempMap.get(s));
				}
				if (s.equals("职务")) {
					addressBook.setLevelName(tempMap.get(s));
				}
				if (s.equals("电话")) {
					addressBook.setTel(tempMap.get(s));
				}
				if (s.equals("手机号")) {
					addressBook.setMobileNo(tempMap.get(s));
				}
				if (s.equals("邮件")) {
					addressBook.setEmail(tempMap.get(s));
				}
				if (s.equals("地址")) {
					addressBook.setAddress(tempMap.get(s));
				}
				if (s.equals("类型")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						SysDic sysDic = new SysDic();
						sysDic.setOrgId(user.getOrgId());
						sysDic.setName(tempMap.get(s));
						sysDic.setCode("linkManType");
						try{
							sysDic = sysDicService.selectOneSysDic(sysDic);
							addressBook.setBookType(sysDic.getKeyValue());
						}catch (Exception e) {
							resList.add("查询类型不能为空!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("类型不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("是否共享")) {
					addressBook.setIsShare(tempMap.get(s));
				}
			}
			addressBook.setCreateTime(createTime);
			addressBook.setCreateUser(user.getAccountId());
			addressBook.setOrgId(user.getOrgId());
			if(insertFlag) {
				addressBookList.add(addressBook);
			}
		}
		for(AddressBook addressBook:addressBookList) {
			insertAddressBook(addressBook);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }

}
