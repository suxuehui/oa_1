package com.core136.service.info;


import com.core136.bean.account.UserInfo;
import com.core136.bean.info.BigStory;
import com.core136.bean.system.PageParam;
import com.core136.bean.system.SysDic;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.BigStoryMapper;
import com.core136.service.system.SysDicService;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 大记事服务类
 * @author lsq
 */
@Service
public class BigStoryService {
    private BigStoryMapper bigStoryMapper;
	private SysDicService sysDicService;
	@Autowired
	public void setBigStoryMapper(BigStoryMapper bigStoryMapper) {
		this.bigStoryMapper = bigStoryMapper;
	}
	@Autowired
	public void setSysDicService(SysDicService sysDicService) {
		this.sysDicService = sysDicService;
	}

	/**
	 * 创建大记事
	 * @param bigStory 大记事对象
	 * @return 成功创建记录数
	 */
    public int insertBigStory(BigStory bigStory) {
        return bigStoryMapper.insert(bigStory);
    }

	/**
	 * 删除大记事
	 * @param bigStory 大记事对象
	 * @return 成功删除记录数
	 */
    public int deleteBigStory(BigStory bigStory) {
        return bigStoryMapper.delete(bigStory);
    }

	/**
	 * 更新大记事
	 * @param example 更新条件
	 * @param bigStory 大记事对象
	 * @return 成功更新记录数
	 */
    public int updateBigStory(Example example, BigStory bigStory) {
        return bigStoryMapper.updateByExampleSelective(bigStory, example);
    }

	/**
	 * 查询单个大记事
	 * @param bigStory 大记事对象
	 * @return 大记事对象
	 */
    public BigStory selectOneBigStory(BigStory bigStory) {
        return bigStoryMapper.selectOne(bigStory);
    }

	/**
	 * 获取所有大记事
	 * @param orgId 机构码
	 * @return 大记事列表
	 */
	public List<Map<String, String>> getBigStoryList(String orgId) {
        return bigStoryMapper.getBigStoryList(orgId);
    }

	/**
	 * 获取大纪事管理列表
	 * @param orgId 机构码
	 * @param eventType 类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 大纪事列表
	 */
    public List<Map<String, String>> getBigStoryListForManage(String orgId, String eventType, String dateQueryType, String beginTime, String endTime, String keyword) {
        return bigStoryMapper.getBigStoryListForManage(orgId, eventType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取大纪事管理列表
	 * @param pageParam 分页参数
	 * @param eventType 类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 大纪事列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getBigStoryListForManage(PageParam pageParam, String eventType,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBigStoryListForManage(pageParam.getOrgId(), eventType,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 批量删除大记事
	 * @param orgId 机构码
	 * @param list 大记事Id 列表
	 * @return 成功删除记录数
	 */
	public RetDataBean deleteBigStoryByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(BigStory.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, bigStoryMapper.deleteByExample(example));
		}
	}

	/**
	 * 导入大记事
	 * @param user 用户对象
	 * @param file 文件
	 * @return 消息结构
	 * @throws IOException 服务器内部异常
	 */
    @Transactional(value = "generalTM")
    public RetDataBean importBigStory(UserInfo user, MultipartFile file) throws IOException {
		List<String> resList = new ArrayList<>();
		List<BigStory> bigStoryList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        List<String> titleList = new ArrayList<>();
		titleList.add("事件标题");
		titleList.add("事件日期");
		titleList.add("大纪事类型");
		titleList.add("事件摘要");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			BigStory bigStory = new BigStory();
			bigStory.setRecordId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("事件标题")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						bigStory.setTitle(tempMap.get(s));
					}else{
						resList.add("事件标题不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("事件日期")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						String happenYear = tempMap.get(s).substring(0, 4);
						bigStory.setHappenYear(happenYear);
						bigStory.setHappenTime(tempMap.get(s));
					}else{
						resList.add("事件日期不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("大纪事类型")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						SysDic sysDic = new SysDic();
						sysDic.setOrgId(user.getOrgId());
						sysDic.setCode("bigStoryType");
						sysDic.setName(tempMap.get(s));
						try{
							sysDic = sysDicService.selectOneSysDic(sysDic);
							if(sysDic!=null){
								bigStory.setEventType(sysDic.getKeyValue());
							}else{
								resList.add("大纪事类型不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e)
						{
							resList.add("大纪事类型查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("大纪事类型不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("事件摘要")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						Document htmlDoc = Jsoup.parse(tempMap.get(s));
						String subheading = htmlDoc.text();
						if (subheading.length() > 50) {
							subheading = subheading.substring(0, 50) + "...";
						}
						bigStory.setContent(tempMap.get(s));
						bigStory.setSubheading(subheading);
					}
				}
			}
			bigStory.setCreateTime(createTime);
			bigStory.setCreateUser(user.getAccountId());
			bigStory.setOrgId(user.getOrgId());
			if(insertFlag)
			{
				bigStoryList.add(bigStory);
			}
		}
		for(BigStory bigStory:bigStoryList){
			insertBigStory(bigStory);
		}
		if (resList.size() == 0) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }

}
