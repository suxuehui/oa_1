package com.core136.service.info;

import com.core136.bean.info.BookMark;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.BookMarkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class BookMarkService {
	private BookMarkMapper bookMarkMapper;
	@Autowired
	public void setBookMarkMapper(BookMarkMapper bookMarkMapper) {
		this.bookMarkMapper = bookMarkMapper;
	}

	public int insertBookMark(BookMark bookMark)
	{
		return bookMarkMapper.insert(bookMark);
	}
	public int deleteBookMark(BookMark bookMark)
	{
		return bookMarkMapper.delete(bookMark);
	}
	public int updateBookMark(Example example,BookMark bookMark)
	{
		return bookMarkMapper.updateByExampleSelective(bookMark,example);
	}

	public BookMark selectOneBookMark(BookMark bookMark)
	{
		return bookMarkMapper.selectOne(bookMark);
	}

	public int getBookMarkCount(BookMark bookMark)
	{
		return bookMarkMapper.selectCount(bookMark);
	}

	public int setBookMark(BookMark bookMark)
	{
		BookMark bookMark1 = new BookMark();
		bookMark1.setOrgId(bookMark.getOrgId());
		bookMark1.setCreateUser(bookMark.getCreateUser());
		if(getBookMarkCount(bookMark1)>0)
		{
			Example example = new Example(BookMark.class);
			example.createCriteria().andEqualTo("orgId",bookMark.getOrgId()).andEqualTo("createUser",bookMark.getCreateUser());
			return updateBookMark(example,bookMark);
		}else {
			bookMark.setRecordId(SysTools.getGUID());
			bookMark.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return insertBookMark(bookMark);
		}
	}


}
