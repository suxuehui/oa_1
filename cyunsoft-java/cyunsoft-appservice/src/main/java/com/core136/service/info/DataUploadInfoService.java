package com.core136.service.info;


import com.core136.bean.account.UserInfo;
import com.core136.bean.info.DataUploadInfo;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.DataUploadInfoMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * 数据上报服务类
 * @author lsq
 */
@Service
public class DataUploadInfoService {
    private DataUploadInfoMapper dataUploadInfoMapper;
    private UserInfoService userInfoService;
	@Autowired
	public void setDataUploadInfoMapper(DataUploadInfoMapper dataUploadInfoMapper) {
		this.dataUploadInfoMapper = dataUploadInfoMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	/**
	 * 创建上报数据
	 * @param dataUploadInfo 上报数据对象
	 * @return 成功创建记录数
	 */
    public int insertDataUploadInfo(DataUploadInfo dataUploadInfo) {
        return dataUploadInfoMapper.insert(dataUploadInfo);
    }

	/**
	 * 删除上报数据记录
	 * @param dataUploadInfo 上报数据对象
	 * @return 成功删除记录数
	 */
    public int deleteDataUploadInfo(DataUploadInfo dataUploadInfo) {
        return dataUploadInfoMapper.delete(dataUploadInfo);
    }

	/**
	 * 更新上报数据记录
	 * @param example 更新条件
	 * @param dataUploadInfo 上报数据对象
	 * @return 成功更新记录数
	 */
    public int updateDataUploadInfo(Example example, DataUploadInfo dataUploadInfo) {
        return dataUploadInfoMapper.updateByExampleSelective(dataUploadInfo, example);
    }

	/**
	 * 更新信息
	 * @param user 用户对象
	 * @param example 更新对象
	 * @param dataUploadInfo 上报数据对象
	 * @return 成功更新记录数
	 */
    public int updateDataUploadInfo(UserInfo user, Example example, DataUploadInfo dataUploadInfo) {
        if (StringUtils.isNotBlank(dataUploadInfo.getMsgType())) {
            String toUser = dataUploadInfo.getToUser();
            String approvedUser = dataUploadInfo.getApprovedUser();
            List<String> userList = new ArrayList<>();
            List<String> arr2 = new ArrayList<>();
            if (StringUtils.isNotBlank(toUser)) {
                userList = new ArrayList<>(Arrays.asList(toUser.split(",")));
            }
            if (StringUtils.isNotBlank(approvedUser)) {
                arr2 = new ArrayList<>(Arrays.asList(approvedUser.split(",")));
            }
            userList.addAll(arr2);
			Set<String> set = new HashSet<>(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
			userList.clear();
            userList.addAll(set);
            List<MsgBody> msgBodyList = new ArrayList<>();
			for (String accountId : userList) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(accountId);
				user2.setOrgId(user.getOrgId());
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("数据上报");
				msgBody.setContent("上报标题为：" + dataUploadInfo.getTitle() + "的查看提醒！");
				msgBody.setSendTime(dataUploadInfo.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setView(GlobalConstant.MSG_TYPE_UPLOAD_DATA);
				msgBody.setRecordId(dataUploadInfo.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(dataUploadInfo.getMsgType().split(","));
            MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
        return dataUploadInfoMapper.updateByExampleSelective(dataUploadInfo, example);
    }

	/**
	 * 查询单一上报数据记录
	 * @param dataUploadInfo 上报数据对象
	 * @return 上报数据对象
	 */
    public DataUploadInfo selectOneDataUploadInfo(DataUploadInfo dataUploadInfo) {
        return dataUploadInfoMapper.selectOne(dataUploadInfo);
    }

	/**
	 * 信息上报
	 * @param user 用户对象
	 * @param dataUploadInfo 上报数据对象
	 * @return 成功创建记录数
	 */
	public int dataUploadInfo(UserInfo user, DataUploadInfo dataUploadInfo) {

        if (StringUtils.isNotBlank(dataUploadInfo.getMsgType())) {
            String toUser = dataUploadInfo.getToUser();
            String approvedUser = dataUploadInfo.getApprovedUser();
            List<String> userList = new ArrayList<>();
            List<String> arr2 = new ArrayList<>();
            if (StringUtils.isNotBlank(toUser)) {
                userList = new ArrayList<>(Arrays.asList(toUser.split(",")));
            }
            if (StringUtils.isNotBlank(approvedUser)) {
                arr2 = new ArrayList<>(Arrays.asList(approvedUser.split(",")));
            }
            userList.addAll(arr2);
			Set<String> set = new HashSet<>(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
			userList.clear();
            userList.addAll(set);
            List<MsgBody> msgBodyList = new ArrayList<>();
			for (String accountId : userList) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(accountId);
				user2.setOrgId(user.getOrgId());
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("数据上报");
				msgBody.setContent("上报标题为：" + dataUploadInfo.getTitle() + "的查看提醒！");
				msgBody.setSendTime(dataUploadInfo.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setView(GlobalConstant.MSG_TYPE_UPLOAD_DATA);
				msgBody.setRecordId(dataUploadInfo.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(dataUploadInfo.getMsgType().split(","));
            MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
        return dataUploadInfoMapper.insert(dataUploadInfo);
    }
	/**
	 * 获取信息上报详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param recordId 上报记录数
	 * @return 信息详情
	 */
	public Map<String, String> getDataUploadInfoById(String orgId,String opFlag,String accountId,String deptId,String recordId) {
		return dataUploadInfoMapper.getDataUploadInfoById(orgId, opFlag, accountId,deptId, recordId);
	}
    /**
     * 获取上报信息列表
     * @param orgId 机构码
     * @param deptId 部辡Id
     * @param fromAccountId 上报账号
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param dataType 上报类型
     * @param approvedType 处理方式
     * @param keyword 查询关键词
     * @return 信息列表
     */
    public List<Map<String, String>> getDataUploadInfoList(String orgId, String deptId, String fromAccountId,String dateQueryType, String beginTime, String endTime, String dataType, String approvedType, String keyword) {
        return dataUploadInfoMapper.getDataUploadInfoList(orgId, deptId, fromAccountId,dateQueryType, beginTime, endTime, dataType, approvedType, "%" + keyword + "%");
    }

    /**
     * 获取上报信息列表
     * @param pageParam 分页参数
     * @param deptId 部门Id
     * @param fromAccountId 上报账号
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param dataType 上报类型
     * @param approvedType 处理方式
     * @return 信息列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getDataUploadInfoList(PageParam pageParam, String deptId, String fromAccountId, String dateQueryType,String beginTime, String endTime, String dataType, String approvedType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getDataUploadInfoList(pageParam.getOrgId(), deptId, fromAccountId,dateQueryType, beginTime, endTime, dataType, approvedType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取持处理的信息列表
     * @param orgId 要构码
     * @param accountId 用户账号
     * @param deptId 部门Id
     * @param fromAccountId 上报账号
     * @param dateQueryType 日期范围对象
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param dataType 上报类型
     * @param approvedType 处理方式
     * @param keyword 查询关键词
     * @return 信息列表
     */
    public List<Map<String, String>> getToProcessInfoList(String orgId, String accountId, String deptId, String fromAccountId,String status,String dateQueryType, String beginTime, String endTime, String dataType, String approvedType, String keyword) {
        return dataUploadInfoMapper.getToProcessInfoList(orgId, accountId, deptId, fromAccountId,status,dateQueryType, beginTime, endTime, dataType, approvedType, "%" + keyword + "%");
    }
    public List<Map<String, String>> getMyDataInfoForDesk(String orgId, String accountId) {
        return dataUploadInfoMapper.getMyDataInfoForDesk(orgId, accountId);
    }

    /**
     * 获取持处理的信息列表
     * @param pageParam 分页参数
     * @param deptId 部门Id
     * @param fromAccountId 上报账号
     * @param dateQueryType 日期范围对象
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param dataType 上报类型
	 * @param approvedType 处理方式
     * @return 信息列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getToProcessInfoList(PageParam pageParam, String deptId, String fromAccountId,String status, String dateQueryType, String beginTime, String endTime, String dataType, String approvedType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getToProcessInfoList(pageParam.getOrgId(), pageParam.getAccountId(), deptId, fromAccountId,status, dateQueryType,beginTime, endTime, dataType, approvedType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
