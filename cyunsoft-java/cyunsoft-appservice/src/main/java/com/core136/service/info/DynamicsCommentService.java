package com.core136.service.info;

import com.core136.bean.info.DynamicsComment;
import com.core136.mapper.info.DynamicsCommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DynamicsCommentService {
	private DynamicsCommentMapper dynamicsCommentMapper;
	@Autowired
	public void setDynamicsCommentMapper(DynamicsCommentMapper dynamicsCommentMapper) {
		this.dynamicsCommentMapper = dynamicsCommentMapper;
	}

	public int insertDynamicsComment(DynamicsComment dynamicsComment)
	{
		return dynamicsCommentMapper.insert(dynamicsComment);
	}

	public int deleteDynamicsComment(DynamicsComment dynamicsComment)
	{
		return dynamicsCommentMapper.delete(dynamicsComment);
	}

	public int updateDynamicsComment(Example example,DynamicsComment dynamicsComment)
	{
		return dynamicsCommentMapper.updateByExampleSelective(dynamicsComment,example);
	}

	public DynamicsComment selectOneDynamicsComment(DynamicsComment dynamicsComment)
	{
		return dynamicsCommentMapper.selectOne(dynamicsComment);
	}

	/**
	 * 获取动态评论列表
	 * @param orgId 机构码
	 * @param parentId 动态记录Id
	 * @return 评论列表
	 */
	public List<Map<String,String>> getDynamicsCommentList(String orgId, String parentId)
	{
		return dynamicsCommentMapper.getDynamicsCommentList(orgId,parentId);
	}

}
