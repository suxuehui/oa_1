package com.core136.service.info;

import com.core136.bean.info.Emergency;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.EmergencyMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 紧急事件操作服务类
 * @author lsq
 */
@Service
public class EmergencyService {
	private EmergencyMapper emergencyMapper;
	@Autowired
	public void setEmergencyMapper(EmergencyMapper emergencyMapper) {
		this.emergencyMapper = emergencyMapper;
	}

	/**
	 * 创建紧急事件
	 * @param emergency 紧急事件对象
	 * @return 成功创建记录数
	 */
	public int insertEmergency(Emergency emergency)
	{
		return emergencyMapper.insert(emergency);
	}

	/**
	 * 删除紧急事件
	 * @param emergency 紧急事件对象
	 * @return 成功删除记录数
	 */
	public int deleteEmergency(Emergency emergency)
	{
		return emergencyMapper.delete(emergency);
	}

	/**
	 * 更新紧急事件
	 * @param example 更新条件
	 * @param emergency 紧急事件对象
	 * @return 成功更新记录数
	 */
	public int updateEmergency(Example example,Emergency emergency)
	{
		return emergencyMapper.updateByExampleSelective(emergency,example);
	}

	/**
	 * 查询单个紧急事件记录
	 * @param emergency 紧急事件对象
	 * @return 紧急事件对象
	 */
	public Emergency selectOneEmergency(Emergency emergency)
	{
		return emergencyMapper.selectOne(emergency);
	}

	/**
	 * 批量删除紧急事件
	 * @param orgId 机构码
	 * @param list 事件Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteEmergencyByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(Emergency.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, emergencyMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取紧急事件列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param emergencyType 紧急事件类型
	 * @param keyword 查询关键词
	 * @return 紧急事件列表
	 */

	public List<Map<String,String>> getEmergencyListForManage(String orgId,String opFlag,String accountId,String dateQueryType,String beginTime,String endTime,String emergencyType,String keyword)
	{
		return emergencyMapper.getEmergencyListForManage(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,emergencyType,"%"+keyword+"%");
	}

	/**
	 * 获取紧急事件列表
	 * @param pageParam 分页参数
	 * @param emergencyType 紧急事件类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 紧急事件列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getEmergencyListForManage(PageParam pageParam, String emergencyType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getEmergencyListForManage(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), dateQueryType, beginTime, endTime, emergencyType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取当前紧急事件列表
	 * @param orgId 机构码
	 * @param accountId 用户对象
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @return 事件列表
	 */
	public List<Map<String,String>>getEmergencyListForDesk(String orgId,String accountId,String deptId,String userLevel)
	{
		String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		return emergencyMapper.getEmergencyListForDesk(orgId,nowTime,accountId,deptId,userLevel);
	}


}
