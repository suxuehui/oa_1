package com.core136.service.info;

import com.core136.bean.info.LeaderMailboxConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.LeaderMailboxConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class LeaderMailboxConfigService {
    private LeaderMailboxConfigMapper leaderMailboxConfigMapper;
	@Autowired
	public void setLeaderMailboxConfigMapper(LeaderMailboxConfigMapper leaderMailboxConfigMapper) {
		this.leaderMailboxConfigMapper = leaderMailboxConfigMapper;
	}

	public int insertLeaderMailboxConfig(LeaderMailboxConfig leaderMailboxConfig) {
        return leaderMailboxConfigMapper.insert(leaderMailboxConfig);
    }

    public int deleteLeaderMailboxConfig(LeaderMailboxConfig leaderMailboxConfig) {
        return leaderMailboxConfigMapper.delete(leaderMailboxConfig);
    }

    public int updateLeaderMailboxConfig(Example example, LeaderMailboxConfig leaderMailboxConfig) {
        return leaderMailboxConfigMapper.updateByExampleSelective(leaderMailboxConfig, example);
    }

    public LeaderMailboxConfig selectOneLeaderMailboxConfig(LeaderMailboxConfig leaderMailboxConfig) {
        return leaderMailboxConfigMapper.selectOne(leaderMailboxConfig);
    }

    public int getCount(LeaderMailboxConfig leaderMailboxConfig) {
        return leaderMailboxConfigMapper.selectCount(leaderMailboxConfig);
    }

	/**
	 * 设置领导信箱配置
	 * @param leaderMailboxConfig 领导信箱配置对象
	 * @return 消息结构
	 */
	public RetDataBean setLeaderMailboxConfig(LeaderMailboxConfig leaderMailboxConfig) {
        LeaderMailboxConfig tempLeaderMailboxConfig = new LeaderMailboxConfig();
        tempLeaderMailboxConfig.setOrgId(leaderMailboxConfig.getOrgId());
        int i = getCount(tempLeaderMailboxConfig);
        if (i == 0) {
            leaderMailboxConfig.setConfigId(SysTools.getGUID());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertLeaderMailboxConfig(leaderMailboxConfig));
        } else {
            Example example = new Example(LeaderMailboxConfig.class);
            example.createCriteria().andEqualTo("orgId", leaderMailboxConfig.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, updateLeaderMailboxConfig(example, leaderMailboxConfig));
        }

    }


}
