package com.core136.service.info;


import com.core136.bean.account.UserInfo;
import com.core136.bean.info.News;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.NewsMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * 单位新闻服务类
 * @author lsq
 */
@Service
public class NewsService {
	private NewsMapper newsMapper;
	private UserInfoService userInfoService;

	@Autowired
	public void setNewsMapper(NewsMapper newsMapper) {
		this.newsMapper = newsMapper;
	}

	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	/**
	 * 创建新闻
	 * @param news 单位新闻对象
	 * @return 成功创建记录数
	 */
	public int insertNews(News news) {
		return newsMapper.insert(news);
	}

	/**
	 * 删除新闻
	 * @param news 单位新闻对象
	 * @return 成功删除记录数
	 */
	public int deleteNews(News news) {
		return newsMapper.delete(news);
	}

	/**
	 * 批量删除新闻
	 *
	 * @param orgId 机构码
	 * @param list 新闻Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteNewsByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(News.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("newsId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, newsMapper.deleteByExample(example));
		}
	}

	/**
	 * 更新新闻
	 * @param news 单位新闻对象
	 * @param example 更新条件
	 * @return 成功创建记录数
	 */
	public int updateNews(News news, Example example) {
		return newsMapper.updateByExampleSelective(news, example);
	}

	/**
	 * 获取单个新闻详情
	 * @param news 单位新闻对象
	 * @return 单位新闻对象
	 */
	public News selectOneNews(News news) {
		return newsMapper.selectOne(news);
	}

	/**
	 * 按条件获取新闻列表
	 * @param example 查询条件
	 * @return 新闻列表
	 */
	public List<News> selectNewsList(Example example) {
		return newsMapper.selectByExample(example);
	}

	/**
	 * 发布新闻
	 * @param news
	 * @param user
	 * @return
	 */
	public int sendNews(News news, UserInfo user) {
		if (StringUtils.isNotBlank(news.getMsgType())) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(news.getOrgId(), news.getUserRole(), news.getDeptRole(), news.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(news.getCreateUser());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(news.getNewsTitle());
				msgBody.setContent(news.getSubheading());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_NEWS);
				msgBody.setRecordId(news.getNewsId());
				msgBody.setOrgId(news.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(news.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return insertNews(news);
	}

	/**
	 * 获取新闻详情
	 *
	 * @param orgId
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param newsId
	 * @return
	 */
	public Map<String, Object> getMyNewsById(String orgId, String opFlag, String accountId, String deptId, String userLevel, String newsId) {
		return newsMapper.getMyNewsById(orgId, opFlag, accountId, deptId, userLevel, newsId);
	}

	/**
	 * 获取新闻维护列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param newsType 新闻类型
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 新闻列表
	 */
	public List<Map<String, Object>> getNewsManageList(String orgId, String opFlag, String accountId, String newsType, String status, String dateQueryType, String beginTime, String endTime, String keyword) {
		return newsMapper.getNewsManageList(orgId, opFlag, accountId, newsType, status, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 新闻管理列表
	 * @param pageParam 分页参数
	 * @param newsType 新闻类型
	 * @param status 状态
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 新闻列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, Object>> getNewsManageList(PageParam pageParam, String newsType, String status, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, Object>> datalist = getNewsManageList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), newsType, status, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取个人权限内的新闻
	 *
	 * @param pageParam 分页参数
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别
	 * @param newsType 类闻类型
	 * @param status 状态标记
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 新闻列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, Object>> getMyNewsList(PageParam pageParam, String deptId, String userLevel, String newsType, String status, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, Object>> datalist = getMyNewsList(pageParam.getOrgId(), pageParam.getAccountId(), deptId, userLevel, newsType, status, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


	/**
	 * 获取个人权限内的新闻
	 *
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政别级Id
	 * @param newsType 新闻类型
	 * @param status 状诚
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 新闻列表
	 */

	public List<Map<String, Object>> getMyNewsList(String orgId, String accountId, String deptId, String userLevel, String newsType, String status, String dateQueryType, String beginTime, String endTime, String keyword) {
		return newsMapper.getMyNewsList(orgId, accountId, deptId, userLevel, newsType, status, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 阅读新闻
	 * @param user 用户对象
	 * @param newsId 新闻Id
	 * @return 消息结构
	 */
	public RetDataBean setNewsReadStatus(UserInfo user, String newsId) {
		News news = new News();
		news.setOrgId(user.getOrgId());
		news.setNewsId(newsId);
		news = selectOneNews(news);
		List<String> list = new ArrayList<>();
		if (StringUtils.isNotBlank(news.getReader())) {
			list = new ArrayList<>(Arrays.asList(news.getReader().split(",")));
		}
		list.add(user.getAccountId());
		HashSet<String> h = new HashSet<>(list);
		list.clear();
		list.addAll(h);
		Example example = new Example(News.class);
		example.createCriteria().andEqualTo("orgId", user.getOrgId()).andEqualTo("newsId", newsId);
		News news1 = new News();
		news1.setOnclickCount(news.getOnclickCount() + 1);
		news1.setReader(StringUtils.join(list, ","));
		return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, updateNews(news1, example));
	}

	/**
	 * 我的桌面上的新闻
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param levelId 行政级别Id
	 * @return 新闻列表
	 */
	public List<Map<String, String>> getMyNewsListForDesk(String orgId, String accountId, String deptId, String levelId) {
		return newsMapper.getMyNewsListForDesk(orgId, accountId, deptId, levelId);
	}

	/**
	 * 获取桌面图片新闻列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param levelId 行政级别Id
	 * @return 新闻列表
	 */
	public List<Map<String, String>> getMyPicNewsListForDesk(String orgId, String accountId, String deptId, String levelId) {
		List<Map<String, String>> resMapList = newsMapper.getMyPicNewsListForDesk(orgId, accountId, deptId, levelId);
		for (Map<String, String> map : resMapList) {
			Document htmlDoc = Jsoup.parse(map.get("content"));
			Elements els = htmlDoc.getElementsByTag("img");
			for (Element el : els) {
				String imgUrl = el.attr("src");
				if (StringUtils.isNotBlank(imgUrl)) {
					map.put("imgUrl", imgUrl);
					break;
				}
			}
		}
		return resMapList;
	}

	/**
	 * 获取移动端企业新闻列表
	 *
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param levelId 行政级别Id
	 * @param page 页码
	 * @return 企业新闻列表
	 */
	public List<Map<String, String>> getMyNewsListForApp(String orgId, String accountId, String deptId, String levelId, Integer page, String keyword) {
		return newsMapper.getMyNewsListForApp(orgId, accountId, deptId, levelId, page, "%" + keyword + "%");
	}

	/**
	 * 获取新闻人员查看状态
	 *
	 * @param orgId 机构码
	 * @param newsId 新闻Id
	 * @param list 用户账号Id 列表
	 * @return 人员查看状态
	 */
	public List<Map<String, String>> getNewsReadStatus(String orgId, String newsId, List<String> list) {
		return newsMapper.getNewsReadStatus(orgId, newsId, list);
	}

}
