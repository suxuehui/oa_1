package com.core136.service.info;


import com.core136.bean.info.NoticeConfig;
import com.core136.mapper.info.NoticeConfigMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 通知公告配置服务类
 * @author lsq
 */
@Service
public class NoticeConfigService {
    private NoticeConfigMapper noticeConfigMapper;
    private UserInfoService userInfoService;
	@Autowired
	public void setNoticeConfigMapper(NoticeConfigMapper noticeConfigMapper) {
		this.noticeConfigMapper = noticeConfigMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	/**
	 * 创建通知公告配置
	 * @param noticeConfig 通知公告配置对象
	 * @return 成功创建记录数
	 */
	public int insertNoticeConfig(NoticeConfig noticeConfig) {
        return noticeConfigMapper.insert(noticeConfig);
    }

	/**
	 * 查询单个通知公告配置
	 * @param noticeConfig 通知公告配置对象
	 * @return 通知公告配置对象
	 */
    public NoticeConfig selectOneNoticeConfig(NoticeConfig noticeConfig) {
        return noticeConfigMapper.selectOne(noticeConfig);
    }

    /**
     * 更新通知公告配置
	 * @param noticeConfig 通知公告配置对象
     * @param example 更新条件
     * @return 成功更新记录数
     */
    public int updateNoticeConfig(NoticeConfig noticeConfig, Example example) {
        return noticeConfigMapper.updateByExampleSelective(noticeConfig, example);
    }

	/**
	 * 判断是否存在配置
	 * @param orgId 机构码
	 * @param noticeType 通知公告类型
	 * @return 是否存在配置
	 */
    public boolean isExistConfig(String orgId,String noticeType)
	{
		NoticeConfig noticeConfig = new NoticeConfig();
		noticeConfig.setOrgId(orgId);
		noticeConfig.setNoticeType(noticeType);
		noticeConfig = noticeConfigMapper.selectOne(noticeConfig);
        return noticeConfig != null;
	}


	/**
	 * 获取通知公告配置列表
	 * @param orgId 机构码
	 * @return 公告配置列表
	 */
	public List<Map<String, String>> getNoticeConfigList(String orgId) {
		List<Map<String, String>> resList = noticeConfigMapper.getNoticeConfigList(orgId);
		for (Map<String, String> map : resList) {
			if (StringUtils.isNotBlank(map.get("approver"))) {
				List<String> list = Arrays.asList(map.get("approver").split(","));
				map.put("approverName", userInfoService.getUserNamesByAccountIds(orgId, list));
			}
			if (StringUtils.isNotBlank(map.get("notApprover"))) {
				List<String> list = Arrays.asList(map.get("notApprover").split(","));
				map.put("notApproverName", userInfoService.getUserNamesByAccountIds(orgId, list));
			}
		}

        return resList;
    }

}
