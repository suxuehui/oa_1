package com.core136.service.info;

import com.core136.bean.info.NoticeReceipt;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.NoticeReceiptMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class NoticeReceiptService {
	private NoticeReceiptMapper noticeReceiptMapper;
	@Autowired
	public void setNoticeReceiptMapper(NoticeReceiptMapper noticeReceiptMapper) {
		this.noticeReceiptMapper = noticeReceiptMapper;
	}

	public int insertNoticeReceipt(NoticeReceipt noticeReceipt)
	{
		return noticeReceiptMapper.insert(noticeReceipt);
	}

	public int deleteNoticeReceipt(NoticeReceipt noticeReceipt)
	{
		return noticeReceiptMapper.delete(noticeReceipt);
	}

	public int updateNoticeReceipt(Example example,NoticeReceipt noticeReceipt)
	{
		return noticeReceiptMapper.updateByExampleSelective(noticeReceipt,example);
	}

	public NoticeReceipt selectOneNoticeReceipt(NoticeReceipt noticeReceipt)
	{
		return noticeReceiptMapper.selectOne(noticeReceipt);
	}
	/**
	 * 按公告Id获取相关的回执列表
	 * @param orgId 机构码
	 * @param noticeId 通知公告Id
	 * @return 回执列表
	 */
	public List<Map<String,String>> getNoticeReceiptList(String orgId, String noticeId)
	{
		return noticeReceiptMapper.getNoticeReceiptList(orgId,noticeId);
	}

	/**
	 *  按公告Id获取相关的回执列表
	 * @param pageParam 分页参数
	 * @param noticeId 通知公告Id
	 * @return 回执列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getNoticeReceiptList(PageParam pageParam,String noticeId) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getNoticeReceiptList(pageParam.getOrgId(), noticeId);
		return new PageInfo<>(datalist);
	}

}
