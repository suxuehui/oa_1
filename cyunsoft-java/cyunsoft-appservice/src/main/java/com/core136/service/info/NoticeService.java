package com.core136.service.info;


import com.core136.bean.account.UserInfo;
import com.core136.bean.file.Attach;
import com.core136.bean.info.Notice;
import com.core136.bean.info.NoticeConfig;
import com.core136.bean.info.NoticeTemplate;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.NoticeMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.file.AttachService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.plexus.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.*;

/**
 * 通知公告服务类
 *
 * @author lsq
 */
@Service
public class NoticeService {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Value("${app.attachpath}")
	private String attachpath;
	private NoticeTemplateService noticeTemplateService;
	private AttachService attachService;
	private NoticeMapper noticeMapper;
	private NoticeConfigService noticeConfigService;
	private UserInfoService userInfoService;

	@Autowired
	public void setNoticeTemplateService(NoticeTemplateService noticeTemplateService) {
		this.noticeTemplateService = noticeTemplateService;
	}

	@Autowired
	public void setAttachService(AttachService attachService) {
		this.attachService = attachService;
	}

	@Autowired
	public void setNoticeMapper(NoticeMapper noticeMapper) {
		this.noticeMapper = noticeMapper;
	}

	@Autowired
	public void setNoticeConfigService(NoticeConfigService noticeConfigService) {
		this.noticeConfigService = noticeConfigService;
	}

	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertNotice(Notice notice) {
		return noticeMapper.insert(notice);
	}

	public int deleteNotice(Notice notice) {
		return noticeMapper.delete(notice);
	}

	public int updateNotice(Notice notice, Example example) {
		return noticeMapper.updateByExampleSelective(notice, example);
	}

	/**
	 * 更新通知公告状态
	 *
	 * @param user    用户对象
	 * @param notice  通知公告对象
	 * @param example 更新条件
	 * @return 公告状态
	 */
	public int setNoticeStatus(UserInfo user, Notice notice, Example example) {
		if (notice.getStatus().equals("1")) {
			Notice tempNotice = new Notice();
			tempNotice.setOrgId(user.getOrgId());
			tempNotice.setNoticeId(notice.getNoticeId());
			tempNotice = selectOneNotice(tempNotice);
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(tempNotice.getOrgId(), tempNotice.getUserRole(), tempNotice.getDeptRole(), tempNotice.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(tempNotice.getCreateUser());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(tempNotice.getNoticeTitle());
				msgBody.setContent(tempNotice.getSubheading());
				msgBody.setSendTime(tempNotice.getCreateTime());
				msgBody.setUserInfo(userInfo);
				msgBody.setAttachId(tempNotice.getAttachId());
				msgBody.setView(GlobalConstant.MSG_TYPE_NOTICE);
				msgBody.setRecordId(tempNotice.getNoticeId());
				msgBody.setOrgId(tempNotice.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(tempNotice.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return noticeMapper.updateByExampleSelective(notice, example);
	}

	/**
	 * 通知公告发布
	 *
	 * @param notice  通知公告对象
	 * @param example 更新条件
	 * @return 成功更新记录数
	 */
	public int reEditNotice(Notice notice, Example example) {
		NoticeConfig noticeConfig = new NoticeConfig();
		noticeConfig.setOrgId(notice.getOrgId());
		noticeConfig.setNoticeType(notice.getNoticeType());
		noticeConfig = noticeConfigService.selectOneNoticeConfig(noticeConfig);
		String createUser = notice.getCreateUser();
		if (noticeConfig == null) {
			notice.setStatus("1");
		} else {
			if (StringUtils.isNotBlank(noticeConfig.getNotApprover())) {
				if (("," + noticeConfig.getNotApprover() + ",").contains("," + createUser + ",")) {
					notice.setStatus("1");
				} else {
					if (StringUtils.isNotBlank(noticeConfig.getApprover())) {
						notice.setStatus("0");
					} else {
						notice.setStatus("1");
					}
				}
			} else {
				notice.setStatus("1");
			}
		}
		return noticeMapper.updateByExampleSelective(notice, example);
	}

	public Notice selectOneNotice(Notice notice) {
		return noticeMapper.selectOne(notice);
	}

	/**
	 * 获取通知公告的维护列表
	 *
	 * @param orgId      机构码
	 * @param opFlag     管理员标识
	 * @param accountId  用户账号
	 * @param noticeType 通知公告类型
	 * @param beginTime  开始时间
	 * @param endTime    结束时间
	 * @param keyword    查询关键间
	 * @return 维护列表
	 */
	public List<Map<String, Object>> getNoticeManageList(String orgId, String opFlag, String accountId, String noticeType, String dateQueryType, String beginTime, String endTime, String keyword) {
		return noticeMapper.getNoticeManageList(orgId, opFlag, accountId, noticeType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}


	/**
	 * 获取审批列表
	 *
	 * @param orgId         机构码
	 * @param accountId     用户对象
	 * @param noticeType    通知公告类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结束时间
	 * @param keyword       查询查询对象
	 * @return 审批列表
	 */
	public List<Map<String, Object>> getNoticeListForApproval(String orgId, String accountId, String noticeType, String dateQueryType, String beginTime, String endTime, String keyword) {
		return noticeMapper.getNoticeListForApproval(orgId, accountId, noticeType, dateQueryType, beginTime, endTime, "%" + keyword + "%");
	}

	/**
	 * 获取审批列表
	 *
	 * @param pageParam     分页参数
	 * @param noticeType    通知公告类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结束时间
	 * @return 审批列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, Object>> getNoticeListForApproval(PageParam pageParam, String noticeType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, Object>> datalist = getNoticeListForApproval(pageParam.getOrgId(), pageParam.getAccountId(), noticeType, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取通知公告的维护列表
	 *
	 * @param pageParam     分页参数
	 * @param noticeType    通知公告类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结束时间
	 * @return 公告的维护列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, Object>> getNoticeManageList(PageParam pageParam, String noticeType, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, Object>> datalist = getNoticeManageList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), noticeType, dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


	/**
	 * 按权限发布通知公告
	 *
	 * @param notice 通知公告对象
	 * @param user   用户对象
	 * @return 成功更新记录数
	 */
	public int sendNotice(Notice notice, UserInfo user) {
		NoticeConfig noticeConfig = new NoticeConfig();
		noticeConfig.setOrgId(notice.getOrgId());
		noticeConfig.setNoticeType(notice.getNoticeType());
		noticeConfig = noticeConfigService.selectOneNoticeConfig(noticeConfig);
		if (StringUtils.isNotBlank(notice.getRedHead())) {
			NoticeTemplate noticeTemplate = new NoticeTemplate();
			noticeTemplate.setOrgId(notice.getOrgId());
			noticeTemplate.setTemplateId(notice.getRedHead());
			noticeTemplate = noticeTemplateService.selectOneNoticeTemplate(noticeTemplate);
			Attach attach = new Attach();
			attach.setOrgId(user.getOrgId());
			attach.setAttachId(noticeTemplate.getAttachId());
			attach = attachService.selectOne(attach);
			try {
				String readHead = FileUtils.fileRead(attachpath + attach.getPath());
				notice.setContent(readHead + notice.getContent());
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
		String createUser = notice.getCreateUser();
		if (noticeConfig == null) {
			notice.setStatus("1");
		} else {
			if (StringUtils.isNotBlank(noticeConfig.getNotApprover())) {
				if (("," + noticeConfig.getNotApprover() + ",").contains("," + createUser + ",")) {
					notice.setStatus("1");
				} else {
					if (StringUtils.isNotBlank(noticeConfig.getApprover())) {
						notice.setStatus("0");
					} else {
						notice.setStatus("1");
					}
				}
			} else {
				notice.setStatus("1");
			}
		}
		int flag = insertNotice(notice);
		if (flag > 0 && (notice.getStatus().equals("1") && (StringUtils.isNotBlank(notice.getMsgType())))) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(notice.getOrgId(), notice.getUserRole(), notice.getDeptRole(), notice.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(notice.getCreateUser());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(notice.getNoticeTitle());
				msgBody.setContent(notice.getSubheading());
				msgBody.setSendTime(notice.getCreateTime());
				msgBody.setUserInfo(userInfo);
				msgBody.setAttachId(notice.getAttachId());
				msgBody.setView(GlobalConstant.MSG_TYPE_NOTICE);
				msgBody.setRecordId(notice.getNoticeId());
				msgBody.setOrgId(notice.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(notice.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);

		}
		return flag;
	}

	/**
	 * 设置通知公告查看状态
	 *
	 * @param user   用户对象
	 * @param notice 通知公告对象
	 * @return 消息结构
	 */
	public RetDataBean setNoticeReadStatus(UserInfo user, Notice notice) {
		notice = selectOneNotice(notice);
		List<String> list = new ArrayList<>();
		if (StringUtils.isNotBlank(notice.getReader())) {
			list = new ArrayList<>(Arrays.asList(notice.getReader().split(",")));
		}
		list.add(user.getAccountId());
		HashSet<String> h = new HashSet<>(list);
		list.clear();
		list.addAll(h);
		Notice notice1 = new Notice();
		if (StringUtils.isBlank(notice.getOnclickCount())) {
			notice1.setOnclickCount("1");
		} else {
			notice1.setOnclickCount(String.valueOf(Integer.parseInt(notice.getOnclickCount()) + 1));
		}
		notice1.setReader(StringUtils.join(list, ","));
		Example example = new Example(Notice.class);
		example.createCriteria().andEqualTo("orgId", notice.getOrgId()).andEqualTo("noticeId", notice.getNoticeId());
		updateNotice(notice1, example);
		return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, updateNotice(notice1, example));
	}

	/**
	 * 批量删除通知公告
	 *
	 * @param orgId 机构码
	 * @param list  通知公告Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteNoticeByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(Notice.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("noticeId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, noticeMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取个人的通知公告
	 *
	 * @param orgId      机构码
	 * @param accountId  用户账号
	 * @param deptId     部门Id
	 * @param levelId    行政级别Id
	 * @param noticeType 通知公告类型
	 * @param beginTime  开始时间
	 * @param endTime    结束时间
	 * @param status     状态
	 * @param keyword    查询关键词
	 * @return 通知公告列表
	 */
	public List<Map<String, String>> getMyNoticeList(String orgId, String accountId, String deptId, String levelId, String noticeType, String dateQueryType, String beginTime, String endTime, String status, String keyword
	) {
		return noticeMapper.getMyNoticeList(orgId, accountId, deptId, levelId, noticeType, dateQueryType, beginTime, endTime, status, "%" + keyword + "%");
	}

	/**
	 * 获取个人的通知公告
	 *
	 * @param pageParam     分页参数
	 * @param noticeType    通知公告类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结时时间
	 * @param status        状态
	 * @return 通知公告列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyNoticeList(PageParam pageParam, String noticeType, String dateQueryType, String beginTime, String endTime, String status) throws Exception {

		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyNoticeList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), noticeType, dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取桌面的通知消息
	 *
	 * @param orgId     机构码
	 * @param endTime   结束时间
	 * @param accountId 用户账号
	 * @param deptId    部门Id
	 * @param levelId   行政级别Id
	 * @return 通知公告列表
	 */
	public List<Map<String, String>> getMyNoticeListForDesk(String orgId, String endTime, String accountId, String deptId, String levelId) {
		return noticeMapper.getMyNoticeListForDesk(orgId, endTime, accountId, deptId, levelId);
	}

	/**
	 * 获取移动端通知公告
	 *
	 * @param orgId     机构码
	 * @param accountId 用户账号
	 * @param deptId    部门Id
	 * @param levelId   行政级别Id
	 * @param page      页码
	 * @param keyword   查询关键词
	 * @return 通知公告列表
	 */
	public List<Map<String, String>> getMyNoticeListForApp(String orgId, String accountId, String deptId, String levelId, Integer page, String keyword) {
		return noticeMapper.getMyNoticeListForApp(orgId, accountId, deptId, levelId, page, "%" + keyword + "%");
	}

	/**
	 * 通知公告人员查看状态
	 *
	 * @param orgId    机构码
	 * @param noticeId 通知公告Id
	 * @param list     用户账号Id 列表
	 * @return 人员查看状态列表
	 */
	public List<Map<String, String>> getNoticeReadStatus(String orgId, String noticeId, List<String> list) {
		if (!list.isEmpty()) {
			return noticeMapper.getNoticeReadStatus(orgId, noticeId, list);
		} else {
			return Collections.emptyList();
		}
	}

	/**
	 * 获取公告详情
	 *
	 * @param orgId     机构码
	 * @param opFlag    管理员标识
	 * @param accountId 用户账号
	 * @param deptId    部门Id
	 * @param userLevel 行政级别Id
	 * @param noticeId  通知公告Id
	 * @return 公告详情
	 */
	public Map<String, Object> getMyNoticeById(String orgId, String opFlag, String accountId, String deptId, String userLevel, String noticeId) {
		return noticeMapper.getMyNoticeById(orgId, opFlag, accountId, deptId, userLevel, noticeId);
	}

}
