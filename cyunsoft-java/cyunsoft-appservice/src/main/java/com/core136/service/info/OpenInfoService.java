package com.core136.service.info;

import com.core136.bean.info.OpenInfo;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.OpenInfoMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class OpenInfoService {
	private OpenInfoMapper openInfoMapper;
	@Autowired
	public void setOpenInfoMapper(OpenInfoMapper openInfoMapper) {
		this.openInfoMapper = openInfoMapper;
	}

	public int insertOpenInfo(OpenInfo openInfo)
	{
		return openInfoMapper.insert(openInfo);
	}

	public int deleteOpenInfo(OpenInfo openInfo)
	{
		return openInfoMapper.delete(openInfo);
	}

	public int updateOpenInfo(Example example,OpenInfo openInfo)
	{
		return openInfoMapper.updateByExampleSelective(openInfo,example);
	}

	public OpenInfo selectOneOpenInfo(OpenInfo openInfo)
	{
		return openInfoMapper.selectOne(openInfo);
	}

	/**
	 * 批量删除政务信息
	 * @param orgId 机构码
	 * @param list 公开政务信息Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteOpenInfoByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(OpenInfo.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, openInfoMapper.deleteByExample(example));
		}
	}
	/**
	 * 获取政务公开信息列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标记
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param infoType 信息公开类型
	 * @param status 信息态息
	 * @param keyword 查询关键词
	 * @return 信息列表
	 */
	List<Map<String,String>> getOpenInfoListForManage(String orgId, String opFlag, String accountId, String dateQueryType,String beginTime,String endTime,String infoType,String status,String keyword){
		return openInfoMapper.getOpenInfoListForManage(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,infoType,status,"%"+keyword+"%");
	}



	/**
	 * 获取政务公开信息列表
	 * @param pageParam 分页参数
	 * @param infoType 信息类型
	 * @param status 信息状态
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return  信息列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getOpenInfoListForManage(PageParam pageParam, String infoType, String status, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getOpenInfoListForManage(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), dateQueryType, beginTime, endTime,infoType,status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}
	/**
	 * 获取桌面模块政务公开例表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 政务公开信息列表
	 */
	public List<Map<String,String>> getOpenInfoListForDesk(String orgId,String accountId,String endTime){
		return openInfoMapper.getOpenInfoListForDesk(orgId,accountId,endTime);
	}

	/**
	 * 获取所有政务公开信息列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标记
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param infoType 信息公开类型
	 * @param readStatus 查阅状态
	 * @param keyword 查询关键词
	 * @return 信息列表
	 */
	List<Map<String,String>> getMyOpenInfoList(String orgId, String opFlag, String accountId, String dateQueryType,String beginTime,String endTime,String infoType,String readStatus,String keyword){
		return openInfoMapper.getMyOpenInfoList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,infoType,readStatus,"%"+keyword+"%");
	}
	/**
	 * 获取所有政务公开信息列表
	 * @param pageParam 分页参数
	 * @param infoType 信息类型
	 * @param readStatus 查阅状态
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return  信息列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyOpenInfoList(PageParam pageParam, String infoType, String readStatus, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyOpenInfoList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), dateQueryType, beginTime, endTime,infoType,readStatus, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 设置查阅人员
	 * @param orgId 机构码
	 * @param accountId 当前用户
	 * @param recordId 政务信息Id
	 */
	public void setReaderUser(String orgId,String accountId,String recordId){
		OpenInfo openInfo = new OpenInfo();
		openInfo.setOrgId(orgId);
		openInfo.setRecordId(recordId);
		openInfo = selectOneOpenInfo(openInfo);
		List<String> readerList = new ArrayList<>();
		readerList.add(accountId);
		if(StringUtils.isNotBlank(openInfo.getReader()))
		{
			readerList.addAll(Arrays.asList(openInfo.getReader().split(",")));
		}
		HashSet<String> set = new HashSet<>(readerList);
		Example example = new Example(OpenInfo.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("recordId",recordId);
		OpenInfo oi = new OpenInfo();
		oi.setReader(StringUtils.join(set,","));
		updateOpenInfo(example,oi);
	}
	/**
	 * 获取政务公开详情
	 * @param orgId 机构码
	 * @param recordId 信息Id
	 * @return 公开信息详情
	 */
	public Map<String,String>getOpenInfoById(String orgId,String recordId)
	{
		return openInfoMapper.getOpenInfoById(orgId,recordId);
	}

}
