package com.core136.service.info;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UserInfo;
import com.core136.bean.info.VoteResult;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.VoteResultMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class VoteResultService {
    private VoteResultMapper voteResultMapper;
    private UserInfoService userInfoService;

	@Autowired
	public void setVoteResultMapper(VoteResultMapper voteResultMapper) {
		this.voteResultMapper = voteResultMapper;
	}

	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertVoteResult(VoteResult voteResult) {
        return voteResultMapper.insert(voteResult);
    }

    public int deleteVoteResult(VoteResult voteResult) {
        return voteResultMapper.delete(voteResult);
    }

    public int updateVoteResult(Example example, VoteResult voteResult) {
        return voteResultMapper.updateByExampleSelective(voteResult, example);
    }

    public VoteResult selectOneVoteResult(VoteResult voteResult) {
        return voteResultMapper.selectOne(voteResult);
    }

	/**
	 * 获取投票结果
	 * @param orgId
	 * @param voteId
	 * @return
	 */
    public List<Map<String, String>> getVoteResult(String orgId, String voteId,String parentId) {
        return voteResultMapper.getVoteResult(orgId, voteId,parentId);
    }


	/**
	 * 判断是否已经投过票
	 * @param voteResult
	 * @return
	 */
	public int getVoteResultCount(VoteResult voteResult) {
        return voteResultMapper.selectCount(voteResult);
    }

	/**
	 * 人员投票
	 * @param user
	 * @param voteId
	 * @param result
	 * @return
	 */
    public RetDataBean addVoteResultRecord(UserInfo user, String voteId, String result) {
        if (StringUtils.isNotBlank(result)) {
            JSONArray jsonArray = JSON.parseArray(result);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                String selected = json.getString("selected");
                if(StringUtils.isNotBlank(selected)) {
					String[] selectArr = selected.split(",");
					for(String itemId:selectArr) {
						VoteResult voteResult = new VoteResult();
						voteResult.setRecordId(SysTools.getGUID());
						voteResult.setItemId(itemId);
						voteResult.setVoteId(voteId);
						voteResult.setParentId(json.getString("itemId"));
						voteResult.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
						voteResult.setCreateUser(user.getAccountId());
						voteResult.setOrgId(user.getOrgId());
						insertVoteResult(voteResult);
					}
				}
            }
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

	/**
	 * 获取投票人员列表
	 * @param orgId 机构码
	 * @param voteId 投票Id
	 * @param itemId
	 * @return
	 */
    public List<Map<String, String>> voteDetailsForUser(String orgId, String voteId, String itemId) {
        List<Map<String, String>> userList = voteResultMapper.voteDetailsForUser(orgId, voteId, itemId);
        List<String> list = new ArrayList<>();
		for (Map<String, String> map : userList) {
			list.add(map.get("createUser"));
		}
        if(userList.isEmpty()) {
			return Collections.emptyList();
		}
        return userInfoService.getUserNamesByAccountIds(list, orgId);
    }


}
