package com.core136.service.info;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.core136.bean.account.UserInfo;
import com.core136.bean.info.Vote;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.info.VoteMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class VoteService {
    private UserInfoService userInfoService;
    private VoteResultService voteResultService;
    private VoteMapper voteMapper;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setVoteResultService(VoteResultService voteResultService) {
		this.voteResultService = voteResultService;
	}
	@Autowired
	public void setVoteMapper(VoteMapper voteMapper) {
		this.voteMapper = voteMapper;
	}

	public int insertVote(Vote vote) {
        return voteMapper.insert(vote);
    }

    public int deleteVote(Vote vote) {
        return voteMapper.delete(vote);
    }

    public JSONArray getVoteResultByVoteId(Vote vote) {
		JSONArray itemJsonArr = JSON.parseArray(vote.getItemJson());
		for(int i=0;i<itemJsonArr.size();i++) {
			String parentId = itemJsonArr.getJSONObject(i).getString("itemId");
			itemJsonArr.getJSONObject(i).put("result",voteResultService.getVoteResult(vote.getOrgId(),vote.getVoteId(),parentId));
		}
		return itemJsonArr;
	}

	public List<Map<String, String>> getVoteStatus(String orgId, String voteId, List<String> list) {
		return voteMapper.getVoteStatus(orgId, voteId, list);
	}

	/**
	 * 批量删除投票
	 * @param orgId 机构码
	 * @param list 投票记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteVoteByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(Vote.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("voteId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, voteMapper.deleteByExample(example));
		}
	}


	public int updateVote(Example example, Vote vote) {
        return voteMapper.updateByExampleSelective(vote, example);
    }

	/**
	 * 获取投票详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 用户行政级别
	 * @param voteId 投票记录Id
	 * @return 投票详情
	 */
	public Map<String, String> getMyVoteById(String orgId, String opFlag,String accountId, String deptId,String userLevel,String voteId) {
		return voteMapper.getMyVoteById(orgId, opFlag, accountId, deptId,userLevel,voteId);
	}

	/**
	 * 消息提醒
	 * @param vote 投票对象
	 * @param user 用户对象
	 */
    public void sendMsgForUser(Vote vote, UserInfo user) {
        vote = selectOneVote(vote);
        if (StringUtils.isNotBlank(vote.getMsgType())) {
            List<MsgBody> msgBodyList = new ArrayList<>();
            List<UserInfo> accountList = userInfoService.getUserInRole(vote.getOrgId(), vote.getUserRole(), vote.getDeptRole(), vote.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(vote.getCreateUser());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(vote.getTitle());
				msgBody.setContent(vote.getSubheading());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_VOTE);
				msgBody.setRecordId(vote.getVoteId());
				msgBody.setOrgId(vote.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(vote.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
    }


    public Vote selectOneVote(Vote vote) {
        return voteMapper.selectOne(vote);
    }

	/**
	 * 获取投票管理列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param voteType 投票类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 投票状态
	 * @param keyword 查询关键词
	 * @return 管理列表
	 */
    public List<Map<String, String>> getVoteListForManage(String orgId, String opFlag, String accountId, String voteType,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
        return voteMapper.getVoteListForManage(orgId, opFlag, accountId, voteType, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

	/**
	 * 获取投票管理列表
	 * @param pageParam 分页参数
	 * @param voteType 投票类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 投票状态
	 * @return 投票管理列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getVoteListForManage(PageParam pageParam, String voteType,String dateQueryType, String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVoteListForManage(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), voteType,dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取历史投票列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param voteType 投票类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 投票状态
	 * @param keyword 查询关键词
	 * @return 历史投票列表
	 */
    public List<Map<String, String>> getMyOldVoteListForVote(String orgId, String accountId, String voteType, String dateQueryType,String beginTime, String endTime, String status, String keyword) {
        return voteMapper.getMyOldVoteListForVote(orgId, accountId, voteType, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

	/**
	 * 获取历史投票列表
	 * @param pageParam 分页参数
	 * @param voteType 投票类型
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 投票状态
	 * @return 历史投票列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyOldVoteListForVote(PageParam pageParam, String voteType,String dateQueryType, String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyOldVoteListForVote(pageParam.getOrgId(), pageParam.getAccountId(), voteType,dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取待我投票列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @return 待我投票列表
	 */
    public List<Map<String, String>> getMyVoteListForVote(String orgId, String accountId, String deptId, String userLevel) {
		String nowTime = SysTools.getTime("yyyy-MM-dd")+" 00:00";
        return voteMapper.getMyVoteListForVote(orgId, nowTime, accountId, deptId, userLevel);
    }


	public List<Map<String, String>> getMyVoteListForVoteForDesk(String orgId, String accountId, String deptId, String userLevel) {
		String nowTime = SysTools.getTime("yyyy-MM-dd")+" 00:00";
		return voteMapper.getMyVoteListForVoteForDesk(orgId, nowTime, accountId, deptId, userLevel);
	}

	/**
	 * 获取移动端个人投票列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param keyword 查询关键词
	 * @param page 页码
	 * @return 个人投票列表
	 */
	public List<Map<String,String>>getMyVoteListForApp(String orgId, String accountId, String deptId, String userLevel,String keyword,Integer page) {
		String nowTime = SysTools.getTime("yyyy-MM-dd")+" 00:00";
		return voteMapper.getMyVoteListForApp(orgId,accountId,deptId,userLevel,nowTime,"%"+keyword+"%",page);
	}


}
