package com.core136.service.invite;

import com.core136.bean.invite.InviteBidding;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteBiddingMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteBiddingService {
	private InviteBiddingMapper inviteBiddingMapper;
	@Autowired
	public void setInviteBiddingMapper(InviteBiddingMapper inviteBiddingMapper) {
		this.inviteBiddingMapper = inviteBiddingMapper;
	}

	public int insertInviteBidding(InviteBidding inviteBidding) {
		return inviteBiddingMapper.insert(inviteBidding);
	}

	public int deleteInviteBidding(InviteBidding inviteBidding) {
		return inviteBiddingMapper.delete(inviteBidding);
	}

	public int updateInviteBidding(Example example,InviteBidding inviteBidding) {
		return inviteBiddingMapper.updateByExampleSelective(inviteBidding,example);
	}

	public InviteBidding selectOneInviteBidding(InviteBidding inviteBidding) {
		return inviteBiddingMapper.selectOne(inviteBidding);
	}

	/**
	 * 批量删除招标记录
	 * @param orgId
	 * @param list
	 * @return
	 */
	public RetDataBean deleteInviteBiddingByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteBidding.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteBiddingMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取投标记录列表
	 * @param orgId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>> getInviteBiddingList(String orgId,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
		return inviteBiddingMapper.getInviteBiddingList(orgId,dateQueryType,beginTime,endTime,status,"%"+keyword+"%");
	}

	/**
	 * 获取待评招记录列表
	 * @param orgId
	 * @param accountId 用户账号
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
	public List<Map<String,String>> getInviteBiddingManageList(String orgId, String accountId,String dateQueryType, String beginTime, String endTime, String keyword) {
		return inviteBiddingMapper.getInviteBiddingManageList(orgId,accountId,dateQueryType,beginTime,endTime,"%"+keyword+"%");
	}


	/**
	 * 获取投标记录列表
	 * @param pageParam 分页参数
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getInviteBiddingList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String status) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteBiddingList(pageParam.getOrgId(),dateQueryType,beginTime,endTime,status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


	/**
	 * 获取待评招记录列表
	 * @param pageParam 分页参数
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getInviteBiddingManageList(PageParam pageParam, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteBiddingManageList(pageParam.getOrgId(),pageParam.getAccountId(),dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


}
