package com.core136.service.invite;

import com.core136.bean.invite.InviteDic;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteDicMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class InviteDicService {
    private InviteDicMapper inviteDicMapper;
	@Autowired
	public void setInviteDicMapper(InviteDicMapper inviteDicMapper) {
		this.inviteDicMapper = inviteDicMapper;
	}

	/**
	 * 创建字典
	 * @param inviteDic 字典对象
	 * @return 成功创建记录数
	 */
    public int insertInviteDic(InviteDic inviteDic)
    {
        return inviteDicMapper.insert(inviteDic);
    }

	/**
	 * 删除字典信息
	 * @param inviteDic 字典对象
	 * @return 成功删除记录数
	 */
    public int deleteInviteDic(InviteDic inviteDic)
    {
        return inviteDicMapper.delete(inviteDic);
    }

	/**
	 * 批量删除字典
	 * @param orgId 机构码
	 * @param list 字典记录ID 数组
	 * @return 成功删除记录数
	 */
	public RetDataBean deleteInviteDicByIds(String orgId, List<String> list) {
		if(!list.isEmpty()) {
			Example example = new Example(InviteDic.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("dicId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS,inviteDicMapper.deleteByExample(example));
		}else {
			return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
		}
	}

	/**
	 * 更新字典信息
	 * @param example 更新条件
	 * @param inviteDic 字典对象
	 * @return 成功更新数据记录
	 */
    public int updateInviteDic(Example example,InviteDic inviteDic)
    {
        return inviteDicMapper.updateByExampleSelective(inviteDic,example);
    }

	/**
	 * 字典排序
	 * @param orgId 机构码
	 * @param oldDicId 历史字典Id
	 * @param newDicId 新字典Id
	 * @param oldSortNo 历史排序号
	 * @param newSortNo 新历史排序号
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	public RetDataBean dropInviteDic(String orgId, String oldDicId, String newDicId, Integer oldSortNo, Integer newSortNo) {
		try {
			InviteDic oldInviteDic = new InviteDic();
			oldInviteDic.setSortNo(newSortNo);
			InviteDic newInviteDic = new InviteDic();
			newInviteDic.setSortNo(oldSortNo);
			Example example = new Example(InviteDic.class);
			example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",oldDicId);
			updateInviteDic(example,oldInviteDic);
			Example example1 = new Example(InviteDic.class);
			example1.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",newDicId);
			updateInviteDic(example1,newInviteDic);
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
		} catch (Exception e) {
			return RetDataTools.NotOk(e.getMessage());
		}
	}

	/**
	 * 判断字典分类下是否有子集
	 * @param orgId 机构码
	 * @param parentId 父级Id
	 * @return 是否有子集
	 */
	public boolean isExistChild(String orgId,String parentId) {
		InviteDic inviteDic = new InviteDic();
		inviteDic.setOrgId(orgId);
		inviteDic.setParentId(parentId);
		return inviteDicMapper.selectCount(inviteDic) > 0;
	}

	/**
	 * 获取分类对象
	 * @param inviteDic
	 * @return 分类对象
	 */
	public InviteDic selectOneInviteDic(InviteDic inviteDic) {
		return inviteDicMapper.selectOne(inviteDic);
	}

	/**
	 * 获取字典分类列表
	 * @param orgId 机构码
	 * @return 字典分类列表
	 */
	public List<Map<String,String>> getInviteDicParentList(String orgId) {
		return inviteDicMapper.getInviteDicParentList(orgId);
	}

	/**
	 *
	 * @param orgId 机构码
	 * @return 字典分类列表
	 */
	List<InviteDic> getAllInviteDic(String orgId) {
		Example example = new Example(InviteDic.class);
		example.createCriteria().andEqualTo("orgId",orgId);
		example.setOrderByClause("sort_no asc");
		return inviteDicMapper.selectByExample(example);
	}

	/**
	 * 获取字典列表数结构
	 * @param orgId 机构码
	 * @return 字典信息列表
	 */
	public List<InviteDic> getInviteDicTree(String orgId) {
		List<InviteDic> list =getAllInviteDic(orgId);
		List<InviteDic> inviteDicList = new ArrayList<>();
		for (InviteDic dic : list) {
			if (dic.getParentId().equals("0")) {
				inviteDicList.add(dic);
			}
		}
		for (InviteDic inviteDic : inviteDicList) {
			inviteDic.setChildren(getChild(inviteDic.getDicId(), list));
		}
		return inviteDicList;
	}

	/**
	 * 获取字典子集
	 * @param dicId 字典Id
	 * @param rootInviteDic 字典列表
	 * @return 字典子集
	 */
	private List<InviteDic> getChild(String dicId, List<InviteDic> rootInviteDic) {
		List<InviteDic> childList = new ArrayList<>();
		for (InviteDic inviteDic : rootInviteDic) {
			if (inviteDic.getParentId().equals(dicId)) {
				childList.add(inviteDic);
			}
		}
		for (InviteDic inviteDic : childList) {
			inviteDic.setChildren(getChild(inviteDic.getDicId(), rootInviteDic));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	/**
	 * 获取字典列表
	 * @param pageParam 分页参数
	 * @return 字典数据列表
	 */
	public PageInfo<Map<String,String>> getInviteDicList(PageParam pageParam, String parentId) throws Exception{
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteDicList(pageParam.getOrgId(),parentId,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取字典列表
	 * @param orgId 机构码
	 * @param parentId 父级Ids
	 * @param keyword 查谒关键词
	 * @return 字典列表
	 */
	public List<Map<String,String>> getInviteDicList(String orgId,String parentId,String keyword) {
		return inviteDicMapper.getInviteDicList(orgId,parentId,"%"+keyword+"%");
	}

	/**
	 * 按标识获取分类码列表
	 * @param orgId 机构码
	 * @param code 字典编码
	 * @return 字典信息列表
	 */

	public List<Map<String, Object>> getInviteDicByCode(String orgId, String code) {
		return inviteDicMapper.getInviteDicByCode(orgId, code);
	}

	/**
	 * 获取字典列表
	 * @param inviteDic 字典对象
	 * @return 字典列表
	 */
	public List<InviteDic> getInviteDicList(InviteDic inviteDic) {
		return inviteDicMapper.select(inviteDic);
	}

}
