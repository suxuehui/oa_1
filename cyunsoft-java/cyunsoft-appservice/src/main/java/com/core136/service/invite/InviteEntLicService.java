package com.core136.service.invite;

import com.core136.bean.invite.InviteEntLic;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteEntLicMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteEntLicService {
    private InviteEntLicMapper inviteEntLicMapper;
	@Autowired
	public void setInviteEntLicMapper(InviteEntLicMapper inviteEntLicMapper) {
		this.inviteEntLicMapper = inviteEntLicMapper;
	}

	public int insertInviteEntLic(InviteEntLic inviteEntLic)
    {
        return inviteEntLicMapper.insert(inviteEntLic);
    }

    public int deleteInviteEntLic(InviteEntLic inviteEntLic)
    {
        return inviteEntLicMapper.delete(inviteEntLic);
    }

    /**
     * 批量删除供应商证照
     * @param orgId 机构码
     * @param list 供应商证照Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteInviteEntLicByIds(String orgId, List<String> list) {
        if (list.isEmpty()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            Example example = new Example(InviteEntLic.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("licId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteEntLicMapper.deleteByExample(example));
        }
    }

    public int updateInviteEntLic(Example example,InviteEntLic inviteEntLic)
    {
        return inviteEntLicMapper.updateByExampleSelective(inviteEntLic,example);
    }

    public InviteEntLic selectOneInviteEntLic(InviteEntLic inviteEntLic)
    {
        return inviteEntLicMapper.selectOne(inviteEntLic);
    }

    /**
     * 按企业获取证照
     * @param orgId 机构码
     * @param entId 企业Id
     * @return 证照列表
     */
    public List<Map<String,String>>getInviteEntLicListByEntId(String orgId,String entId) {
        return inviteEntLicMapper.getInviteEntLicListByEntId(orgId,entId);
    }

    /**
     * 获取供应商证照列表
     * @param orgId 机构码
     * @param entId 企业Id
     * @param licType 证照类型
     * @param status 状态
     * @param keyword 查询关键词
     * @return 供应商证照列表
     */
    public List<Map<String,String>>getInviteEntLicList(String orgId,String entId, String licType, String status, String keyword)
    {
        return inviteEntLicMapper.getInviteEntLicList(orgId,entId,licType,status,"%"+keyword+"%");
    }

    /**
     * 获取供应商证照列表
     * @param pageParam 分页参数
     * @param entId 企业Id
     * @param licType 证照类型
     * @param status 状态
     * @return 供应商证照列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getInviteEntLicList(PageParam pageParam, String entId,String licType, String status) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getInviteEntLicList(pageParam.getOrgId(),entId,licType,status,pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }


}
