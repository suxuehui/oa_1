package com.core136.service.invite;

import com.core136.bean.invite.InviteEnt;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteEntMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class InviteEntService {
    private InviteEntMapper inviteEntMapper;
	@Autowired
	public void setInviteEntMapper(InviteEntMapper inviteEntMapper) {
		this.inviteEntMapper = inviteEntMapper;
	}

	public int insertInviteEnt(InviteEnt inviteEnt)
    {
        return inviteEntMapper.insert(inviteEnt);
    }

    public int deleteInviteEnt(InviteEnt inviteEnt)
    {
        return inviteEntMapper.delete(inviteEnt);
    }

    public int updateInviteEnt(Example example,InviteEnt inviteEnt)
    {
        return inviteEntMapper.updateByExampleSelective(inviteEnt,example);
    }

    public InviteEnt selectOneInviteEnt(InviteEnt inviteEnt)
    {
        return inviteEntMapper.selectOne(inviteEnt);
    }

    /**
     * 批量删除供应商
     * @param orgId 机构码
     * @param list 供应商Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteInviteEntByIds(String orgId, List<String> list) {
        if (list.isEmpty()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            Example example = new Example(InviteEnt.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("entId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteEntMapper.deleteByExample(example));
        }
    }

    /**
     * 获取供应商选择列表
     * @param orgId 机构码
     * @param keyword 查询关键词
     * @return 供应商选择列表
     */
    public List<Map<String,String>>getInviteEntListForSelect(String orgId,String keyword)
    {
        return inviteEntMapper.getInviteEntListForSelect(orgId,"%"+keyword+"%");
    }

    /**
     * 获取企业名称
     * @param orgId 机构码
     * @param entIds 企业Id 数组
     * @return 企业名称
     */
    public String getEntNameStrByEntId(String orgId,String entIds) {
        if(StringUtils.isBlank(entIds)) {
            return "";
        }else{
            List<String>entIdList = Arrays.asList(entIds.split(","));
            Example example = new Example(InviteEnt.class);
            example.createCriteria().andEqualTo("orgId",orgId).andIn("entId",entIdList);
            List<InviteEnt> list = inviteEntMapper.selectByExample(example);
            List<String> res = new ArrayList<>();
            for(InviteEnt inviteEnt:list) {
                res.add(inviteEnt.getName());
            }
            return StringUtils.join(res,",");
        }
    }

    /**
     * 获取供应商列表
     * @param orgId 机构码
     * @param entLevel 企业等级
     * @param entType 企业类型
     * @param status 状态
     * @param creditRating 信用等级
     * @param enterpriseSize 企业规模
     * @param keyword 查询关键词
     * @return 供应商列表
     */
    public List<Map<String,String>>getInviteEntList(String orgId, String entLevel, String entType, String status, String creditRating,String enterpriseSize,String keyword){
        return inviteEntMapper.getInviteEntList(orgId,entLevel,entType,status,creditRating,enterpriseSize,"%" + keyword + "%");
    }

    /**
     * 获取供应商列表
     * @param pageParam 分页参数
	 * @param entLevel 企业等级
	 * @param entType 企业类型
     * @param status 状态
     * @param creditRating 信用等级
     * @param enterpriseSize 企业规模
     * @return 供应商列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getInviteEntList(PageParam pageParam,String entLevel,String entType,String status,String creditRating,String enterpriseSize) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getInviteEntList(pageParam.getOrgId(),entLevel,entType,status,creditRating,enterpriseSize, pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }


}
