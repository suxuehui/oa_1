package com.core136.service.invite;

import com.core136.bean.invite.InviteFileSell;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteFileSellMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteFileSellService {
	private InviteFileSellMapper inviteFileSellMapper;
	@Autowired
	public void setInviteFileSellMapper(InviteFileSellMapper inviteFileSellMapper) {
		this.inviteFileSellMapper = inviteFileSellMapper;
	}

	public int insertInviteFileSell(InviteFileSell inviteFileSell) {
		return inviteFileSellMapper.insert(inviteFileSell);
	}

	public int deleteInviteFileSell(InviteFileSell inviteFileSell) {
		return inviteFileSellMapper.delete(inviteFileSell);
	}

	public int updateInviteFileSell(Example example,InviteFileSell inviteFileSell) {
		return inviteFileSellMapper.updateByExampleSelective(inviteFileSell,example);
	}

	public InviteFileSell selectOneInviteFileSell(InviteFileSell inviteFileSell) {
		return inviteFileSellMapper.selectOne(inviteFileSell);
	}

	/**
	 * 批量删除标书出售记录
	 * @param orgId 机构码
	 * @param list 标书出售记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteFileSellByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteFileSell.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteFileSellMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取招标文件出售记录
	 * @param orgId 机构码
	 * @param date 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param entId 企业Id
	 * @param keyword 查询关键词
	 * @return 文件出售记录列表
	 */
	public List<Map<String,String>>getInviteFileSellList(String orgId,String date,String beginTime,String endTime, String entId, String keyword) {
		return inviteFileSellMapper.getInviteFileSellList(orgId,date,beginTime,endTime,entId,"%"+keyword+"%");
	}

	/**
	 * 获取招标文件出售记录
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param entId 企业Id
	 * @return 文件出售记录列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getInviteFileSellList(PageParam pageParam,String date,String beginTime,String endTime, String entId) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteFileSellList(pageParam.getOrgId(),date,beginTime,endTime,entId,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


}
