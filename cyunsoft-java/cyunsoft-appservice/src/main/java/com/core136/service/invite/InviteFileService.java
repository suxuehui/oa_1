package com.core136.service.invite;

import com.core136.bean.invite.InviteFile;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.invite.InviteFileMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class InviteFileService {
	private InviteFileMapper inviteFileMapper;
	@Autowired
	public void setInviteFileMapper(InviteFileMapper inviteFileMapper) {
		this.inviteFileMapper = inviteFileMapper;
	}

	public int insertInviteFile(InviteFile inviteFile) {
		return inviteFileMapper.insert(inviteFile);
	}

	public int deleteInviteFile(InviteFile inviteFile) {
		return inviteFileMapper.delete(inviteFile);
	}

	public int updateInviteFile(Example example, InviteFile inviteFile) {
		return inviteFileMapper.updateByExampleSelective(inviteFile,example);
	}

	public InviteFile selectOneInviteFile(InviteFile inviteFile) {
		return inviteFileMapper.selectOne(inviteFile);
	}

	/**
	 * 批量删除招标文件
	 * @param orgId 机构码
	 * @param list 招标文件Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteInviteFileByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(InviteFile.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("fileId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, inviteFileMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取招标文件列表
	 * @param orgId 机构码
	 * @param status 文件状态
	 * @param fileType 文件类型
	 * @param keyword 查询关键词
	 * @return 文件列表
	 */
	public List<Map<String,String>> getInviteFileList(String orgId, String status, String fileType, String keyword) {
		return inviteFileMapper.getInviteFileList(orgId,status,fileType,"%"+keyword+"%");
	}

	/**
	 * 获取招标文件列表
	 * @param pageParam 分页参数
	 * @param status 文件状态
	 * @param fileType 文件类型
	 * @return 招标文件列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getInviteFileList(PageParam pageParam,String status, String fileType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getInviteFileList(pageParam.getOrgId(),status,fileType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取招标文件列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 文件列表
	 */
	public List<Map<String,String>>getInviteFileListForSelect(String orgId,String keyword) {
		return inviteFileMapper.getInviteFileListForSelect(orgId,"%"+keyword+"%");
	}


}
