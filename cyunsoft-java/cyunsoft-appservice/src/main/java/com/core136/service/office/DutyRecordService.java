package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.DutyRecord;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.DutyRecordMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class DutyRecordService {
	private DutyRecordMapper dutyRecordMapper;
	private UserInfoService userInfoService;
	@Autowired
	public void setDutyRecordMapper(DutyRecordMapper dutyRecordMapper) {
		this.dutyRecordMapper = dutyRecordMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertDutyRecord(DutyRecord dutyRecord) {
		return dutyRecordMapper.insert(dutyRecord);
	}

	public int deleteDutyRecord(DutyRecord dutyRecord) {
		return dutyRecordMapper.delete(dutyRecord);
	}

	public int updateDutyRecord(Example example,DutyRecord dutyRecord) {
		return dutyRecordMapper.updateByExampleSelective(dutyRecord,example);
	}

	/**
	 * 审批值班记录
	 * @param orgId 机构码
	 * @param recordId
	 * @param status
	 * @return
	 */
	public RetDataBean approvalDutyRecord(String orgId,String recordId,String status) {
		Example example = new Example(DutyRecord.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("recordId",recordId);
		DutyRecord dutyRecord = new DutyRecord();
		dutyRecord.setStatus(status);
		updateDutyRecord(example,dutyRecord);
		if(status.equals("1")) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			DutyRecord dutyRecord1 = new DutyRecord();
			dutyRecord1.setRecordId(recordId);
			dutyRecord1 = selectOneDutyRecord(dutyRecord1);
			if(StringUtils.isNotBlank(dutyRecord1.getMsgType())&&StringUtils.isNotBlank(dutyRecord1.getAccountId())) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(dutyRecord1.getAccountId());
				user2.setOrgId(orgId);
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("值班提醒");
				msgBody.setContent("您有一项值班为：" + dutyRecord1.getTitle() + "的查看提醒！");
				msgBody.setSendTime(dutyRecord1.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setFromAccountId(user2.getAccountId());
				msgBody.setFormUserName(user2.getUserName());
				msgBody.setView(GlobalConstant.MSG_TYPE_DUTY);
				msgBody.setRecordId(dutyRecord1.getRecordId());
				msgBody.setOrgId(user2.getOrgId());
				msgBodyList.add(msgBody);
			}
			if(!msgBodyList.isEmpty()) {
				List<String> msgTypeList = Arrays.asList(dutyRecord1.getMsgType().split(","));
				MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
				messageUnitService.sendMessage(msgTypeList, msgBodyList);
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		}else {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		}
	}


	public DutyRecord selectOneDutyRecord(DutyRecord dutyRecord) {
		return dutyRecordMapper.selectOne(dutyRecord);
	}

	public Map<String,String>getDutyRecordById(String orgId,String recordId) {
		return dutyRecordMapper.getDutyRecordById(orgId,recordId);
	}

	/**
	 * 获取值班列表
	 * @param orgId 机构码
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param sortId
	 * @param status
	 * @return
	 */
	public List<Map<String,String>> getDutyRecordList(String orgId, String dateQueryType, String beginTime, String endTime, String sortId, String status,String keyword) {
		return dutyRecordMapper.getDutyRecordList(orgId,dateQueryType,beginTime,endTime,sortId,status,"%" + keyword + "%");
	}

	/**
	 * 获取值班列表
	 * @param pageParam 分页参数
	 * @param sortId
	 * @param status
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getDutyRecordList(PageParam pageParam, String sortId,String status, String dateQueryType, String beginTime, String endTime) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getDutyRecordList(pageParam.getOrgId(),dateQueryType, beginTime, endTime,sortId, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}


	/**
	 * 获取值班待审批列表
	 * @param orgId 机构码
	 * @return
	 */
	public List<Map<String,String>> getDutyRecordListForApproval(String orgId) {
		return dutyRecordMapper.getDutyRecordListForApproval(orgId);
	}

	/**
	 * 获取值班待审批列表
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getDutyRecordListForApproval(PageParam pageParam) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getDutyRecordListForApproval(pageParam.getOrgId());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取当前值班列表
	 * @param orgId 机构码
	 * @param sortId
	 * @return
	 */
	public List<Map<String,String>> getDutyListBySortId(String orgId,String sortId) {
		String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm");
		return dutyRecordMapper.getDutyListBySortId(orgId,nowTime,sortId);
	}
}
