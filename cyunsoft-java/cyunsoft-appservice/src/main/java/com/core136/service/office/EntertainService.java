package com.core136.service.office;

import com.core136.bean.office.Entertain;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.EntertainMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


@Service
public class EntertainService {
	private EntertainMapper entertainMapper;
	@Autowired
	public void setEntertainMapper(EntertainMapper entertainMapper) {
		this.entertainMapper = entertainMapper;
	}

	public int insertEntertain(Entertain entertain) {
		return entertainMapper.insert(entertain);
	}

	public int deleteEntertain(Entertain entertain)
	{
		return entertainMapper.delete(entertain);
	}

	public int updateEntertain(Example example,Entertain entertain)
	{
		return entertainMapper.updateByExampleSelective(entertain,example);
	}

	public Entertain selectOneEntertain(Entertain entertain)
	{
		return entertainMapper.selectOne(entertain);
	}

	/**
	 * 批量删除招待记录
	 * @param orgId 机构码
	 * @param list 招待记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteEntertainByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(Entertain.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, entertainMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取接待事件列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryTime 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param diningType 用餐类型
	 * @param entertainType 招待类型
	 * @param keyword 查询关键词
	 * @return 接待事件列表
	 */
	public List<Map<String,String>>getEntertainListForManage(String orgId, String opFlag, String accountId,String dateQueryTime,String beginTime,String endTime,String status,String diningType,String entertainType,String keyword)
	{
		return entertainMapper.getEntertainListForManage(orgId,opFlag,accountId,dateQueryTime,beginTime,endTime,status,diningType,entertainType,"%"+keyword+"%");
	}

	/**
	 * 获取接待事件列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param diningType 用餐类型
	 * @param entertainType 招待类型
	 * @return 接待事件列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getEntertainListForManage(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String status,String diningType,String entertainType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getEntertainListForManage(pageParam.getOrgId(),pageParam.getOpFlag(),pageParam.getAccountId(),dateQueryType, beginTime, endTime, status,diningType,entertainType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取待审批事件
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param diningType 用餐类型
	 * @param entertainType 接待事件类型
	 * @param keyword 查询关键词
	 * @return 审批事件列表
	 */
	public List<Map<String,String>>getEntertainListForApprove(String orgId,String dateQueryType,String beginTime,String endTime,String diningType,String entertainType,String keyword)
	{
		return entertainMapper.getEntertainListForApprove(orgId,dateQueryType,beginTime,endTime,diningType,entertainType,"%"+keyword+"%");
	}

	/**
	 * 获取待审批事件
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param diningType 用餐类型
	 * @param entertainType 接待事件类型
	 * @return 事件列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getEntertainListForApprove(PageParam pageParam, String dateQueryType, String beginTime, String endTime,String diningType,String entertainType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getEntertainListForApprove(pageParam.getOrgId(),dateQueryType, beginTime, endTime,diningType,entertainType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 查询招待事件列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param diningType 用餐类型
	 * @param entertainType 接待事件类型
	 * @param keyword 查询关键词
	 * @return 事件列表
	 */
	public List<Map<String,String>>getEntertainListForSearch(String orgId,String dateQueryType,String beginTime,String endTime,String status,String diningType,String entertainType,String keyword)
	{
		return entertainMapper.getEntertainListForSearch(orgId,dateQueryType,beginTime,endTime,status,diningType,entertainType,"%"+keyword+"%");
	}

	/**
	 * 查询招待事件列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param diningType 用餐类型
	 * @param entertainType 接待事件类型
	 * @return 事件列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getEntertainListForSearch(PageParam pageParam, String dateQueryType, String beginTime, String endTime,String status,String diningType,String entertainType) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getEntertainListForSearch(pageParam.getOrgId(),dateQueryType, beginTime, endTime,status,diningType,entertainType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取桌面待办招待事件
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @return 招待事件列表
	 */
	public List<Map<String,String>>getMyEntertainListForDesk(String orgId,String opFlag,String accountId)
	{
		String nowTime = SysTools.getTime("yyyy-MM-dd");
		return entertainMapper.getMyEntertainListForDesk(orgId,opFlag,accountId,nowTime);
	}

}
