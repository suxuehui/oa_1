package com.core136.service.office;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.core136.bean.office.ExamQuestionRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.ExamQuestionRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


@Service
public class ExamQuestionRecordService {
    private ExamQuestionRecordMapper examQuestionRecordMapper;
	@Autowired
	public void setExamQuestionRecordMapper(ExamQuestionRecordMapper examQuestionRecordMapper) {
		this.examQuestionRecordMapper = examQuestionRecordMapper;
	}

	public int insertExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.insert(examQuestionRecord);
    }

    public int deleteExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.delete(examQuestionRecord);
    }

    public int updateExamQuestionRecord(Example example, ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.updateByExampleSelective(examQuestionRecord, example);
    }

    public ExamQuestionRecord selectOneExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.selectOne(examQuestionRecord);
    }
    /**
     * 获取试卷列表
     *
     * @param orgId 机构码
     * @param keyword 查询关键词
     * @return 试卷列表
     */
    public List<Map<String, String>> getExamQuestionRecordList(String orgId,String dateQueryType,String beginTime,String endTime,String sortId, String keyword) {
        return examQuestionRecordMapper.getExamQuestionRecordList(orgId, dateQueryType,beginTime,endTime,sortId,"%" + keyword + "%");
    }

	public JSONArray getExamQuestionListById(String orgId, String recordId) {
		ExamQuestionRecord examQuestionsRecord = new ExamQuestionRecord();
		examQuestionsRecord.setRecordId(recordId);
		examQuestionsRecord.setOrgId(orgId);
		examQuestionsRecord = selectOneExamQuestionRecord(examQuestionsRecord);
		return JSON.parseArray(examQuestionsRecord.getQuestionsList());
	}

    /**
     * 获取试卷列表
     *
     * @param pageParam 分页参数
     * @return
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getExamQuestionRecordList(PageParam pageParam,String dateQueryType,String beginTime,String endTime,String sortId) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamQuestionRecordList(pageParam.getOrgId(),dateQueryType,beginTime,endTime,sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取试卷列表
     *
     * @param orgId 机构码
     * @return
     */
    public List<Map<String, String>> getExamQuestionRecordForSelect(String orgId,String keyword) {
        return examQuestionRecordMapper.getExamQuestionRecordForSelect(orgId,"%"+keyword+"%");
    }

	/**
	 * 获取试卷列表
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getExamQuestionRecordForSelect(PageParam pageParam) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getExamQuestionRecordForSelect(pageParam.getOrgId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}
}
