package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.ExamTest;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.ExamTestMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ExamTestService {
    private ExamTestMapper examTestMapper;
	private UserInfoService userInfoService;
	@Autowired
	public void setExamTestMapper(ExamTestMapper examTestMapper) {
		this.examTestMapper = examTestMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertExamTest(ExamTest examTest) {
        return examTestMapper.insert(examTest);
    }

    public int deleteExamTest(ExamTest examTest) {
        return examTestMapper.delete(examTest);
    }

    public int updateExamTest(Example example, ExamTest examTest) {
        return examTestMapper.updateByExampleSelective(examTest, example);
    }

	public int addExamTest(UserInfo user, ExamTest examTest) {
		if (StringUtils.isNotBlank(examTest.getMsgType())) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), examTest.getUserRole(), examTest.getDeptRole(), examTest.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(examTest.getTitle());
				msgBody.setContent(examTest.getBeginTime() + "~" + examTest.getEndTime());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_EXAM_TEST);
				msgBody.setRecordId(examTest.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(examTest.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return examTestMapper.insert(examTest);
	}

	public int updateExamTest(UserInfo user, Example example, ExamTest examTest) {
		if (StringUtils.isNotBlank(examTest.getMsgType())) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), examTest.getUserRole(), examTest.getDeptRole(), examTest.getLevelRole());
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(examTest.getTitle());
				msgBody.setContent(examTest.getBeginTime() + "~" + examTest.getEndTime());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_EXAM_TEST);
				msgBody.setRecordId(examTest.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(examTest.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return examTestMapper.updateByExampleSelective(examTest, example);
	}

    public ExamTest selectOneExamTest(ExamTest examTest) {
        return examTestMapper.selectOne(examTest);
    }

    public List<Map<String, String>> getExamTestListForSelect(String orgId) {
        return examTestMapper.getExamTestListForSelect(orgId);
    }

	/**
	 * 批量删除试卷
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteExamTestByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(ExamTest.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, examTestMapper.deleteByExample(example));
		}
	}
    /**
     * 获取试卷管理列表
     *
     * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword
     * @return
     */
    public List<Map<String, String>> getExamTestManageList(String orgId, String dateQueryType,String beginTime, String endTime, String keyword) {
        return examTestMapper.getExamTestManageList(orgId,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取试卷管理列表
     *
     * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getExamTestManageList(PageParam pageParam, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamTestManageList(pageParam.getOrgId(),dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取个人考试表表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId
	 * @param levelId
	 * @return
	 */
    public List<Map<String, String>> getMyExamTestList(String orgId, String accountId, String deptId, String levelId) {
		String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm");
        return examTestMapper.getMyExamTestList(orgId, accountId, deptId, levelId, nowTime);
    }

	/**
	 * 获取考试详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId
	 * @param userLevel
	 * @param recordId
	 * @return
	 */
	public Map<String, String> getMyExamTestById(String orgId, String opFlag, String accountId, String deptId, String userLevel, String recordId) {
		String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm");
		return examTestMapper.getMyExamTestById(orgId, opFlag, accountId, deptId, userLevel, recordId,nowTime);
	}
}
