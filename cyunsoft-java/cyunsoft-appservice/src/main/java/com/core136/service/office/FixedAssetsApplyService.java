package com.core136.service.office;

import com.core136.bean.office.FixedAssetsApply;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.FixedAssetsApplyMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class FixedAssetsApplyService {
    private FixedAssetsApplyMapper fixedAssetsApplyMapper;
	@Autowired
	public void setFixedAssetsApplyMapper(FixedAssetsApplyMapper fixedAssetsApplyMapper) {
		this.fixedAssetsApplyMapper = fixedAssetsApplyMapper;
	}

	public int insertFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
        return fixedAssetsApplyMapper.insert(fixedAssetsApply);
    }

    public int deleteFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
        return fixedAssetsApplyMapper.delete(fixedAssetsApply);
    }

    public int updateFixedAssetsApply(Example example, FixedAssetsApply fixedAssetsApply) {
        return fixedAssetsApplyMapper.updateByExampleSelective(fixedAssetsApply, example);
    }

    public FixedAssetsApply selectOneFixedAssetsApply(FixedAssetsApply fixedAssetsApply) {
        return fixedAssetsApplyMapper.selectOne(fixedAssetsApply);
    }

    /**
     * 获取申请列表
     * @param orgId 机构码
     * @param status 状态
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param sortId 分类Id
     * @param keyword 查询关键词
     * @return 申请列表
     */
    public List<Map<String, String>> getFixedAssetsApplyList(String orgId, String status, String dateQueryType,String beginTime, String endTime, String sortId, String keyword) {
        return fixedAssetsApplyMapper.getFixedAssetsApplyList(orgId, status,dateQueryType, beginTime, endTime, sortId, "%" + keyword + "%");
    }
    public List<Map<String, String>> getFixedAssetsApplyOldList(String orgId, String status, String dateQueryType,String beginTime, String endTime, String sortId, String keyword) {
        return fixedAssetsApplyMapper.getFixedAssetsApplyOldList(orgId, status,dateQueryType, beginTime, endTime, sortId, "%" + keyword + "%");
    }

    /**
     * 获取申请列表
     * @param pageParam 分页参数
     * @param status 状态
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param sortId 分类Id
     * @return 申请列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getFixedAssetsApplyList(PageParam pageParam, String status, String dateQueryType,String beginTime, String endTime, String sortId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsApplyList(pageParam.getOrgId(), status,dateQueryType, beginTime, endTime, sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    public PageInfo<Map<String, String>> getFixedAssetsApplyOldList(PageParam pageParam, String status, String dateQueryType,String beginTime, String endTime, String sortId) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsApplyOldList(pageParam.getOrgId(), status,dateQueryType, beginTime, endTime, sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
