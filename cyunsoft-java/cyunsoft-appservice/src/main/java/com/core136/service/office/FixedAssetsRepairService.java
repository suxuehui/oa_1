package com.core136.service.office;

import com.core136.bean.office.FixedAssets;
import com.core136.bean.office.FixedAssetsRepair;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.FixedAssetsRepairMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class FixedAssetsRepairService {
    private FixedAssetsRepairMapper fixedAssetsRepairMapper;
    private FixedAssetsService fixedAssetsService;
	@Autowired
	public void setFixedAssetsRepairMapper(FixedAssetsRepairMapper fixedAssetsRepairMapper) {
		this.fixedAssetsRepairMapper = fixedAssetsRepairMapper;
	}
	@Autowired
	public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
		this.fixedAssetsService = fixedAssetsService;
	}

	public int insertFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.insert(fixedAssetsRepair);
    }

    public int deleteFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.delete(fixedAssetsRepair);
    }

    public int updateFixedAssetsRepair(Example example, FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.updateByExampleSelective(fixedAssetsRepair, example);
    }

    public FixedAssetsRepair selectOneFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        return fixedAssetsRepairMapper.selectOne(fixedAssetsRepair);
    }

    /**
     * 批理删除维修记录
     * @param orgId 机构码
     * @param list 维修记录Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteFixedAssetsRepairByIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(FixedAssetsRepair.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("repairId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, fixedAssetsRepairMapper.deleteByExample(example));
        }
    }

	/**
	 * 添加修改记录
	 * @param fixedAssetsRepair
	 * @return 消息结构
	 */
	public RetDataBean addFixedAssetsRepair(FixedAssetsRepair fixedAssetsRepair) {
        FixedAssets fixedAssets = new FixedAssets();
        fixedAssets.setAssetsCode(fixedAssetsRepair.getAssetsCode());
        fixedAssets.setOrgId(fixedAssetsRepair.getOrgId());
        try {
            fixedAssets = fixedAssetsService.selectOneFixedAssets(fixedAssets);
        } catch (Exception e) {
            return RetDataTools.NotOk(MessageCode.MSG_00032);
        }
        if (fixedAssets == null) {
            return RetDataTools.NotOk(MessageCode.MSG_00032);
        } else {
            fixedAssetsRepair.setAssetsId(fixedAssets.getAssetsId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertFixedAssetsRepair(fixedAssetsRepair));
        }
    }

    /**
     * 获取维修列表
     * @param orgId 机构码
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param sortId 分类Id
     * @param status 状态
     * @param keyword 查询关键词
     * @return 维修列表
     */
    public List<Map<String, String>> getFixedAssetsRepairList(String orgId,String dateQueryType, String beginTime, String endTime, String sortId, String status, String keyword) {
        return fixedAssetsRepairMapper.getFixedAssetsRepairList(orgId, dateQueryType, beginTime, endTime, sortId, status, "%" + keyword + "%");
    }

    /**
     * 获取维修列表
     * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param sortId 分类Id
     * @param status 状态
     * @return 维修列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getFixedAssetsRepairList(PageParam pageParam,String dateQueryType, String beginTime, String endTime, String sortId, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsRepairList(pageParam.getOrgId(),dateQueryType, beginTime, endTime, sortId, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
