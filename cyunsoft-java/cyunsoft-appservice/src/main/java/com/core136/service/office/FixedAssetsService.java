package com.core136.service.office;

import com.core136.bean.office.FixedAssets;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.FixedAssetsMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class FixedAssetsService {
    private FixedAssetsMapper fixedAssetsMapper;
	@Autowired
	public void setFixedAssetsMapper(FixedAssetsMapper fixedAssetsMapper) {
		this.fixedAssetsMapper = fixedAssetsMapper;
	}

	public int insertFixedAssets(FixedAssets fixedAssets) {
        return fixedAssetsMapper.insert(fixedAssets);
    }


    public int deleteFixedAssets(FixedAssets fixedAssets) {
        return fixedAssetsMapper.delete(fixedAssets);
    }

	public Map<String, String> getFixedAssetsById(String orgId, String assetsId) {
		return fixedAssetsMapper.getFixedAssetsById(orgId,assetsId);
	}

	/**
	 * 批量删除固定资产
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteFixedAssetsByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(FixedAssets.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("assetsId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, fixedAssetsMapper.deleteByExample(example));
		}
	}
    public int updateFixedAssets(Example example, FixedAssets fixedAssets) {
        return fixedAssetsMapper.updateByExampleSelective(fixedAssets, example);
    }

    public FixedAssets selectOneFixedAssets(FixedAssets fixedAssets) {
        return fixedAssetsMapper.selectOne(fixedAssets);
    }

	/**
	 * 固定资产列表
	 * @param orgId 机构码
	 * @param sortId
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getFixedAssetsList(String orgId, String sortId, String keyword) {
        return fixedAssetsMapper.getFixedAssetsList(orgId, sortId, "%" + keyword + "%");
    }

	/**
	 * 获取调拨列表
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param sortId
	 * @param applyUser
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getAllocationList(String orgId,String dateQueryType,String beginTime, String endTime, String sortId, String applyUser, String keyword) {
        return fixedAssetsMapper.getAllocationList(orgId,dateQueryType,beginTime, endTime, sortId, applyUser, "%" + keyword + "%");
    }

	/**
	 * 固定资产列表
	 * @param pageParam 分页参数
	 * @param sortId
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getFixedAssetsList(PageParam pageParam, String sortId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsList(pageParam.getOrgId(), sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取调拨列表
	 * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param sortId
	 * @param applyUser
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getAllocationList(PageParam pageParam, String dateQueryType,String beginTime, String endTime, String sortId, String applyUser) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAllocationList(pageParam.getOrgId(),dateQueryType, beginTime, endTime, sortId, applyUser, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 查询固定资产列表
	 * @param orgId 机构码
	 * @param sortId
	 * @param ownDept
	 * @param status
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> queryFixedAssetsList(String orgId,String sortId, String ownDept, String status, String keyword) {
        return fixedAssetsMapper.queryFixedAssetsList(orgId,sortId, ownDept, status, "%" + keyword + "%");
    }

	/**
	 * 查询固定资产列表
	 * @param pageParam 分页参数
	 * @param sortId
	 * @param ownDept
	 * @param status
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> queryFixedAssetsList(PageParam pageParam, String sortId, String ownDept, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = queryFixedAssetsList(pageParam.getOrgId(),sortId, ownDept, status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取可申请的固定资产的列表
	 * @param orgId 机构码
	 * @param sortId
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getApplyFixedAssetsList(String orgId, String sortId, String keyword) {
        return fixedAssetsMapper.getApplyFixedAssetsList(orgId, sortId, "%" + keyword + "%");
    }

	/**
	 * 获取可申请的固定资产的列表
	 * @param pageParam 分页参数
	 * @param sortId
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getApplyFixedAssetsList(PageParam pageParam, String sortId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getApplyFixedAssetsList(pageParam.getOrgId(), sortId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }
}
