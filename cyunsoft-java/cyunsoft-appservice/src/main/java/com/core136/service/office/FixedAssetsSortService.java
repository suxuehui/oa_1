package com.core136.service.office;

import com.core136.bean.office.FixedAssetsSort;
import com.core136.mapper.office.FixedAssetsSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author lsq
 */

@Service
public class FixedAssetsSortService {
    private FixedAssetsSortMapper fixedAssetsSortMapper;
	@Autowired
	public void setFixedAssetsSortMapper(FixedAssetsSortMapper fixedAssetsSortMapper) {
		this.fixedAssetsSortMapper = fixedAssetsSortMapper;
	}

	public int insertFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
        return fixedAssetsSortMapper.insert(fixedAssetsSort);
    }

    public int deleteFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
        return fixedAssetsSortMapper.delete(fixedAssetsSort);
    }

    public int updateFixedAssetsSort(Example example, FixedAssetsSort fixedAssetsSort) {
        return fixedAssetsSortMapper.updateByExampleSelective(fixedAssetsSort, example);
    }

    public FixedAssetsSort selectOneFixedAssetsSort(FixedAssetsSort fixedAssetsSort) {
        return fixedAssetsSortMapper.selectOne(fixedAssetsSort);
    }


	/**
	 * 获取固定资产分类
	 * @param orgId 机构码
	 * @return
	 */
	public List<FixedAssetsSort> getFixedAssetsSortTree(String orgId) {
		List<FixedAssetsSort> list = getAllFixedAssetsSort(orgId);
		List<FixedAssetsSort> fixedAssetsSortList = new ArrayList<>();
		for (FixedAssetsSort fixedAssetsSort : list) {
			if (fixedAssetsSort.getParentId().equals("0")) {
				fixedAssetsSortList.add(fixedAssetsSort);
			}
		}
		for (FixedAssetsSort officeSuppliesSort : fixedAssetsSortList) {
			officeSuppliesSort.setChildren(getChildFixedAssetsSortList(officeSuppliesSort.getSortId(), list));
		}
		return fixedAssetsSortList;
	}

	/**
	 * 获取分类子表
	 * @param sortId 分类Id
	 * @param rootFixedAssetsSortSort 固定资产分类对象列表
	 * @return 分类子表
	 */
	public List<FixedAssetsSort> getChildFixedAssetsSortList(String sortId, List<FixedAssetsSort> rootFixedAssetsSortSort) {
		List<FixedAssetsSort> childList = new ArrayList<>();
		for (FixedAssetsSort fixedAssetsSort : rootFixedAssetsSortSort) {
			if (fixedAssetsSort.getParentId().equals(sortId)) {
				childList.add(fixedAssetsSort);
			}
		}
		for (FixedAssetsSort fixedAssetsSort : childList) {
			fixedAssetsSort.setChildren(getChildFixedAssetsSortList(fixedAssetsSort.getSortId(), rootFixedAssetsSortSort));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	/**
	 * 获取所有分类
	 * @param orgId 机构码
	 * @return 分类列表
	 */
	public List<FixedAssetsSort> getAllFixedAssetsSort(String orgId) {
		FixedAssetsSort fixedAssetsSort = new FixedAssetsSort();
		fixedAssetsSort.setOrgId(orgId);
		return fixedAssetsSortMapper.select(fixedAssetsSort);
	}

	/**
	 * 判断分类下是否有子节点
	 * @param orgId 机构码
	 * @param parentId 父级Id
	 * @return 是否有子节点
	 */
	public boolean isExistChild(String orgId, String parentId) {
		FixedAssetsSort fixedAssetsSort = new FixedAssetsSort();
		fixedAssetsSort.setOrgId(orgId);
		fixedAssetsSort.setParentId(parentId);
		return fixedAssetsSortMapper.selectCount(fixedAssetsSort) > 0;
	}
}
