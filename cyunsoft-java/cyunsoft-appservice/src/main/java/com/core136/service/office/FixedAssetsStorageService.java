package com.core136.service.office;


import com.core136.bean.office.FixedAssetsStorage;
import com.core136.mapper.office.FixedAssetsStorageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class FixedAssetsStorageService {
    private FixedAssetsStorageMapper fixedAssetsStorageMapper;
	@Autowired
	public void setFixedAssetsStorageMapper(FixedAssetsStorageMapper fixedAssetsStorageMapper) {
		this.fixedAssetsStorageMapper = fixedAssetsStorageMapper;
	}

	public int insertFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
        return fixedAssetsStorageMapper.insert(fixedAssetsStorage);
    }

    public int deleteFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
        return fixedAssetsStorageMapper.delete(fixedAssetsStorage);
    }

    public int updateFixedAssetsStorage(Example example, FixedAssetsStorage fixedAssetsStorage) {
        return fixedAssetsStorageMapper.updateByExampleSelective(fixedAssetsStorage, example);
    }

    public FixedAssetsStorage selectOneFixedAssetsStorage(FixedAssetsStorage fixedAssetsStorage) {
        return fixedAssetsStorageMapper.selectOne(fixedAssetsStorage);
    }

	/**
	 * 获取仓库列表
	 * @param orgId 机构码
	 * @return
	 */
	public List<Map<String, String>> getFixedAssetsStorageList(String orgId) {
        return fixedAssetsStorageMapper.getFixedAssetsStorageList(orgId);
    }

	/**
	 * 获取仓库列表
	 * @param orgId 机构码
	 * @return
	 */
	public List<Map<String, String>> getAllFixedAssetsStorageList(String orgId) {
        return fixedAssetsStorageMapper.getAllFixedAssetsStorageList(orgId);
    }

}
