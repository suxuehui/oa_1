package com.core136.service.office;

import com.core136.bean.office.LicenceBorrowing;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.LicenceBorrowingMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LicenceBorrowingService {
    private LicenceBorrowingMapper licenceBorrowingMapper;
	@Autowired
	public void setLicenceBorrowingMapper(LicenceBorrowingMapper licenceBorrowingMapper) {
		this.licenceBorrowingMapper = licenceBorrowingMapper;
	}

	public int insertLicenceBorrowing(LicenceBorrowing licenceBorrowing)
    {
        return licenceBorrowingMapper.insert(licenceBorrowing);
    }

    public int deleteLicenceBorrowing(LicenceBorrowing licenceBorrowing)
    {
        return licenceBorrowingMapper.delete(licenceBorrowing);
    }

    public int updateLicenceBorrowing(Example example,LicenceBorrowing licenceBorrowing)
    {
        return licenceBorrowingMapper.updateByExampleSelective(licenceBorrowing,example);
    }

    public LicenceBorrowing selectOneLicenceBorrowing(LicenceBorrowing licenceBorrowing)
    {
        return licenceBorrowingMapper.selectOne(licenceBorrowing);
    }

    /**
     * 批量删除出借记录
     * @param orgId 机构码
     * @param list 出借记录Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteLicenceBorrowingByIds(String orgId, List<String> list)
    {
        if(list.isEmpty())
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(LicenceBorrowing.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, licenceBorrowingMapper.deleteByExample(example));
        }
    }

    /**
     * 获取证照出借记录
     * @param orgId 机构码
     * @param opFlag 管理员标识
     * @param accountId 用户账号
     * @param dateQueryType 日期范围
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @param licType 证照类型
     * @param keyword 查询关键词
     * @return 出借记录列表
     */
    public List<Map<String,String>>getLicenceBorrowingList(String orgId, String opFlag, String accountId, String dateQueryType, String beginTime, String endTime, String status,String licType, String keyword)
    {
        return licenceBorrowingMapper.getLicenceBorrowingList(orgId,opFlag,accountId,dateQueryType,beginTime,endTime,status,licType,"%"+keyword+"%");
    }

    /**
     * 获取证照出借记录
     * @param pageParam 分页参数
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status 状态
     * @param licType 证照类型
     * @return 证照出借记录
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getLicenceBorrowingList(PageParam pageParam, String dateQueryType, String beginTime, String endTime, String status,String licType) throws Exception {
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLicenceBorrowingList(pageParam.getOrgId(),pageParam.getOpFlag(),pageParam.getAccountId(), dateQueryType,beginTime, endTime,status,licType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
