package com.core136.service.office;

import com.core136.bean.office.LicenceRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.LicenceRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LicenceRecordService {
    private LicenceRecordMapper licenceRecordMapper;
	@Autowired
	public void setLicenceRecordMapper(LicenceRecordMapper licenceRecordMapper) {
		this.licenceRecordMapper = licenceRecordMapper;
	}

	public int insertLicenceRecord(LicenceRecord licenceRecord)
    {
        return licenceRecordMapper.insert(licenceRecord);
    }

    public int deleteLicenceRecord(LicenceRecord licenceRecord)
    {
        return licenceRecordMapper.delete(licenceRecord);
    }

    public int updateLicenceRecord(Example example,LicenceRecord licenceRecord)
    {
        return licenceRecordMapper.updateByExampleSelective(licenceRecord,example);
    }

    public LicenceRecord selectOneLicenceRecord(LicenceRecord licenceRecord)
    {
        return licenceRecordMapper.selectOne(licenceRecord);
    }

	/**
	 * 获取删除证照
	 * @param orgId 机构码
	 * @param list 证照记录Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteLicenceRecordByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(LicenceRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, licenceRecordMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取单位证照备选列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 证照备选列表
	 */
	public List<Map<String,String>>getLicenceRecordListForSelect(String orgId,String keyword)
	{
		if(StringUtils.isBlank(keyword))
		{
			keyword="";
		}
		return licenceRecordMapper.getLicenceRecordListForSelect(orgId,"%"+keyword+"%");
	}

	/**
	 * 获取单位证照列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param licType 证照类型
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 单位证照列表
	 */
	public List<Map<String,String>>getLicenceRecordList(String orgId, String dateQueryType, String beginTime, String endTime, String licType,String status, String keyword)
	{
		return licenceRecordMapper.getLicenceRecordList(orgId,dateQueryType,beginTime,endTime,licType,status,"%"+keyword+"%");
	}

	/**
	 * 获取单位证照列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param licType 证照类型
	 * @param status 状态
	 * @return 单位证照列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getLicenceRecordList(PageParam pageParam, String dateQueryType, String beginTime, String endTime,String licType,String status) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getLicenceRecordList(pageParam.getOrgId(), dateQueryType,beginTime, endTime,licType,status,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取提醒列表
	 * @param orgId 机构码
	 * @return 提醒列表
	 */
	public List<LicenceRecord> getLicenceRecordListForReminder(String orgId)
	{
		Example example = new Example(LicenceRecord.class);
		example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("smsFlag","1").andIsNotNull("reminder");
		return licenceRecordMapper.selectByExample(example);
	}


}
