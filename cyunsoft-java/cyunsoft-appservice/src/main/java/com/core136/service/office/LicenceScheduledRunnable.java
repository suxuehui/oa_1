package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.LicenceRecord;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.SysMsgConfig;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.service.system.SysMsgConfigService;
import com.core136.unit.SpringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 单位证照到期提醒
 */
public class LicenceScheduledRunnable implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final ApplicationContext applicationContext = SpringUtils.getApplicationContext();
	private final UserInfoService userInfoService = applicationContext.getBean(UserInfoService.class);
	private final LicenceRecordService licenceRecordService = applicationContext.getBean(LicenceRecordService.class);
	private final MessageUnitService messageUnitService = applicationContext.getBean(MessageUnitService.class);
	private final SysMsgConfigService sysMsgConfigService = applicationContext.getBean(SysMsgConfigService.class);
	public String orgId;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public void run() {
		logger.info("---------------------------单位证照到期提醒-------------------------------");
		SysMsgConfig sysMsgConfig = new SysMsgConfig();
		sysMsgConfig.setOrgId(orgId);
		sysMsgConfig.setCode("licence");
		sysMsgConfig = sysMsgConfigService.selectOne(sysMsgConfig);
		List<String> msgTypeList = sysMsgConfigService.getMsgType(sysMsgConfig);
		String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		long nowTimeStamp = 0L;
		try {
			nowTimeStamp = SysTools.getTimeStamp(nowTime, "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			logger.error(e.getMessage());
		}
		List<LicenceRecord> licenceRecordList = licenceRecordService.getLicenceRecordListForReminder(orgId);
		for (LicenceRecord licenceRecord : licenceRecordList) {
			long endTimeStamp = 0L;
			String endTime = licenceRecord.getEndTime() + " 00:00:00";
			try {
				endTimeStamp = SysTools.getTimeStamp(endTime, "yyyy-MM-dd HH:mm:ss");
			} catch (ParseException e) {
				logger.error(e.getMessage());
			}
			if ((nowTimeStamp + (10 * 24 * 60 * 60 * 1000)) > endTimeStamp && endTimeStamp < nowTimeStamp) {
				String sendToUser = licenceRecord.getReminder();
				if (StringUtils.isNotBlank(sendToUser)) {
					String[] arr = sendToUser.split(",");
					List<MsgBody> msgBodyList = new ArrayList<>();
					for (String accountId : arr) {
						UserInfo user2 = new UserInfo();
						user2.setAccountId(accountId);
						user2.setOrgId(orgId);
						user2 = userInfoService.selectOneUser(user2);
						MsgBody msgBody = new MsgBody();
						msgBody.setTitle("证照到期提醒");
						msgBody.setContent("内容为：" + licenceRecord.getTitle() + "的证照10天后到期！");
						msgBody.setSendTime(nowTime);
						msgBody.setUserInfo(user2);
						msgBody.setFormUserName("系统消息");
						msgBody.setView(GlobalConstant.MSG_TYPE_LICENCE);
						msgBody.setRecordId(licenceRecord.getRecordId());
						msgBody.setOrgId(orgId);
						msgBodyList.add(msgBody);
						messageUnitService.sendMessage(msgTypeList, msgBodyList);
					}
				}
			}
		}

	}
}
