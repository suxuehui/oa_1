package com.core136.service.office;

import com.core136.bean.office.VehicleInfo;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.VehicleInfoMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class VehicleInfoService {
    private VehicleInfoMapper vehicleInfoMapper;
	@Autowired
	public void setVehicleInfoMapper(VehicleInfoMapper vehicleInfoMapper) {
		this.vehicleInfoMapper = vehicleInfoMapper;
	}

	public int insertVehicleInfo(VehicleInfo vehicleInfo) {
        return vehicleInfoMapper.insert(vehicleInfo);
    }

    public int deleteVehicleInfo(VehicleInfo vehicleInfo) {
        return vehicleInfoMapper.delete(vehicleInfo);
    }

    public int updateVehicleInfo(Example example, VehicleInfo vehicleInfo) {
        return vehicleInfoMapper.updateByExampleSelective(vehicleInfo, example);
    }

    public VehicleInfo selectOneVehicleInfo(VehicleInfo vehicleInfo) {
        return vehicleInfoMapper.selectOne(vehicleInfo);
    }


	/**
	 * 获取车辆下接列表
	 * @param orgId 机构码
	 * @return
	 */
	public List<Map<String, String>> getVehicleListForSelect(String orgId) {
		return vehicleInfoMapper.getVehicleListForSelect(orgId);
	}
	/**
	 * 批量删除车辆信息
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteVehicleInfoByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(VehicleInfo.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("vehicleId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, vehicleInfoMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取车辆列表
	 * @param orgId 机构码
	 * @param onwer
	 * @param carType
	 * @param nature
	 * @param dateQueryType1
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param dateQueryType2
	 * @param beginTime1 开始时间
	 * @param endTime1 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getManageVehicleInfoList(String orgId, String onwer, String carType, String nature,String dateQueryType1, String beginTime, String endTime, String dateQueryType2,String beginTime1, String endTime1, String keyword) {
        return vehicleInfoMapper.getManageVehicleInfoList(orgId, onwer, carType, nature,dateQueryType1, beginTime, endTime, dateQueryType2,beginTime1, endTime1, "%" + keyword + "%");
    }

	/**
	 * 获取车辆列表
	 * @param pageParam 分页参数
	 * @param onwer
	 * @param carType
	 * @param nature
	 * @param dateQueryType1
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param dateQueryType2
	 * @param beginTime1 开始时间
	 * @param endTime1 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getManageVehicleInfoList(PageParam pageParam, String onwer, String carType, String nature, String dateQueryType1,String beginTime, String endTime,String dateQueryType2, String beginTime1, String endTime1) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getManageVehicleInfoList(pageParam.getOrgId(), onwer, carType, nature, dateQueryType1,beginTime, endTime,dateQueryType2, beginTime1, endTime1, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取可调度车辆列表
	 * @param orgId 机构码
	 * @return
	 */
	public List<Map<String, String>> getCanUsedVehicleList(String orgId,String carType) {
        return vehicleInfoMapper.getCanUsedVehicleList(orgId,carType);
    }
}
