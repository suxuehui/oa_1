package com.core136.service.office;

import com.core136.bean.account.UserInfo;
import com.core136.bean.office.VehicleOperator;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.VehicleOperatorMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class VehicleOperatorService {
    private VehicleOperatorMapper vehicleOperatorMapper;
    private UserInfoService userInfoService;
	@Autowired
	public void setVehicleOperatorMapper(VehicleOperatorMapper vehicleOperatorMapper) {
		this.vehicleOperatorMapper = vehicleOperatorMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	public int insertVehicleOperator(VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.insert(vehicleOperator);
    }

    public int deleteVehicleOperator(VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.delete(vehicleOperator);
    }

    public int updateVehicleOperator(Example example, VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.updateByExampleSelective(vehicleOperator, example);
    }

    public VehicleOperator selectOneVehicleOperator(VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.selectOne(vehicleOperator);
    }

	/**
	 * 获取调度人员
	 * @param vehicleOperator
	 * @return
	 */
    public List<Map<String,String>> getVehicleOperatorUserList(VehicleOperator vehicleOperator)
	{
		vehicleOperator = vehicleOperatorMapper.selectOne(vehicleOperator);
		String optUserStr = vehicleOperator.getOptUser();
		if(StringUtils.isNotBlank(optUserStr)) {
			return userInfoService.getUserListByAccountIds(vehicleOperator.getOrgId(), Arrays.asList(optUserStr.split(",")));
		}else
		{
			return new ArrayList<>();
		}
	}

	/**
	 * 设置调度员
	 * @param user 用户对象
	 * @param optUser 词度员账号
	 * @return 消息结构
	 */
	public RetDataBean setVehicleOperator(UserInfo user, String optUser) {
        if (user.getOpFlag().equals("1")) {
            VehicleOperator vehicleOperator = new VehicleOperator();
            vehicleOperator.setOrgId(user.getOrgId());
            vehicleOperator = selectOneVehicleOperator(vehicleOperator);
            if (vehicleOperator == null) {
                VehicleOperator newVehicleOperator = new VehicleOperator();
                newVehicleOperator.setConfigId(SysTools.getGUID());
                newVehicleOperator.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                newVehicleOperator.setCreateUser(user.getAccountId());
                newVehicleOperator.setOptUser(optUser);
                newVehicleOperator.setOrgId(user.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertVehicleOperator(newVehicleOperator));
            } else {
                Example example = new Example(VehicleOperator.class);
                example.createCriteria().andEqualTo("configId", vehicleOperator.getConfigId()).andEqualTo("orgId", user.getOrgId());
                vehicleOperator.setOptUser(optUser);
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, updateVehicleOperator(example, vehicleOperator));
            }
        } else {
            return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
        }
    }

}
