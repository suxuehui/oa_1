package com.core136.service.office;

import com.core136.bean.office.VehicleRepairRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.VehicleRepairRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class VehicleRepairRecordService {
    private VehicleRepairRecordMapper vehicleRepairRecordMapper;
	@Autowired
	public void setVehicleRepairRecordMapper(VehicleRepairRecordMapper vehicleRepairRecordMapper) {
		this.vehicleRepairRecordMapper = vehicleRepairRecordMapper;
	}

	public int insertVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
        return vehicleRepairRecordMapper.insert(vehicleRepairRecord);
    }

    public int deleteVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
        return vehicleRepairRecordMapper.delete(vehicleRepairRecord);
    }

    public int updateVehicleRepairRecord(Example example, VehicleRepairRecord vehicleRepairRecord) {
        return vehicleRepairRecordMapper.updateByExampleSelective(vehicleRepairRecord, example);
    }

    public VehicleRepairRecord selectOneVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
        return vehicleRepairRecordMapper.selectOne(vehicleRepairRecord);
    }


	/**
	 * 批量删除车辆保养记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteVehicleRepairRecordByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(VehicleRepairRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, vehicleRepairRecordMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取维修列表
	 * @param orgId 机构码
	 * @param repairUser
	 * @param repairType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword
	 * @return
	 */
    public List<Map<String, String>> getVehicleRepairRecordList(String orgId, String repairUser, String repairType,String dateQueryType, String beginTime, String endTime, String keyword) {
        return vehicleRepairRecordMapper.getVehicleRepairRecordList(orgId, repairUser, repairType, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    public List<Map<String,String>>getVehicleRepairUserList(String orgId){
    	return vehicleRepairRecordMapper.getVehicleRepairUserList(orgId);
	}

	/**
	 *  获取维修列表
	 * @param pageParam 分页参数
	 * @param repairUser
	 * @param repairType
	 * @param dateQueryType
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getVehicleRepairRecordList(PageParam pageParam, String repairUser, String repairType,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleRepairRecordList(pageParam.getOrgId(), repairUser, repairType,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
