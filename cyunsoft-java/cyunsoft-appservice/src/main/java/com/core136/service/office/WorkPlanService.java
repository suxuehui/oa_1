package com.core136.service.office;


import com.core136.bean.account.UserInfo;
import com.core136.bean.office.WorkPlan;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.office.WorkPlanMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * 工作计划操作服务类
 * @author lsq
 */
@Service
public class WorkPlanService {
    private WorkPlanMapper workPlanMapper;
    private UserInfoService userInfoService;
	@Autowired
	public void setWorkPlanMapper(WorkPlanMapper workPlanMapper) {
		this.workPlanMapper = workPlanMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

	/**
	 * 创建工作计划
	 * @param workPlan 工作计划对象
	 * @return 成功创建记录数
	 */
    public int insertWorkPlan(WorkPlan workPlan) {
        return workPlanMapper.insert(workPlan);
    }

	/**
	 * 删除工作计划
	 * @param workPlan 工作计划对象
	 * @return 成功删除记录数
	 */
    public int deleteWorkPlan(WorkPlan workPlan) {
        return workPlanMapper.delete(workPlan);
    }

	/**
	 * 批量删除工作计划
	 * @param orgId 机构码
	 * @param list 工作计划Id 列表
	 * @return 成功删除记录数
	 */
	public RetDataBean deleteWorkPlanByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(WorkPlan.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("planId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, workPlanMapper.deleteByExample(example));
		}
	}

	/**
	 * 更新工作计
	 * @param example 更新条件
	 * @param workPlan 工作计划对象
	 * @return 成功更新记录数
	 */
    public int updateWorkPlan(Example example, WorkPlan workPlan) {
        return workPlanMapper.updateByExampleSelective(workPlan, example);
    }

	/**
	 * 查询单个工作计划
	 * @param workPlan 工作计划对象
	 * @return 工作计划对象
	 */
    public WorkPlan selectOneWorkPlan(WorkPlan workPlan) {
        return workPlanMapper.selectOne(workPlan);
    }

	/**
	 * 创建工作计划
	 * @param user 用户地象
	 * @param workPlan 工作计划对象
	 * @return 成功创建记录数
	 */
	public int createWorkPlan(UserInfo user, WorkPlan workPlan) {
        if (StringUtils.isNotBlank(workPlan.getMsgType())) {
            String holdUser = workPlan.getHoldUser();
            String joinUser = workPlan.getJoinUser();
            String supUser = workPlan.getSupUser();
            List<String> userList = new ArrayList<>();
            List<String> arr2 = new ArrayList<>();
            List<String> arr3 = new ArrayList<>();
            if (StringUtils.isNotBlank(holdUser)) {
                userList = new ArrayList<>(Arrays.asList(holdUser.split(",")));
            }
            if (StringUtils.isNotBlank(joinUser)) {
                arr2 = new ArrayList<>(Arrays.asList(joinUser.split(",")));
            }
            if (StringUtils.isNotBlank(supUser)) {
                arr3 = new ArrayList<>(Arrays.asList(supUser.split(",")));
            }
            userList.addAll(arr2);
            userList.addAll(arr3);
			Set<String> set = new HashSet<>(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
			userList.clear();
            userList.addAll(set);
            List<MsgBody> msgBodyList = new ArrayList<>();
			for (String accountId : userList) {
				UserInfo user2 = new UserInfo();
				user2.setAccountId(accountId);
				user2.setOrgId(user.getOrgId());
				user2 = userInfoService.selectOneUser(user2);
				MsgBody msgBody = new MsgBody();
				msgBody.setTitle("工作计划提醒");
				msgBody.setContent("工作计划标题为：" + workPlan.getTitle() + "的查看提醒！");
				msgBody.setSendTime(workPlan.getCreateTime());
				msgBody.setUserInfo(user2);
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setView(GlobalConstant.MSG_TYPE_WORK_PLAN);
				msgBody.setRecordId(workPlan.getPlanId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(workPlan.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
        return insertWorkPlan(workPlan);
    }

	/**
	 * 获取工作列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param createUser 创建人账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 工作列表
	 */
    public List<Map<String, String>> getManageWorkPlanList(String orgId, String opFlag, String createUser,String dateQueryType, String beginTime, String endTime, String status, String planType, String keyword) {
        return workPlanMapper.getManageWorkPlanList(orgId, createUser, opFlag, dateQueryType,beginTime, endTime, status, planType, "%" + keyword + "%");
    }

	/**
	 * 获取工作计划详情
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param planId 工作计划Id
	 * @return 工作计划详情
	 */
    public Map<String,String>getMyWorkPlanById(String orgId,String opFlag,String accountId,String deptId,String userLevel,String planId)
	{
		return workPlanMapper.getMyWorkPlanById(orgId,opFlag,accountId,deptId,userLevel,planId);
	}

	/**
	 *  获取工作列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @return 工作列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getManageWorkPlanList(PageParam pageParam, String dateQueryType,String beginTime, String endTime, String status, String planType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getManageWorkPlanList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, planType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 我负责的计划列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 计划列表
	 */
    public List<Map<String, String>> getHoldWorkPlanList(String orgId, String opFlag, String accountId, String dateQueryType, String beginTime, String endTime, String status, String planType, String keyword) {
        return workPlanMapper.getHoldWorkPlanList(orgId, accountId, opFlag,dateQueryType, beginTime, endTime, status, planType, "%" + keyword + "%");
    }

	/**
	 * 我负责的计划列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @return 计划列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHoldWorkPlanList(PageParam pageParam,String dateQueryType, String beginTime, String endTime, String status, String planType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHoldWorkPlanList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, status, planType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 我督查的计划列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 计划列表
	 */
    public List<Map<String, String>> getSupWorkPlanList(String orgId, String opFlag, String accountId, String dateQueryType,String beginTime, String endTime, String status, String planType, String keyword) {
        return workPlanMapper.getSupWorkPlanList(orgId, accountId, opFlag, dateQueryType,beginTime, endTime, status, planType, "%" + keyword + "%");
    }

	/**
	 *  我督查的计划列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @return 计划列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getSupWorkPlanList(PageParam pageParam, String dateQueryType,String beginTime, String endTime, String status, String planType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupWorkPlanList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), dateQueryType,beginTime, endTime, status, planType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 我参与的工作计划
	 * @param orgId 机构码
	 * @param type 参与类型
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @param keyword 查询关键词
	 * @return 工作计划列表
	 */
    public List<Map<String, String>> getMyWorkPlanList(String orgId,String type, String accountId,String deptId,String userLevel, String dateQueryType, String beginTime, String endTime, String status, String planType, String keyword) {
        return workPlanMapper.getMyWorkPlanList(orgId, type,accountId,deptId,userLevel,dateQueryType, beginTime, endTime, status, planType, "%" + keyword + "%");
    }

	/**
	 * 移动端我参与的工作计划
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param deptId 部门Id
	 * @param userLevel 行政级别Id
	 * @param keyword 查询关键词
	 * @param page 页码
	 * @return 工作计划列表
	 */
    public List<Map<String,String>>getMyWorkPlanListForApp(String orgId,String accountId,String deptId,String userLevel,String keyword,Integer page)
	{
		return workPlanMapper.getMyWorkPlanListForApp(orgId,accountId,deptId,userLevel,"%" + keyword + "%",page);
	}

	/**
	 * 获取个人桌面的工作计划列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 计划列表
	 */
	public List<Map<String, String>> getMyWorkPlanForDesk(String orgId,String accountId) {
		return workPlanMapper.getMyWorkPlanForDesk(orgId,accountId);
	}

	/**
	 * 我参与的工作计划
	 * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param planType 计划类型
	 * @return 工作计划列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyWorkPlanList(PageParam pageParam,String type, String dateQueryType, String beginTime, String endTime, String status, String planType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyWorkPlanList(pageParam.getOrgId(),type, pageParam.getAccountId(),pageParam.getDeptId(),pageParam.getLevelId(),dateQueryType, beginTime, endTime, status, planType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }





}
