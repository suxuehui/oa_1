package com.core136.bean.hr;

import java.io.Serializable;

/**
 * 员工评价
 * @author lsq
 */
public class HrEvaluate implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String userId;
    private Double learnLevel;
    private Double attitudeLevel;
    private Double skillLevel;
    private String status;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Double getLearnLevel() {
        return learnLevel;
    }

    public void setLearnLevel(Double learnLevel) {
        this.learnLevel = learnLevel;
    }

    public Double getAttitudeLevel() {
        return attitudeLevel;
    }

    public void setAttitudeLevel(Double attitudeLevel) {
        this.attitudeLevel = attitudeLevel;
    }

    public Double getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(Double skillLevel) {
        this.skillLevel = skillLevel;
    }


}
