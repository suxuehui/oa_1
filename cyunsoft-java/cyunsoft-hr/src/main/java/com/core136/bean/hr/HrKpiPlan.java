package com.core136.bean.hr;

import java.io.Serializable;

/**
 * KPI 考核计划
 * @author lsq
 */
public class HrKpiPlan implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String planId;
    private String title;
    private String beginTime;
    private String endTime;
    private String kpiRule;
    private String chargeUser;
    private String userRole;
    private String deptRole;
    private String levelRole;
    private String attachId;
    private String itemId;
    private String msgType;
    private String remark;
    private String status;
    private String createUser;
    private String createTime;
    private String orgId;

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getKpiRule() {
		return kpiRule;
	}

	public void setKpiRule(String kpiRule) {
		this.kpiRule = kpiRule;
	}

	public String getChargeUser() {
		return chargeUser;
	}

	public void setChargeUser(String chargeUser) {
		this.chargeUser = chargeUser;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getDeptRole() {
		return deptRole;
	}

	public void setDeptRole(String deptRole) {
		this.deptRole = deptRole;
	}

	public String getLevelRole() {
		return levelRole;
	}

	public void setLevelRole(String levelRole) {
		this.levelRole = levelRole;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
}
