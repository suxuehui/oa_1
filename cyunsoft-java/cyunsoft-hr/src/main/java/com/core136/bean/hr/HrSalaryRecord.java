package com.core136.bean.hr;

import java.io.Serializable;

/**
 * 人员薪资记录
 * @author lsq
 */
public class HrSalaryRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String recordId;
    private String userId;
    private String year;
    private String month;
    private Double postSalary;
    private Double levelSalary;
    private Double foodSalary;
    private Double postSalaryOther;
    private Double transportSalary;
    private Double postAllowance;
    private Double sumAmount;
    private Double pensoin;
    private Double unemployment;
    private Double medical;
    private Double accumulationFund;
    private Double tax;
    private Double costOther;
    private Double realCost;
    private String msgType;
    private Double realSalary;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getPostSalary() {
		return postSalary;
	}

	public void setPostSalary(Double postSalary) {
		this.postSalary = postSalary;
	}

	public Double getLevelSalary() {
		return levelSalary;
	}

	public void setLevelSalary(Double levelSalary) {
		this.levelSalary = levelSalary;
	}

	public Double getFoodSalary() {
		return foodSalary;
	}

	public void setFoodSalary(Double foodSalary) {
		this.foodSalary = foodSalary;
	}

	public Double getPostSalaryOther() {
		return postSalaryOther;
	}

	public void setPostSalaryOther(Double postSalaryOther) {
		this.postSalaryOther = postSalaryOther;
	}

	public Double getTransportSalary() {
		return transportSalary;
	}

	public void setTransportSalary(Double transportSalary) {
		this.transportSalary = transportSalary;
	}

	public Double getPostAllowance() {
		return postAllowance;
	}

	public void setPostAllowance(Double postAllowance) {
		this.postAllowance = postAllowance;
	}

	public Double getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(Double sumAmount) {
		this.sumAmount = sumAmount;
	}

	public Double getPensoin() {
		return pensoin;
	}

	public void setPensoin(Double pensoin) {
		this.pensoin = pensoin;
	}

	public Double getUnemployment() {
		return unemployment;
	}

	public void setUnemployment(Double unemployment) {
		this.unemployment = unemployment;
	}

	public Double getMedical() {
		return medical;
	}

	public void setMedical(Double medical) {
		this.medical = medical;
	}

	public Double getAccumulationFund() {
		return accumulationFund;
	}

	public void setAccumulationFund(Double accumulationFund) {
		this.accumulationFund = accumulationFund;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public Double getCostOther() {
		return costOther;
	}

	public void setCostOther(Double costOther) {
		this.costOther = costOther;
	}

	public Double getRealCost() {
		return realCost;
	}

	public void setRealCost(Double realCost) {
		this.realCost = realCost;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Double getRealSalary() {
		return realSalary;
	}

	public void setRealSalary(Double realSalary) {
		this.realSalary = realSalary;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
