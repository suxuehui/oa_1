package com.core136.bean.hr;

import java.io.Serializable;

/**
 * 人力资次职称评定
 * @author lsq
 */
public class HrTitleEvaluation implements Serializable {
    private String recordId;
    private Integer sortNo;
    private String userId;
    private String postName;
    private String getType;
    private String applyTime;
    private String receiveTime;
    private String nextPostName;
    private String nextApplyTime;
    private String employPost;
    private String employComp;
    private String employBeginTime;
    private String employEndTime;
    private String remark;
    private String attachId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getGetType() {
        return getType;
    }

    public void setGetType(String getType) {
        this.getType = getType;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getNextPostName() {
        return nextPostName;
    }

    public void setNextPostName(String nextPostName) {
        this.nextPostName = nextPostName;
    }

    public String getNextApplyTime() {
        return nextApplyTime;
    }

    public void setNextApplyTime(String nextApplyTime) {
        this.nextApplyTime = nextApplyTime;
    }

    public String getEmployPost() {
        return employPost;
    }

    public void setEmployPost(String employPost) {
        this.employPost = employPost;
    }

    public String getEmployComp() {
        return employComp;
    }

    public void setEmployComp(String employComp) {
        this.employComp = employComp;
    }

    public String getEmployBeginTime() {
        return employBeginTime;
    }

    public void setEmployBeginTime(String employBeginTime) {
        this.employBeginTime = employBeginTime;
    }

    public String getEmployEndTime() {
        return employEndTime;
    }

    public void setEmployEndTime(String employEndTime) {
        this.employEndTime = employEndTime;
    }


    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
}
