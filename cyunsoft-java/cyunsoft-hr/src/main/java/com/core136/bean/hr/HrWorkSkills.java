package com.core136.bean.hr;

import java.io.Serializable;

/**
 * 工作技能操作服务类
 * @author lsq
 */
public class HrWorkSkills implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private Integer sortNo;
    private String userId;
    private String name;
    private String skillsLevel;
    private String skillsCerificate;
    private String beginTime;
    private String endTime;
    private String notifiedBody;
    private String attachId;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkillsLevel() {
        return skillsLevel;
    }

    public void setSkillsLevel(String skillsLevel) {
        this.skillsLevel = skillsLevel;
    }

    public String getSkillsCerificate() {
        return skillsCerificate;
    }

    public void setSkillsCerificate(String skillsCerificate) {
        this.skillsCerificate = skillsCerificate;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

	public String getNotifiedBody() {
		return notifiedBody;
	}

	public void setNotifiedBody(String notifiedBody) {
		this.notifiedBody = notifiedBody;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

}
