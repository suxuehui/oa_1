package com.core136.controller.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.hr.*;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/hr")
@CrossOrigin
@Api(value="人力SetController",tags={"人力更新数据接口"})
public class ApiSetHrController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private UserInfoService userInfoService;
	private HrDicService hrDicService;
	private HrDepartmentService hrDepartmentService;
	private HrUserService hrUserService;
	private HrContractService hrContractService;
	private HrLicenceService hrLicenceService;
	private HrLearnRecordService hrLearnRecordService;
	private HrWorkRecordService hrWorkRecordService;
	private HrIncentiveService hrIncentiveService;
	private HrWorkSkillsService hrWorkSkillsService;
	private HrTrainRecordService hrTrainRecordService;
	private HrPersonnelTransferService hrPersonnelTransferService;
	private HrLeaveRecordService hrLeaveRecordService;
	private HrReinstatementService hrReinstatementService;
	private HrTitleEvaluationService hrTitleEvaluationService;
	private HrCareRecordService hrCareRecordService;
	private HrRecruitNeedsService hrRecruitNeedsService;
	private HrRecruitPlanService hrRecruitPlanService;
	private HrRecruitTaskService hrRecruitTaskService;
	private HrKpiPlanService hrKpiPlanService;
	private HrSalaryRecordService hrSalaryRecordService;
	private HrKpiPlanRecordService hrKpiPlanRecordService;
	private HrKpiItemService hrKpiItemService;
	private HrWelfareRecordService hrWelfareRecordService;
	private HrEvaluateService hrEvaluateService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}
	@Autowired
	public void setHrDepartmentService(HrDepartmentService hrDepartmentService) {
		this.hrDepartmentService = hrDepartmentService;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrContractService(HrContractService hrContractService) {
		this.hrContractService = hrContractService;
	}
	@Autowired
	public void setHrLicenceService(HrLicenceService hrLicenceService) {
		this.hrLicenceService = hrLicenceService;
	}
	@Autowired
	public void setHrLearnRecordService(HrLearnRecordService hrLearnRecordService) {
		this.hrLearnRecordService = hrLearnRecordService;
	}
	@Autowired
	public void setHrWorkRecordService(HrWorkRecordService hrWorkRecordService) {
		this.hrWorkRecordService = hrWorkRecordService;
	}
	@Autowired
	public void setHrIncentiveService(HrIncentiveService hrIncentiveService) {
		this.hrIncentiveService = hrIncentiveService;
	}
	@Autowired
	public void setHrWorkSkillsService(HrWorkSkillsService hrWorkSkillsService) {
		this.hrWorkSkillsService = hrWorkSkillsService;
	}
	@Autowired
	public void setHrTrainRecordService(HrTrainRecordService hrTrainRecordService) {
		this.hrTrainRecordService = hrTrainRecordService;
	}
	@Autowired
	public void setHrPersonnelTransferService(HrPersonnelTransferService hrPersonnelTransferService) {
		this.hrPersonnelTransferService = hrPersonnelTransferService;
	}
	@Autowired
	public void setHrLeaveRecordService(HrLeaveRecordService hrLeaveRecordService) {
		this.hrLeaveRecordService = hrLeaveRecordService;
	}
	@Autowired
	public void setHrReinstatementService(HrReinstatementService hrReinstatementService) {
		this.hrReinstatementService = hrReinstatementService;
	}
	@Autowired
	public void setHrTitleEvaluationService(HrTitleEvaluationService hrTitleEvaluationService) {
		this.hrTitleEvaluationService = hrTitleEvaluationService;
	}
	@Autowired
	public void setHrCareRecordService(HrCareRecordService hrCareRecordService) {
		this.hrCareRecordService = hrCareRecordService;
	}
	@Autowired
	public void setHrRecruitNeedsService(HrRecruitNeedsService hrRecruitNeedsService) {
		this.hrRecruitNeedsService = hrRecruitNeedsService;
	}
	@Autowired
	public void setHrRecruitPlanService(HrRecruitPlanService hrRecruitPlanService) {
		this.hrRecruitPlanService = hrRecruitPlanService;
	}
	@Autowired
	public void setHrRecruitTaskService(HrRecruitTaskService hrRecruitTaskService) {
		this.hrRecruitTaskService = hrRecruitTaskService;
	}
	@Autowired
	public void setHrKpiPlanService(HrKpiPlanService hrKpiPlanService) {
		this.hrKpiPlanService = hrKpiPlanService;
	}
	@Autowired
	public void setHrSalaryRecordService(HrSalaryRecordService hrSalaryRecordService) {
		this.hrSalaryRecordService = hrSalaryRecordService;
	}
	@Autowired
	public void setHrKpiPlanRecordService(HrKpiPlanRecordService hrKpiPlanRecordService) {
		this.hrKpiPlanRecordService = hrKpiPlanRecordService;
	}
	@Autowired
	public void setHrKpiItemService(HrKpiItemService hrKpiItemService) {
		this.hrKpiItemService = hrKpiItemService;
	}
	@Autowired
	public void setHrWelfareRecordService(HrWelfareRecordService hrWelfareRecordService) {
		this.hrWelfareRecordService = hrWelfareRecordService;
	}
	@Autowired
	public void setHrEvaluateService(HrEvaluateService hrEvaluateService) {
		this.hrEvaluateService = hrEvaluateService;
	}

	/**
	 * 设置考核分
	 * @param planId 计划Id
	 * @param accountId 用户账号
	 * @param scoreList 分数列表
	 * @return 消息结构
	 */
	@PostMapping(value = "/setScoreToUser")
	public RetDataBean setScoreToUser(String planId, String accountId, String scoreList,Double score) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return hrKpiPlanRecordService.setScoreToUser(userInfo, planId, accountId, scoreList,score);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加人员评价记录
	 * @param hrEvaluate 人员评价对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrEvaluate")
	public RetDataBean insertHrEvaluate(HrEvaluate hrEvaluate) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrEvaluate.setRecordId(SysTools.getGUID());
			hrEvaluate.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrEvaluate.setCreateUser(userInfo.getAccountId());
			hrEvaluate.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrEvaluateService.insertHrEvaluate(hrEvaluate));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员评价记录
	 * @param hrEvaluate 人员评价对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrEvaluate")
	public RetDataBean deleteHrEvaluate(HrEvaluate hrEvaluate) {
		try {
			if (StringUtils.isBlank(hrEvaluate.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrEvaluate.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrEvaluateService.deleteHrEvaluate(hrEvaluate));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除领导评价
	 * @param ids 领导评价Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrEvaluateByIds")
	public RetDataBean deleteHrEvaluateByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrEvaluateService.deleteHrEvaluateByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新人员评价记录
	 * @param hrEvaluate 人员评价对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrEvaluate")
	public RetDataBean updateHrEvaluate(HrEvaluate hrEvaluate) {
		try {
			if (StringUtils.isBlank(hrEvaluate.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrEvaluate.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrEvaluate.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrEvaluateService.updateHrEvaluate(example, hrEvaluate));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量导入人员福利
	 * @param file 导入文件模版
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrWelfareRecord")
	public RetDataBean importHrWelfareRecord(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrWelfareRecordService.importHrWelfareRecord(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加人员福利记录
	 * @param hrWelfareRecord 福利记录对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrWelfareRecord")
	public RetDataBean insertHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrWelfareRecord.setRecordId(SysTools.getGUID());
			hrWelfareRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrWelfareRecord.setCreateUser(userInfo.getAccountId());
			hrWelfareRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrWelfareRecordService.insertHrWelfareRecord(hrWelfareRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员福利记录
	 * @param hrWelfareRecord 人员福利对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrWelfareRecord")
	public RetDataBean deleteHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
		try {
			if (StringUtils.isBlank(hrWelfareRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrWelfareRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrWelfareRecordService.deleteHrWelfareRecord(hrWelfareRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除人员福利记录
	 * @param ids 人员福利记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrWelfareRecordByIds")
	public RetDataBean deleteHrWelfareRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrWelfareRecordService.deleteHrWelfareRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新人员福利记录
	 * @param hrWelfareRecord 人员福利对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrWelfareRecord")
	public RetDataBean updateHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
		try {
			if (StringUtils.isBlank(hrWelfareRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrWelfareRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrWelfareRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrWelfareRecordService.updateHrWelfareRecord(example, hrWelfareRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 *  创建考核计划与考核指标
	 * @param hrKpiPlan 考核计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrKpiPlan")
	public RetDataBean insertHrKpiPlan(HrKpiPlan hrKpiPlan) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrKpiPlan.setPlanId(SysTools.getGUID());
			hrKpiPlan.setStatus("0");
			hrKpiPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrKpiPlan.setCreateUser(userInfo.getAccountId());
			hrKpiPlan.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrKpiPlanService.addHrKpiPlan(userInfo,hrKpiPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 考核计划生效
	 * @param hrKpiPlan 考核计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updatePlanEffect")
	public RetDataBean updatePlanEffect(HrKpiPlan hrKpiPlan) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrKpiPlan.setOrgId(userInfo.getOrgId());
			return hrKpiPlanService.updatePlanEffect(userInfo,hrKpiPlan);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除考核计划
	 * @param hrKpiPlan 考核计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrKpiPlan")
	public RetDataBean deleteHrKpiPlan(HrKpiPlan hrKpiPlan) {
		try {
			if (StringUtils.isBlank(hrKpiPlan.getPlanId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrKpiPlan.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrKpiPlanService.deleteHrKpiPlan(hrKpiPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除考核计划
	 * @param ids 考核计划Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrKpiPlanByIds")
	public RetDataBean deleteHrKpiPlanByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrKpiPlanService.deleteHrKpiPlanByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新考核计划
	 * @param hrKpiPlan 考核计划对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrKpiPlan")
	public RetDataBean updateHrKpiPlan(HrKpiPlan hrKpiPlan) {
		try {
			if (StringUtils.isBlank(hrKpiPlan.getPlanId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			hrKpiPlan.setStatus("0");
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrKpiPlan.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("planId", hrKpiPlan.getPlanId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrKpiPlanService.updateHrKpiPlan(userInfo,example,hrKpiPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加考核指标
	 * @param hrKpiItem 考核指标对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrKpiItem")
	public RetDataBean insertHrKpiItem(HrKpiItem hrKpiItem) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrKpiItem.setItemId(SysTools.getGUID());
			hrKpiItem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrKpiItem.setCreateUser(userInfo.getAccountId());
			hrKpiItem.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrKpiItemService.insertHrKpiItem(hrKpiItem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除考核指标
	 * @param hrKpiItem 考核指标对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrKpiItem")
	public RetDataBean deleteHrKpiItem(HrKpiItem hrKpiItem) {
		try {
			if (StringUtils.isBlank(hrKpiItem.getItemId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrKpiItem.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrKpiItemService.deleteHrKpiItem(hrKpiItem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除考核指标项
	 * @param ids 考核指标项Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrKpiItemByIds")
	public RetDataBean deleteHrKpiItemByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrKpiItemService.deleteHrKpiItemByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新考核指标
	 * @param hrKpiItem 考核指标对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrKpiItem")
	public RetDataBean updateHrKpiItem(HrKpiItem hrKpiItem) {
		try {
			if (StringUtils.isBlank(hrKpiItem.getItemId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrKpiItem.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("itemId", hrKpiItem.getItemId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrKpiItemService.updateHrKpiItem(example, hrKpiItem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 导入工员薪资
	 * @param file 导入文件模版
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrSalaryRecord")
	public RetDataBean importHrSalaryRecord(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrSalaryRecordService.importHrSalaryRecord(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加人员薪资记录
	 * @param hrSalaryRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrSalaryRecord")
	public RetDataBean insertHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrSalaryRecord.setRecordId(SysTools.getGUID());
			hrSalaryRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrSalaryRecord.setCreateUser(userInfo.getAccountId());
			hrSalaryRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrSalaryRecordService.insertHrSalaryRecord(userInfo,hrSalaryRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员薪资记录
	 * @param hrSalaryRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrSalaryRecord")
	public RetDataBean deleteHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
		try {
			if (StringUtils.isBlank(hrSalaryRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrSalaryRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrSalaryRecordService.deleteHrSalaryRecord(hrSalaryRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除员工薪资记录
	 * @param ids 员工薪资Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrSalaryRecordByIds")
	public RetDataBean deleteHrSalaryRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrSalaryRecordService.deleteHrSalaryRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  更新人员薪资记录
	 * @param hrSalaryRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrSalaryRecord")
	public RetDataBean updateHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
		try {
			if (StringUtils.isBlank(hrSalaryRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrSalaryRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrSalaryRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrSalaryRecordService.updateHrSalaryRecord(userInfo,example, hrSalaryRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发起人员培训申请
	 * @param hrTrainRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrTrainRecord")
	public RetDataBean insertHrTrainRecord(HrTrainRecord hrTrainRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrTrainRecord.setRecordId(SysTools.getGUID());
			hrTrainRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrTrainRecord.setStatus("0");
			hrTrainRecord.setCreateUser(userInfo.getAccountId());
			hrTrainRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrTrainRecordService.insertHrTrainRecord(hrTrainRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员培训计划
	 * @param hrTrainRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrTrainRecord")
	public RetDataBean deleteHrTrainRecord(HrTrainRecord hrTrainRecord) {
		try {
			if (StringUtils.isBlank(hrTrainRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrTrainRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrTrainRecordService.deleteHrTrainRecord(hrTrainRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除培训记录
	 * @param ids 培训记录Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrTrainRecordByIds")
	public RetDataBean deleteHrTrainRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrTrainRecordService.deleteHrTrainRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新培训计划
	 * @param hrTrainRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrTrainRecord")
	public RetDataBean updateHrTrainRecord(HrTrainRecord hrTrainRecord) {
		try {
			if (StringUtils.isBlank(hrTrainRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrTrainRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrTrainRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrTrainRecordService.updateHrTrainRecord(example, hrTrainRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建考聘任务
	 * @param hrRecruitTask
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrRecruitTask")
	public RetDataBean insertHrRecruitTask(HrRecruitTask hrRecruitTask) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrRecruitTask.setTaskId(SysTools.getGUID());
			hrRecruitTask.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrRecruitTask.setCreateUser(userInfo.getAccountId());
			hrRecruitTask.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrRecruitTaskService.insertHrRecruitTask(hrRecruitTask));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招聘任务
	 * @param hrRecruitTask
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrRecruitTask")
	public RetDataBean deleteHrRecruitTask(HrRecruitTask hrRecruitTask) {
		try {
			if (StringUtils.isBlank(hrRecruitTask.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrRecruitTask.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrRecruitTaskService.deleteHrRecruitTask(hrRecruitTask));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除招聘任务
	 * @param ids 招聘任务Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrRecruitTaskByIds")
	public RetDataBean deleteHrRecruitTaskByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrRecruitTaskService.deleteHrRecruitTaskByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新招聘任务
	 * @param hrRecruitTask
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrRecruitTask")
	public RetDataBean updateHrRecruitTask(HrRecruitTask hrRecruitTask) {
		try {
			if (StringUtils.isBlank(hrRecruitTask.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrRecruitTask.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskId", hrRecruitTask.getTaskId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrRecruitTaskService.updateHrRecruitTask(example, hrRecruitTask));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  创建招聘计划
	 * @param hrRecruitPlan
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrRecruitPlan")
	public RetDataBean insertHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrRecruitPlan.setPlanId(SysTools.getGUID());
			hrRecruitPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrRecruitPlan.setCreateUser(userInfo.getAccountId());
			hrRecruitPlan.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrRecruitPlanService.insertHrRecruitPlan(hrRecruitPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招聘计划
	 * @param hrRecruitPlan
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrRecruitPlan")
	public RetDataBean deleteHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
		try {
			if (StringUtils.isBlank(hrRecruitPlan.getPlanId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrRecruitPlan.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrRecruitPlanService.deleteHrRecruitPlan(hrRecruitPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除招聘计划
	 * @param ids 招聘计划Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrRecruitPlanByIds")
	public RetDataBean deleteHrRecruitPlanByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrRecruitPlanService.deleteHrRecruitPlanByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新招聘计划
	 * @param hrRecruitPlan
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrRecruitPlan")
	public RetDataBean updateHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
		try {
			if (StringUtils.isBlank(hrRecruitPlan.getPlanId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrRecruitPlan.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("planId", hrRecruitPlan.getPlanId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrRecruitPlanService.updateHrRecruitPlan(example, hrRecruitPlan));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加招聘需求
	 * @param hrRecruitNeeds
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrRecruitNeeds")
	public RetDataBean insertHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrRecruitNeeds.setRecordId(SysTools.getGUID());
			hrRecruitNeeds.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrRecruitNeeds.setCreateUser(userInfo.getAccountId());
			hrRecruitNeeds.setStatus("0");
			hrRecruitNeeds.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrRecruitNeedsService.insertHrRecruitNeeds(hrRecruitNeeds));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除招聘需求
	 * @param hrRecruitNeeds
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrRecruitNeeds")
	public RetDataBean deleteHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
		try {
			if (StringUtils.isBlank(hrRecruitNeeds.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrRecruitNeeds.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrRecruitNeedsService.deleteHrRecruitNeeds(hrRecruitNeeds));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除招聘需求
	 * @param ids 招聘需求Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrRecruitNeedsByIds")
	public RetDataBean deleteHrRecruitNeedsByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrRecruitNeedsService.deleteHrRecruitNeedsByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新招聘需求
	 * @param hrRecruitNeeds
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrRecruitNeeds")
	public RetDataBean updateHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
		try {
			if (StringUtils.isBlank(hrRecruitNeeds.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrRecruitNeeds.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrRecruitNeeds.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrRecruitNeedsService.updateHrRecruitNeeds(example, hrRecruitNeeds));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加员工关怀记录
	 * @param hrCareRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrCareRecord")
	public RetDataBean insertHrCareRecord(HrCareRecord hrCareRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrCareRecord.setRecordId(SysTools.getGUID());
			hrCareRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrCareRecord.setCreateUser(userInfo.getAccountId());
			hrCareRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrCareRecordService.insertHrCareRecord(hrCareRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除员工关怀记录
	 * @param hrCareRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrCareRecord")
	public RetDataBean deleteHrCareRecord(HrCareRecord hrCareRecord) {
		try {
			if (StringUtils.isBlank(hrCareRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrCareRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrCareRecordService.deleteHrCareRecord(hrCareRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除员工福利记录
	 * @param ids 员工福利Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrCareRecordByIds")
	public RetDataBean deleteHrCareRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrCareRecordService.deleteHrCareRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新员工关怀信息
	 * @param hrCareRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrCareRecord")
	public RetDataBean updateHrCareRecord(HrCareRecord hrCareRecord) {
		try {
			if (StringUtils.isBlank(hrCareRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrCareRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrCareRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrCareRecordService.updateHrCareRecord(example, hrCareRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加人员评定记录
	 * @param hrTitleEvaluation
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrTitleEvaluation")
	public RetDataBean insertHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrTitleEvaluation.setRecordId(SysTools.getGUID());
			hrTitleEvaluation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrTitleEvaluation.setCreateUser(userInfo.getAccountId());
			hrTitleEvaluation.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrTitleEvaluationService.insertHrTitleEvaluation(hrTitleEvaluation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员评定记录
	 * @param hrTitleEvaluation
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrTitleEvaluation")
	public RetDataBean deleteHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
		try {
			if (StringUtils.isBlank(hrTitleEvaluation.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrTitleEvaluation.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrTitleEvaluationService.deleteHrTitleEvaluation(hrTitleEvaluation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量人员职称评定记录
	 * @param ids 职称评定Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrTitleEvaluationByIds")
	public RetDataBean deleteHrTitleEvaluationByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrTitleEvaluationService.deleteHrTitleEvaluationByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新人员评定信息
	 * @param hrTitleEvaluation 人员评定对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrTitleEvaluation")
	public RetDataBean updateHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
		try {
			if (StringUtils.isBlank(hrTitleEvaluation.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrTitleEvaluation.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrTitleEvaluation.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrTitleEvaluationService.updateHrTitleEvaluation(example, hrTitleEvaluation));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加人员复职记录
	 * @param hrReinstatement
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrReinstatement")
	public RetDataBean insertHrReinstatement(HrReinstatement hrReinstatement) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrReinstatement.setRecordId(SysTools.getGUID());
			hrReinstatement.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrReinstatement.setCreateUser(userInfo.getAccountId());
			hrReinstatement.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrReinstatementService.insertHrReinstatement(hrReinstatement));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员复职记录
	 * @param hrReinstatement
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrReinstatement")
	public RetDataBean deleteHrReinstatement(HrReinstatement hrReinstatement) {
		try {
			if (StringUtils.isBlank(hrReinstatement.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrReinstatement.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrReinstatementService.deleteHrReinstatement(hrReinstatement));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除复职记录
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrReinstatementByIds")
	public RetDataBean deleteHrReinstatementByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrReinstatementService.deleteHrReinstatementByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新人员复职信息
	 * @param hrReinstatement
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrReinstatement")
	public RetDataBean updateHrReinstatement(HrReinstatement hrReinstatement) {
		try {
			if (StringUtils.isBlank(hrReinstatement.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrReinstatement.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrReinstatement.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrReinstatementService.updateHrReinstatement(example, hrReinstatement));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加人员离职动记录
	 * @param hrLeaveRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrLeaveRecord")
	public RetDataBean insertHrLeaveRecord(HrLeaveRecord hrLeaveRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrLeaveRecord.setRecordId(SysTools.getGUID());
			hrLeaveRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrLeaveRecord.setCreateUser(userInfo.getAccountId());
			hrLeaveRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrLeaveRecordService.insertHrLeaveRecord(hrLeaveRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员离职记录
	 * @param hrLeaveRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrLeaveRecord")
	public RetDataBean deleteHrLeaveRecord(HrLeaveRecord hrLeaveRecord) {
		try {
			if (StringUtils.isBlank(hrLeaveRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrLeaveRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrLeaveRecordService.deleteHrLeaveRecord(hrLeaveRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除人员离职记录
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrLeaveRecordByIds")
	public RetDataBean deleteHrLeaveRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrLeaveRecordService.deleteHrLeaveRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新人员离职信息
	 * @param hrLeaveRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrLeaveRecord")
	public RetDataBean updateHrLeaveRecord(HrLeaveRecord hrLeaveRecord) {
		try {
			if (StringUtils.isBlank(hrLeaveRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrPersonnelTransfer.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrLeaveRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrLeaveRecordService.updateHrLeaveRecord(example, hrLeaveRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加人事调动记录
	 * @param hrPersonnelTrans
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrPersonnelTransfer")
	public RetDataBean insertHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTrans) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrPersonnelTrans.setTransferId(SysTools.getGUID());
			hrPersonnelTrans.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrPersonnelTrans.setCreateUser(userInfo.getAccountId());
			hrPersonnelTrans.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrPersonnelTransferService.insertHrPersonnelTransfer(hrPersonnelTrans));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人事调动记录
	 * @param hrPersonnelTrans
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrPersonnelTransfer")
	public RetDataBean deleteHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTrans) {
		try {
			if (StringUtils.isBlank(hrPersonnelTrans.getTransferId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrPersonnelTrans.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrPersonnelTransferService.deleteHrPersonnelTransfer(hrPersonnelTrans));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除人员调动记录
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrPersonnelTransferByIds")
	public RetDataBean deleteHrPersonnelTransferByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrPersonnelTransferService.deleteHrPersonnelTransferByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新人事调动信息
	 * @param hrPersonnelTrans
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrPersonnelTransfer")
	public RetDataBean updateHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTrans) {
		try {
			if (StringUtils.isBlank(hrPersonnelTrans.getTransferId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrPersonnelTransfer.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("transferId", hrPersonnelTrans.getTransferId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrPersonnelTransferService.updateHrPersonnelTransfer(example, hrPersonnelTrans));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 工作技能导入
	 * @param file 导入文件模版
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrWorkSkills")
	public RetDataBean importHrWorkSkills(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrIncentiveService.importHrIncentive(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加劳动持能记录
	 * @param hrWorkSkills
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrWorkSkills")
	public RetDataBean insertHrWorkSkills(HrWorkSkills hrWorkSkills) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrWorkSkills.setRecordId(SysTools.getGUID());
			hrWorkSkills.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrWorkSkills.setCreateUser(userInfo.getAccountId());
			hrWorkSkills.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrWorkSkillsService.insertHrWorkSkills(hrWorkSkills));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除劳动技能
	 * @param hrWorkSkills
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrWorkSkills")
	public RetDataBean deleteHrWorkSkills(HrWorkSkills hrWorkSkills) {
		try {
			if (StringUtils.isBlank(hrWorkSkills.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrWorkSkills.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrWorkSkillsService.deleteHrWorkSkills(hrWorkSkills));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除劳动技能
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrWorkSkillsByIds")
	public RetDataBean deleteHrWorkSkillsByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrWorkSkillsService.deleteHrWorkSkillsByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新劳动技能
	 * @param hrWorkSkills
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrWorkSkills")
	public RetDataBean updateHrWorkSkills(HrWorkSkills hrWorkSkills) {
		try {
			if (StringUtils.isBlank(hrWorkSkills.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrWorkSkills.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrWorkSkills.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrWorkSkillsService.updateHrWorkSkills(example, hrWorkSkills));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 奖惩记录导入
	 * @param file 导入文件模版
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrIncentive")
	public RetDataBean importHrIncentive(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrIncentiveService.importHrIncentive(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加奖惩记录
	 * @param hrIncentive
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrIncentive")
	public RetDataBean insertHrIncentive(HrIncentive hrIncentive) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrIncentive.setIncentiveId(SysTools.getGUID());
			hrIncentive.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrIncentive.setCreateUser(userInfo.getAccountId());
			hrIncentive.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrIncentiveService.insertHrIncentive(hrIncentive));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除奖惩记录
	 * @param hrIncentive
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrIncentive")
	public RetDataBean deleteHrIncentive(HrIncentive hrIncentive) {
		try {
			if (StringUtils.isBlank(hrIncentive.getIncentiveId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrIncentive.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrIncentiveService.deleteHrIncentive(hrIncentive));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除奖惩记录
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrIncentiveByIds")
	public RetDataBean deleteHrIncentiveByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrIncentiveService.deleteHrIncentiveByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新奖惩记录
	 * @param hrIncentive
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrIncentive")
	public RetDataBean updateHrIncentive(HrIncentive hrIncentive) {
		try {
			if (StringUtils.isBlank(hrIncentive.getIncentiveId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrIncentive.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("incentiveId", hrIncentive.getIncentiveId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrIncentiveService.updateHrIncentive(example, hrIncentive));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 导入劳动记录
	 * @param file 导入文件模版
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrWorkRecord")
	public RetDataBean importHrWorkRecord(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrWorkRecordService.importHrWorkRecord(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加工作记录
	 * @param hrWorkRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrWorkRecord")
	public RetDataBean insertHrWorkRecord(HrWorkRecord hrWorkRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrWorkRecord.setRecordId(SysTools.getGUID());
			hrWorkRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrWorkRecord.setCreateUser(userInfo.getAccountId());
			hrWorkRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrWorkRecordService.insertHrWorkRecord(hrWorkRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除工作记录
	 * @param hrWorkRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrWorkRecord")
	public RetDataBean deleteHrWorkRecord(HrWorkRecord hrWorkRecord) {
		try {
			if (StringUtils.isBlank(hrWorkRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrWorkRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrWorkRecordService.deleteHrWorkRecord(hrWorkRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除工作记录
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrWorkRecordByIds")
	public RetDataBean deleteHrWorkRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrWorkRecordService.deleteHrWorkRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新工作记录
	 * @param hrWorkRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrWorkRecord")
	public RetDataBean updateHrWorkRecord(HrWorkRecord hrWorkRecord) {
		try {
			if (StringUtils.isBlank(hrWorkRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrWorkRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrWorkRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrWorkRecordService.updateHrWorkRecord(example, hrWorkRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 学习记录导入
	 * @param file 导入文件模版
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrLearnRecord")
	public RetDataBean importHrLearnRecord(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrLearnRecordService.importHrLearnRecord(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加学习记录
	 * @param hrLearnRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrLearnRecord")
	public RetDataBean insertHrLearnRecord(HrLearnRecord hrLearnRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrLearnRecord.setRecordId(SysTools.getGUID());
			hrLearnRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrLearnRecord.setCreateUser(userInfo.getAccountId());
			hrLearnRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrLearnRecordService.insertHrLearnRecord(hrLearnRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除学习记录
	 * @param hrLearnRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrLearnRecord")
	public RetDataBean deleteHrLearnRecord(HrLearnRecord hrLearnRecord) {
		try {
			if (StringUtils.isBlank(hrLearnRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrLearnRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrLearnRecordService.deleteHrLearnRecord(hrLearnRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除学习记录
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrLearnRecordByIds")
	public RetDataBean deleteHrLearnRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrLearnRecordService.deleteHrLearnRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新学习记录
	 * @param hrLearnRecord
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrLearnRecord")
	public RetDataBean updateHrLearnRecord(HrLearnRecord hrLearnRecord) {
		try {
			if (StringUtils.isBlank(hrLearnRecord.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrLearnRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", hrLearnRecord.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrLearnRecordService.updateHrLearnRecord(example, hrLearnRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 证照记录导入
	 * @param file
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrLicence")
	public RetDataBean importHrLicence(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrLicenceService.importHrLicence(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加证照记录
	 * @param hrLicence
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrLicence")
	public RetDataBean insertHrLicence(HrLicence hrLicence) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrLicence.setLicenceId(SysTools.getGUID());
			hrLicence.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrLicence.setCreateUser(userInfo.getAccountId());
			hrLicence.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrLicenceService.insertHrLicence(hrLicence));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除证照记录
	 * @param hrLicence
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrLicence")
	public RetDataBean deleteHrLicence(HrLicence hrLicence) {
		try {
			if (StringUtils.isBlank(hrLicence.getLicenceId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrLicence.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrLicenceService.deleteHrLicence(hrLicence));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 批量删除证照记录
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrLicenceByIds")
	public RetDataBean deleteHrLicenceByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrLicenceService.deleteHrLicenceByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新证照记录
	 * @param hrLicence
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrLicence")
	public RetDataBean updateHrLicence(HrLicence hrLicence) {
		try {
			if (StringUtils.isBlank(hrLicence.getLicenceId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrLicence.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("licenceId", hrLicence.getLicenceId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrLicenceService.updateHrLicence(example, hrLicence));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 人事合同导入
	 * @param file
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrContract")
	public RetDataBean importHrContract(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrContractService.importHrContract(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加HR合同
	 * @param hrContract
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrContract")
	public RetDataBean insertHrContract(HrContract hrContract) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrContract.setContractId(SysTools.getGUID());
			hrContract.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrContract.setCreateUser(userInfo.getAccountId());
			hrContract.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrContractService.insertHrContract(hrContract));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除人事合同
	 * @param ids
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrContractByIds")
	public RetDataBean deleteHrContractByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrContractService.deleteHrContractByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 删除合同
	 * @param hrContract
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrContract")
	public RetDataBean deleteHrContract(HrContract hrContract) {
		try {
			if (StringUtils.isBlank(hrContract.getContractId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrContract.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrContractService.deleteHrContract(hrContract));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新合同
	 * @param hrContract
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrContract")
	public RetDataBean updateHrContract(HrContract hrContract) {
		try {
			if (StringUtils.isBlank(hrContract.getContractId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrContract.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("contractId", hrContract.getContractId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrContractService.updateHrContract(example, hrContract));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 人事档案导入
	 * @param file 导入文件模版
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrUser")
	public RetDataBean importHrUser(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrUserService.importHrUser(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加人员信息
	 * @param hrUser 人员信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrUser")
	public RetDataBean insertHrUser(HrUser hrUser) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrUser.setUserId(SysTools.getGUID());
			hrUser.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrUser.setCreateUser(userInfo.getAccountId());
			hrUser.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrUserService.insertHrUser(hrUser));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员信息
	 * @param hrUser 人员信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrUser")
	public RetDataBean deleteHrUser(HrUser hrUser) {
		try {
			if (StringUtils.isBlank(hrUser.getUserId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrUser.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrUserService.deleteHrUser(hrUser));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 *  批量删除人员信息
	 * @param ids 人员信息Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteByHrUserIds")
	public RetDataBean deleteByHrUserIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return hrUserService.deleteByHrUserIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新人员信息
	 * @param hrUser 人员信息对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrUser")
	public RetDataBean updateHrUser(HrUser hrUser) {
		try {
			if (StringUtils.isBlank(hrUser.getUserId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrUser.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("userId", hrUser.getUserId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrUserService.updateHrUser(example, hrUser));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加人员部门
	 *
	 * @param hrDepartment
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrDepartment")
	public RetDataBean insertHrDepartment(HrDepartment hrDepartment) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			hrDepartment.setDeptId(SysTools.getGUID());
			hrDepartment.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			hrDepartment.setCreateUser(userInfo.getAccountId());
			if (StringUtils.isBlank(hrDepartment.getParentId())) {
				hrDepartment.setParentId("0");
				hrDepartment.setLevelId("/");
			} else {
				if (hrDepartment.getParentId().equals("0")) {
					hrDepartment.setLevelId("/");
				} else {
					HrDepartment parentHrDepartment = new HrDepartment();
					parentHrDepartment.setDeptId(hrDepartment.getParentId());
					parentHrDepartment.setOrgId(userInfo.getOrgId());
					parentHrDepartment = hrDepartmentService.selectOneHrDepartment(parentHrDepartment);
					if (parentHrDepartment.getLevelId().equals("/")) {
						hrDepartment.setLevelId(parentHrDepartment.getLevelId() + parentHrDepartment.getDeptId());
					} else {
						hrDepartment.setLevelId(parentHrDepartment.getLevelId() + "/" + hrDepartment.getDeptId());
					}
				}
			}
			hrDepartment.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrDepartmentService.insertHrDepartment(hrDepartment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除人员部门
	 *
	 * @param hrDepartment
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrDepartment")
	public RetDataBean deleteHrDepartment(HrDepartment hrDepartment) {
		try {
			if (StringUtils.isBlank(hrDepartment.getDeptId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			hrDepartment.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrDepartmentService.deleteHrDepartment(hrDepartment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除部门
	 *
	 * @param deptIds
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrDepartmentByBatch")
	public RetDataBean deleteHrDepartmentByBatch(String deptIds) {
		try {
			if (StringUtils.isBlank(deptIds)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			String[] deptIdArr = deptIds.split(",");
			for (String s : deptIdArr) {
				if (hrDepartmentService.isExistChild(userInfo.getOrgId(), s)) {
					return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
				}
			}
			for (String s : deptIdArr) {
				if (userInfoService.isHaveUserByDeptId(userInfo.getOrgId(), s)) {
					return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
				} else {
					HrDepartment hrDepartment = new HrDepartment();
					hrDepartment.setDeptId(s);
					hrDepartment.setOrgId(userInfo.getOrgId());
					hrDepartmentService.deleteHrDepartment(hrDepartment);
				}
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新人员部门信息
	 *
	 * @param hrDepartment Hr部门对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrDepartment")
	public RetDataBean updateHrDepartment(HrDepartment hrDepartment) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(hrDepartment.getDeptId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (hrDepartment.getDeptId().equals(hrDepartment.getLevelId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
			}
			if (StringUtils.isBlank(hrDepartment.getParentId())) {
				hrDepartment.setParentId("0");
				hrDepartment.setLevelId("/");
			} else {
				if (hrDepartment.getParentId().equals("0")) {
					hrDepartment.setLevelId("/");
				} else {
					HrDepartment parentHrDepartment = new HrDepartment();
					parentHrDepartment.setDeptId(hrDepartment.getParentId());
					parentHrDepartment.setOrgId(userInfo.getOrgId());
					parentHrDepartment = hrDepartmentService.selectOneHrDepartment(parentHrDepartment);
					if (parentHrDepartment.getLevelId().equals("/")) {
						hrDepartment.setLevelId(parentHrDepartment.getLevelId() + parentHrDepartment.getDeptId());
					} else {
						hrDepartment.setLevelId(parentHrDepartment.getLevelId() + "/" + hrDepartment.getDeptId());
					}
				}
			}
			Example example = new Example(HrDepartment.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("deptId", hrDepartment.getDeptId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrDepartmentService.updateHrDepartment(example, hrDepartment));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 导入人事部门
	 * @param file 导入文件
	 * @return 消息结构
	 */
	@PostMapping(value = "/importHrDepartment")
	public RetDataBean importHrDepartment(MultipartFile file) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			}
			return hrDepartmentService.importHrDepartment(userInfo, file);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建字典
	 *
	 * @param hrDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertHrDic")
	public RetDataBean insertHrDic(HrDic hrDic) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(hrDic.getParentId())) {
				hrDic.setParentId("0");
			}
			hrDic.setDicId(SysTools.getGUID());
			hrDic.setOrgId(userInfo.getOrgId());
			hrDic.setStatus("1");
			hrDic.setCreateUser(userInfo.getCreateUser());
			hrDic.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrDicService.insertHrDic(hrDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新字典
	 *
	 * @param hrDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateHrDic")
	public RetDataBean updateHrDic(HrDic hrDic) {
		try {
			if (StringUtils.isBlank(hrDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(HrDic.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("dicId", hrDic.getDicId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrDicService.updateHrDic(hrDic, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除字典
	 *
	 * @param hrDic 字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrDic")
	public RetDataBean deleteHrDic(HrDic hrDic) {
		try {
			if (StringUtils.isBlank(hrDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (hrDicService.isExistChild(userInfo.getOrgId(), hrDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
			hrDic.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrDicService.deleteHrDic(hrDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除字典
	 *
	 * @param ids 字典Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteHrDicByIds")
	public RetDataBean deleteHrDicByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return hrDicService.deleteHrDicByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 字典排序
	 *
	 * @param oldDicId
	 * @param newDicId
	 * @param oldSortNo
	 * @param newSortNo
	 * @return 消息结构
	 */
	@PostMapping(value = "/dropHrDic")
	public RetDataBean dropHrDic(String oldDicId, String newDicId, Integer oldSortNo, Integer newSortNo) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrDicService.dropHrDic(userInfo.getOrgId(), oldDicId, newDicId, oldSortNo, newSortNo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
}
