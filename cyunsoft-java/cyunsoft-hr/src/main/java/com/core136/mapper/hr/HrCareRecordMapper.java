package com.core136.mapper.hr;

import com.core136.bean.hr.HrCareRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrCareRecordMapper extends MyMapper<HrCareRecord> {
	/**
	 * 获取人员关怀列表
	 * @param orgId 机构码
	 * @param userId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param careType 关怀类型
	 * @param keyword 查询关键词
	 * @return 关怀列表
	 */
    List<Map<String, String>> getHrCareRecordList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,
												  @Param(value = "dateQueryType") String dateQueryType, @Param(value = "beginTime") String beginTime,
                                                  @Param(value = "endTime") String endTime, @Param(value = "careType") String careType,
                                                  @Param(value = "keyword") String keyword);
}
