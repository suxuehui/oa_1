package com.core136.mapper.hr;

import com.core136.bean.hr.HrContract;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrContractMapper extends MyMapper<HrContract> {

	/**
	 * 获取人事合同详情
	 * @param orgId 机构码
	 * @param contractId
	 * @return
	 */
	Map<String,String>getHrContractById(@Param(value = "orgId") String orgId,@Param(value = "contractId") String contractId);

	/**
	 * 获取合同列表
	 * @param orgId 机构码
	 * @param userId 人才资源Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param contractType 合同类型
	 * @param keyword 查询关键词
	 * @return 合同列表
	 */
    List<Map<String, String>> getHrContractList(@Param(value = "orgId") String orgId,
                                                @Param(value = "userId") String userId,
                                                @Param(value = "dateQueryType") String dateQueryType,
                                                @Param(value = "beginTime") String beginTime,
                                                @Param(value = "endTime") String endTime,
                                                @Param(value = "contractType") String contractType,
												@Param(value = "keyword") String keyword
    );

	/**
	 * 获取快到期的合同列表
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return
	 */
	List<Map<String, String>> getDeskHrContractList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

	/**
	 * 查询自己的合同列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 合同列表
	 */
    List<Map<String, String>> getMyHrContractList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 获取个人人事合同列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param page 当前页码
	 * @return 人事合同列表
	 */
	List<Map<String, String>> getMyHrContractListForApp(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,@Param(value = "page") Integer page);

}
