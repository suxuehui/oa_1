package com.core136.mapper.hr;

import com.core136.bean.hr.HrDic;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrDicMapper extends MyMapper<HrDic> {
    List<Map<String,String>>getHrDicParentList(@Param(value = "orgId") String orgId);

    List<Map<String,String>>getHrDicList(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId, @Param(value = "keyword") String keyword);


    /**
     * 按标识获取分类码列表
     * @param orgId 机构码
     * @param code
     * @return
     */
    List<Map<String, Object>> getHrDicByCode(@Param(value = "orgId") String orgId, @Param(value = "code") String code);
}
