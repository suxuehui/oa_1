package com.core136.mapper.hr;

import com.core136.bean.hr.HrEvaluate;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrEvaluateMapper extends MyMapper<HrEvaluate> {

	/**
	 * 获取人员评价列表
	 * @param orgId 机构码
	 * @param userId
	 * @return
	 */
	List<Map<String, String>> getHrEvaluateByUserIdList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId);

	/**
	 * 获取查询人员评价列表
	 * @param orgId 机构码
	 * @param userId
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrEvaluateQueryList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,@Param(value="dateQueryType")String dateQueryType,
                                                     @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "status") String status,
                                                     @Param(value = "keyword") String keyword
    );


}
