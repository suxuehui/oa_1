package com.core136.mapper.hr;

import com.core136.bean.hr.HrKpiPlanRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrKpiPlanRecordMapper extends MyMapper<HrKpiPlanRecord> {

	/**
	 * 获取个人考核分数明细
	 * @param orgId 机构码
	 * @param planId 考核计划
	 * @param accountId 用户账号
	 * @return 个人考核分数明细
	 */
    List<Map<String, String>> getMyHrKpiScoreList(@Param(value = "orgId") String orgId, @Param(value = "planId") String planId, @Param(value = "accountId") String accountId);

}
