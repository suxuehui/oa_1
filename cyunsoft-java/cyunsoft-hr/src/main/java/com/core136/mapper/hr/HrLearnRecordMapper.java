package com.core136.mapper.hr;

import com.core136.bean.hr.HrLearnRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrLearnRecordMapper extends MyMapper<HrLearnRecord> {
	/**
	 * 获取教育经历列表
	 * @param orgId 机构码
	 * @param userId
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrLearnRecordList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,@Param(value = "dateQueryType") String dateQueryType,
                                                   @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
												   @Param(value = "highsetDegree") String highsetDegree,@Param(value = "keyword") String keyword);

	/**
	 * 个人查询学习记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 学习记录
	 */
    List<Map<String, String>> getMyHrLearnRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
