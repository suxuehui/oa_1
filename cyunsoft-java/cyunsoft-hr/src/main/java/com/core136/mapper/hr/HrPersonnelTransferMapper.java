package com.core136.mapper.hr;

import com.core136.bean.hr.HrPersonnelTransfer;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrPersonnelTransferMapper extends MyMapper<HrPersonnelTransfer> {
	/**
	 * 获取人员调动列表
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param transferType
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrPersonnelTransferList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,@Param(value="dateQueryType")String dateQueryType,
                                                         @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "transferType") String transferType,
                                                         @Param(value = "keyword") String keyword);

	/**
	 * 人个工作调动记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 工作调动记录
	 */
    List<Map<String, String>> getMyHrPersonnelTransferList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
