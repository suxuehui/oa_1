package com.core136.mapper.hr;

import com.core136.bean.hr.HrRecruitNeeds;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrRecruitNeedsMapper extends MyMapper<HrRecruitNeeds> {

	/**
	 * 获取需求列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param occupation
	 * @param education
	 * @param status
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrRecruitNeedsList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                    @Param(value = "occupation") String occupation, @Param(value = "education") String education,
                                                    @Param(value = "status") String status, @Param(value = "dateQueryType") String dateQueryType,
													@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword
    );

	/**
	 * 获取待审批需求列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param occupation
	 * @param education
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getApprovedHrRecruitNeedsList(
            @Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
            @Param(value = "occupation") String occupation, @Param(value = "education") String education,
			@Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,@Param(value = "keyword") String keyword);


}
