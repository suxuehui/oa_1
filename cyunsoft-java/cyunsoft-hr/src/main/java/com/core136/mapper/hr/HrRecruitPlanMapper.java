package com.core136.mapper.hr;

import com.core136.bean.hr.HrRecruitPlan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrRecruitPlanMapper extends MyMapper<HrRecruitPlan> {

	/**
	 * 获取招聘计划列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrRecruitPlanList(@Param(value = "orgId") String orgId,@Param(value = "accountId") String accountId,
												   @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                   @Param(value = "keyword") String keyword
    );

	/**
	 * 获取当前可填报的招聘计划
	 * @param orgId 机构码
	 * @param endTime
	 * @return
	 */
    List<Map<String, String>> getHrRecruitPlanForSelect(@Param(value = "orgId") String orgId, @Param(value = "endTime") String endTime);

}
