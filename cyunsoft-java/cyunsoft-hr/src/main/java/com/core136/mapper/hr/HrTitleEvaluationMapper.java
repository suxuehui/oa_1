package com.core136.mapper.hr;

import com.core136.bean.hr.HrTitleEvaluation;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface HrTitleEvaluationMapper extends MyMapper<HrTitleEvaluation> {

	/**
	 * 获取人员职称评定列表
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param getType
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrTitleEvaluationList(
            @Param(value = "orgId") String orgId,
            @Param(value = "userId") String userId,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "getType") String getType,
            @Param(value = "keyword") String keyword);

}
