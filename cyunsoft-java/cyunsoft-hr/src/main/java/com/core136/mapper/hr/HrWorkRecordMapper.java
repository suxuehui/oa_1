package com.core136.mapper.hr;

import com.core136.bean.hr.HrWorkRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HrWorkRecordMapper extends MyMapper<HrWorkRecord> {
	/**
	 * 获取工作记录
	 * @param orgId 机构码
	 * @param userId 用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param nature
	 * @param keyword 查询关键词
	 * @return
	 */
    List<Map<String, String>> getHrWorkRecordList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,@Param(value="dateQueryType")String dateQueryType,
                                                  @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "nature") String nature,
                                                  @Param(value = "keyword") String keyword);

	/**
	 * 查询个人工作经历
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 个人工作经历
	 */
    List<Map<String, String>> getMyHrWorkRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
