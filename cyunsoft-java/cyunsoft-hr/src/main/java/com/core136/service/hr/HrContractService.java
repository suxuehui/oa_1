package com.core136.service.hr;


import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrContract;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrUser;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrContractMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class HrContractService {
    private HrContractMapper hrContractMapper;
	private HrUserService hrUserService;
	private HrDicService hrDicService;
	@Autowired
	public void setHrContractMapper(HrContractMapper hrContractMapper) {
		this.hrContractMapper = hrContractMapper;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}

	public int insertHrContract(HrContract hrContract) {
        return hrContractMapper.insert(hrContract);
    }

    public int deleteHrContract(HrContract hrContract) {
        return hrContractMapper.delete(hrContract);
    }

    public int updateHrContract(Example example, HrContract hrContract) {
        return hrContractMapper.updateByExampleSelective(hrContract, example);
    }

    public HrContract selectOneHrContract(HrContract hrContract) {
        return hrContractMapper.selectOne(hrContract);
    }

	/**
	 * 批量删除合同
	 * @param orgId 机构码
	 * @param list 合同Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteHrContractByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrContract.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("contractId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrContractMapper.deleteByExample(example));
		}
	}

	/**
	 * 获取合同列表
	 * @param orgId 机构码
	 * @param userId Hr用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param contractType 合同类型
	 * @param keyword 查询关键词
	 * @return 合同列表
	 */
    public List<Map<String, String>> getHrContractList(String orgId, String userId,String dateQueryType, String beginTime, String endTime, String contractType,String keyword) {
        return hrContractMapper.getHrContractList(orgId, userId, dateQueryType,beginTime, endTime, contractType,"%"+keyword+"%");
    }

    /**
     * 获取人事合同详情
     * @param orgId 机构码
     * @param contractId 合同Id
     * @return 合同详情
     */
    public Map<String,String> getHrContractById(String orgId,String contractId)
    {
        return hrContractMapper.getHrContractById(orgId,contractId);
    }

	/**
	 * 查询自己的合同列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 合同列表
	 */
    public List<Map<String, String>> getMyHrContractList(String orgId, String accountId) {
        return hrContractMapper.getMyHrContractList(orgId, accountId);
    }

    /**
     * 获取个人人事合同列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param page 当前页码
     * @return 人事合同列表
     */
    public List<Map<String, String>> getMyHrContractListForApp(String orgId, String accountId,Integer page) {
        return hrContractMapper.getMyHrContractListForApp(orgId, accountId,page);
    }


	/**
	 * 获取合同列表
	 * @param pageParam 分页参数
	 * @param userId Hr用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param contractType 合同类型
	 * @return 合同列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrContractList(PageParam pageParam, String userId,String dateQueryType, String beginTime, String endTime, String contractType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrContractList(pageParam.getOrgId(), userId,dateQueryType, beginTime, endTime, contractType,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 查询自己的合同列表
	 * @param pageParam 分页参数
	 * @return 合同列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyHrContractList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrContractList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
    }

	/**
	 * 人事合同导入
	 * @param user 用户对象
	 * @param file 导入文件模版
	 * @return 消息结构
	 * @throws IOException 服务器内部异常
	 */
	@Transactional(value = "generalTM")
    public RetDataBean importHrContract(UserInfo user, MultipartFile file) throws IOException {
        List<String> titleList = new ArrayList<>();
		List<HrContract> hrContractList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("应聘岗位");
		titleList.add("合同编号");
		titleList.add("合同类型");
		titleList.add("签约公司");
		titleList.add("签约方式");
		titleList.add("关联档案人员");
		titleList.add("合同生效时间");
		titleList.add("合同终止时间");
		titleList.add("合同期限属性");
		titleList.add("合同签订时间");
		titleList.add("备注");
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrContract hrContract = new HrContract();
			hrContract.setContractId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						hrContract.setSortNo(Integer.parseInt(tempMap.get(s)));
					}else{
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("应聘岗位")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic workJob = new HrDic();
						workJob.setOrgId(user.getOrgId());
						workJob.setName(tempMap.get(s));
						workJob.setCode("workJob");
						try{
							workJob = hrDicService.selectOneHrDic(workJob);
							if(workJob!=null) {
								hrContract.setWorkJob(workJob.getKeyValue());
							}else{
								resList.add("未能查询到人员岗位!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询人员岗位出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("人员岗位不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("合同编号")) {
					hrContract.setContractCode(tempMap.get(s));
				}
				if (s.equals("合同类型")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic contractType= new HrDic();
						contractType.setOrgId(user.getOrgId());
						contractType.setName(tempMap.get(s));
						contractType.setCode("contractType");
						try{
							contractType = hrDicService.selectOneHrDic(contractType);
							if(contractType!=null) {
								hrContract.setContractType(contractType.getKeyValue());
							}else{
								resList.add("未能查询到合同类型!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询合同类型出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("合同类型不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("签约公司")) {
					hrContract.setEnterprise(tempMap.get(s));
				}
				if (s.equals("签约方式")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic signType= new HrDic();
						signType.setOrgId(user.getOrgId());
						signType.setName(tempMap.get(s));
						signType.setCode("signType");
						try{
							signType = hrDicService.selectOneHrDic(signType);
							if(signType!=null) {
								hrContract.setContractType(signType.getKeyValue());
							}else{
								resList.add("未能查询到签约方式!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询合同签约方式出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("签约方式不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("关联档案人员")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(user.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try{
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if(hrUser!=null) {
								hrContract.setUserId(hrUser.getUserId());
							}else{
								resList.add("未能查询档案关联人员!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("关联档案人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("关联档案人员不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("合同生效时间")) {
					hrContract.setStartTime(tempMap.get(s));
				}
				if (s.equals("合同终止时间")) {
					hrContract.setEndTime(tempMap.get(s));
				}
				if (s.equals("合同期限属性")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic specialization= new HrDic();
						specialization.setOrgId(user.getOrgId());
						specialization.setName(tempMap.get(s));
						specialization.setCode("specialization");
						try{
							specialization = hrDicService.selectOneHrDic(specialization);
							if(specialization!=null) {
								hrContract.setSpecialization(specialization.getKeyValue());
							}else{
								resList.add("未能查询到合同期限属性!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						}catch (Exception e) {
							resList.add("查询合同合同期限属性出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}else{
						resList.add("合同期限属性不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("合同签订时间")) {
					hrContract.setSignTime(tempMap.get(s));
				}
				if (s.equals("备注")) {
					hrContract.setRemark(tempMap.get(s));
				}

			}
			hrContract.setCreateTime(createTime);
			hrContract.setCreateUser(user.getAccountId());
			hrContract.setOrgId(user.getOrgId());
			if(insertFlag) {
				hrContractList.add(hrContract);
			}
		}
		for(HrContract hrContract:hrContractList) {
			insertHrContract(hrContract);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }


	/**
	 * 获取快到期的合同列表
	 * @param orgId 机构码
	 * @return 到期合同列表
	 */
	public List<Map<String, String>> getDeskHrContractList(String orgId) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d);
        gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));
        gc.add(2, 1);
        String endTime = sf.format(gc.getTime());
        String beginTime = SysTools.getTime("yyyy-MM-dd");
        return hrContractMapper.getDeskHrContractList(orgId, beginTime, endTime);
    }

    /**
     * 获取需要到期提醒的合同
     * @param orgId 机构码
     * @return 到期合同列表
     */
    public List<HrContract> getHrContractReminderRecord(String orgId)
    {
        HrContract hrContract = new HrContract();
        hrContract.setOrgId(orgId);
        hrContract.setReminder("1");
        return hrContractMapper.select(hrContract);
    }

}
