package com.core136.service.hr;

import com.core136.bean.hr.HrDic;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrDicMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class HrDicService {
    private HrDicMapper hrDicMapper;
	@Autowired
	public void setHrDicMapper(HrDicMapper hrDicMapper) {
		this.hrDicMapper = hrDicMapper;
	}

	/**
     * 添加
     * @param hrDic Hr字典
     * @return 成功创建记录数
     */
    public int insertHrDic(HrDic hrDic) {
        return hrDicMapper.insert(hrDic);
    }

    /**
     * 删除
     * @param hrDic Hr字典
     * @return 成功删除记录数
     */
    public int deleteHrDic(HrDic hrDic) {
        return hrDicMapper.delete(hrDic);
    }

    /**
     * 批量删除字典
     * @param orgId 机构码
     * @param list 字典Id 数组
     * @return 消息结构
     */
    public RetDataBean deleteHrDicByIds(String orgId, List<String> list) {
        if(!list.isEmpty()) {
            Example example = new Example(HrDic.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("dicId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS,hrDicMapper.deleteByExample(example));
        }else
        {
            return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
        }
    }


    /**
     * 按条件删除
     * @param example 删除条件
     * @return 成功删除记录数
     */
    public int deleteHrDic(Example example) {
        return hrDicMapper.deleteByExample(example);
    }

    /**
     * 按条件更新
     * @param hrDic Hr字典对象
     * @param example 更新条件
     * @return 成功更新记录数
     */
    public int updateHrDic(HrDic hrDic, Example example) {
        return hrDicMapper.updateByExampleSelective(hrDic, example);
    }

    /**
     * 字典排序
     * @param orgId 机构码
     * @param oldDicId 历史字典Id
     * @param newDicId 亲字典Id
     * @param oldSortNo 历史字典排序号
     * @param newSortNo 新字典排序号
     * @return 消息结构
     */
    @Transactional(value = "generalTM")
    public RetDataBean dropHrDic(String orgId,String oldDicId, String newDicId,Integer oldSortNo,Integer newSortNo) {
        try {
            HrDic oldHrDic = new HrDic();
            oldHrDic.setSortNo(newSortNo);
            HrDic newHrDic = new HrDic();
            newHrDic.setSortNo(oldSortNo);
            Example example = new Example(HrDic.class);
            example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",oldDicId);
            updateHrDic(oldHrDic,example);
            Example example1 = new Example(HrDic.class);
            example1.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",newDicId);
            updateHrDic(newHrDic,example1);
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 判断字典分类下是否有子集
     * @param orgId 机构码
     * @param parentId 父级Id
     * @return 分类是否有子集
     */
    public boolean isExistChild(String orgId,String parentId)
    {
        HrDic hrDic = new HrDic();
        hrDic.setOrgId(orgId);
        hrDic.setParentId(parentId);
        return hrDicMapper.selectCount(hrDic) > 0;
    }

    /**
     * 获取分类
     * @param hrDic Hr字典对象
     * @return 字典对象
     */
    public HrDic selectOneHrDic(HrDic hrDic) {
        return hrDicMapper.selectOne(hrDic);
    }

    /**
     * 获取字典分类列表
     * @param orgId 机构码
     * @return 字典分类列表
     */
    public List<Map<String,String>>getHrDicParentList(String orgId)
    {
        return hrDicMapper.getHrDicParentList(orgId);
    }

    List<HrDic> getAllHrDic(String orgId)
    {
        Example example = new Example(HrDic.class);
        example.createCriteria().andEqualTo("orgId",orgId);
        example.setOrderByClause("sort_no asc");
        return hrDicMapper.selectByExample(example);
    }

    public List<HrDic> getHrDicTree(String orgId) {
        List<HrDic> list =getAllHrDic(orgId);
        List<HrDic> hrDicList = new ArrayList<>();
		for (HrDic dic : list) {
			if (dic.getParentId().equals("0")) {
				hrDicList.add(dic);
			}
		}
        for (HrDic hrDic : hrDicList) {
            hrDic.setChildren(getChild(hrDic.getDicId(), list));
        }
        return hrDicList;
    }

    /**
     * 获取字典子集
     * @param dicId 字典Id
     * @param rootHrDic 字典列表
     * @return 字典列表
     */
    private List<HrDic> getChild(String dicId, List<HrDic> rootHrDic) {
        List<HrDic> childList = new ArrayList<>();
        for (HrDic hrDic : rootHrDic) {
            if (hrDic.getParentId().equals(dicId)) {
                childList.add(hrDic);
            }
        }
        for (HrDic hrDic : childList) {
            hrDic.setChildren(getChild(hrDic.getDicId(), rootHrDic));
        }
        if (childList.isEmpty()) {
            return Collections.emptyList();
        }
        return childList;
    }

    /**
     * 获取字典列表
     * @param pageParam 分页参数
     * @return 字典列表
     */
    public PageInfo<Map<String,String>> getHrDicList(PageParam pageParam, String parentId) throws Exception{
        PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrDicList(pageParam.getOrgId(),parentId,pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }


    /**
     * 获取字典列表
     * @param orgId 机构码
     * @param parentId 父级Id
     * @param keyword 查询关键词
     * @return 字典列表
     */
    public List<Map<String,String>> getHrDicList(String orgId,String parentId,String keyword) {
        return hrDicMapper.getHrDicList(orgId,parentId,"%"+keyword+"%");
    }

    /**
     * 按标识获取分类码列表
     */

    public List<Map<String, Object>> getHrDicByCode(String orgId, String code) {
        return hrDicMapper.getHrDicByCode(orgId, code);
    }

    /**
     * 获取分类列表
     * @param hrDic Hr字典对象
     * @return 分类列表
     */
    public List<HrDic> getHrDicList(HrDic hrDic) {
        return hrDicMapper.select(hrDic);
    }

}
