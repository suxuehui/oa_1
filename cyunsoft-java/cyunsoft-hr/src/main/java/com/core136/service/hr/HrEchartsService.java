package com.core136.service.hr;


import com.core136.bean.account.UserInfo;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.bi.option.property.*;
import com.core136.bi.option.resdata.LegendData;
import com.core136.bi.option.resdata.SeriesData;
import com.core136.bi.option.style.*;
import com.core136.bi.option.units.BarOption;
import com.core136.bi.option.units.PieOption;
import com.core136.mapper.hr.HrEchartsMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class HrEchartsService {
    private final PieOption pieOption = new PieOption();
    private final BarOption barOption = new BarOption();
    private HrEchartsMapper hrEchartsMapper;
	@Autowired
	public void setHrEchartsMapper(HrEchartsMapper hrEchartsMapper) {
		this.hrEchartsMapper = hrEchartsMapper;
	}

	/**
	 * 人员关怀类型分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return  人员关怀类Table
	 */
	public List<Map<String, String>> getCareTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.care_type";
            module = "careType";
        }
        return hrEchartsMapper.getCareAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 人员关怀类型柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 人员关怀类型柱状图
	 */
	public OptionConfig getCareBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.care_type";
            module = "careType";
            optionSeriesName = "人员关怀类型分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getCareAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 人员关怀类型饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 人员关怀类型饼状
	 */
	public OptionConfig getCarePieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.care_type";
            module = "careType";
            optionSeriesName = "人员关怀类型分析";
            optionTitleText = "人员关怀类型统计";
            optionTitleSubtext = "人员关怀类型占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getCareAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 职称分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return  职称分析数据
	 */
	public List<Map<String, String>> getEvaluationTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.get_type";
            module = "getType";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
        } else if (dataType.equals("3")) {
            groupBy = "u.post_name";
            module = "postName";
        }
        return hrEchartsMapper.getEvaluationAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 称职评定柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 称职评定柱状图
	 */
	public OptionConfig getEvaluationBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.get_type";
            module = "getType";
            optionSeriesName = "称职评定分析";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
            optionSeriesName = "称职评定分析";
        } else if (dataType.equals("3")) {
            groupBy = "u.post_name";
            module = "postName";
            optionSeriesName = "称职评定分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getEvaluationAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 称职评定饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 称职评定饼状图
	 */
    public OptionConfig getEvaluationPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.get_type";
            module = "getType";
            optionSeriesName = "称职评定分析";
            optionTitleText = "称职评定统计";
            optionTitleSubtext = "称职评定占比";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
            optionSeriesName = "称职评定分析";
            optionTitleText = "称职评定统计";
            optionTitleSubtext = "称职评定占比";
        } else if (dataType.equals("3")) {
            groupBy = "u.post_name";
            module = "postName";
            optionSeriesName = "称职评定分析";
            optionTitleText = "称职评定统计";
            optionTitleSubtext = "称职评定占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getEvaluationAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 人员复职分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 人员复职分析
	 */
    public List<Map<String, String>> getReinstatTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.reinstatement_type";
            module = "reinstatementType";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
        }
        return hrEchartsMapper.getReinstatAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 人员复职柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
	public OptionConfig getReinstatBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.reinstatement_type";
            module = "reinstatementType";
            optionSeriesName = "人员复职分析";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
            optionSeriesName = "人员复职分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getReinstatAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 人员复职饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 人员复职饼状图
	 */
	public OptionConfig getReinstatPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.reinstatement_type";
            module = "reinstatementType";
            optionSeriesName = "人员复职分析";
            optionTitleText = "人员复职统计";
            optionTitleSubtext = "人员复职占比";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
            optionSeriesName = "人员复职分析";
            optionTitleText = "人员复职统计";
            optionTitleSubtext = "人员复职占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getReinstatAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }


	/**
	 * 人员离职分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 人员离职分析
	 */
    public List<Map<String, String>> getLeaveTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.leave_type";
            module = "leaveType";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
        }
        return hrEchartsMapper.getLeaveAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 人员离职柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
    public OptionConfig getLeaveBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.leave_type";
            module = "leaveType";
            optionSeriesName = "人员离职分析";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
            optionSeriesName = "人员离职分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getLeaveAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 人员离职饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
    public OptionConfig getLeavePieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.leave_type";
            module = "leaveType";
            optionSeriesName = "人员离职分析";
            optionTitleText = "人员离职统计";
            optionTitleSubtext = "人员离职占比";
        } else if (dataType.equals("2")) {
            groupBy = "h.dept_id";
            module = "deptId";
            optionSeriesName = "人员离职分析";
            optionTitleText = "人员离职统计";
            optionTitleSubtext = "人员离职占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getLeaveAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 调动类型分析tabale
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
    public List<Map<String, String>> getTransferTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.transfer_type";
            module = "transferType";
        }
        return hrEchartsMapper.getTransferAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 调动类型柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
    public OptionConfig getTransferBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.transfer_type";
            module = "transferType";
            optionSeriesName = "调动类型分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getTransferAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 调动类型饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
    public OptionConfig getTransferPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.transfer_type";
            module = "transferType";
            optionSeriesName = "调动类型分析";
            optionTitleText = "调动类型统计";
            optionTitleSubtext = "调动类型占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getTransferAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 劳动持能分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
    public List<Map<String, String>> getSkillsTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.skills_level";
            module = "skillsLevel";
        }
        return hrEchartsMapper.getSkillsAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 劳动技能柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 劳动技能柱状图
	 */
    public OptionConfig getSkillsBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.skills_level";
            module = "skillsLevel";
            optionSeriesName = "劳动技能分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getSkillsAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 劳动技能饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 劳动技能饼状图
	 */
    public OptionConfig getSkillsPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.skills_level";
            module = "skillsLevel";
            optionSeriesName = "劳动技能分析";
            optionTitleText = "劳动技能统计";
            optionTitleSubtext = "劳动技能占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getSkillsAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 学习经历分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 学习经历分析
	 */
    public List<Map<String, String>> getLearnTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.highset_degree";
            module = "highsetDegree";
        }
        return hrEchartsMapper.getLearnAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 学习经历柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return  学习经历柱状图
	 */
    public OptionConfig getLearnBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.highset_degree";
            module = "highsetDegree";
            optionSeriesName = "学位分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getLearnAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 学习经历饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 学习经历饼状图
	 */
    public OptionConfig getLearnPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.highset_degree";
            module = "highsetDegree";
            optionSeriesName = "学位分析";
            optionTitleText = "学位分析统计";
            optionTitleSubtext = "学位分析占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getLearnAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 证照类型 table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 证照类型
	 */
	public List<Map<String, String>> getLicenceTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.licence_type";
            module = "licenceType";
        }
        return hrEchartsMapper.getLicenceAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 证照柱状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 证照柱状图
	 */
    public OptionConfig getLicenceBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.licence_type";
            module = "licenceType";
            optionSeriesName = "证照分析";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getLicenceAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 证照饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 证照饼状图
	 */
    public OptionConfig getLicencePieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.licence_type";
            module = "licenceType";
            optionSeriesName = "证照分析";
            optionTitleText = "证照分析统计";
            optionTitleSubtext = "证照分析占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getLicenceAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 奖惩分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 奖惩分析
	 */
    public List<Map<String, String>> getIncentiveTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.incentive_type";
            module = "incentiveType";
        } else if (dataType.equals("2")) {
            groupBy = "u.incentive_item";
            module = "incentiveItem";
        }
        return hrEchartsMapper.getIncentiveAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 奖惩分析柱状图
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 奖惩分析柱状图
	 */
	public OptionConfig getIncentiveBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.INCENTIVE_TYPE";
            module = "incentiveType";
            optionSeriesName = "奖惩类型";
        } else if (dataType.equals("2")) {
            groupBy = "U.INCENTIVE_ITEM";
            module = "incentiveItem";
            optionSeriesName = "处理事项";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getIncentiveAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 奖惩分析饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 奖惩分析饼状图
	 */
	public OptionConfig getIncentivePieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.incentive_type";
            module = "incentiveType";
            optionSeriesName = "奖惩类型";
            optionTitleText = "奖惩类型统计";
            optionTitleSubtext = "奖惩类型占比";
        } else if (dataType.equals("2")) {
            groupBy = "u.incentive_item";
            module = "incentiveItem";
            optionSeriesName = "处理事项";
            optionTitleText = "处理事项统计";
            optionTitleSubtext = "处理事项占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getIncentiveAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }


	/**
	 * 合同分析table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 合同分析
	 */
	public List<Map<String, String>> getContractTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.contract_type";
            module = "contractType";
        } else if (dataType.equals("2")) {
            groupBy = "u.enterprise";
            module = "enterprise";
        } else if (dataType.equals("3")) {
            groupBy = "u.sign_type";
            module = "signType";
        } else if (dataType.equals("4")) {
            groupBy = "u.work_job";
            module = "workJob";
        }
        return hrEchartsMapper.getContractAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 合同柱状图
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 合同柱状图
	 */
    public OptionConfig getContractBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.contract_type";
            module = "contractType";
            optionSeriesName = "合同类型";
        } else if (dataType.equals("2")) {
            groupBy = "u.enterprise";
            module = "enterprise";
            optionSeriesName = "签约公司";
        } else if (dataType.equals("3")) {
            groupBy = "u.sign_type";
            module = "signType";
            optionSeriesName = "签约方式";
        } else if (dataType.equals("4")) {
            groupBy = "u.work_job";
            module = "workJob";
            optionSeriesName = "应聘岗位";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getContractAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 合同饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 合同饼状图
	 */
    public OptionConfig getContractPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.contract_type";
            module = "contractType";
            optionSeriesName = "合同类型";
            optionTitleText = "合同类型统计";
            optionTitleSubtext = "合同类型占比";
        } else if (dataType.equals("2")) {
            groupBy = "u.enterprise";
            module = "enterprise";
            optionSeriesName = "签约公司";
            optionTitleText = "签约公司统计";
            optionTitleSubtext = "签约公司占比";
        } else if (dataType.equals("3")) {
            groupBy = "u.sign_type";
            module = "signType";
            optionSeriesName = "签约方式";
            optionTitleText = "签约方式统计";
            optionTitleSubtext = "签约方式占比";
        } else if (dataType.equals("4")) {
            groupBy = "u.work_job";
            module = "workJob";
            optionSeriesName = "应聘岗位";
            optionTitleText = "应聘岗位统计";
            optionTitleSubtext = "应聘岗位占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getContractAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 人事档案分析Table
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return 人事档案分析
	 */
	public List<Map<String, String>> getBaseInfoTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.work_type";
            module = "workType";
        } else if (dataType.equals("2")) {
            groupBy = "u.highset_shool";
            module = "education";
        } else if (dataType.equals("3")) {
            groupBy = "u.technical_title";
            module = "technicalTitle";
        } else if (dataType.equals("4")) {
            groupBy = "u.native_place";
            module = "nativePlace";
        } else if (dataType.equals("5")) {
            groupBy = "u.wages_level";
            module = "wagesLevel";
        } else if (dataType.equals("6")) {
            groupBy = "u.political_status";
            module = "politicalStatus";
        }
        return hrEchartsMapper.getBaseInfoAnalysis(orgId, deptId, module, groupBy);
    }

	/**
	 * 人事档案柱状图
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
	public OptionConfig getBaseInfoBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.work_type";
            module = "workType";
            optionSeriesName = "工种";
        } else if (dataType.equals("2")) {
            groupBy = "u.highset_shool";
            module = "education";
            optionSeriesName = "学历";
        } else if (dataType.equals("3")) {
            groupBy = "u.technical_title";
            module = "technicalTitle";
            optionSeriesName = "职称";
        } else if (dataType.equals("4")) {
            groupBy = "u.native_place";
            module = "nativePlace";
            optionSeriesName = "地区";
        } else if (dataType.equals("5")) {
            groupBy = "u.wages_level";
            module = "wagesLevel";
            optionSeriesName = "工资级别";
        } else if (dataType.equals("6")) {
            groupBy = "u.political_status";
            module = "politicalStatus";
            optionSeriesName = "政治面貌";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getBaseInfoAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

	/**
	 * 人事基本档案饼状图分析
	 * @param orgId 机构码
	 * @param deptId 部门Id
	 * @param dataType 查询类型
	 * @return
	 */
    public OptionConfig getBaseInfoPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.work_type";
            module = "workType";
            optionSeriesName = "工种";
            optionTitleText = "人员工种统计";
            optionTitleSubtext = "人员工种占比";
        } else if (dataType.equals("2")) {
            groupBy = "u.highset_shool";
            module = "education";
            optionSeriesName = "学历";
            optionTitleText = "人员学历统计";
            optionTitleSubtext = "人员学历占比";
        } else if (dataType.equals("3")) {
            groupBy = "u.technical_title";
            module = "technicalTitle";
            optionSeriesName = "职称";
            optionTitleText = "人员职称统计";
            optionTitleSubtext = "人员职称占比";
        } else if (dataType.equals("4")) {
            groupBy = "u.native_place";
            module = "nativePlace";
            optionSeriesName = "地区";
            optionTitleText = "人员籍贯统计";
            optionTitleSubtext = "人员籍贯占比";
        } else if (dataType.equals("5")) {
			groupBy = "u.wages_level";
			module = "wagesLevel";
            optionSeriesName = "工资级别";
            optionTitleText = "人员工资级别统计";
            optionTitleSubtext = "人员工资级别占比";
        } else if (dataType.equals("6")) {
            groupBy = "u.political_status";
            module = "politicalStatus";
            optionSeriesName = "政治面貌";
            optionTitleText = "人员政治面貌统计";
            optionTitleSubtext = "人员政治面貌占比";
        }
        List<Map<String, String>> resdataList = hrEchartsMapper.getBaseInfoAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

	/**
	 * 档案分析柱状图
	 * @param resdataList
	 * @param seriesDataName
	 * @return
	 */
	public OptionConfig getBarForAnalysis(List<Map<String, String>> resdataList, String seriesDataName) {
        OptionConfig optionConfig = new OptionConfig();
        String[] xdata = new String[resdataList.size()];
        Double[] ydata = new Double[resdataList.size()];
        for (int i = 0; i < resdataList.size(); i++) {
            if (StringUtils.isNotBlank(resdataList.get(i).get("name"))) {
                xdata[i] = resdataList.get(i).get("name");
            } else {
                xdata[i] = "other" + i;
            }
            ydata[i] = Double.valueOf(String.valueOf(resdataList.get(i).get("value")));
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName(seriesDataName);
        optionSeries.setType("bar");
        optionSeries.setBarWidth("60%");
        optionSeries.setData(ydata);
        OptionXAxis xAxis = new OptionXAxis();
        xAxis.setType("category");
        xAxis.setData(xdata);
        AxisTick axisTick = new AxisTick();
        axisTick.setAlignWithLabel(true);
        xAxis.setAxisTick(axisTick);
        optionConfig = barOption.getBarTickAlignChartOption(new OptionXAxis[]{xAxis}, new OptionSeries[]{optionSeries});
        OptionTooltip optionTooltip = new OptionTooltip();
        optionTooltip.setTrigger("axis");
        AxisPointer axisPointer = new AxisPointer();
        axisPointer.setType("shadow");
        optionTooltip.setAxisPointer(axisPointer);
        optionConfig.setTooltip(optionTooltip);
        OptionGrid optionGrid = new OptionGrid();
        optionGrid.setLeft("3%");
        optionGrid.setRight("4%");
        optionGrid.setBottom("3%");
        optionGrid.setContainLabel(true);
        optionConfig.setGrid(optionGrid);
        return optionConfig;
    }


	/**
	 * 人事档案的饼状图分析
	 * @param resdataList
	 * @param optionSeriesName
	 * @param optionTitleText
	 * @param optionTitleSubtext
	 * @return 人事档案的饼状图
	 */
    public OptionConfig getPieForAnalysis(List<Map<String, String>> resdataList, String optionSeriesName, String optionTitleText, String optionTitleSubtext) {
        OptionConfig optionConfig = new OptionConfig();
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName(optionSeriesName);
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText(optionTitleText);
        optionTitle.setSubtext(optionTitleSubtext);
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }


	/**
	 * HR人员籍贯占比
	 * @param user 用户对象
	 * @return HR人员籍贯占比
	 */
	public OptionConfig getNativePlacePie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = hrEchartsMapper.getNativePlacePie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("地区");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("人员籍贯统计");
        optionTitle.setSubtext("人员籍贯占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

	/**
	 * HR人员工种对比
	 * @param userInfo 用户对象
	 * @return HR人员工种对比
	 */
	public OptionConfig getWorkTypeBar(UserInfo userInfo) {
        List<Map<String, Object>> resList = hrEchartsMapper.getWorkTypeBar(userInfo.getOrgId());
        OptionConfig optionConfig = new OptionConfig();
        String[] xAxisData = new String[resList.size()];
        Object[] data = new Object[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            if (resList.get(i).get("name") == null) {
                xAxisData[i] = "其它";
            } else {
                xAxisData[i] = resList.get(i).get("name").toString();
            }
            data[i] = resList.get(i).get("value");
        }
        OptionSeries[] seriesDatas = new OptionSeries[1];
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setType("bar");
        optionSeries.setData(data);

        seriesDatas[0] = optionSeries;
        optionConfig = barOption.getBarSimpleChartOption(xAxisData, seriesDatas);
        return optionConfig;
    }

	/**
	 * 获取HR学历占比
	 * @param user 用户对象
	 * @return
	 */
    public OptionConfig getHighsetShoolPie(UserInfo user) {
        List<Map<String, Object>> resList = hrEchartsMapper.getHighsetShoolPie(user.getOrgId());
        OptionConfig optionConfig = new OptionConfig();
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("学历占比");
        optionSeries.setType("pie");
        optionSeries.setRadius(new String[]{"50%", "70%"});
        optionSeries.setAvoidLabelOverlap(false);
        Label label = new Label();
        label.setShow(false);
        label.setPosition("center");
        optionSeries.setLabel(label);
        Emphasis emphasis = new Emphasis();
        Label label1 = new Label();
        label1.setShow(true);
        label1.setFontSize(20);
        label1.setFontWeight("bold");
        emphasis.setLabel(label1);
        optionSeries.setEmphasis(emphasis);
        LabelLine labelLine = new LabelLine();
        labelLine.setShow(false);
        optionSeries.setLabelLine(labelLine);
        LegendData[] legendDatas = new LegendData[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            LegendData legendData = new LegendData();
            legendData.setName(resList.get(i).get("name").toString());
            legendDatas[i] = legendData;
        }
        optionSeries.setData(resList.toArray());
        optionSeriesArr[0] = optionSeries;
        optionConfig = pieOption.getDoughnutChartOption(legendDatas, optionSeriesArr);
        return optionConfig;
    }

}
