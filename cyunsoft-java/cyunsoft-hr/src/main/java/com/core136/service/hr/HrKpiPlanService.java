package com.core136.service.hr;

import com.core136.bean.account.UnitDept;
import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrKpiPlan;
import com.core136.bean.hr.HrKpiPlanRecord;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrKpiPlanMapper;
import com.core136.service.account.UnitDeptService;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class HrKpiPlanService {
    private HrKpiPlanMapper hrKpiPlanMapper;
    private UserInfoService userInfoService;
	private UnitDeptService unitDeptService;
	private HrKpiPlanRecordService hrKpiPlanRecordService;
	@Autowired
	public void setHrKpiPlanMapper(HrKpiPlanMapper hrKpiPlanMapper) {
		this.hrKpiPlanMapper = hrKpiPlanMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setUnitDeptService(UnitDeptService unitDeptService) {
		this.unitDeptService = unitDeptService;
	}
	@Autowired
	public void setHrKpiPlanRecordService(HrKpiPlanRecordService hrKpiPlanRecordService) {
		this.hrKpiPlanRecordService = hrKpiPlanRecordService;
	}

	public int insertHrKpiPlan(HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.insert(hrKpiPlan);
    }

	/**
	 * 创建考核记录
	 * @param user
	 * @param hrKpiPlan
	 * @return
	 */
    public int addHrKpiPlan(UserInfo user, HrKpiPlan hrKpiPlan)
	{
		if (StringUtils.isNotBlank(hrKpiPlan.getMsgType())) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), hrKpiPlan.getChargeUser(),null,null);
			for (int i = 0; i < accountList.size(); i++) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(hrKpiPlan.getTitle());
				msgBody.setContent("考核任务即将开始:"+hrKpiPlan.getTitle());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(accountList.get(i));
				msgBody.setView(GlobalConstant.MSG_TYPE_HR_KPI);
				msgBody.setRecordId(hrKpiPlan.getPlanId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(hrKpiPlan.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return insertHrKpiPlan(hrKpiPlan);
	}

    public int deleteHrKpiPlan(HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.delete(hrKpiPlan);
    }

	/**
	 * 批量删除考核计划
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrKpiPlanByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrKpiPlan.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("planId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrKpiPlanMapper.deleteByExample(example));
		}
	}

    public int updateHrKpiPlan(Example example, HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.updateByExampleSelective(hrKpiPlan, example);
    }

	public int updateHrKpiPlan(UserInfo user,Example example, HrKpiPlan hrKpiPlan) {
		if (StringUtils.isNotBlank(hrKpiPlan.getMsgType())) {
			List<MsgBody> msgBodyList = new ArrayList<>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), hrKpiPlan.getChargeUser(),null,null);
			for (int i = 0; i < accountList.size(); i++) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle(hrKpiPlan.getTitle());
				msgBody.setContent("考核任务即将开始:"+hrKpiPlan.getTitle());
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(accountList.get(i));
				msgBody.setView(GlobalConstant.MSG_TYPE_HR_KPI);
				msgBody.setRecordId(hrKpiPlan.getPlanId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(hrKpiPlan.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return hrKpiPlanMapper.updateByExampleSelective(hrKpiPlan, example);
	}

    public HrKpiPlan selectOneHrKpiPlan(HrKpiPlan hrKpiPlan) {
        return hrKpiPlanMapper.selectOne(hrKpiPlan);
    }

	/**
	 * 考核计划生效
	 * @param user 用户对象
	 * @param hrKpiPlan 考核计划对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	public RetDataBean updatePlanEffect(UserInfo user, HrKpiPlan hrKpiPlan) {
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		String chargeUser = "";
		hrKpiPlan = selectOneHrKpiPlan(hrKpiPlan);
		List<Map<String, String>> kpiUserList = userInfoService.getAccountIdInRole(hrKpiPlan.getOrgId(), hrKpiPlan.getUserRole(), hrKpiPlan.getDeptRole(), hrKpiPlan.getLevelRole());
		if (kpiUserList != null) {
			if (hrKpiPlan.getKpiRule().equals("1")) {
				chargeUser = hrKpiPlan.getChargeUser();
				if (StringUtils.isBlank(chargeUser)) {
					return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
				}
			}
			for (int k = 0; k < kpiUserList.size(); k++) {
				String accountId = kpiUserList.get(k).get("accountId");
				if (hrKpiPlan.getKpiRule().equals("2")) {
					//部门主管考核
					UserInfo user1 = userInfoService.getUserByAccountId(accountId,hrKpiPlan.getOrgId());
					if(user1!=null) {
						UnitDept unitDept = unitDeptService.selectOneUnitDept(user1.getOrgId(), user1.getDeptId());
						if (unitDept != null) {
							chargeUser = unitDept.getDeptLead();
						}
					}
				} else if (hrKpiPlan.getKpiRule().equals("3")) {
					//逐级考核
					UserInfo user1 = userInfoService.getUserByAccountId(accountId,hrKpiPlan.getOrgId());
					if(user1!=null) {
						chargeUser = user1.getLeadId();
					}
				}
					if (StringUtils.isNotBlank(chargeUser)) {
						HrKpiPlanRecord hrKpiPlanRecord1 = new HrKpiPlanRecord();
						hrKpiPlanRecord1.setAccountId(accountId);
						hrKpiPlanRecord1.setPlanId(hrKpiPlan.getPlanId());
						hrKpiPlanRecord1.setItemId(hrKpiPlan.getItemId());
						hrKpiPlanRecord1.setOrgId(hrKpiPlan.getOrgId());
						int count = hrKpiPlanRecordService.getHrKpiPlanRecordCount(hrKpiPlanRecord1);
						if (count == 0) {
							HrKpiPlanRecord hrKpiPlanRecord = new HrKpiPlanRecord();
							hrKpiPlanRecord.setRecordId(SysTools.getGUID());
							hrKpiPlanRecord.setAccountId(accountId);
							hrKpiPlanRecord.setPlanId(hrKpiPlan.getPlanId());
							hrKpiPlanRecord.setItemId(hrKpiPlan.getItemId());
							hrKpiPlanRecord.setChargeUser(chargeUser);
							hrKpiPlanRecord.setCreateTime(createTime);
							hrKpiPlanRecord.setStatus("0");
							hrKpiPlanRecord.setCreateUser(user.getAccountId());
							hrKpiPlanRecord.setOrgId(hrKpiPlan.getOrgId());
							hrKpiPlanRecordService.insertHrKpiPlanRecord(hrKpiPlanRecord);
						}
					}
			}
			HrKpiPlan hrKpiPlanTemp = new HrKpiPlan();
			hrKpiPlanTemp.setOrgId(hrKpiPlan.getOrgId());
			hrKpiPlanTemp.setPlanId(hrKpiPlan.getPlanId());
			hrKpiPlanTemp.setStatus("1");
			Example example = new Example(HrKpiPlan.class);
			example.createCriteria().andEqualTo("orgId", hrKpiPlanTemp.getOrgId()).andEqualTo("planId", hrKpiPlanTemp.getPlanId());
			updateHrKpiPlan(example, hrKpiPlanTemp);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}
	}

	/**
	 *  获取考核管理列表
	 * @param orgId 机构码
	 * @param status 考核状态
	 * @param kpiRule 考核规则
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 考核管理列表
	 */
    public List<Map<String, String>> getMyHrKpiPlanList(String orgId, String status, String kpiRule,String dateQueryType, String beginTime, String endTime, String keyword) {
        return hrKpiPlanMapper.getMyHrKpiPlanList(orgId, status, kpiRule, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取考核管理列表
	 * @param pageParam 分页参数
	 * @param status 考核状态
	 * @param kpiRule 考核规则
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 考核管理列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyHrKpiPlanList(PageParam pageParam, String status, String kpiRule,String dateQueryType, String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrKpiPlanList(pageParam.getOrgId(), status, kpiRule,dateQueryType, beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取人员考核列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param keyword 查询关键词
	 * @return 考核列表
	 */
    public List<Map<String, String>> getKpiPlanForUserList(String orgId, String accountId,String userId,String status,String dateQueryType,String beginTime,String endTime, String keyword) {
        String nowTime = SysTools.getTime("yyyy-MM-dd");
        return hrKpiPlanMapper.getKpiPlanForUserList(orgId, accountId, userId,status,nowTime,dateQueryType,beginTime,endTime,"%" + keyword + "%");
    }

	/**
	 * 获取人员考核列表
	 * @param pageParam 分页参数
	 * @return 考核列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getKpiPlanForUserList(PageParam pageParam,String userId,String status,String dateQueryType,String beginTime,String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getKpiPlanForUserList(pageParam.getOrgId(), pageParam.getAccountId(),userId,status, dateQueryType,beginTime,endTime,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取个人考核列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 考核列表
	 */
	public List<Map<String, String>> getMyKpiPlanForList(String orgId, String accountId) {
        return hrKpiPlanMapper.getMyKpiPlanForList(orgId, accountId);
    }

	/**
	 * 获取个人考核列表
	 * @param pageParam 分页参数
	 * @return 考核列表
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyKpiPlanForList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyKpiPlanForList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
    }

	/**
	 * 查询获取考核列表
	 * @param orgId 机构码
	 * @param planId 考核计划Id
	 * @param chargeUser 考核人
	 * @param accountId 用户账号
	 * @param keyword 查询关键词
	 * @return 考核列表
	 */
    public List<Map<String, String>> getKpiPlanForUserQueryList(String orgId, String planId, String chargeUser, String accountId, String keyword) {
        return hrKpiPlanMapper.getKpiPlanForUserQueryList(orgId, planId, chargeUser, accountId, "%" + keyword + "%");
    }

	/**
	 * 查询获取考核列表
	 * @param pageParam 分页参数
	 * @param planId 考核计划Id
	 * @param chargeUser 考核人
	 * @return 考核列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getKpiPlanForUserQueryList(PageParam pageParam, String planId, String chargeUser) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getKpiPlanForUserQueryList(pageParam.getOrgId(), planId, chargeUser, pageParam.getAccountId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


	/**
	 * 获取考核计划列表
	 * @param orgId 机构码
	 * @return 考核计划列表
	 */
	public List<Map<String, String>> getKpiPlanForListForSelect(String orgId) {
        return hrKpiPlanMapper.getKpiPlanForListForSelect(orgId);
    }

	/**
	 * 获取考核记录详情
	 * @param orgId 机构码
	 * @param planId 考核计划Id
	 * @return 考核记录详情
	 */
	public Map<String,String>getMyHrKpiPlanById(String orgId,String planId)
	{
		return hrKpiPlanMapper.getMyHrKpiPlanById(orgId,planId);
	}

}
