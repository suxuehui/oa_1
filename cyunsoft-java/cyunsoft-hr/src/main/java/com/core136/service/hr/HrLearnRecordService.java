package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrLearnRecord;
import com.core136.bean.hr.HrUser;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrLearnRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HrLearnRecordService {
    private HrLearnRecordMapper hrLearnRecordMapper;
    private HrUserService hrUserService;
	private HrDicService hrDicService;
	@Autowired
	public void setHrLearnRecordMapper(HrLearnRecordMapper hrLearnRecordMapper) {
		this.hrLearnRecordMapper = hrLearnRecordMapper;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}

	public int insertHrLearnRecord(HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.insert(hrLearnRecord);
    }

    public int deleteHrLearnRecord(HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.delete(hrLearnRecord);
    }

	/**
	 * 批量删除
	 * @param orgId 机构码
	 * @param list 教育经历Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteHrLearnRecordByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrLearnRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrLearnRecordMapper.deleteByExample(example));
		}
	}

    public int updateHrLearnRecord(Example example, HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.updateByExampleSelective(hrLearnRecord, example);
    }

    public HrLearnRecord selectOneHrLearnRecord(HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.selectOne(hrLearnRecord);
    }

	/**
	 * 获取教育经历列表
	 * @param orgId 机构码
	 * @param userId Hr用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 教育经历列表
	 */
    public List<Map<String, String>> getHrLearnRecordList(String orgId, String userId,String dateQueryType, String beginTime, String endTime,String highsetDegree, String keyword) {
        return hrLearnRecordMapper.getHrLearnRecordList(orgId, userId,dateQueryType, beginTime, endTime, highsetDegree,"%" + keyword + "%");
    }

	/**
	 * 个人查询学习记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 教育经历记录
	 */
    public List<Map<String, String>> getMyHrLearnRecordList(String orgId, String accountId) {
        return hrLearnRecordMapper.getMyHrLearnRecordList(orgId, accountId);
    }

	/**
	 * 获取教育经历列表
	 * @param pageParam 分页参数
	 * @param userId Hr用户Id
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 教育经历列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrLearnRecordList(PageParam pageParam, String userId, String dateQueryType,String beginTime, String endTime,String highsetDegree) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrLearnRecordList(pageParam.getOrgId(), userId,dateQueryType, beginTime, endTime, highsetDegree,pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 个人查询学习记录
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyHrLearnRecordList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrLearnRecordList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
    }

	/**
	 * 学习记录导入
	 * @param user 用户对象
	 * @param file 文件
	 * @return 消息结构
	 * @throws IOException 服务器内部错误
	 */
	@Transactional(value = "generalTM")
    public RetDataBean importHrLearnRecord(UserInfo user, MultipartFile file) throws IOException {
		List<String> resList = new ArrayList<>();
		List<HrLearnRecord> hrLearnRecordList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("关联档案人员");
		titleList.add("学校名称");
		titleList.add("所学专业");
		titleList.add("入学日期");
		titleList.add("毕业日期");
		titleList.add("所获学位");
		titleList.add("证明人");
		titleList.add("获取证书");
		titleList.add("获奖情况");
		titleList.add("备注");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrLearnRecord hrLearnRecord = new HrLearnRecord();
			hrLearnRecord.setRecordId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrLearnRecord.setSortNo(Integer.parseInt(tempMap.get(s)));
					} else {
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("关联档案人员")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(user.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try {
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if (hrUser != null) {
								hrLearnRecord.setUserId(hrUser.getUserId());
							} else {
								resList.add("关联档案人员不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("关联档案人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("关联档案人员不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("学校名称")) {
					hrLearnRecord.setSchoolName(tempMap.get(s));
				}
				if (s.equals("所学专业")) {
					hrLearnRecord.setMajor(tempMap.get(s));
				}
				if (s.equals("入学日期")) {
					hrLearnRecord.setBeginTime(tempMap.get(s));
				}
				if (s.equals("毕业日期")) {
					hrLearnRecord.setEndTime(tempMap.get(s));
				}
				if (s.equals("所获学位")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic highsetDegree = new HrDic();
						highsetDegree.setOrgId(user.getOrgId());
						highsetDegree.setName(tempMap.get(s));
						highsetDegree.setCode("highsetDegree");
						try {
							highsetDegree = hrDicService.selectOneHrDic(highsetDegree);
							if (highsetDegree != null) {
								hrLearnRecord.setHighsetDegree(highsetDegree.getKeyValue());
							} else {
								resList.add("所获学位不能为空!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("所获学位查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					}
				}
				if (s.equals("证明人")) {
					hrLearnRecord.setCerifier(tempMap.get(s));
				}
				if (s.equals("获取证书")) {
					hrLearnRecord.setCerificate(tempMap.get(s));
				}
				if (s.equals("获奖情况")) {
					hrLearnRecord.setHonor(tempMap.get(s));
				}
				if (s.equals("备注")) {
					hrLearnRecord.setRemark(tempMap.get(s));
				}
			}
			hrLearnRecord.setCreateTime(createTime);
			hrLearnRecord.setCreateUser(user.getAccountId());
			hrLearnRecord.setOrgId(user.getOrgId());
			if(insertFlag){
				hrLearnRecordList.add(hrLearnRecord);
			}
		}
		for(HrLearnRecord hrLearnRecord:hrLearnRecordList){
			insertHrLearnRecord(hrLearnRecord);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }
}
