package com.core136.service.hr;

import com.core136.bean.hr.HrPersonnelTransfer;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrPersonnelTransferMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrPersonnelTransferService {
    private HrPersonnelTransferMapper hrPersonnelTransferMapper;
	@Autowired
	public void setHrPersonnelTransferMapper(HrPersonnelTransferMapper hrPersonnelTransferMapper) {
		this.hrPersonnelTransferMapper = hrPersonnelTransferMapper;
	}

	public int insertHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.insert(hrPersonnelTransfer);
    }

    public int deleteHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.delete(hrPersonnelTransfer);
    }

	/**
	 * 批量删除人员调动记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrPersonnelTransferByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrPersonnelTransfer.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("transferId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrPersonnelTransferMapper.deleteByExample(example));
		}
	}

    public int updateHrPersonnelTransfer(Example example, HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.updateByExampleSelective(hrPersonnelTransfer, example);
    }

    public HrPersonnelTransfer selectOneHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.selectOne(hrPersonnelTransfer);
    }

	/**
	 * 获取人员调动列表
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param transferType
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getHrPersonnelTransferList(String orgId, String userId, String dateQueryType, String beginTime, String endTime, String transferType, String keyword) {
        return hrPersonnelTransferMapper.getHrPersonnelTransferList(orgId, userId, dateQueryType,beginTime, endTime, transferType, "%" + keyword + "%");
    }

	/**
	 * 人个工作调动记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return  人个工作调动记录
	 */
    public List<Map<String, String>> getMyHrPersonnelTransferList(String orgId, String accountId) {
        return hrPersonnelTransferMapper.getMyHrPersonnelTransferList(orgId, accountId);
    }

	/**
	 * 人个工作调动记录
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyHrPersonnelTransferList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrPersonnelTransferList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取人员调动列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param transferType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrPersonnelTransferList(PageParam pageParam, String userId,String dateQueryType, String beginTime, String endTime, String transferType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrPersonnelTransferList(pageParam.getOrgId(), userId,dateQueryType, beginTime, endTime, transferType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
