package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrSalaryRecord;
import com.core136.bean.hr.HrUser;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrSalaryRecordMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class HrSalaryRecordService {
    private HrSalaryRecordMapper hrSalaryRecordMapper;
	private UserInfoService userInfoService;
	private HrUserService hrUserService;
	@Autowired
	public void setHrSalaryRecordMapper(HrSalaryRecordMapper hrSalaryRecordMapper) {
		this.hrSalaryRecordMapper = hrSalaryRecordMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}

	public int insertHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.insert(hrSalaryRecord);
    }

	public int insertHrSalaryRecord(UserInfo user,HrSalaryRecord hrSalaryRecord) {
		HrUser hrUser = new HrUser();
		hrUser.setUserId(hrUser.getUserId());
		hrUser.setOrgId(user.getOrgId());
		hrUser = hrUserService.selectOneHrUser(hrUser);
		if (StringUtils.isNotBlank(hrSalaryRecord.getMsgType())&&StringUtils.isNotBlank(hrUser.getAccountId())) {
			List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), hrUser.getAccountId(),null,null);
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle("人力薪资");
				msgBody.setContent(hrSalaryRecord.getMonth() + "月份薪资记录");
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_HR_SALARY);
				msgBody.setRecordId(hrSalaryRecord.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(hrSalaryRecord.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return insertHrSalaryRecord(hrSalaryRecord);
	}


    public int deleteHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.delete(hrSalaryRecord);
    }

	/**
	 * 批量删除员工薪资记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrSalaryRecordByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrSalaryRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrSalaryRecordMapper.deleteByExample(example));
		}
	}
    public int updateHrSalaryRecord(Example example, HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.updateByExampleSelective(hrSalaryRecord, example);
    }

	public int updateHrSalaryRecord(UserInfo user,Example example, HrSalaryRecord hrSalaryRecord) {
		HrUser hrUser = new HrUser();
		hrUser.setUserId(hrUser.getUserId());
		hrUser.setOrgId(user.getOrgId());
		hrUser = hrUserService.selectOneHrUser(hrUser);
		if (StringUtils.isNotBlank(hrSalaryRecord.getMsgType())&&StringUtils.isNotBlank(hrUser.getAccountId())) {
			List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
			List<UserInfo> accountList = userInfoService.getUserInRole(user.getOrgId(), hrUser.getAccountId(),null,null);
			for (UserInfo userInfo : accountList) {
				MsgBody msgBody = new MsgBody();
				msgBody.setFromAccountId(user.getAccountId());
				msgBody.setFormUserName(user.getUserName());
				msgBody.setTitle("人力薪资");
				msgBody.setContent(hrSalaryRecord.getMonth() + "月份薪资记录");
				msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				msgBody.setUserInfo(userInfo);
				msgBody.setView(GlobalConstant.MSG_TYPE_HR_SALARY);
				msgBody.setRecordId(hrSalaryRecord.getRecordId());
				msgBody.setOrgId(user.getOrgId());
				msgBodyList.add(msgBody);
			}
			List<String> msgTypeList = Arrays.asList(hrSalaryRecord.getMsgType().split(","));
			MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
			messageUnitService.sendMessage(msgTypeList, msgBodyList);
		}
		return hrSalaryRecordMapper.updateByExampleSelective(hrSalaryRecord, example);
	}

    public HrSalaryRecord selectOneHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.selectOne(hrSalaryRecord);
    }

	/**
	 * 获取人员薪资列表
	 * @param orgId 机构码
	 * @param userId
	 * @param year
	 * @param month
	 * @return
	 */
    public List<Map<String, String>> getHrSalaryRecordList(String orgId, String userId, String year, String month) {
        return hrSalaryRecordMapper.getHrSalaryRecordList(orgId, userId, year, month);
    }

	/**
	 * 个人薪资查询
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 个人薪资
	 */
    public List<Map<String, String>> getMyHrSalaryRecordList(String orgId, String accountId) {
        return hrSalaryRecordMapper.getMyHrSalaryRecordList(orgId, accountId);
    }


	/**
	 * 获取移动端个人薪资查询
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param page
	 * @return
	 */
	public List<Map<String, String>> getMyHrSalaryRecordListForApp(String orgId, String accountId,Integer page) {
		return hrSalaryRecordMapper.getMyHrSalaryRecordListForApp(orgId, accountId,page);
	}

	/**
	 * 获取人员薪资列表
	 * @param pageParam 分页参数
	 * @param userId
	 * @param year
	 * @param month
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrSalaryRecordList(PageParam pageParam, String userId, String year, String month) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrSalaryRecordList(pageParam.getOrgId(), userId, year, month);
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取人员薪资列表
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getMyHrSalaryRecordList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrSalaryRecordList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
    }

	/**
	 * 导入人员薪资
	 * @param user
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@Transactional(value = "generalTM")
    public RetDataBean importHrSalaryRecord(UserInfo user, MultipartFile file) throws IOException {
		List<HrSalaryRecord> hrSalaryRecordList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
        List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("薪资年度");
		titleList.add("薪资月份");
		titleList.add("人员姓名");
		titleList.add("岗位工资");
		titleList.add("薪级工资");
		titleList.add("粮油补贴");
		titleList.add("特殊岗位津贴");
		titleList.add("交通补贴");
		titleList.add("岗位津贴");
		titleList.add("应发合计数");
		titleList.add("退休金");
		titleList.add("失业保险");
		titleList.add("医保");
		titleList.add("公积金");
		titleList.add("个人所得税");
		titleList.add("其它费用");
		titleList.add("实扣合计数");
		titleList.add("实发合计数");
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrSalaryRecord hrSalaryRecord = new HrSalaryRecord();
			hrSalaryRecord.setRecordId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						hrSalaryRecord.setSortNo(Integer.parseInt(tempMap.get(s)));
					}else{
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag= false;
						continue;
					}
				}
				if (s.equals("薪资年度")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						hrSalaryRecord.setYear(tempMap.get(s));
					}else{
						resList.add("薪资年度不能为空!-->" + tempMap.get(s) + "--");
						insertFlag= false;
						continue;
					}
				}
				if (s.equals("薪资月份")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						hrSalaryRecord.setMonth(tempMap.get(s));
					}else{
						resList.add("薪资月份不能为空!-->" + tempMap.get(s) + "--");
						insertFlag= false;
						continue;
					}
				}
				if (s.equals("人员姓名")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(user.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try{
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if(hrUser!=null) {
								hrSalaryRecord.setUserId(hrUser.getUserId());
							}else{
								resList.add("未能查询关联人员!-->" + tempMap.get(s) + "--");
								insertFlag= false;
								continue;
							}
						}catch (Exception e) {
							resList.add("关联人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag= false;
							continue;
						}
					} else {
						resList.add("人员姓名不能为空!-->" + tempMap.get(s) + "--");
						insertFlag= false;
						continue;
					}
				}
				try {
					if (s.equals("岗位工资")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setPostSalary(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setPostSalary(0.0);
						}
					}
					if (s.equals("薪级工资")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setLevelSalary(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setLevelSalary(0.0);
						}
					}
					if (s.equals("粮油补贴")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setFoodSalary(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setFoodSalary(0.0);
						}
					}
					if (s.equals("特殊岗位津贴")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setPostSalaryOther(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setPostSalaryOther(0.0);
						}
					}
					if (s.equals("交通补贴")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setTransportSalary(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setTransportSalary(0.0);
						}
					}
					if (s.equals("岗位津贴")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setPostSalary(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setPostSalary(0.0);
						}
					}
					if (s.equals("应发合计数")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setSumAmount(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setSumAmount(0.0);
						}
					}
					if (s.equals("退休金")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setPensoin(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setPensoin(0.0);
						}
					}
					if (s.equals("失业保险")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setUnemployment(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setUnemployment(0.0);
						}
					}
					if (s.equals("医保")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setMedical(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setMedical(0.0);
						}
					}
					if (s.equals("公积金")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setAccumulationFund(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setAccumulationFund(0.0);
						}
					}
					if (s.equals("个人所得税")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setTax(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setTax(0.0);
						}
					}
					if (s.equals("其它费用")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setCostOther(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setCostOther(0.0);
						}
					}
					if (s.equals("实扣合计数")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setRealCost(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setRealCost(0.0);
						}
					}
					if (s.equals("实发合计数")) {
						if (StringUtils.isNotBlank(tempMap.get(s))) {
							hrSalaryRecord.setRealSalary(Double.valueOf(tempMap.get(s)));
						} else {
							hrSalaryRecord.setRealSalary(0.0);
						}
					}
				}catch (Exception e) {
					resList.add("薪资数据格式出错!-->" + tempMap.get("人员姓名") + "--");
					insertFlag= false;
					continue;
				}
			}
			hrSalaryRecord.setCreateTime(createTime);
			hrSalaryRecord.setCreateUser(user.getAccountId());
			hrSalaryRecord.setOrgId(user.getOrgId());
			if(insertFlag){
				hrSalaryRecordList.add(hrSalaryRecord);
			}
		}
		for(HrSalaryRecord hrSalaryRecord:hrSalaryRecordList) {
			insertHrSalaryRecord(hrSalaryRecord);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }

	/**
	 * 获取薪资记录详情
	 * @param orgId 机构码
	 * @param recordId
	 * @return
	 */
    public Map<String,String>getHrSalaryRecordById(String orgId,String recordId) {
		return hrSalaryRecordMapper.getHrSalaryRecordById(orgId,recordId);
	}

}
