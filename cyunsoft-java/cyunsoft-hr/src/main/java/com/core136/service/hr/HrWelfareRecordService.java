package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrUser;
import com.core136.bean.hr.HrWelfareRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrWelfareRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HrWelfareRecordService {
    private HrWelfareRecordMapper hrWelfareRecordMapper;
	private HrUserService hrUserService;
	private HrDicService hrDicService;
	@Autowired
	public void setHrWelfareRecordMapper(HrWelfareRecordMapper hrWelfareRecordMapper) {
		this.hrWelfareRecordMapper = hrWelfareRecordMapper;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}

	public int insertHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.insert(hrWelfareRecord);
    }

    public int deleteHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.delete(hrWelfareRecord);
    }

	/**
	 * 批量删除人员福利记录
	 * @param orgId 机构码
	 * @param list
	 * @return
	 */
	public RetDataBean deleteHrWelfareRecordByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrWelfareRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrWelfareRecordMapper.deleteByExample(example));
		}
	}
    public int updateHrWelfareRecord(Example example, HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.updateByExampleSelective(hrWelfareRecord, example);
    }

    public HrWelfareRecord selectOneHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.selectOne(hrWelfareRecord);
    }

	/**
	 * 获取福利列表
	 * @param orgId 机构码
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param userId
	 * @param welfareType
	 * @param keyword 查询关键词
	 * @return
	 */
    public List<Map<String, String>> getHrWelfareRecordList(String orgId, String dateQueryType,String beginTime, String endTime, String userId, String welfareType, String keyword) {
        return hrWelfareRecordMapper.getHrWelfareRecordList(orgId, dateQueryType,beginTime, endTime, welfareType, userId, "%" + keyword + "%");
    }

	/**
	 * 获取福利列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param userId
	 * @param welfareType
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrWelfareRecordList(PageParam pageParam,String dateQueryType, String beginTime, String endTime, String userId, String welfareType) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrWelfareRecordList(pageParam.getOrgId(),dateQueryType, beginTime, endTime, userId, welfareType, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 批量导入人员福利
	 * @param user
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@Transactional(value = "generalTM")
    public RetDataBean importHrWelfareRecord(UserInfo user, MultipartFile file) throws IOException {
		List<HrWelfareRecord> hrWelfareRecordList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
        titleList.add("福利标题");
        titleList.add("福利年度");
        titleList.add("福利月份");
        titleList.add("人员姓名");
        titleList.add("福利类型");
        titleList.add("福利金额");
        titleList.add("备注");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrWelfareRecord hrWelfareRecord = new HrWelfareRecord();
			hrWelfareRecord.setRecordId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrWelfareRecord.setSortNo(Integer.parseInt(tempMap.get(s)));
					} else {
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("福利标题")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrWelfareRecord.setTitle(tempMap.get(s));
					} else {
						resList.add("福利标题不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("福利年度")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrWelfareRecord.setYear(tempMap.get(s));
					} else {
						resList.add("福利年度不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("福利月份")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrWelfareRecord.setMonth(tempMap.get(s));
					} else {
						resList.add("福利月份不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("人员姓名")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(user.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try {
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if (hrUser != null) {
								hrWelfareRecord.setUserId(hrUser.getUserId());
							} else {
								resList.add("未能查询档案关联人员!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("关联档案人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("关联档案人员不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("福利类型")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic welfareType = new HrDic();
						welfareType.setOrgId(user.getOrgId());
						welfareType.setName(tempMap.get(s));
						welfareType.setCode("welfareType");
						try {
							welfareType = hrDicService.selectOneHrDic(welfareType);
							if (welfareType != null) {
								hrWelfareRecord.setWelfareType(welfareType.getKeyValue());
							} else {
								resList.add("未能查询到福利类型!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("查询福利类型出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("福利类型不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("福利金额")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						try{
							hrWelfareRecord.setAmount(Double.parseDouble(tempMap.get(s)));
						}catch (Exception e) {
							resList.add("福利金额格式不正确!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						hrWelfareRecord.setAmount(0.0);
					}
				}
				if (s.equals("备注")) {
					hrWelfareRecord.setRemark(tempMap.get(s));
				}
			}
			hrWelfareRecord.setCreateTime(createTime);
			hrWelfareRecord.setCreateUser(user.getAccountId());
			hrWelfareRecord.setOrgId(user.getOrgId());
			if (insertFlag) {
				hrWelfareRecordList.add(hrWelfareRecord);
			}
		}
		for (HrWelfareRecord hrWelfareRecord : hrWelfareRecordList) {
			insertHrWelfareRecord(hrWelfareRecord);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }
}
