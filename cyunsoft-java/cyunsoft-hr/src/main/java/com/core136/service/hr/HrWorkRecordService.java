package com.core136.service.hr;

import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrUser;
import com.core136.bean.hr.HrWorkRecord;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrWorkRecordMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HrWorkRecordService {
	private HrWorkRecordMapper hrWorkRecordMapper;
	private HrUserService hrUserService;
	private HrDicService hrDicService;
	@Autowired
	public void setHrWorkRecordMapper(HrWorkRecordMapper hrWorkRecordMapper) {
		this.hrWorkRecordMapper = hrWorkRecordMapper;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}

	public int insertHrWorkRecord(HrWorkRecord hrWorkRecord) {
		return hrWorkRecordMapper.insert(hrWorkRecord);
	}

	public int deleteHrWorkRecord(HrWorkRecord hrWorkRecord) {
		return hrWorkRecordMapper.delete(hrWorkRecord);
	}

	/**
	 * 批量删除
	 *
	 * @param orgId 机构码
	 * @param list 工作经历Id 数组
	 * @return 消息结构
	 */
	public RetDataBean deleteHrWorkRecordByIds(String orgId, List<String> list) {
		if (list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		} else {
			Example example = new Example(HrWorkRecord.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrWorkRecordMapper.deleteByExample(example));
		}
	}

	public int updateHrWorkRecord(Example example, HrWorkRecord hrWorkRecord) {
		return hrWorkRecordMapper.updateByExampleSelective(hrWorkRecord, example);
	}

	public HrWorkRecord selectOneHrWorkRecord(HrWorkRecord hrWorkRecord) {
		return hrWorkRecordMapper.selectOne(hrWorkRecord);
	}

	/**
	 * 获取工作经历列表
	 *
	 * @param orgId 机构码
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结束时间
	 * @param nature
	 * @param keyword 查询关键词
	 * @return
	 */
	public List<Map<String, String>> getHrWorkRecordList(String orgId, String userId, String dateQueryType, String beginTime, String endTime, String nature, String keyword) {
		return hrWorkRecordMapper.getHrWorkRecordList(orgId, userId, dateQueryType, beginTime, endTime, nature, "%" + keyword + "%");
	}

	/**
	 * 查询个人工作经历
	 *
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 个人工作经历
	 */
	public List<Map<String, String>> getMyHrWorkRecordList(String orgId, String accountId) {
		return hrWorkRecordMapper.getMyHrWorkRecordList(orgId, accountId);
	}

	/**
	 * 获取工作经历列表
	 *
	 * @param pageParam     分页参数
	 * @param userId
	 * @param dateQueryType 日期范围类型
	 * @param beginTime     开始时间
	 * @param endTime       结束时间
	 * @param nature
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getHrWorkRecordList(PageParam pageParam, String userId, String dateQueryType, String beginTime, String endTime, String nature) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getHrWorkRecordList(pageParam.getOrgId(), userId, dateQueryType, beginTime, endTime, nature, pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 查询个人工作经历
	 *
	 * @param pageParam 分页参数
	 * @return
	 * @throws Exception SQL排序有注入风险异常
	 */
	public PageInfo<Map<String, String>> getMyHrWorkRecordList(PageParam pageParam) throws Exception {
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getMyHrWorkRecordList(pageParam.getOrgId(), pageParam.getAccountId());
		return new PageInfo<>(datalist);
	}

	/**
	 * 工作经历导入
	 *
	 * @param user
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@Transactional(value = "generalTM")
	public RetDataBean importHrWorkRecord(UserInfo user, MultipartFile file) throws IOException {
		List<HrWorkRecord> hrWorkRecordList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("关联档案人员");
		titleList.add("公司名称");
		titleList.add("所在部门");
		titleList.add("所属行业");
		titleList.add("公司类型");
		titleList.add("担任职务");
		titleList.add("证明人");
		titleList.add("入职日期");
		titleList.add("离职日期");
		titleList.add("工作内容");
		titleList.add("工作成就");
		titleList.add("离职原因");
		titleList.add("备注");
		List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrWorkRecord hrWorkRecord = new HrWorkRecord();
			hrWorkRecord.setRecordId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						hrWorkRecord.setSortNo(Integer.parseInt(tempMap.get(s)));
					} else {
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("关联档案人员")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(user.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try {
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if (hrUser != null) {
								hrWorkRecord.setUserId(hrUser.getUserId());
							} else {
								resList.add("未能查询档案关联人员!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("关联档案人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("关联档案人员不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("公司名称")) {
					hrWorkRecord.setCompName(tempMap.get(s));
				}
				if (s.equals("所在部门")) {
					hrWorkRecord.setDeptName(tempMap.get(s));
				}
				if (s.equals("所属行业")) {
					hrWorkRecord.setIndustry(tempMap.get(s));
				}
				if (s.equals("公司类型")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic natureType = new HrDic();
						natureType.setOrgId(user.getOrgId());
						natureType.setName(tempMap.get(s));
						natureType.setCode("natureType");
						try {
							natureType = hrDicService.selectOneHrDic(natureType);
							if (natureType != null) {
								hrWorkRecord.setNature(natureType.getKeyValue());
							} else {
								resList.add("未能查询到公司类型!-->" + tempMap.get(s) + "--");
								insertFlag = false;
								continue;
							}
						} catch (Exception e) {
							resList.add("查询公司类型出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
							continue;
						}
					} else {
						resList.add("公司类型不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
						continue;
					}
				}
				if (s.equals("担任职务")) {
					hrWorkRecord.setPost(tempMap.get(s));
				}
				if (s.equals("证明人")) {
					hrWorkRecord.setCerifier(tempMap.get(s));
				}
				if (s.equals("入职日期")) {
					hrWorkRecord.setBeginTime(tempMap.get(s));
				}
				if (s.equals("离职日期")) {
					hrWorkRecord.setEndTime(tempMap.get(s));
				}
				if (s.equals("工作内容")) {
					hrWorkRecord.setJobContent(tempMap.get(s));
				}
				if (s.equals("工作成就")) {
					hrWorkRecord.setAchievement(tempMap.get(s));
				}
				if (s.equals("离职原因")) {
					hrWorkRecord.setReasonForLeave(tempMap.get(s));
				}
				if (s.equals("备注")) {
					hrWorkRecord.setRemark(tempMap.get(s));
				}
			}
			hrWorkRecord.setCreateTime(createTime);
			hrWorkRecord.setCreateUser(user.getAccountId());
			hrWorkRecord.setOrgId(user.getOrgId());
			if (insertFlag) {
				hrWorkRecordList.add(hrWorkRecord);
			}
		}
		for (HrWorkRecord hrWorkRecord : hrWorkRecordList) {
			insertHrWorkRecord(hrWorkRecord);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
	}

}
