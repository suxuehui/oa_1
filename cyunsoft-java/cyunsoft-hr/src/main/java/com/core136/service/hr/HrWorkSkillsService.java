package com.core136.service.hr;


import com.core136.bean.account.UserInfo;
import com.core136.bean.hr.HrDic;
import com.core136.bean.hr.HrUser;
import com.core136.bean.hr.HrWorkSkills;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrWorkSkillsMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HrWorkSkillsService {
    private HrWorkSkillsMapper hrWorkSkillsMapper;
    private HrUserService hrUserService;
	private HrDicService hrDicService;
	@Autowired
	public void setHrWorkSkillsMapper(HrWorkSkillsMapper hrWorkSkillsMapper) {
		this.hrWorkSkillsMapper = hrWorkSkillsMapper;
	}
	@Autowired
	public void setHrUserService(HrUserService hrUserService) {
		this.hrUserService = hrUserService;
	}
	@Autowired
	public void setHrDicService(HrDicService hrDicService) {
		this.hrDicService = hrDicService;
	}

	public int insertHrWorkSkills(HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.insert(hrWorkSkills);
    }

    public int deleteHrWorkSkills(HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.delete(hrWorkSkills);
    }

	/**
	 * 批量删除劳动技能
	 * @param orgId 机构码
	 * @param list 特长记录Id  列表
	 * @return 消息结构
	 */
	public RetDataBean deleteHrWorkSkillsByIds(String orgId, List<String> list) {
		if(list.isEmpty()) {
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(HrWorkSkills.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrWorkSkillsMapper.deleteByExample(example));
		}
	}

    public int updateHrWorkSkills(Example example, HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.updateByExampleSelective(hrWorkSkills, example);
    }

    public HrWorkSkills selectOneHrWorkSkills(HrWorkSkills hrWorkSkills) {
        return hrWorkSkillsMapper.selectOne(hrWorkSkills);
    }

	/**
	 * 工作特长列表
	 * @param orgId 机构码
	 * @param userId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param skillsLevel 特长等级
	 * @param keyword 查询关键词
	 * @return 特长列表
	 */
    public List<Map<String, String>> getHrWorkSkillsList(String orgId, String userId, String dateQueryType,String beginTime, String endTime, String skillsLevel, String keyword) {
        return hrWorkSkillsMapper.getHrWorkSkillsList(orgId, userId, dateQueryType,beginTime, endTime, skillsLevel, "%" + keyword + "%");
    }

	/**
	 * 工作特长列表
	 * @param pageParam 分页参数
	 * @param userId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param skillsLevel 特长等级
	 * @return 特长列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getHrWorkSkillsList(PageParam pageParam, String userId,String dateQueryType, String beginTime, String endTime, String skillsLevel) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrWorkSkillsList(pageParam.getOrgId(), userId, dateQueryType,beginTime, endTime, skillsLevel, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 工作技能导入
	 * @param user 用户对象
	 * @param file 文件
	 * @return 消息结构
	 * @throws IOException 服务器内部错误
	 */
	@Transactional(value = "generalTM")
    public RetDataBean importHrWorkSkills(UserInfo user, MultipartFile file) throws IOException {
		List<HrWorkSkills> hrWorkSkillsList = new ArrayList<>();
		List<String> resList = new ArrayList<>();
		String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
		List<String> titleList = new ArrayList<>();
		titleList.add("排序号");
		titleList.add("关联档案人员");
		titleList.add("技能名称");
		titleList.add("技能等级");
		titleList.add("是否有证书");
		titleList.add("发证机构");
		titleList.add("发证日期");
		titleList.add("终止日期");
		titleList.add("备注");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
		for (Map<String, String> tempMap : recordList) {
			boolean insertFlag = true;
			HrWorkSkills hrWorkSkills = new HrWorkSkills();
			hrWorkSkills.setRecordId(SysTools.getGUID());
			for (String s : titleList) {
				if (s.equals("排序号")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						hrWorkSkills.setSortNo(Integer.parseInt(tempMap.get(s)));
					}else{
						resList.add("排序号不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
					}
				}else if (s.equals("关联档案人员")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrUser hrUser = new HrUser();
						hrUser.setOrgId(user.getOrgId());
						hrUser.setUserName(tempMap.get(s));
						try{
							hrUser = hrUserService.selectOneHrUser(hrUser);
							if(hrUser!=null) {
								hrWorkSkills.setUserId(hrUser.getUserId());
							}else{
								resList.add("未能查询档案关联人员!-->" + tempMap.get(s) + "--");
								insertFlag = false;
							}
						}catch (Exception e) {
							resList.add("关联档案人员查询出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
						}
					} else {
						resList.add("关联档案人员不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
					}
				} else if (s.equals("技能名称")) {
					if(StringUtils.isNotBlank(tempMap.get(s))) {
						hrWorkSkills.setName(tempMap.get(s));
					}else{
						resList.add("技能名称不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
					}
				}else if (s.equals("技能等级")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						HrDic skillsLevel= new HrDic();
						skillsLevel.setOrgId(user.getOrgId());
						skillsLevel.setName(tempMap.get(s));
						skillsLevel.setCode("skillsLevel");
						try{
							skillsLevel = hrDicService.selectOneHrDic(skillsLevel);
							if(skillsLevel!=null) {
								hrWorkSkills.setSkillsLevel(skillsLevel.getKeyValue());
							}else{
								resList.add("未能查询到技能等级!-->" + tempMap.get(s) + "--");
								insertFlag = false;
							}
						}catch (Exception e) {
							resList.add("查询技能等级出错!-->" + tempMap.get(s) + "--");
							insertFlag = false;
						}
					}else{
						resList.add("技能等级不能为空!-->" + tempMap.get(s) + "--");
						insertFlag = false;
					}
				}else if (s.equals("是否有证书")) {
					if (StringUtils.isNotBlank(tempMap.get(s))) {
						if(tempMap.get(s).equals("是")) {
							hrWorkSkills.setSkillsCerificate("1");
						}else{
							hrWorkSkills.setSkillsCerificate("0");
						}
					} else {
						hrWorkSkills.setSkillsCerificate("0");
					}
				}else if (s.equals("发证机构")) {
					hrWorkSkills.setNotifiedBody(tempMap.get(s));
				}else if (s.equals("发证日期")) {
					hrWorkSkills.setBeginTime(tempMap.get(s));
				}else if (s.equals("终止日期")) {
					hrWorkSkills.setEndTime(tempMap.get(s));
				}else if (s.equals("备注")) {
					hrWorkSkills.setRemark(tempMap.get(s));
				}
			}
			hrWorkSkills.setCreateTime(createTime);
			hrWorkSkills.setCreateUser(user.getAccountId());
			hrWorkSkills.setOrgId(user.getOrgId());
			if(insertFlag) {
				hrWorkSkillsList.add(hrWorkSkills);
			}
		}
		for(HrWorkSkills hrWorkSkills:hrWorkSkillsList){
			insertHrWorkSkills(hrWorkSkills);
		}
		if (resList.isEmpty()) {
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
		} else {
			return RetDataTools.NotOk(StringUtils.join(resList, ","));
		}
    }
}
