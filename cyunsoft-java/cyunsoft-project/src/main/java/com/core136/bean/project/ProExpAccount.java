package com.core136.bean.project;

import java.io.Serializable;
import java.util.List;

/**
 * 项目费用科目
 */
public class ProExpAccount implements Serializable {
    private String expId;
    private Integer sortNo;
    private String expName;
	private transient List<ProExpAccount> children;
    private String parentId;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getExpId() {
        return expId;
    }

    public void setExpId(String expId) {
        this.expId = expId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getExpName() {
        return expName;
    }

    public void setExpName(String expName) {
        this.expName = expName;
    }

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

	public List<ProExpAccount> getChildren() {
		return children;
	}

	public void setChildren(List<ProExpAccount> children) {
		this.children = children;
	}
}
