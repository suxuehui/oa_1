package com.core136.bean.project;

import java.io.Serializable;

/**
 * 项目权限
 */
public class ProRole implements Serializable {
    private String roleId;
    private String createUserRole;
    private String createDeptRole;
    private String createLevelRole;
    private String approvalUserRole;
    private String notNeedApprovalRole;
    private String createTime;
    private String createUser;
    private String orgId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getCreateUserRole() {
		return createUserRole;
	}

	public void setCreateUserRole(String createUserRole) {
		this.createUserRole = createUserRole;
	}

	public String getCreateDeptRole() {
		return createDeptRole;
	}

	public void setCreateDeptRole(String createDeptRole) {
		this.createDeptRole = createDeptRole;
	}

	public String getCreateLevelRole() {
		return createLevelRole;
	}

	public void setCreateLevelRole(String createLevelRole) {
		this.createLevelRole = createLevelRole;
	}

	public String getApprovalUserRole() {
		return approvalUserRole;
	}

	public void setApprovalUserRole(String approvalUserRole) {
		this.approvalUserRole = approvalUserRole;
	}

	public String getNotNeedApprovalRole() {
		return notNeedApprovalRole;
	}

	public void setNotNeedApprovalRole(String notNeedApprovalRole) {
		this.notNeedApprovalRole = notNeedApprovalRole;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
