package com.core136.bean.project;

import java.io.Serializable;

public class ProTaskLink implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String taskLinkId;
    private String proId;
    private Integer sortNo;
    private String source;
    private String target;
    private String linkType;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getTaskLinkId() {
        return taskLinkId;
    }

    public void setTaskLinkId(String taskLinkId) {
        this.taskLinkId = taskLinkId;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
