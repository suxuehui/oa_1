package com.core136.controller.project;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.core136.bean.account.UserInfo;
import com.core136.bean.project.ProProblemReply;
import com.core136.bean.project.ProRole;
import com.core136.bean.system.PageParam;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.project.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/get/project")
@CrossOrigin
@Api(value="项目GetController",tags={"项目获取数据接口"})
public class ApiGetProjectController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private UserInfoService userInfoService;
	private ProExpAccountService proExpAccountService;
	private ProRecordService proRecordService;
	private ProDicService proDicService;
	private ProSortService proSortService;
	private ProRoleService proRoleService;
	private ProTaskService proTaskService;
	private ProFileService proFileService;
	private ProProblemService proProblemService;
	private ProBudgetService proBudgetService;
	private ProTaskProcessService proTaskProcessService;
	private ProProblemReplyService proProblemReplyService;
	private EchartsProjectService echartsProjectService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setProExpAccountService(ProExpAccountService proExpAccountService) {
		this.proExpAccountService = proExpAccountService;
	}
	@Autowired
	public void setProRecordService(ProRecordService proRecordService) {
		this.proRecordService = proRecordService;
	}
	@Autowired
	public void setProDicService(ProDicService proDicService) {
		this.proDicService = proDicService;
	}
	@Autowired
	public void setProSortService(ProSortService proSortService) {
		this.proSortService = proSortService;
	}
	@Autowired
	public void setProRoleService(ProRoleService proRoleService) {
		this.proRoleService = proRoleService;
	}
	@Autowired
	public void setProTaskService(ProTaskService proTaskService) {
		this.proTaskService = proTaskService;
	}
	@Autowired
	public void setProFileService(ProFileService proFileService) {
		this.proFileService = proFileService;
	}
	@Autowired
	public void setProProblemService(ProProblemService proProblemService) {
		this.proProblemService = proProblemService;
	}
	@Autowired
	public void setProBudgetService(ProBudgetService proBudgetService) {
		this.proBudgetService = proBudgetService;
	}
	@Autowired
	public void setProTaskProcessService(ProTaskProcessService proTaskProcessService) {
		this.proTaskProcessService = proTaskProcessService;
	}
	@Autowired
	public void setProProblemReplyService(ProProblemReplyService proProblemReplyService) {
		this.proProblemReplyService = proProblemReplyService;
	}
	@Autowired
	public void setEchartsProjectService(EchartsProjectService echartsProjectService) {
		this.echartsProjectService = echartsProjectService;
	}

	/**
	 * 项目类型统计
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProSortPie")
	public RetDataBean getProSortPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsProjectService.getProSortPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 项目类型统计
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProSortBar")
	public RetDataBean getProSortBar() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsProjectService.getProSortBar(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 项目费用类型统计
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProCostTypePie")
	public RetDataBean getProCostTypePie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsProjectService.getProCostTypePie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取费用类型统计
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProCostTypeBar")
	public RetDataBean getProCostTypeBar() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsProjectService.getProCostTypeBar(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新任务问题列表
	 *
	 * @param problemId 问题Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProblemReplyList")
	public RetDataBean getProblemReplyList(String problemId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proProblemReplyService.getProblemReplyList(userInfo.getOrgId(), problemId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取问题回复详情
	 *
	 * @param proProblemReply 问题回复对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProProblemReplyById")
	public RetDataBean getProProblemReplyById(ProProblemReply proProblemReply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proProblemReply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proProblemReplyService.selectOneProProblemReply(proProblemReply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取任务子任务详情
	 *
	 * @param taskId 任务Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProTaskById")
	public RetDataBean getProTaskById(String taskId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.getProTaskById(userInfo.getOrgId(),taskId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 按任务ID获取处理过程列表
	 *
	 * @param pageParam 分页参数
	 * @param taskId 任务Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProTaskProcessList")
	public RetDataBean getProTaskProcessList(PageParam pageParam, String taskId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("p.create_time");
			} else {
				pageParam.setProp("p."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskProcessService.getProTaskProcessList(pageParam, taskId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 项目预算列表
	 *
	 * @param pageParam 分页参数 分页参数
	 * @param proId 项目Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProBudgetListByProId")
	public RetDataBean getProBudgetListByProId(PageParam pageParam,String date,String sortId, String proId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("f.sort_no");
			} else {
				pageParam.setProp("f."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = proBudgetService.getProBudgetListByProId(pageParam,sortId, proId,date,timeArr[0],timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取项目问题
	 * @param pageParam 分页参数 分页参数
	 * @param date 日期范围类型
	 * @param proId 项目Id
	 * @param sortId 分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProProblemListByPro")
	public RetDataBean getProProblemListByPro(PageParam pageParam,String date, String proId, String sortId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("p.create_time");
			} else {
				pageParam.setProp("p."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proProblemService.getProProblemListByPro(pageParam, proId, sortId,date,timeArr[0],timeArr[1]));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取项目选择列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProRecordListForSelect")
	public RetDataBean getProRecordListForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proRecordService.getProRecordListForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按分类获取项目
	 * @param sortId 分类Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProRecordForFilter")
	public RetDataBean getProRecordForFilter(String sortId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<Map<String,String>> list = new ArrayList<>();
			Map<String,String> map = new HashMap<>();
			map.put("label","全部");
			map.put("value","");
			list.add(map);
			list.addAll(proRecordService.getProRecordForFilter(userInfo.getOrgId(),sortId));
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, list);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**获取项目文档
	 * @param pageParam 分页参数 分页参数
	 * @param proId 项目Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProFileListByProId")
	public RetDataBean getProFileListByProId(PageParam pageParam,String date,String sortId, String proId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("f.sort_no");
			} else {
				pageParam.setProp("f."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = proFileService.getProFileListByProId(pageParam, sortId,proId,date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取所有待分配子任务列表
	 * @param proSort 项目分类
	 * @param date 日期范围类型
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getTaskAllocationList")
	public RetDataBean getTaskAllocationList(String proSort,String proLevel, String date, String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			String[] timeArr = SysTools.convertDateToDayAfter(date);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.getTaskAllocationList(userInfo.getOrgId(), proLevel, proSort, date, timeArr[0], timeArr[1], keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取子任甘特图信息
	 *
	 * @param proId 项目Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProTaskInfo")
	public RetDataBean getProTaskInfo(String proId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.getProTaskInfo(userInfo.getOrgId(), proId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取待审批项目列表
	 * @param pageParam 分页参数 分页参数
	 * @param proSort 项目分类
	 * @param proLevel 项目等级
	 * @param date 日期范围类型
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProRecordListForApproval")
	public RetDataBean getProRecordListForApproval(PageParam pageParam, String proSort,String proLevel, String date,String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.create_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			ProRole proRole = new ProRole();
			proRole.setOrgId(userInfo.getOrgId());
			proRole = proRoleService.selectOneProRole(proRole);
			String approvalUserRole;
			if (proRole != null) {
				approvalUserRole = proRole.getApprovalUserRole();
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_FAILED,new PageInfo<Map<String, String>>());
			}
			if (StringUtils.isNotBlank(approvalUserRole)) {
				if (Arrays.asList(approvalUserRole.split(",")).contains(userInfo.getAccountId())) {
					pageParam.setOrgId(userInfo.getOrgId());
					pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
					PageInfo<Map<String, String>> pageInfo = proRecordService.getProRecordListForApproval(pageParam,proSort, proLevel,status,date, timeArr[0], timeArr[1]);
					return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
				} else {
					return RetDataTools.NotOk(MessageCode.MSG_00015);
				}
			} else {
				return RetDataTools.NotOk(MessageCode.MSG_00015);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取项目子任务列表
	 * @param pageParam 分页参数
	 * @param proSort 项目分类
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMyTaskWorkList")
	public RetDataBean getMyTaskWorkList(PageParam pageParam, String proSort, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("t.start_date");
			} else {
				pageParam.setProp("t."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.getMyTaskWorkList(pageParam, proSort, date,timeArr[0], timeArr[1]));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 按状态获取项目列表
	 * @param pageParam 分页参数
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProRecordListByStatus")
	public RetDataBean getProRecordListByStatus(PageParam pageParam, String status, String proLevel, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.create_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = proRecordService.getProRecordListByStatus(pageParam, proLevel, date,timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 项目记录查询
	 * @param pageParam 分页参数
	 * @param proSort 项目分类
	 * @param status 状态
	 * @param proLevel 项目等级
	 * @param date 日期范围类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProRecordListByQuery")
	public RetDataBean getProRecordListByQuery(PageParam pageParam, String proSort, String status, String proLevel, String date) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("r.create_time");
			} else {
				pageParam.setProp("r."+ StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			}else
			{
				if(pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				}else if(pageParam.getOrder().equals("ascending"))
				{
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToDay(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = proRecordService.getProRecordListByQuery(pageParam, proSort, proLevel, date,timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取项目分类树结构
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProSortTree")
	public RetDataBean getProSortTree(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  proSortService.getProSortTree(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按项目Id获取项目信息
	 * @param proId 项目Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProRecordById")
	public RetDataBean getProRecordById(String proId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  proRecordService.getProRecordById(userInfo.getOrgId(),proId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取分类列表
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProSort")
	public RetDataBean getProSort() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proSortService.getProSort(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取字典分类列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProDicParentList")
	public RetDataBean getProDicParentList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proDicService.getProDicParentList(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取字典数
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProDicTree")
	public RetDataBean getProDicTree() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proDicService.getProDicTree(userInfo.getOrgId()));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按模块获取分类列表
	 *
	 * @param code 字典分类码
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProDicByCode")
	public RetDataBean getProDicByCode(String code) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proDicService.getProDicByCode(userInfo.getOrgId(), code));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典列表
	 *
	 * @param pageParam 分页参数
	 * @param parentId 字典父级Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProDicList")
	public RetDataBean getProDicList(PageParam pageParam, String parentId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.sort_no");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("asc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setDeptId(userInfo.getDeptId());
			pageParam.setLevelId(userInfo.getUserLevel());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proDicService.getProDicList(pageParam, parentId));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 获取项目人员权限详情
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProRoleInfo")
	public RetDataBean getProRoleInfo() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ProRole ProRole = new ProRole();
			ProRole.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proRoleService.selectOneProRole(ProRole));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取项项目审批人员
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProApprovalUserList")
	public RetDataBean getProApprovalUserList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ProRole proRole = new ProRole();
			proRole.setOrgId(userInfo.getOrgId());
			proRole = proRoleService.selectOneProRole(proRole);
			if (proRole == null) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, new JSONArray());
			} else {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userInfoService.getAllUserByAccountList(userInfo.getOrgId(), proRole.getApprovalUserRole()));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取项目费用科目树结构
	 * @return 消息结构
	 */
	@GetMapping(value = "/getProExpAccountTree")
	public RetDataBean getProExpAccountTree(String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,  proExpAccountService.getProExpAccountTree(userInfo.getOrgId(),keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
}
