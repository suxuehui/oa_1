package com.core136.controller.project;

import com.core136.bean.account.UserInfo;
import com.core136.bean.project.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.project.*;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/project")
@CrossOrigin
@Api(value="项目SetController",tags={"项目更新数据接口"})
public class ApiSetProjectController {
	private UserInfoService userInfoService;
	private ProExpAccountService proExpAccountService;
	private ProRecordService proRecordService;
	private ProDicService proDicService;
	private ProSortService proSortService;
	private ProRoleService proRoleService;
	private ProTaskService proTaskService;
	private ProFileService proFileService;
	private ProProblemService proProblemService;
	private ProBudgetService proBudgetService;
	private ProTaskProcessService proTaskProcessService;
	private ProProblemReplyService proProblemReplyService;
	private ProApprovalService proApprovalService;
	private ProTaskLinkService proTaskLinkService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setProExpAccountService(ProExpAccountService proExpAccountService) {
		this.proExpAccountService = proExpAccountService;
	}
	@Autowired
	public void setProRecordService(ProRecordService proRecordService) {
		this.proRecordService = proRecordService;
	}
	@Autowired
	public void setProDicService(ProDicService proDicService) {
		this.proDicService = proDicService;
	}
	@Autowired
	public void setProSortService(ProSortService proSortService) {
		this.proSortService = proSortService;
	}
	@Autowired
	public void setProRoleService(ProRoleService proRoleService) {
		this.proRoleService = proRoleService;
	}
	@Autowired
	public void setProTaskService(ProTaskService proTaskService) {
		this.proTaskService = proTaskService;
	}
	@Autowired
	public void setProFileService(ProFileService proFileService) {
		this.proFileService = proFileService;
	}
	@Autowired
	public void setProProblemService(ProProblemService proProblemService) {
		this.proProblemService = proProblemService;
	}
	@Autowired
	public void setProBudgetService(ProBudgetService proBudgetService) {
		this.proBudgetService = proBudgetService;
	}
	@Autowired
	public void setProTaskProcessService(ProTaskProcessService proTaskProcessService) {
		this.proTaskProcessService = proTaskProcessService;
	}
	@Autowired
	public void setProProblemReplyService(ProProblemReplyService proProblemReplyService) {
		this.proProblemReplyService = proProblemReplyService;
	}
	@Autowired
	public void setProApprovalService(ProApprovalService proApprovalService) {
		this.proApprovalService = proApprovalService;
	}
	@Autowired
	public void setProTaskLinkService(ProTaskLinkService proTaskLinkService) {
		this.proTaskLinkService = proTaskLinkService;
	}

	/**
	 * 回复项目问题
	 *
	 * @param proProblemReply 回复项目问题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProProblemReply")
	public RetDataBean insertProProblemReply(ProProblemReply proProblemReply) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proProblemReply.setRecordId(SysTools.getGUID());
			proProblemReply.setCreateUser(userInfo.getAccountId());
			proProblemReply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proProblemReply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proProblemReplyService.insertProProblemReply(proProblemReply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除问题回复记录
	 *
	 * @param proProblemReply 回复项目问题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProProblemReply")
	public RetDataBean deleteProProblemReply(ProProblemReply proProblemReply) {
		try {
			if (StringUtils.isBlank(proProblemReply.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proProblemReply.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proProblemReplyService.deleteProProblemReply(proProblemReply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新问题回复
	 *
	 * @param proProblemReply 回复项目问题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProProblemReply")
	public RetDataBean updateProProblemReply(ProProblemReply proProblemReply) {
		try {
			if (StringUtils.isBlank(proProblemReply.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProProblemReply.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", proProblemReply.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proProblemReplyService.updateProProblemReply(example, proProblemReply));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加项目任务处理结果的同时更新任务的进度
	 * @param proTaskProcess 任务处理过程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/createProcess")
	public RetDataBean createProcess(ProTaskProcess proTaskProcess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proTaskProcess.setProcessId(SysTools.getGUID());
			proTaskProcess.setCreateUser(userInfo.getAccountId());
			proTaskProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proTaskProcess.setOrgId(userInfo.getOrgId());
			return proTaskProcessService.createProcess(proTaskProcess);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加任务处理过程
	 *
	 * @param proTaskProcess 任务处理过程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProTaskProcess")
	public RetDataBean insertProTaskProcess(ProTaskProcess proTaskProcess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proTaskProcess.setProcessId(SysTools.getGUID());
			proTaskProcess.setCreateUser(userInfo.getAccountId());
			proTaskProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proTaskProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proTaskProcessService.insertProTaskProcess(proTaskProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除项目任务处理过程
	 *
	 * @param proTaskProcess 任务处理过程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProTaskProcess")
	public RetDataBean deleteProTaskProcess(ProTaskProcess proTaskProcess) {
		try {
			if (StringUtils.isBlank(proTaskProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proTaskProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proTaskProcessService.deleteProTaskProcess(proTaskProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新项目处理过程
	 *
	 * @param proTaskProcess 任务处理过程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProTaskProcess")
	public RetDataBean updateProTaskProcess(ProTaskProcess proTaskProcess) {
		try {
			if (StringUtils.isBlank(proTaskProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProTaskProcess.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", proTaskProcess.getProcessId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proTaskProcessService.updateProTaskProcess(example, proTaskProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建项目经费
	 *
	 * @param proBudget 项目经费对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProBudget")
	public RetDataBean insertProBudget(ProBudget proBudget) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proBudget.setRecordId(SysTools.getGUID());
			proBudget.setCreateUser(userInfo.getAccountId());
			proBudget.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proBudget.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proBudgetService.insertProBudget(proBudget));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除项目经费
	 *
	 * @param proBudget 项目经费对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProBudget")
	public RetDataBean deleteProBudget(ProBudget proBudget) {
		try {
			if (StringUtils.isBlank(proBudget.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proBudget.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proBudgetService.deleteProBudget(proBudget));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除项目费用
	 * @param ids 项目经费Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProBudgetByIds")
	public RetDataBean deleteProBudgetByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return proBudgetService.deleteProBudgetByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新项目经费
	 *
	 * @param proBudget 项目经费对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProBudget")
	public RetDataBean updateProBudget(ProBudget proBudget) {
		try {
			if (StringUtils.isBlank(proBudget.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProBudget.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", proBudget.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proBudgetService.updateProBudget(example, proBudget));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加项目问题反馈
	 *
	 * @param proProblem 目问题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProProblem")
	public RetDataBean insertProProblem(ProProblem proProblem) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proProblem.setRecordId(SysTools.getGUID());
			proProblem.setCreateUser(userInfo.getAccountId());
			proProblem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proProblem.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proProblemService.insertProProblem(proProblem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除项目问题反馈
	 *
	 * @param proProblem 目问题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProProblem")
	public RetDataBean deleteProProblem(ProProblem proProblem) {
		try {
			if (StringUtils.isBlank(proProblem.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proProblem.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proProblemService.deleteProProblem(proProblem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除项目问题
	 * @param ids 项目问题Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProProblemByIds")
	public RetDataBean deleteProProblemByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return proProblemService.deleteProProblemByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新项目问题
	 *
	 * @param proProblem 目问题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProProblem")
	public RetDataBean updateProProblem(ProProblem proProblem) {
		try {
			if (StringUtils.isBlank(proProblem.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProProblem.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", proProblem.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proProblemService.updateProProblem(example, proProblem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建项目文档
	 *
	 * @param proFile 项目文档对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProFile")
	public RetDataBean insertProFile(ProFile proFile) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proFile.setFileId(SysTools.getGUID());
			proFile.setCreateUser(userInfo.getAccountId());
			proFile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proFile.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proFileService.insertProFile(proFile));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 删除项目文档
	 *
	 * @param proFile 项目文档对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProFile")
	public RetDataBean deleteProFile(ProFile proFile) {
		try {
			if (StringUtils.isBlank(proFile.getFileId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proFile.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proFileService.deleteProFile(proFile));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除文档
	 * @param ids 项目文档Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProFileByIds")
	public RetDataBean deleteProFileByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return proFileService.deleteProFileByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新项目文档
	 *
	 * @param proFile 项目文档对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProFile")
	public RetDataBean updateProFile(ProFile proFile) {
		try {
			if (StringUtils.isBlank(proFile.getFileId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProFile.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("fileId", proFile.getFileId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proFileService.updateProFile(example, proFile));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加项目审批意见
	 * @param proApproval 项目审批对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setApprovalProRecord")
	public RetDataBean setApprovalProRecord(ProApproval proApproval) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proApproval.setRecordId(SysTools.getGUID());
			proApproval.setCreateUser(userInfo.getAccountId());
			proApproval.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proApproval.setOrgId(userInfo.getOrgId());
			return proApprovalService.setApprovalProRecord(proApproval);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建任务关联
	 *
	 * @param proTaskLink 行务关联对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProTaskLink")
	public RetDataBean insertProTaskLink(ProTaskLink proTaskLink) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proTaskLink.setTaskLinkId(SysTools.getGUID());
			proTaskLink.setCreateUser(userInfo.getAccountId());
			proTaskLink.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proTaskLink.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proTaskLinkService.insertProTaskLink(proTaskLink));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除任务关联记录
	 *
	 * @param proTaskLink 行务关联对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProTaskLink")
	public RetDataBean deleteProTaskLink(ProTaskLink proTaskLink) {
		try {
			if (StringUtils.isBlank(proTaskLink.getTaskLinkId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proTaskLink.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proTaskLinkService.deleteProTaskLink(proTaskLink));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新任务关联记录
	 *
	 * @param proTaskLink 行务关联对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProTaskLink")
	public RetDataBean updateProTaskLink(ProTaskLink proTaskLink) {
		try {
			if (StringUtils.isBlank(proTaskLink.getTaskLinkId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProTaskLink.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskLinkId", proTaskLink.getTaskLinkId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proTaskLinkService.updateProTaskLink(example, proTaskLink));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 创建子任务
	 *
	 * @param proTask 项目任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProTask")
	public RetDataBean insertProTask(ProTask proTask) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proTask.setCreateUser(userInfo.getAccountId());
			proTask.setStatus("0");
			proTask.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proTask.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proTaskService.insertProTask(proTask));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除子任务
	 *
	 * @param proTask 项目任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProTask")
	public RetDataBean deleteProTask(ProTask proTask) {
		try {
			if (StringUtils.isBlank(proTask.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proTask.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proTaskService.deleteProTask(proTask));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新子任务
	 *
	 * @param proTask 项目任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProTask")
	public RetDataBean updateProTask(ProTask proTask) {
		try {
			if (StringUtils.isBlank(proTask.getTaskId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProTask.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("taskId", proTask.getTaskId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proTaskService.updateProTask(example, proTask));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 提交审批
	 *
	 * @param proRecord 项目对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/approvalProRecord")
	public RetDataBean approvalProRecord(ProRecord proRecord) {
		try {
			if (StringUtils.isBlank(proRecord.getProId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			ProRole proRole = new ProRole();
			proRole.setOrgId(userInfo.getOrgId());
			proRole = proRoleService.selectOneProRole(proRole);
			String notNeedApprovalRole = proRole.getNotNeedApprovalRole();
			if (StringUtils.isNotBlank(notNeedApprovalRole) && Arrays.asList(notNeedApprovalRole.split(",")).contains(userInfo.getAccountId())) {
				proRecord.setStatus("3");
			}
			Example example = new Example(ProRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("proId", proRecord.getProId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proRecordService.updateProRecord(example, proRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加项目类型
	 * @param proSort 项目分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProSort")
	public RetDataBean insertProSort(ProSort proSort) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proSort.setSortId(SysTools.getGUID());
			if(StringUtils.isBlank(proSort.getParentId()))
			{
				proSort.setParentId("0");
			}
			proSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proSort.setCreateUser(userInfo.getAccountId());
			proSort.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proSortService.insertProSort(proSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除项目类型
	 * @param proSort 项目分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProSort")
	public RetDataBean deleteProSort(ProSort proSort) {
		try {
			if (StringUtils.isBlank(proSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proSort.setOrgId(userInfo.getOrgId());
			if(proSortService.isExistChild(userInfo.getOrgId(),proSort.getSortId())==0) {
				return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proSortService.deleteProSort(proSort));
			}else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_EXIST_CHILD);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新项目类型
	 * @param proSort 项目分类对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProSort")
	public RetDataBean updateProSort(ProSort proSort) {
		try {
			if (StringUtils.isBlank(proSort.getSortId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if(StringUtils.isBlank(proSort.getParentId()))
			{
				proSort.setParentId("0");
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProSort.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("sortId", proSort.getSortId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proSortService.updateProSort(example, proSort));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建项目
	 *
	 * @param proRecord 项目对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProRecord")
	public RetDataBean insertProRecord(ProRecord proRecord) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (proRoleService.isInRole(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getUserLevel()) > 0) {
				proRecord.setProId(SysTools.getGUID());
				proRecord.setCreateUser(userInfo.getAccountId());
				proRecord.setStatus("0");
				proRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
				proRecord.setOrgId(userInfo.getOrgId());
				return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proRecordService.insertProRecord(proRecord));
			} else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除项目
	 *
	 * @param proRecord 项目对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProRecord")
	public RetDataBean deleteProRecord(ProRecord proRecord) {
		try {
			if (StringUtils.isBlank(proRecord.getProId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proRecord.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proRecordService.deleteProRecord(proRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除项目记录
	 * @param ids 项目Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProRecordByIds")
	public RetDataBean deleteProRecordByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return proRecordService.deleteProRecordByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 更新项目信息
	 *
	 * @param proRecord 项目对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProRecord")
	public RetDataBean updateProRecord(ProRecord proRecord) {
		try {
			if (StringUtils.isBlank(proRecord.getProId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProRecord.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("proId", proRecord.getProId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proRecordService.updateProRecord(example, proRecord));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 创建字典
	 *
	 * @param proDic 项目字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProDic")
	public RetDataBean insertProDic(ProDic proDic) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(proDic.getParentId())) {
				proDic.setParentId("0");
			}
			proDic.setDicId(SysTools.getGUID());
			proDic.setOrgId(userInfo.getOrgId());
			proDic.setStatus("1");
			proDic.setCreateUser(userInfo.getCreateUser());
			proDic.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, proDicService.insertProDic(proDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新字典
	 *
	 * @param proDic 项目字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProDic")
	public RetDataBean updateProDic(ProDic proDic) {
		try {
			if (StringUtils.isBlank(proDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProDic.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("dicId", proDic.getDicId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, proDicService.updateProDic(proDic, example));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除字典
	 *
	 * @param proDic 项目字典对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProDic")
	public RetDataBean deleteProDic(ProDic proDic) {
		try {
			if (StringUtils.isBlank(proDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (proDicService.isExistChild(userInfo.getOrgId(), proDic.getDicId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
			}
			proDic.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proDicService.deleteProDic(proDic));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除字典
	 *
	 * @param ids 字典Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProDicByIds")
	public RetDataBean deleteProDicByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<String> idsList = Arrays.asList(ids);
			return proDicService.deleteProDicByIds(userInfo.getOrgId(), idsList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 字典排序
	 *
	 * @param oldDicId 原字典Id
	 * @param newDicId 新字典Id
	 * @param oldSortNo 原排序号
	 * @param newSortNo 新排序号
	 * @return 消息结构
	 */
	@PostMapping(value = "/dropProDic")
	public RetDataBean dropProDic(String oldDicId, String newDicId, Integer oldSortNo, Integer newSortNo) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proDicService.dropProDic(userInfo.getOrgId(), oldDicId, newDicId, oldSortNo, newSortNo));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置项目人员权限
	 * @param proRole 项目权限对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setProRole")
	public RetDataBean setProRole(ProRole proRole) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proRoleService.setProRole(userInfo, proRole));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 添加项目费用科目
	 *
	 * @param proExpAccount 费用科目对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertProExpAccount")
	public RetDataBean insertProExpAccount(ProExpAccount proExpAccount) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			proExpAccount.setExpId(SysTools.getGUID());
			proExpAccount.setCreateUser(userInfo.getAccountId());
			proExpAccount.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			proExpAccount.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proExpAccountService.insertProExpAccount(proExpAccount));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除项目费用科目
	 *
	 * @param proExpAccount 费用科目对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteProExpAccount")
	public RetDataBean deleteProExpAccount(ProExpAccount proExpAccount) {
		try {
			if (StringUtils.isBlank(proExpAccount.getExpId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			proExpAccount.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proExpAccountService.deleteProExpAccount(proExpAccount));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新项目费用科目
	 *
	 * @param proExpAccount 费用科目对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateProExpAccount")
	public RetDataBean updateProExpAccount(ProExpAccount proExpAccount) {
		try {
			if (StringUtils.isBlank(proExpAccount.getExpId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(ProExpAccount.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("expId", proExpAccount.getExpId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proExpAccountService.updateProExpAccount(example, proExpAccount));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
}
