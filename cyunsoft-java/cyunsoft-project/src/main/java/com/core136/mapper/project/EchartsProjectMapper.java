package com.core136.mapper.project;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EchartsProjectMapper {
    /**
     * 项目费用类型统计
     *
     * @param orgId 机构码
     * @return 费用类型统计列表
     */
    List<Map<String, String>> getProCostTypePie(@Param(value = "orgId") String orgId);


    List<Map<String, String>> getProCostTypeBar(@Param(value = "orgId") String orgId);

	List<Map<String, String>> getProSortBar(@Param(value = "orgId") String orgId);

	List<Map<String, String>> getProSortPic(@Param(value = "orgId") String orgId);


}
