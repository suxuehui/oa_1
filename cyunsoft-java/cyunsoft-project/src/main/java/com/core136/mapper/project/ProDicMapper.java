package com.core136.mapper.project;

import com.core136.bean.project.ProDic;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProDicMapper extends MyMapper<ProDic> {
	List<Map<String,String>>getProDicParentList(@Param(value = "orgId") String orgId);

	List<Map<String,String>>getProDicList(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId, @Param(value = "keyword") String keyword);
	/**
	 * 按标识获取分类码列表
	 * @param orgId 机构码
	 * @param code 分类码
	 * @return 分类码列表
	 */
	List<Map<String, Object>> getProDicByCode(@Param(value = "orgId") String orgId, @Param(value = "code") String code);
}
