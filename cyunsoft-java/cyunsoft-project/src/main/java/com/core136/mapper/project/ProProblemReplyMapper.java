package com.core136.mapper.project;

import com.core136.bean.project.ProProblemReply;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProProblemReplyMapper extends MyMapper<ProProblemReply> {
    /**
     * 更新任务问题列表
     *
     * @param orgId 机构码
     * @param problemId 项目问题Id
     * @return 任务问题列表
     */
    List<Map<String, String>> getProblemReplyList(@Param(value = "orgId") String orgId, @Param(value = "problemId") String problemId);
}
