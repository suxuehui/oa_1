package com.core136.mapper.project;

import com.core136.bean.project.ProRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProRecordMapper extends MyMapper<ProRecord> {

	List<Map<String, String>> getProRecordForFilter(@Param(value = "orgId") String orgId,@Param(value="sortId")String sortId);
	List<Map<String, String>> getProRecordListForSelect(@Param(value = "orgId") String orgId);

	Map<String, String> getProRecordById(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);

	/**
     * 按状态获取项目列表
     * @param orgId 机构码
     * @param proLevel 项目等级
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status 项目状态
     * @param keyword 查询关键词
     * @return 项目记录
     */
    List<Map<String, String>> getProRecordListByStatus(@Param(value = "orgId") String orgId, @Param(value = "proLevel") String proLevel,@Param(value="dateQueryType")String dateQueryType,
                                                       @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                       @Param(value = "status") String status, @Param(value = "keyword") String keyword);

    /**
     * 项目记录查询
     * @param orgId 机构码
     * @param proSort 项目分类对象
     * @param proLevel 项目等级
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param status 项目状态
     * @param keyword 查询关键词
     * @return 项目记录
     */
    List<Map<String, String>> getProRecordListByQuery(@Param(value = "orgId") String orgId, @Param(value = "proSort") String proSort, @Param(value = "proLevel") String proLevel,
                                                      @Param(value="dateQueryType")String dateQueryType,@Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                      @Param(value = "status") String status, @Param(value = "keyword") String keyword);


    List<Map<String, String>> getProRecordListForApproval(@Param(value = "orgId") String orgId, @Param(value="proSort")String proSort,@Param(value = "proLevel") String proLevel,@Param("status")String status,@Param("dateQueryType")String dateQueryType,
                                                          @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


    Map<String, String> getProRecordStatusCountList(@Param(value = "orgId") String orgId);

}
