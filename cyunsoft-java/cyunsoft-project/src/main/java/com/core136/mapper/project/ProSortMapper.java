package com.core136.mapper.project;

import com.core136.bean.project.ProSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProSortMapper extends MyMapper<ProSort> {
	List<ProSort> getAllProSort(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

	List<Map<String,String>>getProSort(@Param(value="orgId")String orgId);
}
