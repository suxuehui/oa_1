package com.core136.mapper.project;

import com.core136.bean.project.ProTaskLink;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ProTaskLinkMapper extends MyMapper<ProTaskLink> {
    /**
     * 获取任务关联列表
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @return 任务关联列表
     */
    List<Map<String, String>> getProTaskLinkList(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);
}
