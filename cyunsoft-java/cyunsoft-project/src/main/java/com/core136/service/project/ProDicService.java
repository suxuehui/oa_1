package com.core136.service.project;

import com.core136.bean.project.ProDic;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProDicMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class ProDicService {
    private ProDicMapper proDicMapper;

	@Autowired
	public void setProDicMapper(ProDicMapper proDicMapper) {
		this.proDicMapper = proDicMapper;
	}

	public int insertProDic(ProDic proDic) {
        return proDicMapper.insert(proDic);
    }

    public int deleteProDic(ProDic proDic) {
        return proDicMapper.delete(proDic);
    }

    public int updateProDic( ProDic proDic,Example example) {
        return proDicMapper.updateByExampleSelective(proDic, example);
    }

    public ProDic selectOneProDic(ProDic proDic) {
        return proDicMapper.selectOne(proDic);
    }

    public List<ProDic> getProDicList(Example example) {
        return proDicMapper.selectByExample(example);
    }

    public int deleteProDic(Example example) {
        return proDicMapper.deleteByExample(example);
    }

	/**
	 * 批量删除字典
	 * @param orgId 机构码
	 * @param list 字典Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteProDicByIds(String orgId, List<String> list) {
		if(!list.isEmpty()) {
			Example example = new Example(ProDic.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("dicId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS,proDicMapper.deleteByExample(example));
		}else {
			return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
		}
	}

	/**
	 * 判断字典分类下是否有子集
	 * @param orgId 机构码
	 * @param parentId 父级Id
	 * @return 是否存在子集
	 */
	public boolean isExistChild(String orgId,String parentId) {
		ProDic proDic = new ProDic();
		proDic.setOrgId(orgId);
		proDic.setParentId(parentId);
		return proDicMapper.selectCount(proDic) > 0;
	}

	/**
	 * 按标识获取分类码列表
	 */

	public List<Map<String, Object>> getProDicByCode(String orgId, String code) {
		return proDicMapper.getProDicByCode(orgId, code);
	}

	/**
	 * 字典排序
	 * @param orgId 机构码
	 * @param oldDicId 历史字典Id
	 * @param newDicId 新字典Id
	 * @param oldSortNo 历史字内排序号
	 * @param newSortNo 新子典排序号
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	public RetDataBean dropProDic(String orgId,String oldDicId, String newDicId,Integer oldSortNo,Integer newSortNo) {
		try {
			ProDic oldProDic = new ProDic();
			oldProDic.setSortNo(newSortNo);
			ProDic newProDic = new ProDic();
			newProDic.setSortNo(oldSortNo);
			Example example = new Example(ProDic.class);
			example.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",oldDicId);
			updateProDic(oldProDic,example);
			Example example1 = new Example(ProDic.class);
			example1.createCriteria().andEqualTo("orgId",orgId).andEqualTo("dicId",newDicId);
			updateProDic(newProDic,example1);
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取字典列表
	 * @param pageParam 分页参数
	 * @return 字典列表
	 */
	public PageInfo<Map<String,String>> getProDicList(PageParam pageParam, String parentId) throws Exception{
		PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
		List<Map<String, String>> datalist = getProDicList(pageParam.getOrgId(),parentId,pageParam.getKeyword());
		return new PageInfo<>(datalist);
	}

	/**
	 * 获取字典列表
	 * @param orgId 机构码
	 * @param parentId 父组Id
	 * @param keyword 查询关键词
	 * @return 字典列表
	 */
	public List<Map<String,String>> getProDicList(String orgId,String parentId,String keyword) {
		return proDicMapper.getProDicList(orgId,parentId,"%"+keyword+"%");
	}


	public List<ProDic> getProDicTree(String orgId) {
		List<ProDic> list =getAllProDic(orgId);
		List<ProDic> proDicList = new ArrayList<>();
		for (ProDic dic : list) {
			if (dic.getParentId().equals("0")) {
				proDicList.add(dic);
			}
		}
		for (ProDic proDic : proDicList) {
			proDic.setChildren(getChild(proDic.getDicId(), list));
		}
		return proDicList;
	}

	/**
	 * 获取字典子集
	 * @param dicId 字典Id
	 * @param rootProDic 项目字典对象列表
	 * @return 字典子集
	 */
	private List<ProDic> getChild(String dicId, List<ProDic> rootProDic) {
		List<ProDic> childList = new ArrayList<>();
		for (ProDic proDic : rootProDic) {
			if (proDic.getParentId().equals(dicId)) {
				childList.add(proDic);
			}
		}
		for (ProDic proDic : childList) {
			proDic.setChildren(getChild(proDic.getDicId(), rootProDic));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	List<ProDic> getAllProDic(String orgId) {
		Example example = new Example(ProDic.class);
		example.createCriteria().andEqualTo("orgId",orgId);
		example.setOrderByClause("sort_no asc");
		return proDicMapper.selectByExample(example);
	}

	/**
	 * 获取字典分类列表
	 * @param orgId 机构码
	 * @return 字典分类列表
	 */
	public List<Map<String,String>>getProDicParentList(String orgId) {
		return proDicMapper.getProDicParentList(orgId);
	}
}
