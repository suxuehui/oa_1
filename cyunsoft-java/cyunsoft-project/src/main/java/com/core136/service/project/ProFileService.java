package com.core136.service.project;

import com.core136.bean.project.ProFile;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProFileMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProFileService {
    private ProFileMapper proFileMapper;
	@Autowired
	public void setProFileMapper(ProFileMapper proFileMapper) {
		this.proFileMapper = proFileMapper;
	}

	public int insertProFile(ProFile proFile) {
        return proFileMapper.insert(proFile);
    }

    public int deleteProFile(ProFile proFile) {
        return proFileMapper.delete(proFile);
    }

	/**
	 * 批量删除文档
	 * @param orgId 机构码
	 * @param list 文档Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteProFileByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(ProFile.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("fileId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, proFileMapper.deleteByExample(example));
		}
	}
    public int updateProFile(Example example, ProFile proFile) {
        return proFileMapper.updateByExampleSelective(proFile, example);
    }

    public ProFile selectOneProFile(ProFile proFile) {
        return proFileMapper.selectOne(proFile);
    }

    /**
     * 获取项目文档
     *
     * @param orgId 机构码
     * @param proId 项目Id
     * @param keyword 查询关键词
     * @return 项目文档列表
     */
    public List<Map<String, String>> getProFileListByProId(String orgId,String sortId, String proId,String dateQueryType,String beginTime,String endTime, String keyword) {
        return proFileMapper.getProFileListByProId(orgId,sortId, proId,dateQueryType,beginTime,endTime, "%" + keyword + "%");
    }

    /**
     * 获取项目文档
     *
     * @param pageParam 分页参数
     * @param proId 项目Id
     * @return 项目文档列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getProFileListByProId(PageParam pageParam,String sortId, String proId,String dateQueryType,String beginTime,String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProFileListByProId(pageParam.getOrgId(),sortId, proId,dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
