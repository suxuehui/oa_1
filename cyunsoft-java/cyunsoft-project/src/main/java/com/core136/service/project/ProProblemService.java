package com.core136.service.project;

import com.core136.bean.project.ProProblem;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProProblemMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProProblemService {
    private ProProblemMapper proProblemMapper;
	@Autowired
	public void setProProblemMapper(ProProblemMapper proProblemMapper) {
		this.proProblemMapper = proProblemMapper;
	}

	public int insertProProblem(ProProblem proProblem) {
        return proProblemMapper.insert(proProblem);
    }

    public int deleteProProblem(ProProblem proProblem) {
        return proProblemMapper.delete(proProblem);
    }
	/**
	 * 批量删除项目文档
	 * @param orgId 机构码
	 * @param list 项目文档Id 列表
	 * @return 消息结构
	 */
	public RetDataBean deleteProProblemByIds(String orgId, List<String> list)
	{
		if(list.isEmpty())
		{
			return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
		}else {
			Example example = new Example(ProProblem.class);
			example.createCriteria().andEqualTo("orgId", orgId).andIn("recordId", list);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, proProblemMapper.deleteByExample(example));
		}
	}
    public int updateProProblem(Example example, ProProblem proProblem) {
        return proProblemMapper.updateByExampleSelective(proProblem, example);
    }

    public ProProblem selectOneProProblem(ProProblem proProblem) {
        return proProblemMapper.selectOne(proProblem);
    }

    /**
     * 获取项目问题
     * @param orgId 机构码
	 * @param proId 项目Id
	 * @param sortId 分类Id
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 项目问题列表
     */
    public List<Map<String, String>> getProProblemListByPro(String orgId, String proId, String sortId,String dateQueryType,String beginTime,String endTime, String keyword) {
        return proProblemMapper.getProProblemListByPro(orgId, proId, sortId, dateQueryType,beginTime,endTime,"%" + keyword + "%");
    }

    /**
     * 获取项目问题
     * @param pageParam 分页参数
     * @param proId 项目Id
     * @param sortId 分类Id
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return 项目问题列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getProProblemListByPro(PageParam pageParam, String proId, String sortId,String dateQueryType,String beginTime,String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProProblemListByPro(pageParam.getOrgId(), proId, sortId,dateQueryType,beginTime,endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
