package com.core136.service.project;

import com.core136.bean.project.ProSort;
import com.core136.mapper.project.ProSortMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class ProSortService {
    private ProSortMapper proSortMapper;

	@Autowired
	public void setProSortMapper(ProSortMapper proSortMapper) {
		this.proSortMapper = proSortMapper;
	}

	public int insertProSort(ProSort proSort) {
        return proSortMapper.insert(proSort);
    }

    public int deleteProSort(ProSort proSort) {
        return proSortMapper.delete(proSort);
    }

    public int updateProSort(Example example, ProSort proSort) {
        return proSortMapper.updateByExampleSelective(proSort, example);
    }

    public ProSort selectOneProSort(ProSort proSort) {
        return proSortMapper.selectOne(proSort);
    }

	public List<ProSort> getAllProSort(String orgId,String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword="";
		}
		return proSortMapper.getAllProSort(orgId,"%"+keyword+"%");
	}

	public List<Map<String,String>>getProSort(String orgId) {
		return proSortMapper.getProSort(orgId);
	}

	/**
	 * 获取分类树结构
	 * @param orgId 机构码
	 * @return 分类树结构
	 */
	public List<ProSort> getProSortTree(String orgId,String keyword) {
		List<ProSort> list = getAllProSort(orgId,keyword);
		List<ProSort> proSortList = new ArrayList<>();
		for (ProSort sort : list) {
			if (sort.getParentId().equals("0")) {
				proSortList.add(sort);
			}
		}
		for (ProSort proSort : proSortList) {
			proSort.setChildren(getChildSortList(proSort.getSortId(), list));
		}
		return proSortList;
	}

	/**
	 * 获取分类子表
	 * @param sortId 分类Id
	 * @param rootProSort 分类对象列表
	 * @return 分类对象列表
	 */
	public List<ProSort> getChildSortList(String sortId, List<ProSort> rootProSort) {
		List<ProSort> childList = new ArrayList<>();
		for (ProSort proSort : rootProSort) {
			if (proSort.getParentId().equals(sortId)) {
				childList.add(proSort);
			}
		}
		for (ProSort proSort : childList) {
			proSort.setChildren(getChildSortList(proSort.getSortId(), rootProSort));
		}
		if (childList.isEmpty()) {
			return Collections.emptyList();
		}
		return childList;
	}

	/**
	 * 判断分类下是否有子分类
	 * @param orgId 机构码
	 * @param sortId 分类Id
	 * @return 是否子分类
	 */
	public int isExistChild(String orgId, String sortId) {
		ProSort proSort = new ProSort();
		proSort.setOrgId(orgId);
		proSort.setParentId(sortId);
		return proSortMapper.selectCount(proSort);
	}


}
