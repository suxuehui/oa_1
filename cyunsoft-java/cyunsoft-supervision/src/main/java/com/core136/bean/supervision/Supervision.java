package com.core136.bean.supervision;

import java.io.Serializable;

/**
 * @author lsq
 */
public class Supervision implements Serializable {
    private static final long serialVersionUID = 1L;

    private String supervisionId;
    private String title;
    private String subheading;
    private String eventType;
    private String levelId;
    private String beginTime;
    private String endTime;
    private String status;
    private String remark;
    private String leadId;
    private String joinUser;
    private String handedUser;
    private String attachId;
    private String finishRemark;
    private String msgType;
    private String finishTime;
	private String createTime;
	private String createUser;
    private String orgId;

	public String getSupervisionId() {
		return supervisionId;
	}

	public void setSupervisionId(String supervisionId) {
		this.supervisionId = supervisionId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubheading() {
		return subheading;
	}

	public void setSubheading(String subheading) {
		this.subheading = subheading;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getLevelId() {
		return levelId;
	}

	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}

	public String getJoinUser() {
		return joinUser;
	}

	public void setJoinUser(String joinUser) {
		this.joinUser = joinUser;
	}

	public String getHandedUser() {
		return handedUser;
	}

	public void setHandedUser(String handedUser) {
		this.handedUser = handedUser;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getFinishRemark() {
		return finishRemark;
	}

	public void setFinishRemark(String finishRemark) {
		this.finishRemark = finishRemark;
	}
}
