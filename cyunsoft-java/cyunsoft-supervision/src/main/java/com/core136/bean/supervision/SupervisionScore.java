package com.core136.bean.supervision;

import java.io.Serializable;

/**
 * @author lsq
 *
 */
public class SupervisionScore implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String scoreId;
    private String supervisionId;
    private String remark;
    private Double sysScore;
    private Double userScore;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getSupervisionId() {
        return supervisionId;
    }

    public void setSupervisionId(String supervisionId) {
        this.supervisionId = supervisionId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Double getSysScore() {
        return sysScore;
    }

    public void setSysScore(Double sysScore) {
        this.sysScore = sysScore;
    }

    public Double getUserScore() {
        return userScore;
    }

    public void setUserScore(Double userScore) {
        this.userScore = userScore;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
