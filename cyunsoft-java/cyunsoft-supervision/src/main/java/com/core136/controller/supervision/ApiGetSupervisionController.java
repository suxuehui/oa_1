package com.core136.controller.supervision;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.core136.bean.account.UserInfo;
import com.core136.bean.supervision.Supervision;
import com.core136.bean.supervision.SupervisionConfig;
import com.core136.bean.supervision.SupervisionIntegral;
import com.core136.bean.system.PageParam;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.supervision.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/get/supervision")
@CrossOrigin
@Api(value="督查督办GetController",tags={"督查督办获取数据接口"})
public class ApiGetSupervisionController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private UserInfoService userInfoService;
	private SupervisionIntegralService supervisionIntegralService;
	private SupervisionConfigService supervisionConfigService;
	private SupervisionService supervisionService;
	private SupervisionScoreService supervisionScoreService;
	private SupervisionProcessService supervisionProcessService;
	private SupervisionDelayService supervisionDelayService;
	private SupervisionResultService supervisionResultService;
	private SupervisionProblemService supervisionProblemService;
	private EchartsSupervisionService echartsSupervisionService;
	private SupervisionBpmListService supervisionBpmListService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setSupervisionIntegralService(SupervisionIntegralService supervisionIntegralService) {
		this.supervisionIntegralService = supervisionIntegralService;
	}
	@Autowired
	public void setSupervisionConfigService(SupervisionConfigService supervisionConfigService) {
		this.supervisionConfigService = supervisionConfigService;
	}
	@Autowired
	public void setSupervisionService(SupervisionService supervisionService) {
		this.supervisionService = supervisionService;
	}
	@Autowired
	public void setSupervisionScoreService(SupervisionScoreService supervisionScoreService) {
		this.supervisionScoreService = supervisionScoreService;
	}
	@Autowired
	public void setSupervisionProcessService(SupervisionProcessService supervisionProcessService) {
		this.supervisionProcessService = supervisionProcessService;
	}
	@Autowired
	public void setSupervisionDelayService(SupervisionDelayService supervisionDelayService) {
		this.supervisionDelayService = supervisionDelayService;
	}
	@Autowired
	public void setSupervisionResultService(SupervisionResultService supervisionResultService) {
		this.supervisionResultService = supervisionResultService;
	}
	@Autowired
	public void setSupervisionProblemService(SupervisionProblemService supervisionProblemService) {
		this.supervisionProblemService = supervisionProblemService;
	}
	@Autowired
	public void setEchartsSupervisionService(EchartsSupervisionService echartsSupervisionService) {
		this.echartsSupervisionService = echartsSupervisionService;
	}
	@Autowired
	public void setSupervisionBpmListService(SupervisionBpmListService supervisionBpmListService) {
		this.supervisionBpmListService = supervisionBpmListService;
	}

	/**
	 * 移动端督查督办列表
	 *
	 * @param page 页码
	 * @return 督查督办列表
	 */
	@GetMapping(value = "/getMobileMySupervisionList")
	public RetDataBean getMobileMySupervisionList(Integer page) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			page = (page - 1) * 10;
			UserInfo userInfo = userInfoService.getRedisUser();
			List<Map<String, String>> pageInfo = supervisionService.getMobileMySupervisionList(userInfo.getOrgId(), userInfo.getAccountId(), page);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取督查督办事件详情
	 *
	 * @param supervisionId 督查督办事件Id
	 * @return 督查督办事件详情
	 */
	@GetMapping(value = "/getSupervisionById")
	public RetDataBean getSupervisionById(String supervisionId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Map<String, String> resMap = supervisionService.getSupervisionById(userInfo.getOrgId(), supervisionId);
			String accountIds = resMap.get("joinUser");
			if (StringUtils.isNotBlank(accountIds)) {
				String[] paramsList = accountIds.split(",");
				List<String> list = Arrays.asList(paramsList);
				List<Map<String, String>> userListMap = userInfoService.getUserNamesByAccountIds(list, userInfo.getOrgId());
				List<String> userList = new ArrayList<>();
				for (Map<String, String> map : userListMap) {
					userList.add(map.get("userName"));
				}
				resMap.put("joinUserName", StringUtils.join(userList, ","));
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, resMap);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取延时任务列表
	 * @return  延时任务列表
	 */
	@GetMapping(value = "/getAllSupervisionProcessForDesk")
	public RetDataBean getAllSupervisionProcessForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionProcessService.getAllSupervisionProcessForDesk(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有督查事件
	 *
	 * @return 所有督查事件
	 */
	@GetMapping(value = "/getAllFinishSupervisionForDesk")
	public RetDataBean getAllFinishSupervisionForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionService.getAllFinishSupervisionForDesk(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取所有事件列表前5
	 *
	 * @return 所有事件列表前5列表
	 */
	@GetMapping(value = "/getAllSupervisionForDesk")
	public RetDataBean getAllSupervisionForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionService.getAllSupervisionForDesk(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取督查督办待办记录
	 *
	 * @return 督查督办待办列表
	 */
	@GetMapping(value = "/getSupervisionProcessListForDesk")
	public RetDataBean getSupervisionProcessListForDesk() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			List<Map<String, String>> resList = supervisionProcessService.getSupervisionProcessListForDesk(userInfo.getOrgId(), userInfo.getAccountId());
			for (Map<String, String> tmpMap : resList) {
				String accountIds = tmpMap.get("operator");
				if (StringUtils.isNotBlank(accountIds)) {
					String[] paramsList = accountIds.split(",");
					List<String> list = Arrays.asList(paramsList);
					List<Map<String, String>> userListMap = userInfoService.getUserNamesByAccountIds(list, userInfo.getOrgId());
					List<String> userList = new ArrayList<>();
					for (Map<String, String> map : userListMap) {
						userList.add(map.get("userName"));
					}
					tmpMap.put("operatorUserName", StringUtils.join(userList, ","));
				}
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, resList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取监管事件列表
	 * @return 监管事件列表
	 */
	@GetMapping(value = "/getManageSupervisionListForDesk")
	public RetDataBean getManageSupervisionListForDesk() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:manage")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			List<Map<String, String>> resList = supervisionService.getManageSupervisionListForDesk(userInfo.getOrgId(), userInfo.getAccountId());
			for (Map<String, String> tmpMap : resList) {
				String accountIds = tmpMap.get("joinUser");
				if (StringUtils.isNotBlank(accountIds)) {
					String[] paramsList = accountIds.split(",");
					List<String> list = Arrays.asList(paramsList);
					List<Map<String, String>> userListMap = userInfoService.getUserNamesByAccountIds(list, userInfo.getOrgId());
					List<String> userList = new ArrayList<>();
					for (Map<String, String> map : userListMap) {
						userList.add(map.get("userName"));
					}
					tmpMap.put("joinUserName", StringUtils.join(userList, ","));
				}
			}
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, resList);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 前10位领导的人员工作量占比
	 * @return 人员工作量占比
	 */
	@GetMapping(value = "/getBiSupervisionByLeadPie")
	public RetDataBean getBiSupervisionByLeadPie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsSupervisionService.getBiSupervisionByLeadPie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按月份统计工作量
	 * @return 工作量
	 */
	@GetMapping(value = "/getBiSupervisionByMonthLine")
	public RetDataBean getBiSupervisionByMonthLine() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsSupervisionService.getBiSupervisionByMonthLine(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取督察督办事件分类前10的占比
	 *
	 * @return 前10的占比
	 */
	@GetMapping(value = "/getBiSupervisionTypePie")
	public RetDataBean getBiSupervisionTypePie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsSupervisionService.getBiSupervisionTypePie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取督查督办当前状态总数
	 *
	 * @return 当前状态总数
	 */
	@GetMapping(value = "/getBiSupervisionStatusTypePie")
	public RetDataBean getBiSupervisionStatusTypePie() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			ObjectMapper om = new ObjectMapper();
			om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
			OptionConfig optionConfig = echartsSupervisionService.getBiSupervisionStatusTypePie(userInfo);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, JSON.parseObject(om.writeValueAsString(optionConfig)));
		} catch (Exception e) {

			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按部门汇总
	 *
	 * @return 部门汇总
	 */
	@GetMapping(value = "/getQuerySupervisionForDept")
	public RetDataBean getQuerySupervisionForDept() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionProcessService.getQuerySupervisionForDept(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 按类型汇总
	 *
	 * @return 类型汇总
	 */
	@GetMapping(value = "/getQuerySupervisionForType")
	public RetDataBean getQuerySupervisionForType() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionConfigService.getQuerySupervisionForType(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 督查督办积分
	 *
	 * @param pageParam 分页参数
	 * @return 积分列表
	 */
	@GetMapping(value = "/getSupervisionScoreList")
	public RetDataBean getSupervisionScoreList(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("c.create_time");
			} else {
				pageParam.setProp("c." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionScoreService.getSupervisionScoreList(pageParam);
			List<Map<String, String>> recordList = pageInfo.getList();
			List<Map<String, String>> resList = new ArrayList<>();
			for (Map<String, String> tmpMap : recordList) {
				String accountIds = tmpMap.get("joinUser");
				if (StringUtils.isNotBlank(accountIds)) {
					String[] paramsList = accountIds.split(",");
					List<String> list = Arrays.asList(paramsList);
					List<Map<String, String>> userListMap = userInfoService.getUserNamesByAccountIds(list, userInfo.getOrgId());
					List<String> userList = new ArrayList<>();
					for (Map<String, String> map : userListMap) {
						userList.add(map.get("userName"));
					}
					tmpMap.put("joinUserName", StringUtils.join(userList, ","));
				}
				resList.add(tmpMap);
			}
			pageInfo.setList(resList);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取问题回复
	 *
	 * @param recordId 督查事件Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMySupervisionAnswerList")
	public RetDataBean getMySupervisionAnswerList(String recordId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionProblemService.getMySupervisionAnswerList(userInfo.getOrgId(), userInfo.getAccountId(), recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取完成督查督办列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param eventType 事件类型
	 * @param handedUser 督查人
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionFinishList")
	public RetDataBean getSupervisionFinishList(PageParam pageParam, String date, String eventType, String handedUser) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("tmp.create_time");
			} else {
				pageParam.setProp("tmp." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionService.getSupervisionFinishList(pageParam, eventType, handedUser, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取问题列表
	 *
	 * @param processId 处理过程Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getMySupervisionProblemList")
	public RetDataBean getMySupervisionProblemList(String processId, Integer page) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionProblemService.getMySupervisionProblemList(userInfo.getOrgId(), processId, userInfo.getOpFlag(), userInfo.getAccountId(), page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	@GetMapping(value = "/getLeadSupervisionProblemList")
	public RetDataBean getLeadSupervisionProblemList(String processId, Integer page) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionProblemService.getLeadSupervisionProblemList(userInfo.getOrgId(), userInfo.getAccountId(), processId, page));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取任务处理结果
	 *
	 * @param processId 处理过程Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionResultList")
	public RetDataBean getSupervisionResultList(String processId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionResultService.getSupervisionResultList(userInfo.getOrgId(), processId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取我所管控的任务列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param supervisionId 督查事件Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getControlProcessList")
	public RetDataBean getControlProcessList(PageParam pageParam, String date, String supervisionId) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("tmp.create_time");
			} else {
				pageParam.setProp("tmp." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionProcessService.getControlProcessList(pageParam, date, timeArr[0], timeArr[1], supervisionId);
			List<Map<String, String>> recordList = pageInfo.getList();
			List<Map<String, String>> resList = new ArrayList<>();
			for (Map<String, String> tmpMap : recordList) {
				String accountIds = tmpMap.get("operator");
				if (StringUtils.isNotBlank(accountIds)) {
					String[] paramsList = accountIds.split(",");
					List<String> list = Arrays.asList(paramsList);
					List<Map<String, String>> userListMap = userInfoService.getUserNamesByAccountIds(list, userInfo.getOrgId());
					List<String> userList = new ArrayList<>();
					for (Map<String, String> map : userListMap) {
						userList.add(map.get("userName"));
					}
					tmpMap.put("operatorUserName", StringUtils.join(userList, ","));
				}
				resList.add(tmpMap);
			}
			pageInfo.setList(resList);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取督查列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionTree")
	public RetDataBean getSupervisionTree() {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionService.getSupervisionTree(userInfo.getOrgId()));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 获取延期审批列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getDelayApplyList")
	public RetDataBean getDelayApplyList(PageParam pageParam, String date, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("d.create_time");
			} else {
				pageParam.setProp("d." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOpFlag(userInfo.getOpFlag());
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			String[] timeArr = SysTools.convertDateToTime(date);
			PageInfo<Map<String, String>> pageInfo = supervisionDelayService.getDelayApplyList(pageParam, date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取我所管控的任务列表
	 *
	 * @param processId 处理过程Id
	 * @param keyword 查询关键词
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionProcessForSelect")
	public RetDataBean getSupervisionProcessForSelect(String processId, String keyword) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionProcessService.getSupervisionProcessForSelect(userInfo.getOrgId(), userInfo.getOpFlag(), userInfo.getAccountId(), processId, keyword));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	@GetMapping(value = "/getSupervisionProcessUserList")
	public RetDataBean getSupervisionProcessUserList(String recordId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionProblemService.getSupervisionProcessUserList(userInfo.getOrgId(), recordId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取转办给我的列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param eventType 事件类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionProcessForChangeList")
	public RetDataBean getSupervisionProcessForChangeList(PageParam pageParam, String date, String eventType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("tmp.create_time");
			} else {
				pageParam.setProp("tmp." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionProcessService.getSupervisionProcessForChangeList(pageParam, eventType, date, timeArr[0], timeArr[1]);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取待处理的督查列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param eventType 事件类型
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionProcessList")
	public RetDataBean getSupervisionProcessList(PageParam pageParam, String date, String eventType) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("tmp.create_time");
			} else {
				pageParam.setProp("tmp." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionProcessService.getSupervisionProcessList(pageParam, eventType, date, timeArr[0], timeArr[1]);
			List<Map<String, String>> recordList = pageInfo.getList();
			List<Map<String, String>> resList = new ArrayList<>();
			for (Map<String, String> tmpMap : recordList) {
				String accountIds = tmpMap.get("operator");
				if (StringUtils.isNotBlank(accountIds)) {
					String[] paramsList = accountIds.split(",");
					List<String> list = Arrays.asList(paramsList);
					List<Map<String, String>> userListMap = userInfoService.getUserNamesByAccountIds(list, userInfo.getOrgId());
					List<String> userList = new ArrayList<>();
					for (Map<String, String> map : userListMap) {
						userList.add(map.get("userName"));
					}
					tmpMap.put("operatorUserName", StringUtils.join(userList, ","));
				}
				resList.add(tmpMap);
			}
			pageInfo.setList(resList);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取我创建的督查事件
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param eventType 事件类型
	 * @param handedUser 督办人
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionList")
	public RetDataBean getSupervisionList(PageParam pageParam, String date, String eventType, String handedUser, String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("tmp.create_time");
			} else {
				pageParam.setProp("tmp." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionService.getSupervisionList(pageParam, eventType, handedUser, date, timeArr[0], timeArr[1], status);
			List<Map<String, String>> recordList = pageInfo.getList();
			List<Map<String, String>> resList = new ArrayList<>();
			for (Map<String, String> tmpMap : recordList) {
				String accountIds = tmpMap.get("joinUser");
				if (StringUtils.isNotBlank(accountIds)) {
					String[] paramsList = accountIds.split(",");
					List<String> list = Arrays.asList(paramsList);
					List<Map<String, String>> userListMap = userInfoService.getUserNamesByAccountIds(list, userInfo.getOrgId());
					List<String> userList = new ArrayList<>();
					for (Map<String, String> map : userListMap) {
						userList.add(map.get("userName"));
					}
					tmpMap.put("joinUserName", StringUtils.join(userList, ","));
				}
				resList.add(tmpMap);
			}
			pageInfo.setList(resList);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 获取督查督办事件列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param eventType 事件类型
	 * @param handedUser 督办人
	 * @return 消息结构
	 */
	@GetMapping(value = "/getQueryOldSupervisionList")
	public RetDataBean getQueryOldSupervisionList(PageParam pageParam, String date, String eventType, String handedUser, String status) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("tmp.create_time");
			} else {
				pageParam.setProp("tmp." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionService.getQueryOldSupervisionList(pageParam, eventType, handedUser, date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取当前用户管控的事件列表
	 *
	 * @param pageParam 分页参数
	 * @param date 日期范围
	 * @param eventType 事件类型
	 * @param handedUser 督办人
	 * @param status 状态
	 * @return 消息结构
	 */
	@GetMapping(value = "/getLeadManageSupervisionList")
	public RetDataBean getLeadManageSupervisionList(PageParam pageParam, String date, String eventType, String handedUser, String status) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("tmp.create_time");
			} else {
				pageParam.setProp("tmp." + StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			String[] timeArr = SysTools.convertDateToTime(date);
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setAccountId(userInfo.getAccountId());
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			PageInfo<Map<String, String>> pageInfo = supervisionService.getLeadManageSupervisionList(pageParam, eventType, handedUser, date, timeArr[0], timeArr[1], status);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取事件参与人列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionHandedUserForSelect")
	public RetDataBean getSupervisionHandedUserForSelect(Supervision supervision) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:query")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			supervision.setOrgId(userInfo.getOrgId());
			supervision = supervisionService.selectOneSupervision(supervision);
			String[] paramsList = supervision.getJoinUser().split(",");
			List<String> list = Arrays.asList(paramsList);
			List<Map<String, String>> userListMap = userInfoService.getUserListByAccountIds(userInfo.getOrgId(), list);
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userListMap);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取督查类型列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionConfigForSelect")
	public RetDataBean getSupervisionConfigForSelect() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionConfigService.getSupervisionConfigForSelect(userInfo.getOrgId()));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取积分规则
	 *
	 * @param supervisionIntegral 积分规则对象
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionIntegral")
	public RetDataBean getSupervisionIntegral(SupervisionIntegral supervisionIntegral) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionIntegral.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionIntegralService.selectOneSupervisionIntegral(supervisionIntegral));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取类型列表
	 *
	 * @param pageParam 分页参数
	 * @return 消息结构
	 */
	@GetMapping(value = "/getAllSupervisionConfig")
	public RetDataBean getAllSupervisionConfig(PageParam pageParam) {
		try {
			if (StringUtils.isBlank(pageParam.getProp())) {
				pageParam.setProp("sort_no");
			} else {
				pageParam.setProp(StrTools.lowerCharToUnderLine(pageParam.getProp()));
			}
			if (StringUtils.isBlank(pageParam.getOrder())) {
				pageParam.setSortOrder("desc");
			} else {
				if (pageParam.getOrder().equals("descending")) {
					pageParam.setSortOrder("desc");
				} else if (pageParam.getOrder().equals("ascending")) {
					pageParam.setSortOrder("asc");
				}
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			pageParam.setOrgId(userInfo.getOrgId());
			pageParam.setOrderBy(pageParam.getProp() + " " + pageParam.getSortOrder());
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionConfigService.getAllSupervisionConfig(pageParam));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 获取督查领导列表
	 *
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionLeadList")
	public RetDataBean getSupervisionLeadList() {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SupervisionConfig.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId());
			List<SupervisionConfig> resList = supervisionConfigService.getAllConfig(example);
			List<String> list = new ArrayList<>();
			for (SupervisionConfig supervisionConfig : resList) {
				list.add(supervisionConfig.getLeadId());
			}
			if (!list.isEmpty()) {
				return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userInfoService.getAllUserByAccountList(userInfo.getOrgId(), list));
			} else {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR, new JSONArray());
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 督查督办历史流程
	 * @param processId 处理过程Id
	 * @return 消息结构
	 */
	@GetMapping(value = "/getSupervisionBpmRecordList")
	public RetDataBean getSupervisionBpmRecordList(String processId) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, supervisionBpmListService.getSupervisionBpmRecordList(userInfo.getOrgId(),userInfo.getOpFlag(),userInfo.getAccountId(),processId));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

}
