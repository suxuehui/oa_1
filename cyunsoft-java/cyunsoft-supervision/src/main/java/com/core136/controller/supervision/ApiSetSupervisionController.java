package com.core136.controller.supervision;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UserInfo;
import com.core136.bean.bpm.BpmList;
import com.core136.bean.bpm.BpmStepRun;
import com.core136.bean.supervision.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.UserInfoService;
import com.core136.service.bpm.BpmFormDataService;
import com.core136.service.supervision.*;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/set/supervision")
@CrossOrigin
@Api(value="督查督办SetController",tags={"督查督办更新数据接口"})
public class ApiSetSupervisionController {
	private UserInfoService userInfoService;
	private SupervisionIntegralService supervisionIntegralService;
	private SupervisionConfigService supervisionConfigService;
	private SupervisionService supervisionService;
	private SupervisionScoreService supervisionScoreService;
	private SupervisionProcessService supervisionProcessService;
	private SupervisionDelayService supervisionDelayService;
	private SupervisionResultService supervisionResultService;
	private SupervisionProblemService supervisionProblemService;
	private SupervisionBpmListService supervisionBpmListService;
	private BpmFormDataService bpmFormDataService;
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setSupervisionIntegralService(SupervisionIntegralService supervisionIntegralService) {
		this.supervisionIntegralService = supervisionIntegralService;
	}
	@Autowired
	public void setSupervisionConfigService(SupervisionConfigService supervisionConfigService) {
		this.supervisionConfigService = supervisionConfigService;
	}
	@Autowired
	public void setSupervisionService(SupervisionService supervisionService) {
		this.supervisionService = supervisionService;
	}
	@Autowired
	public void setSupervisionScoreService(SupervisionScoreService supervisionScoreService) {
		this.supervisionScoreService = supervisionScoreService;
	}
	@Autowired
	public void setSupervisionProcessService(SupervisionProcessService supervisionProcessService) {
		this.supervisionProcessService = supervisionProcessService;
	}
	@Autowired
	public void setSupervisionDelayService(SupervisionDelayService supervisionDelayService) {
		this.supervisionDelayService = supervisionDelayService;
	}
	@Autowired
	public void setSupervisionResultService(SupervisionResultService supervisionResultService) {
		this.supervisionResultService = supervisionResultService;
	}
	@Autowired
	public void setSupervisionProblemService(SupervisionProblemService supervisionProblemService) {
		this.supervisionProblemService = supervisionProblemService;
	}
	@Autowired
	public void setSupervisionBpmListService(SupervisionBpmListService supervisionBpmListService) {
		this.supervisionBpmListService = supervisionBpmListService;
	}
	@Autowired
	public void setBpmFormDataService(BpmFormDataService bpmFormDataService) {
		this.bpmFormDataService = bpmFormDataService;
	}

	/**
	 * 发起任务处理流程
	 * @param supervisionProcess 任务处理过程
	 * @return 消息结构
	 */
	@PostMapping(value = "/createBpmBySupervisionRule")
	public RetDataBean createBpmBySupervisionRule(SupervisionProcess supervisionProcess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionProcess.setOrgId(userInfo.getOrgId());
			supervisionProcess = supervisionProcessService.selectOneSupervisionProcess(supervisionProcess);
			Supervision supervision = new Supervision();
			supervision.setSupervisionId(supervisionProcess.getSupervisionId());
			supervision.setOrgId(userInfo.getOrgId());
			supervision = supervisionService.selectOneSupervision(supervision);
			SupervisionConfig supervisionConfig = new SupervisionConfig();
			if(StringUtils.isNotBlank(supervision.getEventType()))
			{
				supervisionConfig.setConfigId(supervision.getEventType());
				supervisionConfig.setOrgId(userInfo.getOrgId());
				supervisionConfig = supervisionConfigService.selectOneSupervisionConfig(supervisionConfig);
			}
			SupervisionBpmList supervisionBpmList = new SupervisionBpmList();
			supervisionBpmList.setRecordId(SysTools.getGUID());
			supervisionBpmList.setProcessId(supervisionProcess.getProcessId());
			supervisionBpmList.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			supervisionBpmList.setCreateUser(userInfo.getAccountId());
			supervisionBpmList.setOrgId(userInfo.getOrgId());
			BpmList bpmList = new BpmList();
			bpmList.setFlowTitle("《"+supervisionProcess.getTitle()+"》的任务处理");
			bpmList.setUrgency("0");
			bpmList.setFlowId(supervisionConfig.getFlowId());
			JSONObject formDataJson = new JSONObject();
			if(StringUtils.isNotBlank(supervisionConfig.getDataMap())) {
				String[] dataMapList = supervisionConfig.getDataMap().split(",");
				JSONObject vJson = (JSONObject) JSON.toJSON(supervisionProcess);
				for (String vkStr : dataMapList) {
					String[] vkArr = vkStr.split(":");
					formDataJson.put(vkArr[1], vJson.get(vkArr[0]));
				}
			}
			RetDataBean retdataBean = bpmFormDataService.createBpmFormData(userInfo, bpmList, new BpmStepRun(), formDataJson);
			JSONObject resJson = JSON.parseObject(retdataBean.getData().toString());
			supervisionBpmList.setRunId(resJson.getString("runId"));
			supervisionBpmList.setFlowId(supervisionConfig.getFlowId());
			supervisionBpmListService.insertSupervisionBpmList(supervisionBpmList);
			return retdataBean;
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建评分
	 * @param supervisionScore 督查督办评分对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertSupervisionScore")
	public RetDataBean insertSupervisionScore(SupervisionScore supervisionScore) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionScore.setScoreId(SysTools.getGUID());
			if (supervisionScore.getUserScore() > 100) {
				supervisionScore.setUserScore(100.00);
			}
			supervisionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			supervisionScore.setCreateUser(userInfo.getAccountId());
			supervisionScore.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionScoreService.insertSupervisionScore(supervisionScore));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 评分
	 * @param supervisionScore 督查督办评分对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/setSupervisionScore")
	public RetDataBean setSupervisionScore(SupervisionScore supervisionScore) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (supervisionScore.getUserScore() > 100) {
				supervisionScore.setUserScore(100.00);
			}
			return supervisionScoreService.setSupervisionScore(userInfo, supervisionScore);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除评分
	 * @param supervisionScore 督查督办评分对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionScore")
	public RetDataBean deleteSupervisionScore(SupervisionScore supervisionScore) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionScore.getScoreId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervisionScore.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionScoreService.deleteSupervisionScore(supervisionScore));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新评分
	 * @param supervisionScore 督查督办评分对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervisionScore")
	public RetDataBean updateSupervisionScore(SupervisionScore supervisionScore) {
		try {
			if (StringUtils.isBlank(supervisionScore.getScoreId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SupervisionScore.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("scoreId", supervisionScore.getScoreId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionScoreService.updateSupervisionScore(example, supervisionScore));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 反馈问题
	 *
	 * @param supervisionProblem 问题对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertSupervisionProblem")
	public RetDataBean insertSupervisionProblem(SupervisionProblem supervisionProblem) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionProblem.setRecordId(SysTools.getGUID());
			if (StringUtils.isBlank(supervisionProblem.getParentId())) {
				supervisionProblem.setParentId("0");
			}
			supervisionProblem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			supervisionProblem.setCreateUser(userInfo.getAccountId());
			supervisionProblem.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionProblemService.insertSupervisionProblem(supervisionProblem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除反馈问题
	 *
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionProblem")
	public RetDataBean deleteSupervisionProblem(SupervisionProblem supervisionProblem) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionProblem.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervisionProblem.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionProblemService.deleteSupervisionProblem(supervisionProblem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新反馈问题
	 *
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervisionProblem")
	public RetDataBean updateSupervisionProblem(SupervisionProblem supervisionProblem) {
		try {
			if (StringUtils.isBlank(supervisionProblem.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SupervisionProblem.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", supervisionProblem.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionProblemService.updateSupervisionProblem(example, supervisionProblem));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}
	/**
	 * 添加任务处理结果
	 *
	 * @param supervisionResult 任务处理结果对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	@PostMapping(value = "/insertSupervisionResult")
	public RetDataBean insertSupervisionResult(SupervisionResult supervisionResult) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionResult.setRecordId(SysTools.getGUID());
			supervisionResult.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			if (supervisionResult.getProgress() == null) {
				supervisionResult.setProgress(0.0);
			}
			SupervisionProcess supervisionProcess = new SupervisionProcess();
			if(supervisionResult.getProgress()>=1)
			{
				supervisionProcess.setStatus("2");
				supervisionProcess.setFinishTime(supervisionResult.getFinishTime());
			}
			supervisionProcess.setProgress(supervisionResult.getProgress());
			supervisionProcess.setFinishTime(supervisionResult.getFinishTime());
			Example example = new Example(SupervisionProcess.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", supervisionResult.getProcessId());
			supervisionProcessService.updateSupervisionProcess(example, supervisionProcess);
			supervisionResult.setCreateUser(userInfo.getAccountId());
			supervisionResult.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, supervisionResultService.insertSupervisionResult(supervisionResult));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 删除任务处理结果
	 *
	 * @param supervisionResult 任务处理结果对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionResult")
	public RetDataBean deleteSupervisionResult(SupervisionResult supervisionResult) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionResult.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervisionResult.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionResultService.deleteSupervisionResult(supervisionResult));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新任务处理结果
	 *
	 * @param supervisionResult 任务处理结果对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervisionResult")
	public RetDataBean updateSupervisionResult(SupervisionResult supervisionResult) {
		try {
			if (StringUtils.isBlank(supervisionResult.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			if (supervisionResult.getProgress() == null) {
				supervisionResult.setProgress(0.0);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SupervisionResult.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("recordId", supervisionResult.getRecordId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionResultService.updateSupervisionResult(example, supervisionResult));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 发起延期审批申请
	 *
	 * @param supervisionDelay 期审批申请对象
	 * @param supervisionId 督查督办记录Id
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertSupervisionDelay")
	public RetDataBean insertSupervisionDelay(SupervisionDelay supervisionDelay, String supervisionId) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Supervision supervision = new Supervision();
			supervision.setOrgId(userInfo.getOrgId());
			supervision.setSupervisionId(supervisionId);
			supervision = supervisionService.selectOneSupervision(supervision);
			supervisionDelay.setDelayId(SysTools.getGUID());
			supervisionDelay.setLeadId(supervision.getLeadId());
			supervisionDelay.setPassStatus("0");
			supervisionDelay.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			supervisionDelay.setCreateUser(userInfo.getAccountId());
			supervisionDelay.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, supervisionDelayService.insertSupervisionDelay(supervisionDelay));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除延期申请记录
	 *
	 * @param supervisionDelay 期审批申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionDelay")
	public RetDataBean deleteSupervisionDelay(SupervisionDelay supervisionDelay) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionDelay.getDelayId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervisionDelay.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionDelayService.deleteSupervisionDelay(supervisionDelay));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 延期更改
	 *
	 * @param supervisionDelay 期审批申请对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervisionDelay")
	public RetDataBean updateSupervisionDelay(SupervisionDelay supervisionDelay) {
		try {
			if (StringUtils.isBlank(supervisionDelay.getDelayId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionDelay.setApplyTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			Example example = new Example(SupervisionDelay.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("delayId", supervisionDelay.getDelayId());
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionDelayService.updateSupervisionDelay(example, supervisionDelay));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 延期审批
	 *
	 * @param supervisionDelay 期审批申请对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	@PostMapping(value = "/updateSupervisionDelayApply")
	public RetDataBean updateSupervisionDelayApply(SupervisionDelay supervisionDelay) {
		try {
			if (StringUtils.isBlank(supervisionDelay.getDelayId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (supervisionDelay.getPassStatus().equals("1")) {
				SupervisionDelay supervisionDelay2 = new SupervisionDelay();
				supervisionDelay2.setOrgId(userInfo.getOrgId());
				supervisionDelay2.setDelayId(supervisionDelay.getDelayId());
				supervisionDelay2 = supervisionDelayService.selectOneSupervisionDelay(supervisionDelay2);
				SupervisionProcess supervisionProcess = new SupervisionProcess();
				supervisionProcess.setEndTime(supervisionDelay2.getDelayTime());
				supervisionProcess.setStatus("1");
				Example example1 = new Example(SupervisionProcess.class);
				example1.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", supervisionDelay2.getProcessId());
				supervisionProcessService.updateSupervisionProcess(example1, supervisionProcess);
				//处理积分
				SupervisionIntegral supervisionIntegral = new SupervisionIntegral();
				supervisionIntegral.setOrgId(userInfo.getOrgId());
				supervisionIntegral = supervisionIntegralService.selectOneSupervisionIntegral(supervisionIntegral);
				if (supervisionIntegral != null) {
					SupervisionProcess tmpSupervisionProcess = new SupervisionProcess();
					tmpSupervisionProcess.setProcessId(supervisionDelay2.getProcessId());
					tmpSupervisionProcess.setOrgId(userInfo.getOrgId());
					tmpSupervisionProcess = supervisionProcessService.selectOneSupervisionProcess(tmpSupervisionProcess);
					String time1 = tmpSupervisionProcess.getEndTime();
					String time2 = supervisionDelay.getDelayTime();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date date1 = sdf.parse(time1);
					Date date2 = sdf.parse(time2);
					long between_days = (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24);
					DecimalFormat df = new DecimalFormat("#.##");
					double resDouble = Double.parseDouble(df.format(between_days));
					if (resDouble > 0) {
						supervisionScoreService.setDelayScore(userInfo, tmpSupervisionProcess.getSupervisionId(), supervisionIntegral, resDouble / supervisionIntegral.getDelay());
					}
				}
			}
			supervisionDelay.setApplyTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			Example example = new Example(SupervisionDelay.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("delayId", supervisionDelay.getDelayId());
			return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, supervisionDelayService.updateSupervisionDelay(example, supervisionDelay));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


	/**
	 * 任务转办
	 *
	 * @param processId 任务处理记录Id
	 * @param changeUser 管控人
	 * @return 消息结构
	 */
	@PostMapping(value = "/setChangeUserByProcess")
	public RetDataBean setChangeUserByProcess(String processId, String changeUser) {
		try {
			if (StringUtils.isBlank(changeUser) || StringUtils.isBlank(processId)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			SupervisionProcess supervisionProcess = new SupervisionProcess();
			supervisionProcess.setChangeUser(changeUser);
			supervisionProcess.setChangeStatus("0");
			supervisionProcess.setChangeTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			Example example = new Example(SupervisionProcess.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", processId);
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionProcessService.updateSupervisionProcess(example, supervisionProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 任务分配成功
	 *
	 * @param supervisionProcess 督查任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertSupervisionProcess")
	public RetDataBean insertSupervisionProcess(SupervisionProcess supervisionProcess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionProcess.setProcessId(SysTools.getGUID());
			supervisionProcess.setStatus("0");
			supervisionProcess.setScore(0);
			supervisionProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			supervisionProcess.setCreateUser(userInfo.getAccountId());
			supervisionProcess.setOrgId(userInfo.getOrgId());
			supervisionProcess.setChangeStatus("0");
			return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionProcessService.insertSupervisionProcess(supervisionProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 任务删除成功
	 *
	 * @param supervisionProcess 督查任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionProcess")
	public RetDataBean deleteSupervisionProcess(SupervisionProcess supervisionProcess) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervisionProcess.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionProcessService.deleteSupervisionProcess(supervisionProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 任务更新成功
	 *
	 * @param supervisionProcess 督查任务对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervisionProcess")
	public RetDataBean updateSupervisionProcess(SupervisionProcess supervisionProcess) {
		try {
			if (StringUtils.isBlank(supervisionProcess.getProcessId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SupervisionProcess.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", supervisionProcess.getProcessId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionProcessService.updateSupervisionProcess(example, supervisionProcess));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 接收转办任务
	 *
	 * @param processId 任务记录Id
	 * @param passStatus 转办状态
	 * @return 消息结构
	 */
	@PostMapping(value = "/revChangeUserProcess")
	public RetDataBean revChangeUserProcess(String processId, String passStatus) {
		try {
			if (StringUtils.isBlank(processId) || StringUtils.isBlank(passStatus)) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			SupervisionProcess supervisionProcess = new SupervisionProcess();
			if (passStatus.equals("1")) {
				supervisionProcess.setOrgId(userInfo.getOrgId());
				supervisionProcess.setProcessId(processId);
				supervisionProcess = supervisionProcessService.selectOneSupervisionProcess(supervisionProcess);
				supervisionProcess.setHolder(supervisionProcess.getChangeUser());
				supervisionProcess.setChangeStatus("1");
				Example example = new Example(SupervisionProcess.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", processId);
				return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionProcessService.updateSupervisionProcess(example, supervisionProcess));
			} else {
				supervisionProcess.setChangeStatus("2");
				Example example = new Example(SupervisionProcess.class);
				example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("processId", processId);
				return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionProcessService.updateSupervisionProcess(example, supervisionProcess));
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置督查督办为完成状态
	 *
	 * @param supervision 督查督办对象
	 * @return 消息结构
	 */
	@Transactional(value = "generalTM")
	@PostMapping(value = "/setFinishStatus")
	public RetDataBean setFinishStatus(Supervision supervision) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(supervision.getSupervisionId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervision.setStatus("2");
			UserInfo userInfo = userInfoService.getRedisUser();
			//处理积分
			SupervisionIntegral supervisionIntegral = new SupervisionIntegral();
			supervisionIntegral.setOrgId(userInfo.getOrgId());
			supervisionIntegral = supervisionIntegralService.selectOneSupervisionIntegral(supervisionIntegral);
			if (supervisionIntegral != null) {
				Supervision tmpSupervision = new Supervision();
				tmpSupervision.setSupervisionId(supervision.getSupervisionId());
				tmpSupervision.setOrgId(userInfo.getOrgId());
				tmpSupervision = supervisionService.selectOneSupervision(tmpSupervision);
				String time1 = tmpSupervision.getEndTime();
				String time2 = supervision.getFinishTime();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date1 = sdf.parse(time1);
				Date date2 = sdf.parse(time2);
				long between_days = (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24);
				DecimalFormat df = new DecimalFormat("#.##");
				double resDouble = Double.parseDouble(df.format(between_days));
				if (resDouble > 0) {
					supervisionScoreService.setPassTimeScore(userInfo, supervision.getSupervisionId(), supervisionIntegral, resDouble / supervisionIntegral.getPassTime());
				}
			}
			Example example = new Example(Supervision.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("supervisionId", supervision.getSupervisionId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionService.updateSupervision(example, supervision));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建督查项目
	 *
	 * @param supervision 督查督办对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertSupervision")
	public RetDataBean insertSupervision(Supervision supervision) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:insert")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			supervision.setSupervisionId(SysTools.getGUID());
			supervision.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			supervision.setStatus("0");
			supervision.setCreateUser(userInfo.getAccountId());
			supervision.setOrgId(userInfo.getOrgId());
			Document htmlDoc = Jsoup.parse(supervision.getRemark());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			supervision.setSubheading(subheading);
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, supervisionService.createSupervision(userInfo, supervision));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除督查项目
	 *
	 * @param supervision 督查督办对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervision")
	public RetDataBean deleteSupervision(Supervision supervision) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			if (StringUtils.isBlank(supervision.getSupervisionId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			supervision.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionService.deleteSupervision(supervision));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 批量删除督查事件
	 *
	 * @param ids 督查督办对象Id 数组
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionByIds")
	public RetDataBean deleteSupervisionByIds(@RequestParam(value = "ids[]") String[] ids) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:delete")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			if (!userInfo.getOpFlag().equals("1")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
			} else {
				List<String> idsList = Arrays.asList(ids);
				return supervisionService.deleteSupervisionByIds(userInfo.getOrgId(), idsList);
			}
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新督查项目
	 *
	 * @param supervision 督查督办对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervision")
	public RetDataBean updateSupervision(Supervision supervision) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(supervision.getSupervisionId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			Document htmlDoc = Jsoup.parse(supervision.getRemark());
			String subheading = htmlDoc.text();
			if (subheading.length() > 50) {
				subheading = subheading.substring(0, 50) + "...";
			}
			supervision.setSubheading(subheading);
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(Supervision.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("supervisionId", supervision.getSupervisionId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionService.updateSupervision(userInfo, example, supervision));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新督查督办状态
	 *
	 * @param supervision 督查督办对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervisionStatus")
	public RetDataBean updateSupervisionStatus(Supervision supervision) {
		try {
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("supervision:update")) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
			}
			if (StringUtils.isBlank(supervision.getSupervisionId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(Supervision.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("supervisionId", supervision.getSupervisionId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionService.updateSupervision(example, supervision));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 设置积分规则
	 *
	 * @param supervisionIntegral 督查督办积分规则
	 * @return 消息结构
	 */
	@PostMapping(value = "/setSupervisionIntegral")
	public RetDataBean setSupervisionIntegral(SupervisionIntegral supervisionIntegral) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			return supervisionIntegralService.setIntegralRule(userInfo, supervisionIntegral);
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 创建类型
	 *
	 * @param supervisionConfig 督查督办配置
	 * @return 消息结构
	 */
	@PostMapping(value = "/insertSupervisionConfig")
	public RetDataBean insertSupervisionConfig(SupervisionConfig supervisionConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			supervisionConfig.setConfigId(SysTools.getGUID());
			supervisionConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
			supervisionConfig.setCreateUser(userInfo.getAccountId());
			supervisionConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, supervisionConfigService.insertSupervisionConfig(supervisionConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除类型
	 *
	 * @param supervisionConfig 督查督办配置
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionConfig")
	public RetDataBean deleteSupervisionConfig(SupervisionConfig supervisionConfig) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionConfig.getConfigId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervisionConfig.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionConfigService.deleteSupervisionConfig(supervisionConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 更新分类
	 *
	 * @param supervisionConfig 督查督办配置
	 * @return 消息结构
	 */
	@PostMapping(value = "/updateSupervisionConfig")
	public RetDataBean updateSupervisionConfig(SupervisionConfig supervisionConfig) {
		try {
			if (StringUtils.isBlank(supervisionConfig.getConfigId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			UserInfo userInfo = userInfoService.getRedisUser();
			Example example = new Example(SupervisionConfig.class);
			example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("configId", supervisionConfig.getConfigId());
			return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, supervisionConfigService.updateSupervisionConfig(example, supervisionConfig));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}

	/**
	 * 删除任务处理流程
	 * @param supervisionBpmList 任务处理流程对象
	 * @return 消息结构
	 */
	@PostMapping(value = "/deleteSupervisionBpmList")
	public RetDataBean deleteSupervisionBpmList(SupervisionBpmList supervisionBpmList) {
		try {
			UserInfo userInfo = userInfoService.getRedisUser();
			if (StringUtils.isBlank(supervisionBpmList.getRecordId())) {
				return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
			}
			supervisionBpmList.setOrgId(userInfo.getOrgId());
			return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, supervisionBpmListService.deleteSupervisionBpmList(supervisionBpmList));
		} catch (Exception e) {
			return RetDataTools.Error(e.getMessage());
		}
	}


}
