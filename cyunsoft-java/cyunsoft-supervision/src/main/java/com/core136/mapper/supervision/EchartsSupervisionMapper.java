package com.core136.mapper.supervision;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EchartsSupervisionMapper {


	/**
	 * 前10位领导的人员工作量占比
	 * @param orgId 机构码
	 * @return 人员工作量占比
	 */
	List<Map<String, String>> getBiSupervisionByLeadPie(@Param(value = "orgId") String orgId);

	/**
	 * 获取督查督办分类前10的占比
	 * @param orgId 机构码
	 * @return 督查督办分类前10的占比
	 */
	List<Map<String, String>> getBiSupervisionTypePie(@Param(value = "orgId") String orgId);


	/**
	 * 获取督查督办当前状态总数
	 * @param orgId 机构码
	 * @return 督查督办当前状态总数
	 */
	List<Map<String, String>> getBiSupervisionStatusTypePie(@Param(value = "orgId") String orgId);

	/**
	 * 按月份统计工作量
	 * @param orgId 机构码
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 月份统计工作量
	 */
	List<Map<String, Object>> getBiSupervisionByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

}
