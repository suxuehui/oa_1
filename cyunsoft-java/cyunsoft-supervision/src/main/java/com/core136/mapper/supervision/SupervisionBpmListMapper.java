package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionBpmList;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SupervisionBpmListMapper extends MyMapper<SupervisionBpmList> {
	/**
	 * 督查督办历史流程
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param processId 处理过程Id
	 * @return 督办历史流程
	 */
	List<Map<String, String>> getSupervisionBpmRecordList(@Param(value = "orgId") String orgId,@Param(value = "opFlag") String opFlag,
														  @Param(value="accountId")String accountId, @Param(value = "processId") String processId);

}
