package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionDelay;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface SupervisionDelayMapper extends MyMapper<SupervisionDelay> {
	/**
	 * 获取延期审批列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 延期审批列表
	 */
    List<Map<String, String>> getDelayApplyList(
            @Param(value = "orgId") String orgId,
			@Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
			@Param(value = "dateQueryType") String dateQueryType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
			@Param(value = "status") String status,
            @Param(value = "keyword") String keyword
    );



}
