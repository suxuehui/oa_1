package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionIntegral;
import com.core136.common.dbutils.MyMapper;

public interface SupervisionIntegralMapper extends MyMapper<SupervisionIntegral> {
}
