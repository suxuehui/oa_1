package com.core136.mapper.supervision;

import com.core136.bean.supervision.Supervision;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface SupervisionMapper extends MyMapper<Supervision> {
	/**
	 * 获取督查督办事件详情
	 * @param orgId 机构码
	 * @param supervisionId 督查督办事件Id
	 * @return 督查督办事件详情
	 */
	Map<String, String>getSupervisionById(@Param(value="orgId")String orgId,@Param(value="supervisionId")String supervisionId);

	List<Map<String, String>> getSupervisionListByType(@Param(value="orgId")String orgId,@Param(value="eventType")String eventType);
	/**
	 * 我的历史记录
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param eventType 事件类型
	 * @param handedUser 管控人
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 历史记录
	 */
	List<Map<String, String>> getSupervisionList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
												 @Param(value = "eventType") String eventType, @Param(value = "handedUser") String handedUser,
												 @Param(value = "dateQueryType") String dateQueryType, @Param(value = "beginTime") String beginTime,
												 @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "keyword") String keyword);

    List<Map<String, String>> getSupervisionForTree(@Param(value = "orgId") String orgId, @Param(value = "eventType") String eventType);


    /**
     * 获取完成督查督办列表
     * @param orgId 机构码
	 * @param eventType 事件类型
	 * @param handedUser 管控人
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 完成督查督办列表
     */
    List<Map<String, String>> getSupervisionFinishList(@Param(value = "orgId") String orgId, @Param(value = "eventType") String eventType, @Param(value = "handedUser") String handedUser,
													    @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                         @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


    /**
     * 获取督查督办事件列表
     * @param orgId 机构码
     * @param eventType 事件类型
     * @param handedUser 管控人
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 督查督办事件列表
     */
    List<Map<String, String>> getQueryOldSupervisionList(@Param(value = "orgId") String orgId, @Param(value = "eventType") String eventType, @Param(value = "handedUser") String handedUser,
														 	@Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                           @Param(value = "endTime") String endTime, @Param(value = "status") String status,@Param(value = "keyword") String keyword);

	/**
	 * 获取当前用户管控的事件列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param eventType 事件类型
	 * @param handedUser 管控人
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param status 状态
	 * @param keyword 查询关键词
	 * @return 当前用户管控的事件列表
	 */
	List<Map<String, String>> getLeadManageSupervisionList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
														   @Param(value = "eventType") String eventType, @Param(value = "handedUser") String handedUser,
														   @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
														   @Param(value = "endTime") String endTime, @Param(value = "status") String status,@Param(value = "keyword") String keyword);


	List<Map<String, String>> getManageSupervisionListForDesk(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

    /**
     * 移动端督查督办列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param page 页码
     * @return 端督查督办列表
     */
    List<Map<String, String>> getMobileMySupervisionList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "page") Integer page);

    /**
     * 获取所有督查事件
     * @param orgId 机构码
     * @return 所有督查事件
     */
    List<Map<String, String>> getAllSupervisionForDesk(@Param(value = "orgId") String orgId);

	/**
	 * 获取桌面模块督查督办事件列表
	 * @param orgId 机构码
	 * @return 督查督办事件列表
	 */
    List<Map<String, String>> getAllFinishSupervisionForDesk(@Param(value = "orgId") String orgId);
}


