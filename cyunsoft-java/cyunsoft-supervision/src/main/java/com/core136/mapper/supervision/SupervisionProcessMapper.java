package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionProcess;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface SupervisionProcessMapper extends MyMapper<SupervisionProcess> {
	/**
	 * 获取我所管控的任务列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param keyword 查询关键词
	 * @return 管控的任务列表
	 */
	List<Map<String, String>>getSupervisionProcessForSelect(@Param(value = "orgId") String orgId,@Param(value = "opFlag") String opFlag,
															@Param(value = "accountId") String accountId,@Param(value = "processId") String processId,@Param(value = "keyword") String keyword);
	/**
	 * 获取我所管控的任务列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param supervisionId 督查督办Id
	 * @param keyword 查询关键词
	 * @return 我所管控的任务列表
	 */
    List<Map<String, String>> getControlProcessList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
													@Param(value = "accountId") String accountId,@Param(value="dateQueryType")String dateQueryType,
													@Param(value = "beginTime") String beginTime,@Param(value = "endTime") String endTime,
													@Param(value = "supervisionId") String supervisionId, @Param(value = "keyword") String keyword);

	/**
	 *
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return
	 */
    List<Map<String, String>> getSupervisionProcessListForSelect(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

	/**
	 * 获取事件处理过程列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param eventType 事件类型
	 * @param keyword 查询关键词
	 * @return 事件处理过程列表
	 */
    List<Map<String, String>> getOldProcessList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime,
                                                @Param(value = "endTime") String endTime, @Param(value = "eventType") String eventType, @Param(value = "keyword") String keyword);


	/**
	 * 获取待处理的督查列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param eventType 事件类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 待处理的督查列表
	 */
    List<Map<String, String>> getSupervisionProcessList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                          @Param(value = "eventType") String eventType, @Param(value = "dateQueryType") String dateQueryType,@Param(value = "beginTime") String beginTime,
                                                          @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


    /**
     * 获取转办给我的列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param eventType 事件类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 转办给我的列表
     */
    List<Map<String, String>> getSupervisionProcessForChangeList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                                   @Param(value = "eventType") String eventType,@Param(value = "dateQueryType") String dateQueryType, @Param(value = "beginTime") String beginTime,
                                                                   @Param(value = "endTime") String endTime, @Param(value = "keyword") String keyword);


	/**
	 * 按部门汇总
	 * @param orgId 机构码
	 * @return 部门汇总列表
	 */
	List<Map<String, Object>> getQuerySupervisionForDept(@Param(value = "orgId") String orgId);


	/**
	 * 获取督查督办待办事件
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 督查督办待办事件
	 */
    List<Map<String, String>> getSupervisionProcessListForDesk(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

    /**
     * 获取延时任务列表
     * @param orgId 机构码
     * @return 延时任务列表
     */
    List<Map<String, String>> getAllSupervisionProcessForDesk(@Param(value = "orgId") String orgId);

}
