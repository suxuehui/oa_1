package com.core136.mapper.supervision;

import com.core136.bean.supervision.SupervisionScore;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public interface SupervisionScoreMapper extends MyMapper<SupervisionScore> {
	/**
	 * 获取督查事件积分列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 督查事件积分列表
	 */
    List<Map<String, String>> getSupervisionScoreList(@Param(value = "orgId") String orgId, @Param(value = "keyword") String keyword);

}
