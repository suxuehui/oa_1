package com.core136.service.supervision;


import com.core136.bean.account.UserInfo;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.bi.option.property.OptionSeries;
import com.core136.bi.option.property.OptionTitle;
import com.core136.bi.option.resdata.LegendData;
import com.core136.bi.option.resdata.SeriesData;
import com.core136.bi.option.style.Emphasis;
import com.core136.bi.option.style.ItemStyle;
import com.core136.bi.option.units.LineOption;
import com.core136.bi.option.units.PieOption;
import com.core136.mapper.supervision.EchartsSupervisionMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EchartsSupervisionService {
    private final PieOption pieOption = new PieOption();
    private final LineOption lineOption = new LineOption();
    private EchartsSupervisionMapper echartsSupervisionMapper;
	@Autowired
	public void setEchartsSupervisionMapper(EchartsSupervisionMapper echartsSupervisionMapper) {
		this.echartsSupervisionMapper = echartsSupervisionMapper;
	}

	/**
	 * 按月份统计工作量
	 * @param user 用户对象
	 * @return Echart数据结构
	 */
	public OptionConfig getBiSupervisionByMonthLine(UserInfo user) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date y = c.getTime();
        String endTime = format.format(y);
        c.add(Calendar.YEAR, -1);
        y = c.getTime();
        String beginTime = format.format(y);
        List<Map<String, Object>> resList = echartsSupervisionMapper.getBiSupervisionByMonthLine(user.getOrgId(), beginTime, endTime);
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        return lineOption.getBasicLineChartOption(xAxisData, resData);
    }

	/**
	 * 获取督查督办当前状态总数
	 * @param user 用户对象
	 * @return Echart数据结构
	 */
	public OptionConfig getBiSupervisionStatusTypePie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = echartsSupervisionMapper.getBiSupervisionStatusTypePie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLength = Math.min(dataArr.length, 10);
		String[] selected = new String[selectedLength];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLength) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("督查督办状态");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("督查督办状态总数统计");
        optionTitle.setSubtext("状态总数占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }


	/**
	 * 获取BPM使用分类前10的占比
	 * @param user 用户对象
	 * @return selectedLength
	 */
	public OptionConfig getBiSupervisionTypePie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = echartsSupervisionMapper.getBiSupervisionTypePie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLength = Math.min(dataArr.length, 10);
		String[] selected = new String[selectedLength];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLength) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("督查督办分类");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("督查督办分类总数统计");
        optionTitle.setSubtext("分类总数占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

	/**
	 * 前10位领导的人员工作量占比
	 * @param user 用户对象
	 * @return Echart数据结构
	 */
	public OptionConfig getBiSupervisionByLeadPie(UserInfo user) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = echartsSupervisionMapper.getBiSupervisionByLeadPie(user.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLength = Math.min(dataArr.length, 10);
		String[] selected = new String[selectedLength];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLength) {
                selected[i] = resdataList.get(i).get("name");
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name"));
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name"));
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("领导");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("领导督查量统计");
        optionTitle.setSubtext("领导督查量占比");
        optionTitle.setLeft("left");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }
}
