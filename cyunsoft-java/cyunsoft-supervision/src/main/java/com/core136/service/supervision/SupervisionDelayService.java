package com.core136.service.supervision;

import com.core136.bean.supervision.SupervisionDelay;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.supervision.SupervisionDelayMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class SupervisionDelayService {
    private SupervisionDelayMapper supervisionDelayMapper;
	@Autowired
	public void setSupervisionDelayMapper(SupervisionDelayMapper supervisionDelayMapper) {
		this.supervisionDelayMapper = supervisionDelayMapper;
	}

	public int insertSupervisionDelay(SupervisionDelay supervisionDelay) {
        return supervisionDelayMapper.insert(supervisionDelay);
    }


    public int deleteSupervisionDelay(SupervisionDelay supervisionDelay) {
        return supervisionDelayMapper.delete(supervisionDelay);
    }

    public int updateSupervisionDelay(Example example, SupervisionDelay supervisionDelay) {
        return supervisionDelayMapper.updateByExampleSelective(supervisionDelay, example);
    }

    public SupervisionDelay selectOneSupervisionDelay(SupervisionDelay supervisionDelay) {
        return supervisionDelayMapper.selectOne(supervisionDelay);
    }

	/**
	 * 获取延期审批列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param keyword 查询关键词
	 * @return 延期审批列表
	 */
    public List<Map<String, String>> getDelayApplyList(String orgId,String opFlag, String accountId, String dateQueryType,String beginTime, String endTime,String status, String keyword) {
        return supervisionDelayMapper.getDelayApplyList(orgId,opFlag, accountId, dateQueryType,beginTime, endTime, status,"%" + keyword + "%");
    }

	/**
	 * 获取延期审批列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 延期审批列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getDelayApplyList(PageParam pageParam,String dateQueryType, String beginTime, String endTime,String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getDelayApplyList(pageParam.getOrgId(),pageParam.getOpFlag(), pageParam.getAccountId(), dateQueryType,beginTime, endTime,status, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }


}
