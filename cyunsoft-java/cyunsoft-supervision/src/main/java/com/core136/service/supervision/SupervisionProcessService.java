package com.core136.service.supervision;

import com.core136.bean.supervision.SupervisionProcess;
import com.core136.bean.system.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.supervision.SupervisionProcessMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class SupervisionProcessService {
    private SupervisionProcessMapper supervisionProcessMapper;
	@Autowired
	public void setSupervisionProcessMapper(SupervisionProcessMapper supervisionProcessMapper) {
		this.supervisionProcessMapper = supervisionProcessMapper;
	}

	public int insertSupervisionProcess(SupervisionProcess supervisionProcess) {
        return supervisionProcessMapper.insert(supervisionProcess);
    }

    public int deleteSupervisionProcess(SupervisionProcess supervisionProcess) {
        return supervisionProcessMapper.delete(supervisionProcess);
    }

    public int updateSupervisionProcess(Example example, SupervisionProcess supervisionProcess) {
        return supervisionProcessMapper.updateByExampleSelective(supervisionProcess, example);
    }

    public SupervisionProcess selectOneSupervisionProcess(SupervisionProcess supervisionProcess) {
        return supervisionProcessMapper.selectOne(supervisionProcess);
    }

	/**
	 * 获取事件处理过程列表
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param eventType 事件类型
	 * @param keyword 查询关键词
	 * @return 处理过程列表
	 */
    public List<Map<String, String>> getOldProcessList(String orgId, String accountId, String beginTime, String endTime, String eventType, String keyword) {
        return supervisionProcessMapper.getOldProcessList(orgId, accountId, beginTime, endTime, eventType, "%" + keyword + "%");
    }

	/**
	 * 获取我所管控的任务列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param supervisionId 督查督办Id
	 * @param keyword 查询关键词
	 * @return 管控的任务列表
	 */
    public List<Map<String, String>> getControlProcessList(String orgId,String opFlag, String accountId, String dateQueryType,String beginTime, String endTime, String supervisionId, String keyword) {
        return supervisionProcessMapper.getControlProcessList(orgId, opFlag,accountId,dateQueryType, beginTime, endTime, supervisionId, "%" + keyword + "%");
    }


	/**
	 * 获取事件处理过程列表
	 * @param pageParam 分页参数
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param supervisionId 督查督办Id
	 * @return 处理过程列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getOldProcessList(PageParam pageParam, String beginTime, String endTime, String supervisionId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOldProcessList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, supervisionId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 获取我所管控的任务列表
	 * @param pageParam 分页参数
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @param supervisionId 类型
	 * @return 任务列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getControlProcessList(PageParam pageParam, String dateQueryType,String beginTime, String endTime, String supervisionId) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getControlProcessList(pageParam.getOrgId(),pageParam.getOpFlag(), pageParam.getAccountId(),dateQueryType, beginTime, endTime, supervisionId, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取待处理的督查列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param eventType 事件类型
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 待处理的督查列表
     */
    public List<Map<String, String>> getSupervisionProcessList(String orgId, String accountId, String eventType,String dateQueryType, String beginTime, String endTime, String keyword) {
        return supervisionProcessMapper.getSupervisionProcessList(orgId, accountId, eventType, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取待处理的督查列表
     * @param pageParam 分页参数
     * @param eventType 事件类型
     * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @return 待处理的督查列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getSupervisionProcessList(PageParam pageParam, String eventType, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupervisionProcessList(pageParam.getOrgId(), pageParam.getAccountId(), eventType, dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

    /**
     * 获取转办给我的列表
     *
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param eventType 事件类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 转办给我的列表
     */
    public List<Map<String, String>> getSupervisionProcessForChangeList(String orgId, String accountId, String eventType,String dateQueryType, String beginTime, String endTime, String keyword) {
        return supervisionProcessMapper.getSupervisionProcessForChangeList(orgId, accountId, eventType,dateQueryType, beginTime, endTime, "%" + keyword + "%");
    }

	/**
	 * 获取转办给我的列表
	 * @param pageParam 分页参数
	 * @param eventType 事件类型
	 * @param dateQueryType 日期范围类型
	 * @param beginTime 开始时间
	 * @param endTime 结束时间
	 * @return 转办给我的列表
	 * @throws Exception SQL排序有注入风险异常
	 */
    public PageInfo<Map<String, String>> getSupervisionProcessForChangeList(PageParam pageParam, String eventType, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupervisionProcessForChangeList(pageParam.getOrgId(), pageParam.getAccountId(), eventType, dateQueryType,beginTime, endTime, pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

	/**
	 * 按部门汇总
	 * @param orgId 机构码
	 * @return 部门汇总列表
	 */
	public List<Map<String, Object>> getQuerySupervisionForDept(String orgId) {

		List<Map<String,Object>> list = supervisionProcessMapper.getQuerySupervisionForDept(orgId);
		List<Map<String,Object>> deptList = new ArrayList<>();
		for (Map<String, Object> oMap : list) {
			if ((oMap.get("parentId").toString()).equals("0")) {
				deptList.add(oMap);
			}
		}
		for (Map<String,Object> dept : deptList) {
			dept.put("children",getChildDeptList(dept.get("deptId").toString(), list));
		}
        return deptList;
    }


	/**
	 * 获取子部门列表
	 * @param deptId 部门Id
	 * @param rootUnitDept 子部门列表
	 * @return 子部门列表
	 */
	public List<Map<String, Object>> getChildDeptList(String deptId, List<Map<String, Object>> rootUnitDept) {
		List<Map<String, Object>> childList = new ArrayList<>();
		for (Map<String, Object> unitDept : rootUnitDept) {
			if (unitDept.get("parentId").equals(deptId)) {
				childList.add(unitDept);
			}
		}
		for (Map<String, Object> unitDept : childList) {
			unitDept.put("children",getChildDeptList(unitDept.get("deptId").toString(), rootUnitDept));
		}
		if (childList.isEmpty()) {
			return null;
		}
		return childList;
	}

	/**
	 * 获取督查督办待办事件
	 * @param orgId 机构码
	 * @param accountId 用户账号
	 * @return 督查督办待办事件
	 */
    public List<Map<String, String>> getSupervisionProcessListForDesk(String orgId, String accountId) {
        return supervisionProcessMapper.getSupervisionProcessListForDesk(orgId, accountId);
    }


    public List<Map<String, String>> getSupervisionProcessListForSelect(String orgId, String accountId) {
        return supervisionProcessMapper.getSupervisionProcessListForSelect(orgId, accountId);
    }

    /**
     * 获取延时任务列表
     *
     * @param orgId 机构码
     * @return 延时任务列表
     */
    public List<Map<String, String>> getAllSupervisionProcessForDesk(String orgId) {
        return supervisionProcessMapper.getAllSupervisionProcessForDesk(orgId);
    }

	/**
	 * 获取我所管控的任务列表
	 * @param orgId 机构码
	 * @param opFlag 管理员标识
	 * @param accountId 用户账号
	 * @param keyword 查询关键词
	 * @return 我所管控的任务列表
	 */
	public List<Map<String, String>> getSupervisionProcessForSelect(String orgId,String opFlag,String accountId,String processId,String keyword) {
		if(StringUtils.isBlank(keyword)) {
			keyword="%%";
		}else {
			keyword = "%"+keyword+"%";
		}
		return supervisionProcessMapper.getSupervisionProcessForSelect(orgId,opFlag,accountId,processId,keyword);
	}
}
