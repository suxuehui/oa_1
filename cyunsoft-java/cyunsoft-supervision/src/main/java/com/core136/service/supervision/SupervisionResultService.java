package com.core136.service.supervision;

import com.core136.bean.supervision.SupervisionResult;
import com.core136.mapper.supervision.SupervisionResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SupervisionResultService {
    private SupervisionResultMapper supervisionResultMapper;
	@Autowired
	public void setSupervisionResultMapper(SupervisionResultMapper supervisionResultMapper) {
		this.supervisionResultMapper = supervisionResultMapper;
	}

	public int insertSupervisionResult(SupervisionResult supervisionResult) {
        return supervisionResultMapper.insert(supervisionResult);
    }

    public int deleteSupervisionResult(SupervisionResult supervisionResult) {
        return supervisionResultMapper.delete(supervisionResult);
    }

    public int updateSupervisionResult(Example example, SupervisionResult supervisionResult) {
        return supervisionResultMapper.updateByExampleSelective(supervisionResult, example);
    }

    public SupervisionResult selectOneSupervisionResult(SupervisionResult supervisionResult) {
        return supervisionResultMapper.selectOne(supervisionResult);
    }

    /**
     * 获取任务处理过程列表
     *
     * @param orgId 机构码
     * @param processId 任务处理过程Id
     * @return 任务处理过程列表
     */
    public List<Map<String, String>> getSupervisionResultList(String orgId, String processId) {
        return supervisionResultMapper.getSupervisionResultList(orgId, processId);
    }

}
