package com.core136.service.supervision;

import com.core136.bean.account.UserInfo;
import com.core136.bean.supervision.SupervisionIntegral;
import com.core136.bean.supervision.SupervisionScore;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.supervision.SupervisionScoreMapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class SupervisionScoreService {
    private SupervisionScoreMapper supervisionScoreMapper;
	@Autowired
	public void setSupervisionScoreMapper(SupervisionScoreMapper supervisionScoreMapper) {
		this.supervisionScoreMapper = supervisionScoreMapper;
	}

	public int insertSupervisionScore(SupervisionScore supervisionScore) {
        return supervisionScoreMapper.insert(supervisionScore);
    }

    public int deleteSupervisionScore(SupervisionScore supervisionScore) {
        return supervisionScoreMapper.delete(supervisionScore);
    }

    public int updateSupervisionScore(Example example, SupervisionScore supervisionScore) {
        return supervisionScoreMapper.updateByExampleSelective(supervisionScore, example);
    }

    public SupervisionScore selectOneSupervisionScore(SupervisionScore supervisionScore) {
        return supervisionScoreMapper.selectOne(supervisionScore);
    }

    public RetDataBean setSupervisionScore(UserInfo user, SupervisionScore supervisionScore) {
        SupervisionScore tmpSupervisionScore = new SupervisionScore();
        tmpSupervisionScore.setOrgId(user.getOrgId());
        tmpSupervisionScore.setSupervisionId(supervisionScore.getSupervisionId());
        tmpSupervisionScore = selectOneSupervisionScore(tmpSupervisionScore);
        if (tmpSupervisionScore == null) {
            supervisionScore.setScoreId(SysTools.getGUID());
            supervisionScore.setCreateUser(user.getAccountId());
            supervisionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            supervisionScore.setOrgId(user.getOrgId());
            insertSupervisionScore(supervisionScore);
        } else {
            Example example = new Example(SupervisionScore.class);
            example.createCriteria().andEqualTo("supervisionId", supervisionScore.getSupervisionId()).andEqualTo("orgId", user.getOrgId());
            updateSupervisionScore(example, supervisionScore);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }


    /**
     * 设置延期积分
     *
     * @param user 用户对象
     * @param supervisionId 督查督办Id
     * @param supervisionIntegral 积分规则对象
     * @param delay 积分
     * @return 消息结构
     */
    public RetDataBean setDelayScore(UserInfo user, String supervisionId, SupervisionIntegral supervisionIntegral, Double delay) {
        SupervisionScore supervisionScore = new SupervisionScore();
        supervisionScore.setOrgId(user.getOrgId());
        supervisionScore.setSupervisionId(supervisionId);
        supervisionScore = selectOneSupervisionScore(supervisionScore);
        SupervisionScore tmpSupervisionScore = new SupervisionScore();
        if (supervisionScore == null) {
            tmpSupervisionScore.setScoreId(SysTools.getGUID());
            tmpSupervisionScore.setSupervisionId(supervisionId);
            tmpSupervisionScore.setOrgId(user.getOrgId());
            if (supervisionIntegral.getDelayPoint() == null) {
                supervisionIntegral.setDelay(0.0);
            }
            tmpSupervisionScore.setSysScore(supervisionIntegral.getDelayPoint() * delay);
            tmpSupervisionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSupervisionScore.setCreateUser(user.getAccountId());
            insertSupervisionScore(tmpSupervisionScore);
        } else {
            if (supervisionIntegral.getDelayPoint() == null) {
                supervisionIntegral.setDelay(0.0);
            }
            tmpSupervisionScore.setSysScore(tmpSupervisionScore.getSysScore() + supervisionIntegral.getDelayPoint() * delay);
            tmpSupervisionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSupervisionScore.setCreateUser(user.getAccountId());
            Example example = new Example(SupervisionScore.class);
            example.createCriteria().andEqualTo("orgId", user.getOrgId()).andEqualTo("supervisionId", supervisionId);
            updateSupervisionScore(example, tmpSupervisionScore);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
    }


    /**
     * 设置超时积分
     *
     * @param user 用户对象
     * @param supervisionId 督查督办Id
     * @param supervisionIntegral 积分规则
     * @param passTime 过基进间
     * @return 消息结构
     */
    public RetDataBean setPassTimeScore(UserInfo user, String supervisionId, SupervisionIntegral supervisionIntegral, Double passTime) {
        SupervisionScore supervisionScore = new SupervisionScore();
        supervisionScore.setOrgId(user.getOrgId());
        supervisionScore.setSupervisionId(supervisionId);
        supervisionScore = selectOneSupervisionScore(supervisionScore);
        SupervisionScore tmpSupervisionScore = new SupervisionScore();
        if (supervisionScore == null) {
            tmpSupervisionScore.setScoreId(SysTools.getGUID());
            tmpSupervisionScore.setSupervisionId(supervisionId);
            tmpSupervisionScore.setOrgId(user.getOrgId());
            if (supervisionIntegral.getPassTimePoint() == null) {
                supervisionIntegral.setPassTimePoint(0.0);
            }
            tmpSupervisionScore.setSysScore(supervisionIntegral.getPassTimePoint() * passTime);
            tmpSupervisionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSupervisionScore.setCreateUser(user.getAccountId());
            insertSupervisionScore(tmpSupervisionScore);
        } else {
            if (supervisionIntegral.getPassTimePoint() == null) {
                supervisionIntegral.setPassTimePoint(0.0);
            }
            tmpSupervisionScore.setSysScore(tmpSupervisionScore.getSysScore() + supervisionIntegral.getPassTimePoint() * passTime);
            tmpSupervisionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSupervisionScore.setCreateUser(user.getAccountId());
            Example example = new Example(SupervisionScore.class);
            example.createCriteria().andEqualTo("orgId", user.getOrgId()).andEqualTo("supervisionId", supervisionId);
            updateSupervisionScore(example, tmpSupervisionScore);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
    }

	/**
	 * 获取督查事件积分列表
	 * @param orgId 机构码
	 * @param keyword 查询关键词
	 * @return 督查事件积分列表
	 */
    public List<Map<String, String>> getSupervisionScoreList(String orgId, String keyword) {
        return supervisionScoreMapper.getSupervisionScoreList(orgId, "%" + keyword + "%");
    }
	/**
	 * 获取督查事件积分列表
	 * @param pageParam 分页参数
	 * @return 督查事件积分列表
	 */
    public PageInfo<Map<String, String>> getSupervisionScoreList(PageParam pageParam) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupervisionScoreList(pageParam.getOrgId(), pageParam.getKeyword());
		return new PageInfo<>(datalist);
    }

}
