package com.core136.service.supervision;

import com.core136.bean.account.UserInfo;
import com.core136.bean.supervision.Supervision;
import com.core136.bean.system.MsgBody;
import com.core136.bean.system.PageParam;
import com.core136.common.enums.GlobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.supervision.SupervisionMapper;
import com.core136.service.account.UserInfoService;
import com.core136.service.system.MessageUnitService;
import com.core136.unit.SpringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @author lsq
 */
@Service
public class SupervisionService {
    private SupervisionMapper supervisionMapper;
    private UserInfoService userInfoService;
    private SupervisionConfigService supervisionConfigService;
	@Autowired
	public void setSupervisionMapper(SupervisionMapper supervisionMapper) {
		this.supervisionMapper = supervisionMapper;
	}
	@Autowired
	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}
	@Autowired
	public void setSupervisionConfigService(SupervisionConfigService supervisionConfigService) {
		this.supervisionConfigService = supervisionConfigService;
	}

	public int insertSupervision(Supervision supervision) {
        return supervisionMapper.insert(supervision);
    }


    public int deleteSupervision(Supervision supervision) {
        return supervisionMapper.delete(supervision);
    }

    public List<Map<String,Object>> getSupervisionTree(String orgId) {
        List<Map<String,Object>> configList = supervisionConfigService.getSupervisionConfigTree(orgId);
        for(Map<String,Object> map:configList) {
            String type = map.get("id").toString();
            List<Map<String,String>> children = getSupervisionListByType(orgId,type);
            if(!children.isEmpty()) {
                map.put("children",children);
            }
        }
        return configList;
    }

    public List<Map<String,String>>getSupervisionListByType(String orgId,String type) {
        return supervisionMapper.getSupervisionListByType(orgId,type);
    }

    /**
     * 批量删除督查事件
     * @param orgId 机构码
     * @param list 督查事件Id 列表
     * @return 消息结构
     */
    public RetDataBean deleteSupervisionByIds(String orgId, List<String> list) {
        if(list.isEmpty()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }else {
            Example example = new Example(Supervision.class);
            example.createCriteria().andEqualTo("orgId", orgId).andIn("supervisionId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, supervisionMapper.deleteByExample(example));
        }
    }
    public int updateSupervision(Example example, Supervision supervision) {
        return supervisionMapper.updateByExampleSelective(supervision, example);
    }

    public Supervision selectOneSupervision(Supervision supervision) {
        return supervisionMapper.selectOne(supervision);
    }

    public List<Map<String, String>> getSupervisionForTree(String orgId, String type) {
        return supervisionMapper.getSupervisionForTree(orgId, type);
    }

    /**
     * 创建督查督办
     * @param user 用户对象
     * @param supervision 督查督办对象
     * @return 成功创建记录数
     */
    public int createSupervision(UserInfo user, Supervision supervision) {
        if (StringUtils.isNotBlank(supervision.getMsgType())) {
            String leadId = supervision.getLeadId();
            String handedUser = supervision.getHandedUser();
            String joinUser = supervision.getJoinUser();
            List<String> userList = new ArrayList<>();
            List<String> arr2 = new ArrayList<>();
            List<String> arr3 = new ArrayList<>();
            if (StringUtils.isNotBlank(leadId)) {
                userList = new ArrayList<>(Arrays.asList(leadId.split(",")));
            }
            if (StringUtils.isNotBlank(handedUser)) {
                arr2 = new ArrayList<>(Arrays.asList(handedUser.split(",")));
            }
            if (StringUtils.isNotBlank(joinUser)) {
                arr3 = new ArrayList<>(Arrays.asList(joinUser.split(",")));
            }
            userList.addAll(arr2);
            userList.addAll(arr3);
			Set<String> set = new HashSet<>(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
			userList.clear();
            userList.addAll(set);
            List<MsgBody> msgBodyList = new ArrayList<>();
            for (String accountId : userList) {
                UserInfo user2 = new UserInfo();
                user2.setAccountId(accountId);
                user2.setOrgId(user.getOrgId());
                user2 = userInfoService.selectOneUser(user2);
                MsgBody msgBody = new MsgBody();
                msgBody.setTitle("督查督办提醒");
                msgBody.setContent("任务标题为：" + supervision.getTitle() + "的查看提醒！");
                msgBody.setUserInfo(user2);
                msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                msgBody.setFromAccountId(user.getAccountId());
                msgBody.setFormUserName(user.getUserName());
                msgBody.setView(GlobalConstant.MSG_TYPE_SUPERVISION);
                msgBody.setRecordId(supervision.getSupervisionId());
                msgBody.setOrgId(user.getOrgId());
                msgBodyList.add(msgBody);
            }
            List<String> msgTypeList = Arrays.asList(supervision.getMsgType().split(","));
            MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
        return supervisionMapper.insert(supervision);
    }


    public int updateSupervision(UserInfo user, Example example, Supervision supervision) {
        if (StringUtils.isNotBlank(supervision.getMsgType())) {
            String leadId = supervision.getLeadId();
            String handedUser = supervision.getHandedUser();
            String joinUser = supervision.getJoinUser();
            List<String> userList = new ArrayList<>();
            List<String> arr2 = new ArrayList<>();
            List<String> arr3 = new ArrayList<>();
            if (StringUtils.isNotBlank(leadId)) {
                userList = new ArrayList<>(Arrays.asList(leadId.split(",")));
            }
            if (StringUtils.isNotBlank(handedUser)) {
                arr2 = new ArrayList<>(Arrays.asList(handedUser.split(",")));
            }
            if (StringUtils.isNotBlank(joinUser)) {
                arr3 = new ArrayList<>(Arrays.asList(joinUser.split(",")));
            }
            userList.addAll(arr2);
            userList.addAll(arr3);
			Set<String> set = new HashSet<>(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
			userList.clear();
            userList.addAll(set);
            List<MsgBody> msgBodyList = new ArrayList<>();
            for (String accountId : userList) {
                UserInfo user2 = new UserInfo();
                user2.setAccountId(accountId);
                user2.setOrgId(user.getOrgId());
                user2 = userInfoService.selectOneUser(user2);
                MsgBody msgBody = new MsgBody();
                msgBody.setTitle("督查督办提醒");
                msgBody.setContent("任务标题为：" + supervision.getTitle() + "的查看提醒！");
                msgBody.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                msgBody.setUserInfo(user2);
                msgBody.setFromAccountId(user.getAccountId());
                msgBody.setFormUserName(user.getUserName());
                msgBody.setView(GlobalConstant.MSG_TYPE_SUPERVISION);
                msgBody.setRecordId(supervision.getSupervisionId());
                msgBody.setOrgId(user.getOrgId());
                msgBodyList.add(msgBody);
            }
            List<String> msgTypeList = Arrays.asList(supervision.getMsgType().split(","));
            MessageUnitService messageUnitService = SpringUtils.getBean(MessageUnitService.class);
            messageUnitService.sendMessage(msgTypeList, msgBodyList);
        }
        return supervisionMapper.updateByExampleSelective(supervision, example);
    }

    /**
     * 我的督查列表
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param evenType 事件类型
     * @param handedUser 管控人
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @param keyword 查询对象
     * @return 督查列表
     */
    public List<Map<String, String>> getSupervisionList(String orgId, String accountId, String evenType, String handedUser,String dateQueryType, String beginTime, String endTime, String status, String keyword) {
        return supervisionMapper.getSupervisionList(orgId, accountId, evenType, handedUser,dateQueryType, beginTime, endTime, status, "%" + keyword + "%");
    }

    /**
     * 获取督查督办事件列表
     *
     * @param orgId 机构码
     * @param evenType 事件类型
     * @param handedUser 管控人
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 督查督办事件列表
     */
    public List<Map<String, String>> getQueryOldSupervisionList(String orgId, String evenType, String handedUser,String dateQueryType, String beginTime, String endTime,String status, String keyword) {
        return supervisionMapper.getQueryOldSupervisionList(orgId, evenType, handedUser, dateQueryType,beginTime, endTime, status,"%" + keyword + "%");
    }

    /**
     * 获取督查督办事件列表
     *
     * @param pageParam 分页参数
     * @param eventType 事件类型
     * @param handedUser 管控人
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 督查督办事件列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getQueryOldSupervisionList(PageParam pageParam, String eventType, String handedUser,String dateQueryType, String beginTime, String endTime,String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getQueryOldSupervisionList(pageParam.getOrgId(), eventType, handedUser,dateQueryType, beginTime, endTime, status,pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }

    /**
     * 获取当前用户管控的事件列表
     * @param orgId 机构码
     * @param accountId 用户账号
	 * @param eventType 事件类型
	 * @param handedUser 管控人
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @param keyword 查询关键词
     * @return 当前用户管控的事件列表
     */
    public List<Map<String, String>> getLeadManageSupervisionList(String orgId, String accountId, String eventType, String handedUser,String dateQueryType, String beginTime, String endTime,String status, String keyword) {
        return supervisionMapper.getLeadManageSupervisionList(orgId, accountId, eventType, handedUser, dateQueryType,beginTime, endTime, status, "%" + keyword + "%");
    }

    /**
     * 获取当前用户管控的事件列表
     * @param pageParam 分页参数
	 * @param eventType 事件类型
	 * @param handedUser 管控人
     * @param dateQueryType 日期范围类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @return 当前用户管控的事件列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getLeadManageSupervisionList(PageParam pageParam, String eventType, String handedUser, String dateQueryType,String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeadManageSupervisionList(pageParam.getOrgId(), pageParam.getAccountId(), eventType, handedUser,dateQueryType, beginTime, endTime, status, pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }

    /**
     * 我的督查列表
     * @param pageParam 分页参数
	 * @param eventType 事件类型
	 * @param handedUser 管控人
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @return 督查列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getSupervisionList(PageParam pageParam, String eventType, String handedUser, String dateQueryType,String beginTime, String endTime, String status) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupervisionList(pageParam.getOrgId(), pageParam.getAccountId(), eventType, handedUser, dateQueryType,beginTime, endTime, status, pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }

    /**
     * 获取完成督查督办列表
     *
     * @param orgId 机构码
	 * @param eventType 事件类型
	 * @param handedUser 管控人
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param keyword 查询关键词
     * @return 完成督查督办列表
     */
    public List<Map<String, String>> getSupervisionFinishList(String orgId, String eventType, String handedUser, String dateQueryType,String beginTime, String endTime, String keyword) {
        return supervisionMapper.getSupervisionFinishList(orgId, eventType, handedUser, dateQueryType,beginTime, endTime, "%" + keyword + "%");
    }

    /**
     * 获取完成督查督办列表
     *
     * @param pageParam 分页参数
	 * @param eventType 事件类型
	 * @param handedUser 管控人
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 完成督查督办列表
     * @throws Exception SQL排序有注入风险异常
     */
    public PageInfo<Map<String, String>> getSupervisionFinishList(PageParam pageParam, String eventType, String handedUser, String dateQueryType,String beginTime, String endTime) throws Exception {
       PageMethod.startPage(pageParam.getPage(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupervisionFinishList(pageParam.getOrgId(), eventType, handedUser, dateQueryType,beginTime, endTime, pageParam.getKeyword());
        return new PageInfo<>(datalist);
    }

    public List<Map<String, String>> getManageSupervisionListForDesk(String orgId, String accountId) {
        return supervisionMapper.getManageSupervisionListForDesk(orgId, accountId);
    }

    /**
     * 移动端督查督办列表
     *
     * @param orgId 机构码
     * @param accountId 用户账号
     * @param page 页码
     * @return 端督查督办列表
     */
    public List<Map<String, String>> getMobileMySupervisionList(String orgId, String accountId, Integer page) {
        return supervisionMapper.getMobileMySupervisionList(orgId, accountId, page);
    }

    public List<Map<String, String>> getAllSupervisionForDesk(String orgId) {
        return supervisionMapper.getAllSupervisionForDesk(orgId);
    }

    public List<Map<String, String>> getAllFinishSupervisionForDesk(String orgId) {
        return supervisionMapper.getAllFinishSupervisionForDesk(orgId);
    }

    /**
     * 获取督查督办事件详情
     * @param orgId 机构码
     * @param supervisionId 督查督办事件Id
     * @return 督查督办事件详情
     */
    public Map<String,String>getSupervisionById(String orgId,String supervisionId) {
        return supervisionMapper.getSupervisionById(orgId,supervisionId);
    }

}
