import config from "@/config"
import http from "@/utils/request"

export default {
	archivesBorrowing:{
		createArchivesBorrowingRule:{
			url: `${config.API_URL}/set/archives/createArchivesBorrowingRule`,
			name: "发起档案借阅申请",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getArchivesBorrowingList:{
			url: `${config.API_URL}/get/archives/getArchivesBorrowingList`,
			name: "获取档案借阅列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	echarts:{
		getArchivesSortForPie:{
			url: `${config.API_URL}/get/archives/getArchivesSortForPie`,
			name: "按分类获取饼状图",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getArchivesSmForPie:{
			url: `${config.API_URL}/get/archives/getArchivesSmForPie`,
			name: "按涉密等级获取饼状图",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getArchivesByMonthLine:{
			url: `${config.API_URL}/get/archives/getArchivesByMonthLine`,
			name: "获取一段时间内按月份统计档案数据上架走势",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getArchivesFrameForBar:{
			url: `${config.API_URL}/get/archives/getArchivesFrameForBar`,
			name: "档案架档案数量对比",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	archivesSearch:{
		getHostKeywords:{
			url: `${config.API_URL}/get/archives/getHostKeywords`,
			name: "获取热门关键字",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	archivesOutbound:{
		getArchivesOutboundList:{
			url: `${config.API_URL}/get/archives/getArchivesOutboundList`,
			name: "获取档案出库记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesOutbound:{
			url: `${config.API_URL}/set/archives/insertArchivesOutbound`,
			name: "添加档案出库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesOutbound:{
			url: `${config.API_URL}/set/archives/deleteArchivesOutbound`,
			name: "删除档案出库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesOutboundByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesOutboundByIds`,
			name: "批量删除档案出库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesOutbound:{
			url: `${config.API_URL}/set/archives/updateArchivesOutbound`,
			name: "更新档案出库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesMove:{
		getArchivesMoveList:{
			url: `${config.API_URL}/get/archives/getArchivesMoveList`,
			name: "获取档案移库记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesMove:{
			url: `${config.API_URL}/set/archives/insertArchivesMove`,
			name: "添加档案移库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesMove:{
			url: `${config.API_URL}/set/archives/deleteArchivesMove`,
			name: "删除档案移库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesMoveByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesMoveByIds`,
			name: "批量删除档案移库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesMove:{
			url: `${config.API_URL}/set/archives/updateArchivesMove`,
			name: "更新档案移库记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesInventory:{
		getArchivesInventoryList:{
			url: `${config.API_URL}/get/archives/getArchivesInventoryList`,
			name: "获取档案盘点记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesInventory:{
			url: `${config.API_URL}/set/archives/insertArchivesInventory`,
			name: "添加档案盘点记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesInventory:{
			url: `${config.API_URL}/set/archives/deleteArchivesInventory`,
			name: "删除档案盘点记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesInventoryByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesInventoryByIds`,
			name: "批量删除档案盘点记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesInventory:{
			url: `${config.API_URL}/set/archives/updateArchivesInventory`,
			name: "更新档案盘点记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesTransfer:{
		getArchivesTransferList:{
			url: `${config.API_URL}/get/archives/getArchivesTransferList`,
			name: "获取档案转移列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesTransfer:{
			url: `${config.API_URL}/set/archives/insertArchivesTransfer`,
			name: "添加档案转移记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesTransfer:{
			url: `${config.API_URL}/set/archives/deleteArchivesTransfer`,
			name: "删除档案转移记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesTransferByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesTransferByIds`,
			name: "批量删除档案转移记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesTransfer:{
			url: `${config.API_URL}/set/archives/updateArchivesTransfer`,
			name: "更新档案转移记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesPatrol:{
		getArchivesPatrolList:{
			url: `${config.API_URL}/get/archives/getArchivesPatrolList`,
			name: "获取档案巡检记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesPatrol:{
			url: `${config.API_URL}/set/archives/insertArchivesPatrol`,
			name: "添加巡检记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesPatrol:{
			url: `${config.API_URL}/set/archives/deleteArchivesPatrol`,
			name: "删除巡检记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesPatrolByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesPatrolByIds`,
			name: "批量删除巡检记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesPatrol:{
			url: `${config.API_URL}/set/archives/updateArchivesPatrol`,
			name: "更新巡检记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesRecord:{
		searchIndex:{
			url: `${config.API_URL}/get/archives/searchIndex`,
			name: "全文检索",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getArchivesRecordListBySort:{
			url: `${config.API_URL}/get/archives/getArchivesRecordListBySort`,
			name: "按分类获取档案列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getArchivesRecordListForFile:{
			url: `${config.API_URL}/get/archives/getArchivesRecordListForFile`,
			name: "获取待归档档案列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getArchivesRecordListForFrame:{
			url: `${config.API_URL}/get/archives/getArchivesRecordListForFrame`,
			name: "获取待上架档案列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		setArchivesIdentify:{
			url: `${config.API_URL}/set/archives/setArchivesIdentify`,
			name: "档案鉴定",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getArchivesRecordListForSelect:{
			url: `${config.API_URL}/get/archives/getArchivesRecordListForSelect`,
			name: "获取档案下接列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesRecord:{
			url: `${config.API_URL}/set/archives/insertArchivesRecord`,
			name: "添加档案记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesRecord:{
			url: `${config.API_URL}/set/archives/deleteArchivesRecord`,
			name: "删除档案记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesRecordByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesRecordByIds`,
			name: "批量删除档案记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesRecord:{
			url: `${config.API_URL}/set/archives/updateArchivesRecord`,
			name: "更新档案记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesDestroy:{
		getArchivesDestroyList:{
			url: `${config.API_URL}/get/archives/getArchivesDestroyList`,
			name: "获取档案销毁记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesDestroy:{
			url: `${config.API_URL}/set/archives/insertArchivesDestroy`,
			name: "添加档案销毁记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesDestroy:{
			url: `${config.API_URL}/set/archives/deleteArchivesDestroy`,
			name: "删除档案销毁记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesDestroyIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesDestroyIds`,
			name: "批量删除档案销毁记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesDestroy:{
			url: `${config.API_URL}/set/archives/updateArchivesDestroy`,
			name: "更新档案销毁记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesTempHum:{
		getArchivesTempHumList:{
			url: `${config.API_URL}/get/archives/getArchivesTempHumList`,
			name: "获取档案库房温湿度检查列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesTempHum:{
			url: `${config.API_URL}/set/archives/insertArchivesTempHum`,
			name: "创建温湿度记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesTempHum:{
			url: `${config.API_URL}/set/archives/deleteArchivesTempHum`,
			name: "删除温湿度记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesTempHumByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesTempHumByIds`,
			name: "批量删除温湿度记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesTempHum:{
			url: `${config.API_URL}/set/archives/updateArchivesTempHum`,
			name: "更新温湿度记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesAccess:{
		getArchivesAccessList:{
			url: `${config.API_URL}/get/archives/getArchivesAccessList`,
			name: "获取档案库房进出记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesAccess:{
			url: `${config.API_URL}/set/archives/insertArchivesAccess`,
			name: "创建进出管理记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesAccess:{
			url: `${config.API_URL}/set/archives/deleteArchivesAccess`,
			name: "删除进出管理记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesAccessByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesAccessByIds`,
			name: "批量删除进出管理记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesAccess:{
			url: `${config.API_URL}/set/archives/updateArchivesAccess`,
			name: "更新进出管理记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesEnter:{
		getArchivesEnterList:{
			url: `${config.API_URL}/get/archives/getArchivesEnterList`,
			name: "获取档案登记记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesEnter:{
			url: `${config.API_URL}/set/archives/insertArchivesEnter`,
			name: "创建档案登记记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesEnter:{
			url: `${config.API_URL}/set/archives/deleteArchivesEnter`,
			name: "删除档案登记记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesEnterByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesEnterByIds`,
			name: "批量删除档案登记记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesEnter:{
			url: `${config.API_URL}/set/archives/updateArchivesEnter`,
			name: "更新档案登记记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesLibrary:{
		getArchivesLibraryList:{
			url: `${config.API_URL}/get/archives/getArchivesLibraryList`,
			name: "获取档案库列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesLibrary:{
			url: `${config.API_URL}/set/archives/insertArchivesLibrary`,
			name: "创建档案卷库",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesLibrary:{
			url: `${config.API_URL}/set/archives/deleteArchivesLibrary`,
			name: "删除档案卷库",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesLibraryByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesLibraryByIds`,
			name: "批量删除档案卷库",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesLibrary:{
			url: `${config.API_URL}/set/archives/updateArchivesLibrary`,
			name: "更新档案卷库",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesFrame:{
		getArchivesFrameListForManage:{
			url: `${config.API_URL}/get/archives/getArchivesFrameListForManage`,
			name: "获取档案架号列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getArchivesFrameListForSelect:{
			url: `${config.API_URL}/get/archives/getArchivesFrameListForSelect`,
			name: "获取档案架号列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertArchivesFrame:{
			url: `${config.API_URL}/set/archives/insertArchivesFrame`,
			name: "创建档案架",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesFrame:{
			url: `${config.API_URL}/set/archives/deleteArchivesFrame`,
			name: "删除档案架",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesFrameByIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesFrameByIds`,
			name: "批量删除档案架",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesFrame:{
			url: `${config.API_URL}/set/archives/updateArchivesFrame`,
			name: "更新档案架",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesRoom:{
		getArchivesRoomListForSelect:{
			url: `${config.API_URL}/get/archives/getArchivesRoomListForSelect`,
			name: "获取档案库房列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getAllArchivesRoomList:{
			url: `${config.API_URL}/get/archives/getAllArchivesRoomList`,
			name: "获取所有档案库房列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertArchivesRoom: {
			url: `${config.API_URL}/set/archives/insertArchivesRoom`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesRoom: {
			url: `${config.API_URL}/set/archives/updateArchivesRoom`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesRoom: {
			url: `${config.API_URL}/set/archives/deleteArchivesRoom`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesDic: {
		getArchivesDicTree: {
			url: `${config.API_URL}/get/archives/getArchivesDicTree`,
			name: "获取字典树",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getArchivesDicList: {
			url: `${config.API_URL}/get/archives/getArchivesDicList`,
			name: "字典明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getArchivesDicParentList: {
			url: `${config.API_URL}/get/archives/getArchivesDicParentList`,
			name: "获取字典数据",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/archives/insertArchivesDic`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/archives/deleteArchivesDic`,
			name: "删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		dropArchivesDic: {
			url: `${config.API_URL}/set/archives/dropArchivesDic`,
			name: "字典拖拽排序",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesDicByIds: {
			url: `${config.API_URL}/set/archives/deleteArchivesDicByIds`,
			name: "批量删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/archives/updateArchivesDic`,
			name: "更新字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getArchivesDicByCode: {
			url: `${config.API_URL}/get/archives/getArchivesDicByCode`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	},
	archivesConfig:{
		getArchivesConfig: {
			url: `${config.API_URL}/get/archives/getArchivesConfig`,
			name: "获取档案参数配置",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		setArchivesConfig: {
			url: `${config.API_URL}/set/archives/setArchivesConfig`,
			name: "更新档案参数配置",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	archivesSort:{
		insertArchivesSort: {
			url: `${config.API_URL}/set/archives/insertArchivesSort`,
			name: "创建档案分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateArchivesSort: {
			url: `${config.API_URL}/set/archives/updateArchivesSort`,
			name: "更新档案分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesSort: {
			url: `${config.API_URL}/set/archives/deleteArchivesSort`,
			name: "删除档案分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteArchivesSortIds:{
			url: `${config.API_URL}/set/archives/deleteArchivesSortIds`,
			name: "批量删除分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getArchivesSortTree: {
			url: `${config.API_URL}/get/archives/getArchivesSortTree`,
			name: "获取所有档案分类树型结构",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		getAllArchivesSortForSelectTree:{
			url: `${config.API_URL}/get/archives/getAllArchivesSortForSelectTree`,
			name: "获取档案分类树形结构",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	}
}
