import config from "@/config"
import http from "@/utils/request"

export default {
	bigDataPro:{
		getAllBigDataProList:{
			url: `${config.API_URL}/get/bigdata/getAllBigDataProList`,
			name: "获取项目列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBigDataPro:{
			url: `${config.API_URL}/set/bigdata/insertBigDataPro`,
			name: "创建项目",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		updateBigDataPro:{
			url: `${config.API_URL}/set/bigdata/updateBigDataPro`,
			name: "更新项目",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		deleteBigDataPro:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataPro`,
			name: "删除项目",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
	},
	bigDataChart:{
		getAllBigDataChartList:{
			url: `${config.API_URL}/get/bigdata/getAllBigDataChartList`,
			name: "获取图表列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBigDataChart:{
			url: `${config.API_URL}/set/bigdata/insertBigDataChart`,
			name: "创建图表",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		updateBigDataChart:{
			url: `${config.API_URL}/set/bigdata/updateBigDataChart`,
			name: "更新图表",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		deleteBigDataChart:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataChart`,
			name: "删除图表",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
	},
	bigDataApi:{
		getAllBigDataApiList:{
			url: `${config.API_URL}/get/bigdata/getAllBigDataApiList`,
			name: "获取接口列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBigDataApi:{
			url: `${config.API_URL}/set/bigdata/insertBigDataApi`,
			name: "创建接口",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		updateBigDataApi:{
			url: `${config.API_URL}/set/bigdata/updateBigDataApi`,
			name: "更新接口",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		deleteBigDataApi:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataApi`,
			name: "删除接口",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		deleteBigDataApiByIds:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataItemApi`,
			name: "批量接口",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
	},
	bigDataItem:{
		getBigDataItemTree:{
			url: `${config.API_URL}/get/bigdata/getBigDataItemTree`,
			name: "获取图形分类数形结构",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBigDataItem:{
			url: `${config.API_URL}/set/bigdata/insertBigDataItem`,
			name: "创建图表分类",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		updateBigDataItem:{
			url: `${config.API_URL}/set/bigdata/updateBigDataItem`,
			name: "更新图表分类",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		deleteBigDataItem:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataItem`,
			name: "删除图表分类",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		deleteBigDataItemByIds:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataItemByIds`,
			name: "批量删除图表分类",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
	},
	bigDataSource:{
		getBigDataSourceListForSelect:{
			url: `${config.API_URL}/get/bigdata/getBigDataSourceListForSelect`,
			name: "获取下拉菜单的数据源列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBigDataSourceList:{
			url: `${config.API_URL}/get/bigdata/getBigDataSourceList`,
			name: "获取第三方数据源列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertBigDataSource:{
			url: `${config.API_URL}/set/bigdata/insertBigDataSource`,
			name: "添加第三方数据源",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		updateBigDataSource:{
			url: `${config.API_URL}/set/bigdata/updateBigDataSource`,
			name: "更新第三方数据源",
			post: async function (data) {
				return await http.bigPost(this.url, data,
					{headers: {'Content-Type': 'application/json;charset=UTF-8'}}
				);
			},
		},
		deleteBigDataSource:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataSource`,
			name: "删除第三方数据源",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
		deleteBigDataSourceByIds:{
			url: `${config.API_URL}/set/bigdata/deleteBigDataSourceByIds`,
			name: "批量删除第三方数据源",
			post: async function (params) {
				return await http.post(this.url,params);
			}
		},
	}
}
