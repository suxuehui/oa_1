import config from "@/config"
import http from "@/utils/request"

export default {
	getAllCalendarList:{
		url: `${config.API_URL}/get/calendar/getAllCalendarList`,
		name: "获取日程详情",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getCalendarById:{
		url: `${config.API_URL}/get/calendar/getCalendarById`,
		name: "获取日程详情",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getMySubordinateCalendarList:{
		url: `${config.API_URL}/get/calendar/getMySubordinateCalendarList`,
		name: "获取下属当前日程",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	getMyCalendarList:{
		url: `${config.API_URL}/get/calendar/getMyCalendarList`,
		name: "获取人个当前日程",
		get: async function (params) {
			return await http.get(this.url,params);
		}
	},
	insertCalendar:{
		url: `${config.API_URL}/set/calendar/insertCalendar`,
		name: "添加日程",
		post: async function (params) {
			return await http.post(this.url,params);
		}
	},
	updateCalendar:{
		url: `${config.API_URL}/set/calendar/updateCalendar`,
		name: "日程更新",
		post: async function (params) {
			return await http.post(this.url,params);
		}
	},
	deleteCalendar:{
		url: `${config.API_URL}/set/calendar/deleteCalendar`,
		name: "删除日程",
		post: async function (params) {
			return await http.post(this.url,params);
		}
	},
	deleteCalendarByIds:{
		url: `${config.API_URL}/set/calendar/deleteCalendarByIds`,
		name: "批量删除日程",
		post: async function (params) {
			return await http.post(this.url,params);
		}
	}
}
