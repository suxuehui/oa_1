import config from "@/config"
import http from "@/utils/request"

export default {
	echarts:{
		getCarePieForAnalysis:{
			url: `${config.API_URL}/get/hr/getCarePieForAnalysis`,
			name: "人员关怀复饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getCareBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getCareBarForAnalysis`,
			name: "人员关怀复职柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getCareTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getCareTableForAnalysis`,
			name: "人员关怀复职分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getEvaluationPieForAnalysis:{
			url: `${config.API_URL}/get/hr/getEvaluationPieForAnalysis`,
			name: "职称评定饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getEvaluationBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getEvaluationBarForAnalysis`,
			name: "职称评定柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getEvaluationTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getEvaluationTableForAnalysis`,
			name: "职称评定分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getReinstatPieForAnalysis:{
			url: `${config.API_URL}/get/hr/getReinstatPieForAnalysis`,
			name: "人员复饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getReinstatBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getReinstatBarForAnalysis`,
			name: "人员复职柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getReinstatTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getReinstatTableForAnalysis`,
			name: "人员复职分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeavePieForAnalysis:{
			url: `${config.API_URL}/get/hr/getLeavePieForAnalysis`,
			name: "人员离职饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeaveBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getLeaveBarForAnalysis`,
			name: "人员离职柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeaveTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getLeaveTableForAnalysis`,
			name: "人员离职分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getTransferPieForAnalysis:{
			url: `${config.API_URL}/get/hr/getTransferPieForAnalysis`,
			name: "调动类型饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getTransferBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getTransferBarForAnalysis`,
			name: "调动类型柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getTransferTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getTransferTableForAnalysis`,
			name: "调动类型分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSkillsPieForAnalysis:{
			url: `${config.API_URL}/get/hr/getSkillsPieForAnalysis`,
			name: "劳动技能饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSkillsBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getSkillsBarForAnalysis`,
			name: "劳动技能柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSkillsTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getSkillsTableForAnalysis`,
			name: "劳动技能分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLearnPieForAnalysis:{
			url: `${config.API_URL}/get/hr/getLearnPieForAnalysis`,
			name: "学习经历饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLearnBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getLearnBarForAnalysis`,
			name: "学习经历柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLearnTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getLearnTableForAnalysis`,
			name: "学习经功分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLicencePieForAnalysis:{
			url: `${config.API_URL}/get/hr/getLicencePieForAnalysis`,
			name: "证照饼状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLicenceBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getLicenceBarForAnalysis`,
			name: "证照柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLicenceTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getLicenceTableForAnalysis`,
			name: "证照分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getIncentivePieForAnalysis:{
			url: `${config.API_URL}/get/hr/getIncentivePieForAnalysis`,
			name: "人事档案分析饼状图",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getIncentiveBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getIncentiveBarForAnalysis`,
			name: "奖惩柱状图分析",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getIncentiveTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getIncentiveTableForAnalysis`,
			name: "奖惩分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getContractPieForAnalysis:{
			url: `${config.API_URL}/get/hr/getContractPieForAnalysis`,
			name: "人事档案分析饼状图",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getContractBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getContractBarForAnalysis`,
			name: "人事档案分析饼状图",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getContractTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getContractTableForAnalysis`,
			name: "合同分析tabale",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBaseInfoPieForAnalysis:{
			url: `${config.API_URL}/get/hr/getBaseInfoPieForAnalysis`,
			name: "人事档案分析饼状图",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBaseInfoBarForAnalysis:{
			url: `${config.API_URL}/get/hr/getBaseInfoBarForAnalysis`,
			name: "人事档案分析柱状图",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getBaseInfoTableForAnalysis:{
			url: `${config.API_URL}/get/hr/getBaseInfoTableForAnalysis`,
			name: "人事档案分析Table",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrEvaluate:{
		insertHrEvaluate:{
			url: `${config.API_URL}/set/hr/insertHrEvaluate`,
			name: "添加人员评价记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrEvaluate:{
			url: `${config.API_URL}/set/hr/deleteHrEvaluate`,
			name: "删除人员评价记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrEvaluateByIds:{
			url: `${config.API_URL}/set/hr/deleteHrEvaluateByIds`,
			name: "批量删除人员评价记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrEvaluate:{
			url: `${config.API_URL}/set/hr/updateHrEvaluate`,
			name: "更新人员评价记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrEvaluateQueryList: {
			url: `${config.API_URL}/get/hr/getHrEvaluateQueryList`,
			name: "查询领导评价",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrEvaluateByUserIdList: {
			url: `${config.API_URL}/get/hr/getHrEvaluateByUserIdList`,
			name: "获取人员评价列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrWelfare:{
		importHrWelfareRecord:{
			url: `${config.API_URL}/set/hr/importHrWelfareRecord`,
			name: "批量导入人员福利",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrWelfareRecord:{
			url: `${config.API_URL}/set/hr/insertHrWelfareRecord`,
			name: "添加人员福利记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrWelfareRecord:{
			url: `${config.API_URL}/set/hr/deleteHrWelfareRecord`,
			name: "删除人员福利记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrWelfareRecordByIds:{
			url: `${config.API_URL}/set/hr/deleteHrWelfareRecordByIds`,
			name: "批量删除人员福利记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrWelfareRecord:{
			url: `${config.API_URL}/set/hr/updateHrWelfareRecord`,
			name: "更新人员福利记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrWelfareRecordList: {
			url: `${config.API_URL}/get/hr/getHrWelfareRecordList`,
			name: "获取福利列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrSalary:{
		importHrSalaryRecord:{
			url: `${config.API_URL}/set/hr/importHrSalaryRecord`,
			name: "导入工员薪资",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrSalaryRecord:{
			url: `${config.API_URL}/set/hr/insertHrSalaryRecord`,
			name: "添加人员薪资记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrSalaryRecord:{
			url: `${config.API_URL}/set/hr/deleteHrSalaryRecord`,
			name: "删除人员薪资记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrSalaryRecordByIds:{
			url: `${config.API_URL}/set/hr/deleteHrSalaryRecordByIds`,
			name: "批量删除人员薪资记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrSalaryRecord:{
			url: `${config.API_URL}/set/hr/updateHrSalaryRecord`,
			name: "更新人员薪资记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrSalaryRecordList: {
			url: `${config.API_URL}/get/hr/getHrSalaryRecordList`,
			name: "获取人员薪资列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrSalaryRecordList:{
			url: `${config.API_URL}/get/hr/getMyHrSalaryRecordList`,
			name: "获取人员薪资列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrSalaryRecordListForApp:{
			url: `${config.API_URL}/get/hr/getMyHrSalaryRecordListForApp`,
			name: "获取移动端个人薪资查询",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrSalaryRecordById:{
			url: `${config.API_URL}/get/hr/getHrSalaryRecordById`,
			name: "获取薪资记录详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrKpiItem:{
		insertHrKpiItem:{
			url: `${config.API_URL}/set/hr/insertHrKpiItem`,
			name: "添加考核指标",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrKpiItem:{
			url: `${config.API_URL}/set/hr/deleteHrKpiItem`,
			name: "删除考核指标",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrKpiItemByIds:{
			url: `${config.API_URL}/set/hr/deleteHrKpiItemByIds`,
			name: "批量删除考核指标",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrKpiItem:{
			url: `${config.API_URL}/set/hr/updateHrKpiItem`,
			name: "更新考核指标",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrKpiItemListForSelect:{
			url: `${config.API_URL}/get/hr/getHrKpiItemListForSelect`,
			name: "获取考核指标集",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrKpiItemList:{
			url: `${config.API_URL}/get/hr/getHrKpiItemList`,
			name: "获取考核指标列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrKpiPlan:{
		insertHrKpiPlan:{
			url: `${config.API_URL}/set/hr/insertHrKpiPlan`,
			name: "创建考核计划与考核指标",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updatePlanEffect:{
			url: `${config.API_URL}/set/hr/updatePlanEffect`,
			name: "考核计划生效",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrKpiPlan:{
			url: `${config.API_URL}/set/hr/deleteHrKpiPlan`,
			name: "删除考核计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrKpiPlanByIds:{
			url: `${config.API_URL}/set/hr/deleteHrKpiPlanByIds`,
			name: "批量删除考核计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrKpiPlan:{
			url: `${config.API_URL}/set/hr/updateHrKpiPlan`,
			name: "更新考核计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setScoreToUser:{
			url: `${config.API_URL}/set/hr/setScoreToUser`,
			name: "设置考核分",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMyHrKpiPlanList:{
			url: `${config.API_URL}/get/hr/getMyHrKpiPlanList`,
			name: "获取考核管理列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrKpiScoreList:{
			url: `${config.API_URL}/get/hr/getMyHrKpiScoreList`,
			name: "获取个人考核分数明细",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getKpiPlanForUserList:{
			url: `${config.API_URL}/get/hr/getKpiPlanForUserList`,
			name: "获取人员考核列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrKpiRecordForUser:{
			url: `${config.API_URL}/get/hr/getHrKpiRecordForUser`,
			name: "获取人员考核",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrKpiRecordForDetails:{
			url: `${config.API_URL}/get/hr/getHrKpiRecordForDetails`,
			name: "获取考核详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyKpiPlanForList:{
			url: `${config.API_URL}/get/hr/getMyKpiPlanForList`,
			name: "获取人员考核",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrKpiPlanById:{
			url: `${config.API_URL}/get/hr/getMyHrKpiPlanById`,
			name: "获取考核记录详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrTrainRecord:{
		insertHrTrainRecord:{
			url: `${config.API_URL}/set/hr/insertHrTrainRecord`,
			name: "发起人员培训申请",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrTrainRecord:{
			url: `${config.API_URL}/set/hr/deleteHrTrainRecord`,
			name: "删除人员培训计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrTrainRecordByIds:{
			url: `${config.API_URL}/set/hr/deleteHrTrainRecordByIds`,
			name: "批量删除人员培训计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrTrainRecord:{
			url: `${config.API_URL}/set/hr/updateHrTrainRecord`,
			name: "更新培训计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrTrainRecordList: {
			url: `${config.API_URL}/get/hr/getHrTrainRecordList`,
			name: "获取培训列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrTrainRecordApprovedList:{
			url: `${config.API_URL}/get/hr/getHrTrainRecordApprovedList`,
			name: "获取培训审批记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrRecruitTask:{
		insertHrRecruitTask:{
			url: `${config.API_URL}/set/hr/insertHrRecruitTask`,
			name: "创建考聘任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrRecruitTask:{
			url: `${config.API_URL}/set/hr/deleteHrRecruitTask`,
			name: "删除招聘任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrRecruitTaskByIds:{
			url: `${config.API_URL}/set/hr/deleteHrRecruitTaskByIds`,
			name: "批量删除招聘任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrRecruitTask:{
			url: `${config.API_URL}/set/hr/updateHrRecruitTask`,
			name: "更新招聘任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrRecruitTaskList: {
			url: `${config.API_URL}/get/hr/getHrRecruitTaskList`,
			name: "获取招聘任务列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrRecruitPlan:{
		insertHrRecruitPlan:{
			url: `${config.API_URL}/set/hr/insertHrRecruitPlan`,
			name: "创建招聘计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrRecruitPlan:{
			url: `${config.API_URL}/set/hr/deleteHrRecruitPlan`,
			name: "删除招聘计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrRecruitPlanByIds:{
			url: `${config.API_URL}/set/hr/deleteHrRecruitPlanByIds`,
			name: "批量删除招聘计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrRecruitPlan:{
			url: `${config.API_URL}/set/hr/updateHrRecruitPlan`,
			name: "更新招聘计划",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrRecruitPlanList: {
			url: `${config.API_URL}/get/hr/getHrRecruitPlanList`,
			name: "获取招聘计划列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrRecruitPlanForSelect:{
			url: `${config.API_URL}/get/hr/getHrRecruitPlanForSelect`,
			name: "获取当前可填报的招聘计划",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	hrRecruitNeeds:{
		insertHrRecruitNeeds:{
			url: `${config.API_URL}/set/hr/insertHrRecruitNeeds`,
			name: "添加招聘需求",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrRecruitNeeds:{
			url: `${config.API_URL}/set/hr/deleteHrRecruitNeeds`,
			name: "删除招聘需求",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrRecruitNeedsByIds:{
			url: `${config.API_URL}/set/hr/deleteHrRecruitNeedsByIds`,
			name: "批量删除招聘需求",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrRecruitNeeds:{
			url: `${config.API_URL}/set/hr/updateHrRecruitNeeds`,
			name: "更新招聘需求",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrRecruitNeedsList: {
			url: `${config.API_URL}/get/hr/getHrRecruitNeedsList`,
			name: "获取招聘需求列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getApprovedHrRecruitNeedsList:{
			url: `${config.API_URL}/get/hr/getApprovedHrRecruitNeedsList`,
			name: "获取待审批需求列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrCare:{
		insertHrCareRecord:{
			url: `${config.API_URL}/set/hr/insertHrCareRecord`,
			name: "添加员工关怀记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrCareRecord:{
			url: `${config.API_URL}/set/hr/deleteHrCareRecord`,
			name: "删除员工关怀记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrCareRecordByIds:{
			url: `${config.API_URL}/set/hr/deleteHrCareRecordByIds`,
			name: "批量删除员工福利记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrCareRecord:{
			url: `${config.API_URL}/set/hr/updateHrCareRecord`,
			name: "更新员工关怀信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrCareRecordList: {
			url: `${config.API_URL}/get/hr/getHrCareRecordList`,
			name: "获取人员职称评定列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrTitleEvaluation:{
		insertHrTitleEvaluation:{
			url: `${config.API_URL}/set/hr/insertHrTitleEvaluation`,
			name: "添加人员评定记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrTitleEvaluation:{
			url: `${config.API_URL}/set/hr/deleteHrTitleEvaluation`,
			name: "删除人员评定记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrTitleEvaluationByIds:{
			url: `${config.API_URL}/set/hr/deleteHrTitleEvaluationByIds`,
			name: "批量人员职称评定记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrTitleEvaluation:{
			url: `${config.API_URL}/set/hr/updateHrTitleEvaluation`,
			name: "更新人员评定信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrTitleEvaluationList: {
			url: `${config.API_URL}/get/hr/getHrTitleEvaluationList`,
			name: "获取人员职称评定列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrReinstatement:{
		insertHrReinstatement:{
			url: `${config.API_URL}/set/hr/insertHrReinstatement`,
			name: "添加人员复职记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrReinstatement:{
			url: `${config.API_URL}/set/hr/deleteHrReinstatement`,
			name: "删除人员复职记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrReinstatementByIds:{
			url: `${config.API_URL}/set/hr/deleteHrReinstatementByIds`,
			name: "批量删除人员复职记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrReinstatement:{
			url: `${config.API_URL}/set/hr/updateHrReinstatement`,
			name: "更新人员复职信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrReinstatementList: {
			url: `${config.API_URL}/get/hr/getHrReinstatementList`,
			name: "获取复职记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrLeave:{
		insertHrLeaveRecord:{
			url: `${config.API_URL}/set/hr/insertHrLeaveRecord`,
			name: "添加人员离职动记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrLeaveRecord:{
			url: `${config.API_URL}/set/hr/deleteHrLeaveRecord`,
			name: "删除人员离职记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrLeaveRecordByIds:{
			url: `${config.API_URL}/set/hr/deleteHrLeaveRecordByIds`,
			name: "批量删除人员离职记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrLeaveRecord:{
			url: `${config.API_URL}/set/hr/updateHrLeaveRecord`,
			name: "更新人员离职信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrLeaveRecordList: {
			url: `${config.API_URL}/get/hr/getHrLeaveRecordList`,
			name: "获取离职人员列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrTransfer:{
		insertHrPersonnelTransfer:{
			url: `${config.API_URL}/set/hr/insertHrPersonnelTransfer`,
			name: "添加人事调动记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrPersonnelTransfer:{
			url: `${config.API_URL}/set/hr/deleteHrPersonnelTransfer`,
			name: "删除人事调动记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrPersonnelTransferByIds:{
			url: `${config.API_URL}/set/hr/deleteHrPersonnelTransferByIds`,
			name: "批量删除人员调动记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrPersonnelTransfer:{
			url: `${config.API_URL}/set/hr/updateHrPersonnelTransfer`,
			name: "更新人事调动信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrPersonnelTransferList: {
			url: `${config.API_URL}/get/hr/getHrPersonnelTransferList`,
			name: "获取人员调动列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrPersonnelTransferList:{
			url: `${config.API_URL}/get/hr/getMyHrPersonnelTransferList`,
			name: "人个工作调动记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrWorkSkill:{
		importHrWorkSkills:{
			url: `${config.API_URL}/set/hr/importHrWorkSkills`,
			name: "工作技能导入",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrWorkSkills:{
			url: `${config.API_URL}/set/hr/insertHrWorkSkills`,
			name: "添加劳动持能记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrWorkSkills:{
			url: `${config.API_URL}/set/hr/deleteHrWorkSkills`,
			name: "删除劳动技能",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrWorkSkillsByIds:{
			url: `${config.API_URL}/set/hr/deleteHrWorkSkillsByIds`,
			name: "批量删除劳动技能",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrWorkSkills:{
			url: `${config.API_URL}/set/hr/updateHrWorkSkills`,
			name: "更新劳动技能",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrWorkSkillsList: {
			url: `${config.API_URL}/get/hr/getHrWorkSkillsList`,
			name: "工作特长列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrIncentive:{
		importHrIncentive:{
			url: `${config.API_URL}/set/hr/importHrIncentive`,
			name: "导入奖惩记录",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrIncentive:{
			url: `${config.API_URL}/set/hr/insertHrIncentive`,
			name: "添加奖惩记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrIncentive:{
			url: `${config.API_URL}/set/hr/deleteHrIncentive`,
			name: "删除奖惩记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrIncentiveByIds:{
			url: `${config.API_URL}/set/hr/deleteHrIncentiveByIds`,
			name: "批量删除奖惩记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrIncentive:{
			url: `${config.API_URL}/set/hr/updateHrIncentive`,
			name: "更新奖惩记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrIncentiveList: {
			url: `${config.API_URL}/get/hr/getHrIncentiveList`,
			name: "获取奖惩记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrIncentiveList: {
			url: `${config.API_URL}/get/hr/getMyHrIncentiveList`,
			name: "个人查询奖惩记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	hrWork:{
		importHrWorkRecord:{
			url: `${config.API_URL}/set/hr/importHrWorkRecord`,
			name: "导入劳动记录",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrWorkRecord:{
			url: `${config.API_URL}/set/hr/insertHrWorkRecord`,
			name: "添加工作记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrWorkRecord:{
			url: `${config.API_URL}/set/hr/deleteHrWorkRecord`,
			name: "删除工作记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrWorkRecordByIds:{
			url: `${config.API_URL}/set/hr/deleteHrWorkRecordByIds`,
			name: "批量删除工作记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrWorkRecord:{
			url: `${config.API_URL}/set/hr/updateHrWorkRecord`,
			name: "更新工作记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrWorkRecordList: {
			url: `${config.API_URL}/get/hr/getHrWorkRecordList`,
			name: "获取工作记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrWorkRecordList:{
			url: `${config.API_URL}/get/hr/getMyHrWorkRecordList`,
			name: "查询个人工作经历",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrLearn:{
		importHrLearnRecord:{
			url: `${config.API_URL}/set/hr/importHrLearnRecord`,
			name: "学习记录导入",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrLearnRecord:{
			url: `${config.API_URL}/set/hr/insertHrLearnRecord`,
			name: "添加学习记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrLearnRecord:{
			url: `${config.API_URL}/set/hr/deleteHrLearnRecord`,
			name: "删除学习记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrLearnRecordByIds:{
			url: `${config.API_URL}/set/hr/deleteHrLearnRecordByIds`,
			name: "批量删除学习记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrLearnRecord:{
			url: `${config.API_URL}/set/hr/updateHrLearnRecord`,
			name: "更新学习记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrLearnRecordList: {
			url: `${config.API_URL}/get/hr/getHrLearnRecordList`,
			name: "获取教育经历列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrLearnRecordList:{
			url: `${config.API_URL}/get/hr/getMyHrLearnRecordList`,
			name: "个人查询学习记录",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrLicence:{
		importHrLicence:{
			url: `${config.API_URL}/set/hr/importHrLicence`,
			name: "证照记录导入",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrLicence:{
			url: `${config.API_URL}/set/hr/insertHrLicence`,
			name: "添加证照记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrLicence:{
			url: `${config.API_URL}/set/hr/deleteHrLicence`,
			name: "删除证照记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrLicenceByIds:{
			url: `${config.API_URL}/set/hr/deleteHrLicenceByIds`,
			name: "批量删除证照记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrLicence:{
			url: `${config.API_URL}/set/hr/updateHrLicence`,
			name: "更新证照记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrLicenceList: {
			url: `${config.API_URL}/get/hr/getHrLicenceList`,
			name: "获取证照列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrLicenceById:{
			url: `${config.API_URL}/get/hr/getHrLicenceById`,
			name: "获取证照详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrLicenceList:{
			url: `${config.API_URL}/get/hr/getMyHrLicenceList`,
			name: "获取个人证照列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrLicenceListForApp:{
			url: `${config.API_URL}/get/hr/getHrLicenceListForApp`,
			name: "获取个人证照列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrContract:{
		importHrContract:{
			url: `${config.API_URL}/set/hr/importHrContract`,
			name: "人事合同导入",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrContract:{
			url: `${config.API_URL}/set/hr/insertHrContract`,
			name: "添加HR合同",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrContract:{
			url: `${config.API_URL}/set/hr/deleteHrContract`,
			name: "删除合同",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrContractByIds:{
			url: `${config.API_URL}/set/hr/deleteHrContractByIds`,
			name: "删除合同",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrContract:{
			url: `${config.API_URL}/set/hr/updateHrContract`,
			name: "更新合同",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrContractList: {
			url: `${config.API_URL}/get/hr/getHrContractList`,
			name: "获取合同列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrContractById:{
			url: `${config.API_URL}/get/hr/getHrContractById`,
			name: "获取人事合同详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrContractList: {
			url: `${config.API_URL}/get/hr/getMyHrContractList`,
			name: "查询自己的合同列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMyHrContractListForApp:{
			url: `${config.API_URL}/get/hr/getMyHrContractListForApp`,
			name: "获取个人人事合同列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDeskHrContractList: {
			url: `${config.API_URL}/get/hr/getDeskHrContractList`,
			name: "获取快到期的合同列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
	},
	hrUser: {
		getHrUserByAccountId:{
			url: `${config.API_URL}/get/hr/getHrUserByAccountId`,
			name: "获取个人事档案",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getHrUserListByDeptIdForSelect:{
			url: `${config.API_URL}/get/hr/getHrUserListByDeptIdForSelect`,
			name: "按部门获取账户信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrUserListByUserIds:{
			url: `${config.API_URL}/get/hr/getHrUserListByUserIds`,
			name: "按UserId获取人员列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrUserListByDeptId: {
			url: `${config.API_URL}/get/hr/getHrUserListByDeptId`,
			name: "获取部门下的人员列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		importHrUser:{
			url: `${config.API_URL}/set/hr/importHrUser`,
			name: "人事档案导入",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		insertHrUser: {
			url: `${config.API_URL}/set/hr/insertHrUser`,
			name: "添加人员信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrUser: {
			url: `${config.API_URL}/set/hr/deleteHrUser`,
			name: "删除人员信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteByHrUserIds:{
			url: `${config.API_URL}/set/hr/deleteByHrUserIds`,
			name: "批量删除人员信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrUser: {
			url: `${config.API_URL}/set/hr/updateHrUser`,
			name: "更新人员信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getExpUserRecord:{
			url: `${config.API_URL}/get/hr/getExpUserRecord`,
			name: "导出人员信息",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data,{
					headers: {}
				})
			}
		},
	},
	hrDepartment: {
		insertHrDepartment: {
			url: `${config.API_URL}/set/hr/insertHrDepartment`,
			name: "添加人员部门",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrDepartment: {
			url: `${config.API_URL}/set/hr/deleteHrDepartment`,
			name: "删除人员部门",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrDepartmentByBatch: {
			url: `${config.API_URL}/set/hr/deleteHrDepartment`,
			name: "批量删除人员部门",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateHrDepartment: {
			url: `${config.API_URL}/set/hr/updateHrDepartment`,
			name: "更新人员部门信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		importHrDepartment: {
			url: `${config.API_URL}/set/hr/importHrDepartment`,
			name: "导入人事部门",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		getHrDepartmentTree: {
			url: `${config.API_URL}/get/hr/getHrDepartmentTree`,
			name: "获取部门列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getDeptListByDeptIdForSelect:{
			url: `${config.API_URL}/get/hr/getDeptListByDeptIdForSelect`,
			name: "按条件获取部门列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getHrDeptByDeptIds:{
			url: `${config.API_URL}/get/hr/getHrDeptByDeptIds`,
			name: "获取部门列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	hrDic: {
		getHrDicTree: {
			url: `${config.API_URL}/get/hr/getHrDicTree`,
			name: "获取字典树",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getHrDicList: {
			url: `${config.API_URL}/get/hr/getHrDicList`,
			name: "字典明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getHrDicParentList: {
			url: `${config.API_URL}/get/hr/getHrDicParentList`,
			name: "获取字典数据",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/hr/insertHrDic`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/hr/deleteHrDic`,
			name: "删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		dropHrDic: {
			url: `${config.API_URL}/set/hr/dropHrDic`,
			name: "字典拖拽排序",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteHrDicByIds: {
			url: `${config.API_URL}/set/hr/deleteHrDicByIds`,
			name: "批量删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/hr/updateHrDic`,
			name: "更新字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getHrDicByCode: {
			url: `${config.API_URL}/get/hr/getHrDicByCode`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	},
}
