import config from "@/config"
import http from "@/utils/request"

export default {
	inviteEvaluation:{
		getInviteEvaluationByInviteIdList:{
			url: `${config.API_URL}/get/invite/getInviteEvaluationByInviteIdList`,
			name: "获取评标记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteEvaluation:{
			url: `${config.API_URL}/set/invite/insertInviteEvaluation`,
			name: "录入评标记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteEvaluation:{
			url: `${config.API_URL}/set/invite/deleteInviteEvaluation`,
			name: "删除评标记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteEvaluationByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteEvaluationByIds`,
			name: "批量删除评标记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteEvaluation:{
			url: `${config.API_URL}/set/invite/updateInviteEvaluation`,
			name: "更新评标记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteOpen:{
		getInviteOpenList:{
			url: `${config.API_URL}/get/invite/getInviteOpenList`,
			name: "获取开标记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteOpen:{
			url: `${config.API_URL}/set/invite/insertInviteOpen`,
			name: "录入开标记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteOpen:{
			url: `${config.API_URL}/set/invite/deleteInviteOpen`,
			name: "删除开标记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteOpenByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteOpenByIds`,
			name: "批量删除开标记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteOpen:{
			url: `${config.API_URL}/set/invite/updateInviteOpen`,
			name: "更新开标记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteBidding:{
		getInviteBiddingManageList:{
			url: `${config.API_URL}/get/invite/getInviteBiddingManageList`,
			name: "获取待评标记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getInviteBiddingList:{
			url: `${config.API_URL}/get/invite/getInviteBiddingList`,
			name: "获取招标记录列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteBidding:{
			url: `${config.API_URL}/set/invite/insertInviteBidding`,
			name: "录入投标文件记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteBidding:{
			url: `${config.API_URL}/set/invite/deleteInviteBidding`,
			name: "删除投标文件记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteBiddingByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteBiddingByIds`,
			name: "批量删除投标文件记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteBidding:{
			url: `${config.API_URL}/set/invite/updateInviteBidding`,
			name: "更新投标文件记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteFileSell:{
		getInviteFileSellList:{
			url: `${config.API_URL}/get/invite/getInviteFileSellList`,
			name: "获取招标保证金列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteFileSell:{
			url: `${config.API_URL}/set/invite/insertInviteFileSell`,
			name: "录入招标出售记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteFileSell:{
			url: `${config.API_URL}/set/invite/deleteInviteFileSell`,
			name: "删除招标出售记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteFileSellByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteFileSellByIds`,
			name: "批量删除招标出售记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteFileSell:{
			url: `${config.API_URL}/set/invite/updateInviteFileSell`,
			name: "更新招标出售记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteDeposit:{
		getInviteDepositList:{
			url: `${config.API_URL}/get/invite/getInviteDepositList`,
			name: "获取招标保证金列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteDeposit:{
			url: `${config.API_URL}/set/invite/insertInviteDeposit`,
			name: "录入招标保证金",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteDeposit:{
			url: `${config.API_URL}/set/invite/deleteInviteDeposit`,
			name: "删除招标保证金",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteDepositByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteDepositByIds`,
			name: "批量删除招标保证金",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteDeposit:{
			url: `${config.API_URL}/set/invite/updateInviteDeposit`,
			name: "更新招标保证金",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteFile:{
		getInviteFileListForSelect:{
			url: `${config.API_URL}/get/invite/getInviteFileListForSelect`,
			name: "获取招标文件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getInviteFileList:{
			url: `${config.API_URL}/get/invite/getInviteFileList`,
			name: "获取招标文件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteFile:{
			url: `${config.API_URL}/set/invite/insertInviteFile`,
			name: "录入招标文件",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteFile:{
			url: `${config.API_URL}/set/invite/deleteInviteFile`,
			name: "删除招标文件",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteFileByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteFileByIds`,
			name: "批量删除招标文件",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteFile:{
			url: `${config.API_URL}/set/invite/updateInviteFile`,
			name: "更新招标文件",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteScore:{
		getEntScoreListByEntId:{
			url: `${config.API_URL}/get/invite/getEntScoreListByEntId`,
			name: "获取企业评分列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		setInviteScore:{
			url: `${config.API_URL}/set/invite/setInviteScore`,
			name: "录入供应商评分记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertInviteScore:{
			url: `${config.API_URL}/set/invite/insertInviteScore`,
			name: "录入供应商评分记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteScore:{
			url: `${config.API_URL}/set/invite/deleteInviteScore`,
			name: "删除供应商评分记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteScoreByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteScoreByIds`,
			name: "批量删除供应商评分记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteScore:{
			url: `${config.API_URL}/set/invite/updateInviteScore`,
			name: "更新供应商评分记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteRecord:{
		getInviteRecordListForSelect:{
			url: `${config.API_URL}/get/invite/getInviteRecordListForSelect`,
			name: "获取招标信息选择列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getInviteRecordList:{
			url: `${config.API_URL}/get/invite/getInviteRecordList`,
			name: "获取招标信息列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteRecord:{
			url: `${config.API_URL}/set/invite/insertInviteRecord`,
			name: "发布招标信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteRecord:{
			url: `${config.API_URL}/set/invite/deleteInviteRecord`,
			name: "删除招标信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteRecordByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteRecordByIds`,
			name: "批量删除招标信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteRecord:{
			url: `${config.API_URL}/set/invite/updateInviteRecord`,
			name: "更新招标信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteEntLic:{
		getInviteEntLicListByEntId:{
			url: `${config.API_URL}/get/invite/getInviteEntLicListByEntId`,
			name: "按企业获取证照",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getInviteEntLicList:{
			url: `${config.API_URL}/get/invite/getInviteEntLicList`,
			name: "获取供应商证照列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteEntLic:{
			url: `${config.API_URL}/set/invite/insertInviteEntLic`,
			name: "录入供应商证照",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteEntLic:{
			url: `${config.API_URL}/set/invite/deleteInviteEntLic`,
			name: "删除供应商证照",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteEntLicByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteEntLicByIds`,
			name: "批量删除供应商证照",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteEntLic:{
			url: `${config.API_URL}/set/invite/updateInviteEntLic`,
			name: "更新供供应商证照",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteEnt:{
		getInviteEntListForSelect:{
			url: `${config.API_URL}/get/invite/getInviteEntListForSelect`,
			name: "获取供应商选择列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getInviteEntList:{
			url: `${config.API_URL}/get/invite/getInviteEntList`,
			name: "获取供应商列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		insertInviteEnt:{
			url: `${config.API_URL}/set/invite/insertInviteEnt`,
			name: "添加供应商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteEnt:{
			url: `${config.API_URL}/set/invite/deleteInviteEnt`,
			name: "删除供应商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteEntByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteEntByIds`,
			name: "批量删除供应商",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteEnt:{
			url: `${config.API_URL}/set/invite/updateInviteEnt`,
			name: "更新供应商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	inviteTeam:{
		getInviteTeamList:{
			url: `${config.API_URL}/get/invite/getInviteTeamList`,
			name: "获取成员列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getInviteTeamListForSelect:{
			url: `${config.API_URL}/get/invite/getInviteTeamListForSelect`,
			name: "获取评标小组列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertInviteTeam:{
			url: `${config.API_URL}/set/invite/insertInviteTeam`,
			name: "添加评标小组",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteTeam:{
			url: `${config.API_URL}/set/invite/deleteInviteTeam`,
			name: "删除评标小组",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteInviteTeamByIds:{
			url: `${config.API_URL}/set/invite/deleteInviteTeamByIds`,
			name: "批量删除评标小组",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteTeam:{
			url: `${config.API_URL}/set/invite/updateInviteTeam`,
			name: "更新评标小组",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		}
	},
	inviteDic:{
		getInviteDicTree: {
			url: `${config.API_URL}/get/invite/getInviteDicTree`,
			name: "获取字典树",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getInviteDicList: {
			url: `${config.API_URL}/get/invite/getInviteDicList`,
			name: "字典明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getInviteDicParentList: {
			url: `${config.API_URL}/get/invite/getInviteDicParentList`,
			name: "获取字典数据",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertInviteDic: {
			url: `${config.API_URL}/set/invite/insertInviteDic`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteInviteDic: {
			url: `${config.API_URL}/set/invite/deleteInviteDic`,
			name: "删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		dropInviteDic: {
			url: `${config.API_URL}/set/invite/dropInviteDic`,
			name: "字典拖拽排序",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteInviteDicByIds: {
			url: `${config.API_URL}/set/invite/deleteInviteDicByIds`,
			name: "批量删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateInviteDic: {
			url: `${config.API_URL}/set/invite/updateInviteDic`,
			name: "更新字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getInviteDicByCode: {
			url: `${config.API_URL}/get/invite/getInviteDicByCode`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	},
	inviteRole:{
		getInviteRole: {
			url: `${config.API_URL}/get/invite/getInviteRole`,
			name: "获取招标权限",
			get: async function () {
				return await http.get(this.url);
			}
		},
		setInviteRole: {
			url: `${config.API_URL}/set/invite/setInviteRole`,
			name: "设置招标权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	}
}
