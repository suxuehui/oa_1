import config from "@/config"
import http from "@/utils/request"

export default {
	licenceBorrowing: {
		getLicenceBorrowingList:{
			url: `${config.API_URL}/get/office/getLicenceBorrowingList`,
			name: "获取证照出借记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		createBpmByLicenceRecordRule:{
			url: `${config.API_URL}/set/office/createBpmByLicenceRecordRule`,
			name: "发起出借申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insertLicenceBorrowing: {
			url: `${config.API_URL}/set/office/insertLicenceBorrowing`,
			name: "添加出借记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteLicenceBorrowing: {
			url: `${config.API_URL}/set/office/deleteLicenceBorrowing`,
			name: "删除出借记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateLicenceBorrowing: {
			url: `${config.API_URL}/set/office/updateLicenceBorrowing`,
			name: "更新出借记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteLicenceBorrowingByIds: {
			url: `${config.API_URL}/set/office/deleteLicenceBorrowingByIds`,
			name: "批量删除出借记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	licenceRecord: {
		getLicenceRecordListForSelect:{
			url: `${config.API_URL}/get/office/getLicenceRecordListForSelect`,
			name: "获取单位证照备选列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getLicenceRecordList:{
			url: `${config.API_URL}/get/office/getLicenceRecordList`,
			name: "获取证书列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertLicenceRecord: {
			url: `${config.API_URL}/set/office/insertLicenceRecord`,
			name: "添加证照",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteLicenceRecord: {
			url: `${config.API_URL}/set/office/deleteLicenceRecord`,
			name: "删除证照",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateLicenceRecord: {
			url: `${config.API_URL}/set/office/updateLicenceRecord`,
			name: "更新证照",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteLicenceRecordByIds: {
			url: `${config.API_URL}/set/office/deleteLicenceRecordByIds`,
			name: "批量删除证照",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	licenceType: {
		getLicenceTypeList:{
			url: `${config.API_URL}/get/office/getLicenceTypeList`,
			name: "获取分类列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getLicenceTypeTree: {
			url: `${config.API_URL}/get/office/getLicenceTypeTree`,
			name: "获取分类树型结构",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertLicenceType: {
			url: `${config.API_URL}/set/office/insertLicenceType`,
			name: "添加证照类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteLicenceType: {
			url: `${config.API_URL}/set/office/deleteLicenceType`,
			name: "删除证照类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateLicenceType: {
			url: `${config.API_URL}/set/office/updateLicenceType`,
			name: "更新证照类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteLicenceTypeByIds: {
			url: `${config.API_URL}/set/office/deleteLicenceTypeByIds`,
			name: "批量删除证照类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	duty: {
		getDutyRecordById: {
			url: `${config.API_URL}/get/office/getDutyRecordById`,
			name: "获取值班记录详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDutyListBySortId: {
			url: `${config.API_URL}/get/office/getDutyListBySortId`,
			name: "获取当前值班列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDutyRecordListForApproval: {
			url: `${config.API_URL}/get/office/getDutyRecordListForApproval`,
			name: "获取值班待审批列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertDutyRecord: {
			url: `${config.API_URL}/set/office/insertDutyRecord`,
			name: "添加值班记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteDutyRecord: {
			url: `${config.API_URL}/set/office/deleteDutyRecord`,
			name: "删除值班记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateDutyRecord: {
			url: `${config.API_URL}/set/office/updateDutyRecord`,
			name: "更新值班记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		approvalDutyRecord: {
			url: `${config.API_URL}/set/office/approvalDutyRecord`,
			name: "审批值班记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getDutyRecordList: {
			url: `${config.API_URL}/get/office/getDutyRecordList`,
			name: "获取值班列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertDutySort: {
			url: `${config.API_URL}/set/office/insertDutySort`,
			name: "添加值班分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteDutySort: {
			url: `${config.API_URL}/set/office/deleteDutySort`,
			name: "删除值班分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateDutySort: {
			url: `${config.API_URL}/set/office/updateDutySort`,
			name: "更新值班分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getDutySortList: {
			url: `${config.API_URL}/get/office/getDutySortList`,
			name: "获取值办分类",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getDutySortListForSelect: {
			url: `${config.API_URL}/get/office/getDutySortListForSelect`,
			name: "获取已启用的分类列表",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	exam: {
		getExamGradeById: {
			url: `${config.API_URL}/get/office/getExamGradeById`,
			name: "获取考试成绩",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getExamTestListForSelect: {
			url: `${config.API_URL}/get/office/getExamTestListForSelect`,
			name: "获取考试列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getExamGradeOldList: {
			url: `${config.API_URL}/get/office/getExamGradeOldList`,
			name: "获取考试成绩列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getExamGradeList: {
			url: `${config.API_URL}/get/office/getExamGradeList`,
			name: "获取考试成绩列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		approvalExamTest: {
			url: `${config.API_URL}/set/office/approvalExamTest`,
			name: "试卷判卷",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		addExamGrade: {
			url: `${config.API_URL}/set/office/addExamGrade`,
			name: "提交考试成绩",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamGrade: {
			url: `${config.API_URL}/set/office/deleteExamGrade`,
			name: "删除考试成绩",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateExamGrade: {
			url: `${config.API_URL}/set/office/updateExamGrade`,
			name: "更新考试成绩",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getExamTestById: {
			url: `${config.API_URL}/get/office/getExamTestById`,
			name: "获取试卷详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getExamQuestionListById: {
			url: `${config.API_URL}/get/office/getExamQuestionListById`,
			name: "获取试题列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyExamTestList: {
			url: `${config.API_URL}/get/office/getMyExamTestList`,
			name: "获取试卷列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getExamQuestionRecordForSelect: {
			url: `${config.API_URL}/get/office/getExamQuestionRecordForSelect`,
			name: "获取试卷列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getExamTestManageList: {
			url: `${config.API_URL}/get/office/getExamTestManageList`,
			name: "获取试卷管理列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertExamTest: {
			url: `${config.API_URL}/set/office/insertExamTest`,
			name: "添加试题测试",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamTest: {
			url: `${config.API_URL}/set/office/deleteExamTest`,
			name: "删除试题测试",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamTestByIds: {
			url: `${config.API_URL}/set/office/deleteExamTestByIds`,
			name: "批量删除试题",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateExamTest: {
			url: `${config.API_URL}/set/office/updateExamTest`,
			name: "更新试题测试",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateExamTestStatus: {
			url: `${config.API_URL}/set/office/updateExamTestStatus`,
			name: "更新试题测试",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getExamQuestionRecordList: {
			url: `${config.API_URL}/get/office/getExamQuestionRecordList`,
			name: "获取试卷列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertExamQuestionRecord: {
			url: `${config.API_URL}/set/office/insertExamQuestionRecord`,
			name: "添加试卷试题",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamQuestionRecord: {
			url: `${config.API_URL}/set/office/deleteExamQuestionRecord`,
			name: "删除试卷试题",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateExamQuestionRecord: {
			url: `${config.API_URL}/set/office/updateExamQuestionRecord`,
			name: "更新试卷试题",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertExamTestSort: {
			url: `${config.API_URL}/set/office/insertExamTestSort`,
			name: "添加考试类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamTestSort: {
			url: `${config.API_URL}/set/office/deleteExamTestSort`,
			name: "删除考试类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateExamTestSort: {
			url: `${config.API_URL}/set/office/updateExamTestSort`,
			name: "更新试题分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getExamTestSortTree: {
			url: `${config.API_URL}/get/office/getExamTestSortTree`,
			name: "获取试题分类树结构",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertExamQuestions: {
			url: `${config.API_URL}/set/office/insertExamQuestions`,
			name: "添加试题",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamQuestions: {
			url: `${config.API_URL}/set/office/deleteExamQuestions`,
			name: "删除试题",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamQuestionsByIds: {
			url: `${config.API_URL}/set/office/deleteExamQuestionsByIds`,
			name: "批量删除试题",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateExamQuestions: {
			url: `${config.API_URL}/set/office/updateExamQuestions`,
			name: "更新试题内容",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getExamQuestionsListBySortId: {
			url: `${config.API_URL}/get/office/getExamQuestionsListBySortId`,
			name: "按分类获取试题列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getExamSortTree: {
			url: `${config.API_URL}/get/office/getExamSortTree`,
			name: "获取试题分类树结构",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertExamSort: {
			url: `${config.API_URL}/set/office/insertExamSort`,
			name: "添加试题类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteExamSort: {
			url: `${config.API_URL}/set/office/deleteExamSort`,
			name: "删除试题类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateExamSort: {
			url: `${config.API_URL}/set/office/updateExamSort`,
			name: "更新试题分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getMyExamTestById: {
			url: `${config.API_URL}/get/office/getMyExamTestById`,
			name: "获取考试详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	task: {
		getMyTaskListForDesk: {
			url: `${config.API_URL}/get/office/getMyTaskListForDesk`,
			name: "获取桌面个人任务",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getTaskProcessInfo: {
			url: `${config.API_URL}/get/office/getTaskProcessInfo`,
			name: "获取处理事件详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertTaskProcess: {
			url: `${config.API_URL}/set/office/insertTaskProcess`,
			name: "添加子任务处理结果",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTaskProcess: {
			url: `${config.API_URL}/set/office/deleteTaskProcess`,
			name: "删除子任务处理结果",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateTaskProcess: {
			url: `${config.API_URL}/set/office/updateTaskProcess`,
			name: "修改子任务的处理过程",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getGanttInfoByTaskId: {
			url: `${config.API_URL}/get/office/getGanttInfoByTaskId`,
			name: "获取Gantt详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getTaskById: {
			url: `${config.API_URL}/get/office/getTaskById`,
			name: "获取任务详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyTaskWorkList: {
			url: `${config.API_URL}/get/office/getMyTaskWorkList`,
			name: "获取我的任务",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyTaskWorkListForApp: {
			url: `${config.API_URL}/get/office/getMyTaskWorkListForApp`,
			name: "获取我的任务",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyChargeTaskList: {
			url: `${config.API_URL}/get/office/getMyChargeTaskList`,
			name: "获取负责和督查的任务",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getTaskAllocationList: {
			url: `${config.API_URL}/get/office/getTaskAllocationList`,
			name: "获取所有待分配子任务列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertTaskGanttLink: {
			url: `${config.API_URL}/set/office/insertTaskGanttLink`,
			name: "创建关联",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTaskGanttLink: {
			url: `${config.API_URL}/set/office/deleteTaskGanttLink`,
			name: "删除关联关系",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateTaskGanttLink: {
			url: `${config.API_URL}/set/office/updateTaskGanttLink`,
			name: "更新子任务路径",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getAssignmentTaskList: {
			url: `${config.API_URL}/get/office/getAssignmentTaskList`,
			name: "获取待分解任务列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertTaskGanttData: {
			url: `${config.API_URL}/set/office/insertTaskGanttData`,
			name: "创建子任务",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTaskGanttData: {
			url: `${config.API_URL}/set/office/deleteTaskGanttData`,
			name: "删除子任务",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateTaskGanttData: {
			url: `${config.API_URL}/set/office/updateTaskGanttData`,
			name: "更新子任务",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertTask: {
			url: `${config.API_URL}/set/office/insertTask`,
			name: "创建任务",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTask: {
			url: `${config.API_URL}/set/office/deleteTask`,
			name: "删除任务",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteTaskByIds: {
			url: `${config.API_URL}/set/office/deleteTaskByIds`,
			name: "批量删除任务",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateTask: {
			url: `${config.API_URL}/set/office/updateTask`,
			name: "更新任务",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		searchTaskList: {
			url: `${config.API_URL}/get/office/searchTaskList`,
			name: "任务查询",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getManageTaskList: {
			url: `${config.API_URL}/get/office/getManageTaskList`,
			name: "获取任务列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyTaskProcessList: {
			url: `${config.API_URL}/get/office/getMyTaskProcessList`,
			name: "获取子任务的处理过程列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	},
	vehicle: {
		getVehicleReturnList: {
			url: `${config.API_URL}/get/office/getVehicleReturnList`,
			name: "获取待还车记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getVehicleApprovalResult: {
			url: `${config.API_URL}/get/office/getVehicleApprovalResult`,
			name: "获取审批结果",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getVehicleOperatorUserList: {
			url: `${config.API_URL}/get/office/getVehicleOperatorUserList`,
			name: "获取调度人员列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		setVehicleOperator: {
			url: `${config.API_URL}/set/office/setVehicleOperator`,
			name: "设置调度员",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getVehicleOperatorByOrgId: {
			url: `${config.API_URL}/get/office/getVehicleOperatorByOrgId`,
			name: "获取申请记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getVehicleApprovedList: {
			url: `${config.API_URL}/get/office/getVehicleApprovedList`,
			name: "获取车辆申请审批列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getVehicleApplyList: {
			url: `${config.API_URL}/get/office/getVehicleApplyList`,
			name: "获取申请记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		setReturnVehicle: {
			url: `${config.API_URL}/set/office/setReturnVehicle`,
			name: "车辆归还",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		setApprovalStatus: {
			url: `${config.API_URL}/set/office/setApprovalStatus`,
			name: "调度员审批",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertVehicleApply: {
			url: `${config.API_URL}/set/office/insertVehicleApply`,
			name: "车辆使用申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateVehicleApply: {
			url: `${config.API_URL}/set/office/updateVehicleApply`,
			name: "更新车辆使用申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleApply: {
			url: `${config.API_URL}/set/office/deleteVehicleApply`,
			name: "删除车辆使用申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getVehicleRepairUserList: {
			url: `${config.API_URL}/get/office/getVehicleRepairUserList`,
			name: "获取维修经办人列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getVehicleRepairRecordList: {
			url: `${config.API_URL}/get/office/getVehicleRepairRecordList`,
			name: "获取维修列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertVehicleRepairRecord: {
			url: `${config.API_URL}/set/office/insertVehicleRepairRecord`,
			name: "添加车辆维护记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleRepairRecordByIds: {
			url: `${config.API_URL}/set/office/deleteVehicleRepairRecordByIds`,
			name: "批量删除车辆维护记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleRepairRecord: {
			url: `${config.API_URL}/set/office/deleteVehicleRepairRecord`,
			name: "删除车辆维护记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateVehicleRepairRecord: {
			url: `${config.API_URL}/set/office/updateVehicleRepairRecord`,
			name: "更新车辆维护记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getVehicleListForSelect: {
			url: `${config.API_URL}/get/office/getVehicleListForSelect`,
			name: "获取车辆下拉列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getManageVehicleInfoList: {
			url: `${config.API_URL}/get/office/getManageVehicleInfoList`,
			name: "获取车辆列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertVehicleInfo: {
			url: `${config.API_URL}/set/office/insertVehicleInfo`,
			name: "添加车辆信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleInfoByIds: {
			url: `${config.API_URL}/set/office/deleteVehicleInfoByIds`,
			name: "批量删除车辆信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleInfo: {
			url: `${config.API_URL}/set/office/deleteVehicleInfo`,
			name: "删除车辆信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateVehicleInfo: {
			url: `${config.API_URL}/set/office/updateVehicleInfo`,
			name: "更新车辆信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getVehicleDriverList: {
			url: `${config.API_URL}/get/office/getVehicleDriverList`,
			name: "获取司机列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertVehicleDriver: {
			url: `${config.API_URL}/set/office/insertVehicleDriver`,
			name: "添加司机信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleDriver: {
			url: `${config.API_URL}/set/office/deleteVehicleDriver`,
			name: "删除司机信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateVehicleDriver: {
			url: `${config.API_URL}/set/office/updateVehicleDriver`,
			name: "更新司机信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getVehicleDriverListForSelect: {
			url: `${config.API_URL}/get/office/getVehicleDriverListForSelect`,
			name: "获取可调度车辆",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCanUsedVehicleList: {
			url: `${config.API_URL}/get/office/getCanUsedVehicleList`,
			name: "获取可调度车辆",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCanUsedOilCardList: {
			url: `${config.API_URL}/get/office/getCanUsedOilCardList`,
			name: "获取油卡列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getVehicleOilCardList: {
			url: `${config.API_URL}/get/office/getVehicleOilCardList`,
			name: "获取可通的油卡列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertVehicleOilCard: {
			url: `${config.API_URL}/set/office/insertVehicleOilCard`,
			name: "添加油卡信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleOilCard: {
			url: `${config.API_URL}/set/office/deleteVehicleOilCard`,
			name: "删除油卡信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteVehicleOilCardByIds: {
			url: `${config.API_URL}/set/office/deleteVehicleOilCardByIds`,
			name: "批量删除油卡信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateVehicleOilCard: {
			url: `${config.API_URL}/set/office/updateVehicleOilCard`,
			name: "更新油卡信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	},
	fixed: {
		getFixedAssetsRepairList: {
			url: `${config.API_URL}/get/office/getFixedAssetsRepairList`,
			name: "获取维修列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		addFixedAssetsAllocation: {
			url: `${config.API_URL}/set/office/addFixedAssetsAllocation`,
			name: "资产调拨",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertFixedAssetsRepair: {
			url: `${config.API_URL}/set/office/insertFixedAssetsRepair`,
			name: "发起维修",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateFixedAssetsRepair: {
			url: `${config.API_URL}/set/office/updateFixedAssetsRepair`,
			name: "更新维修记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteFixedAssetsRepairByIds: {
			url: `${config.API_URL}/set/office/deleteFixedAssetsRepairByIds`,
			name: "批量删除维修记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteFixedAssetsRepair: {
			url: `${config.API_URL}/set/office/deleteFixedAssetsRepair`,
			name: "删除维修记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getFixedAssetsAllocationOldList: {
			url: `${config.API_URL}/get/office/getFixedAssetsAllocationOldList`,
			name: "获取历史调拨记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAllocationList: {
			url: `${config.API_URL}/get/office/getAllocationList`,
			name: "获取调拨列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getFixedAssetsApplyList: {
			url: `${config.API_URL}/get/office/getFixedAssetsApplyList`,
			name: "获取申请列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getFixedAssetsApplyOldList: {
			url: `${config.API_URL}/get/office/getFixedAssetsApplyOldList`,
			name: "获取历史申请列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		approvalFixedAssetsApply: {
			url: `${config.API_URL}/set/office/approvalFixedAssetsApply`,
			name: "固定资产申请审批",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertFixedAssetsApply: {
			url: `${config.API_URL}/set/office/insertFixedAssetsApply`,
			name: "发起申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateFixedAssetsApply: {
			url: `${config.API_URL}/set/office/updateFixedAssetsApply`,
			name: "更新申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteFixedAssetsApply: {
			url: `${config.API_URL}/set/office/deleteFixedAssetsApply`,
			name: "删除申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertFixedAssets: {
			url: `${config.API_URL}/set/office/insertFixedAssets`,
			name: "添加固定资产记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateFixedAssets: {
			url: `${config.API_URL}/set/office/updateFixedAssets`,
			name: "更新固定资产",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteFixedAssetsByIds: {
			url: `${config.API_URL}/set/office/deleteFixedAssetsByIds`,
			name: "批量删除固定资产记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		setRecoverFixedAssetsById: {
			url: `${config.API_URL}/set/office/setRecoverFixedAssetsById`,
			name: "收回固定资产",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteFixedAssets: {
			url: `${config.API_URL}/set/office/deleteFixedAssets`,
			name: "删除固定资产记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getApplyFixedAssetsList: {
			url: `${config.API_URL}/get/office/getApplyFixedAssetsList`,
			name: "获取可申请的固定资产列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		queryFixedAssetsList: {
			url: `${config.API_URL}/get/office/queryFixedAssetsList`,
			name: "查询固定资产列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getFixedAssetsList: {
			url: `${config.API_URL}/get/office/getFixedAssetsList`,
			name: "获取固定资产列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getFixedAssetsById: {
			url: `${config.API_URL}/get/office/getFixedAssetsById`,
			name: "获取固定资产详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertFixedAssetsSort: {
			url: `${config.API_URL}/set/office/insertFixedAssetsSort`,
			name: "创建固定资产的分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateFixedAssetsSort: {
			url: `${config.API_URL}/set/office/updateFixedAssetsSort`,
			name: "更新固定资产的分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteFixedAssetsSort: {
			url: `${config.API_URL}/set/office/deleteFixedAssetsSort`,
			name: "删除固定资产的分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getFixedAssetsSortTree: {
			url: `${config.API_URL}/get/office/getFixedAssetsSortTree`,
			name: "获取固定资产的分类",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertFixedAssetsStorage: {
			url: `${config.API_URL}/set/office/insertFixedAssetsStorage`,
			name: "创建仓库",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteFixedAssetsStorage: {
			url: `${config.API_URL}/set/office/deleteFixedAssetsStorage`,
			name: "删除仓库",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateFixedAssetsStorage: {
			url: `${config.API_URL}/set/office/updateFixedAssetsStorage`,
			name: "更新仓库信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getAllFixedAssetsStorageList: {
			url: `${config.API_URL}/get/office/getAllFixedAssetsStorageList`,
			name: "获取所有仓库列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getFixedAssetsStorageList: {
			url: `${config.API_URL}/get/office/getFixedAssetsStorageList`,
			name: "获取仓库列表",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	meeting: {
		getMyMeetingListForApp: {
			url: `${config.API_URL}/get/office/getMyMeetingListForApp`,
			name: "获取移动端待办会议",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyMeetingListForDesk: {
			url: `${config.API_URL}/get/office/getMyMeetingListForDesk`,
			name: "获取桌面会议",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyMeetingList: {
			url: `${config.API_URL}/get/office/getMyMeetingList`,
			name: "获取当前用户待参加会议",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getNotNotesMeetingList: {
			url: `${config.API_URL}/get/office/getNotNotesMeetingList`,
			name: "获取没有会议纪要的会议列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMeetingDeviceListName: {
			url: `${config.API_URL}/get/office/getMeetingDeviceListName`,
			name: "获取设备名称列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getApplyMeetingList: {
			url: `${config.API_URL}/get/office/getApplyMeetingList`,
			name: "获取待审批的会议列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyApplyMeetingList: {
			url: `${config.API_URL}/get/office/getMyApplyMeetingList`,
			name: "获取个人历史会议申请记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMeetingListByDayStr: {
			url: `${config.API_URL}/get/office/getMeetingListByDayStr`,
			name: "按日期获取当前会议室使用情况",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMeetingByDay: {
			url: `${config.API_URL}/get/office/getMeetingByDay`,
			name: "获取禁用的会议时间段",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyMeetingById: {
			url: `${config.API_URL}/get/office/getMyMeetingById`,
			name: "获取会议详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		cancelMeeting: {
			url: `${config.API_URL}/set/office/cancelMeeting`,
			name: "会议取消通知",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		sendMeetingMsg: {
			url: `${config.API_URL}/set/office/sendMeetingMsg`,
			name: "发送会议通知",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertMeeting: {
			url: `${config.API_URL}/set/office/insertMeeting`,
			name: "申请会议",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data, {headers: {'Content-Type': 'application/json;charset=UTF-8'}})
			}
		},
		deleteMeeting: {
			url: `${config.API_URL}/set/office/deleteMeeting`,
			name: "撤销会议",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateMeeting: {
			url: `${config.API_URL}/set/office/updateMeeting`,
			name: "申请会议",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data, {headers: {'Content-Type': 'application/json;charset=UTF-8'}})
			}
		},
		insertMeetingNotes: {
			url: `${config.API_URL}/set/office/insertMeetingNotes`,
			name: "添加会议记要",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data, {headers: {'Content-Type': 'application/json;charset=UTF-8'}})
			}
		},
		deleteMeetingNotes: {
			url: `${config.API_URL}/set/office/deleteMeetingNotes`,
			name: "删除会议记要",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteMeetingNotesByIds: {
			url: `${config.API_URL}/set/office/deleteMeetingNotesByIds`,
			name: "批量删除会议记要",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateMeetingNotes: {
			url: `${config.API_URL}/set/office/updateMeetingNotes`,
			name: "删除会议记要",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data, {headers: {'Content-Type': 'application/json;charset=UTF-8'}})
			}
		},
		getMyMeetingNotesListForDesk: {
			url: `${config.API_URL}/get/office/getMyMeetingNotesListForDesk`,
			name: "获取桌面会议记要",
			get: async function () {
				return await http.get(this.url);
			}
		},
		queryMeetingNotesList: {
			url: `${config.API_URL}/get/office/queryMeetingNotesList`,
			name: "会议记要查询",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMeetingNotesList: {
			url: `${config.API_URL}/get/office/getMeetingNotesList`,
			name: "获取会议记要列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertMeetingDevice: {
			url: `${config.API_URL}/set/office/insertMeetingDevice`,
			name: "添加会议室设备",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteMeetingDevice: {
			url: `${config.API_URL}/set/office/deleteMeetingDevice`,
			name: "删除会议室设备",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateMeetingDevice: {
			url: `${config.API_URL}/set/office/updateMeetingDevice`,
			name: "更新设备信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getMeetingDeviceList: {
			url: `${config.API_URL}/get/office/getMeetingDeviceList`,
			name: "获取会议设备列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMeetingRoomList: {
			url: `${config.API_URL}/get/office/getMeetingRoomList`,
			name: "会议室列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getCanUseDeviceList: {
			url: `${config.API_URL}/get/office/getCanUseDeviceList`,
			name: "获取权限内可用的会议室设备",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyApplyMeetingRoomList: {
			url: `${config.API_URL}/get/office/getMyApplyMeetingRoomList`,
			name: "获取我历史申请过的会议室列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getCanUseMeetingRoomList: {
			url: `${config.API_URL}/get/office/getCanUseMeetingRoomList`,
			name: "获取当前用户可用的会议室",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insertMeetingRoom: {
			url: `${config.API_URL}/set/office/insertMeetingRoom`,
			name: "添加会议室",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateMeetingRoom: {
			url: `${config.API_URL}/set/office/updateMeetingRoom`,
			name: "更新会议室",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteMeetingRoom: {
			url: `${config.API_URL}/set/office/deleteMeetingRoom`,
			name: "删除会议室",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		}

	},
	officeSupplies: {
		insertOfficeSuppliesGrant: {
			url: `${config.API_URL}/set/office/insertOfficeSuppliesGrant`,
			name: "添加发放记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getMyApplyOfficeSuppliesList: {
			url: `${config.API_URL}/get/office/getMyApplyOfficeSuppliesList`,
			name: "获取个人历史申请记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getGrantOfficeList: {
			url: `${config.API_URL}/get/office/getGrantOfficeList`,
			name: "获取办公用品发放列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getGrantOfficeSuppliesList: {
			url: `${config.API_URL}/get/office/getGrantOfficeSuppliesList`,
			name: "待发放用品列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getOfficeSuppliesApplyRecordList: {
			url: `${config.API_URL}/get/office/getOfficeSuppliesApplyRecordList`,
			name: "获取审批列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertOfficeSuppliesApply: {
			url: `${config.API_URL}/set/office/insertOfficeSuppliesApply`,
			name: "发起申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteOfficeSuppliesApply: {
			url: `${config.API_URL}/set/office/deleteOfficeSuppliesApply`,
			name: "删除申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateOfficeSuppliesApply: {
			url: `${config.API_URL}/set/office/updateOfficeSuppliesApply`,
			name: "更新申请",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		approvalOfficeSuppliesApply: {
			url: `${config.API_URL}/set/office/approvalOfficeSuppliesApply`,
			name: "领用物资申请审批",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		insertOfficeSupplies: {
			url: `${config.API_URL}/set/office/insertOfficeSupplies`,
			name: "录入物资信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteOfficeSupplies: {
			url: `${config.API_URL}/set/office/deleteOfficeSupplies`,
			name: "删除物资信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteOfficeSuppliesByIds: {
			url: `${config.API_URL}/set/office/deleteOfficeSuppliesByIds`,
			name: "批量删除物资信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateOfficeSupplies: {
			url: `${config.API_URL}/set/office/updateOfficeSupplies`,
			name: "更新物资信息",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		importOfficeSuppliesInfo: {
			url: `${config.API_URL}/set/office/importOfficeSuppliesInfo`,
			name: "批量导入物资",
			post: async function (data, config = {}) {
				return await http.postFile(this.url, data, config);
			}
		},
		getApplyOfficeSuppliesList: {
			url: `${config.API_URL}/get/office/getApplyOfficeSuppliesList`,
			name: "获取可以领用的办公用品列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getOfficeSuppliesListBySortId: {
			url: `${config.API_URL}/get/office/getOfficeSuppliesListBySortId`,
			name: "按分类获取物资列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertOfficeSuppliesSort: {
			url: `${config.API_URL}/set/office/insertOfficeSuppliesSort`,
			name: "创建物资分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteOfficeSuppliesSort: {
			url: `${config.API_URL}/set/office/deleteOfficeSuppliesSort`,
			name: "删除物资分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateOfficeSuppliesSort: {
			url: `${config.API_URL}/set/office/updateOfficeSuppliesSort`,
			name: "更新物资分类",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getOfficeSuppliesSortTree: {
			url: `${config.API_URL}/get/office/getOfficeSuppliesSortTree`,
			name: "获取办公用品分类",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	diary: {
		getMyDiaryCount: {
			url: `${config.API_URL}/get/office/getMyDiaryCount`,
			name: "获取个人日志总数",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyDiaryListForApp: {
			url: `${config.API_URL}/get/office/getMyDiaryListForApp`,
			name: "获取个人工作日志列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDiaryCommentsList: {
			url: `${config.API_URL}/get/office/getDiaryCommentsList`,
			name: "获取日志评论",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertDiaryComments: {
			url: `${config.API_URL}/set/office/insertDiaryComments`,
			name: "添加日志评论",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteDiaryComments: {
			url: `${config.API_URL}/set/office/deleteDiaryComments`,
			name: "删除日志评论",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getShareDiaryUserList: {
			url: `${config.API_URL}/get/office/getShareDiaryUserList`,
			name: "获取共享人员列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getShareDiaryList: {
			url: `${config.API_URL}/get/office/getShareDiaryList`,
			name: "获取他人分享的工作日志",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMySubordinatesDiaryList: {
			url: `${config.API_URL}/get/office/getShareDiaryList`,
			name: "获取下属的工作日志",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyDiaryList: {
			url: `${config.API_URL}/get/office/getMyDiaryList`,
			name: "获取当前用户的历史工作日志",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getDiaryConfig: {
			url: `${config.API_URL}/get/office/getDiaryConfig`,
			name: "获取工作日志配置",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyDiaryById: {
			url: `${config.API_URL}/get/office/getMyDiaryById`,
			name: "获取工作日志详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		setDiaryConfig: {
			url: `${config.API_URL}/set/office/setDiaryConfig`,
			name: "设置工作日志权限",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data, {headers: {'Content-Type': 'application/json;charset=UTF-8'}})
			}
		},
		createDiary: {
			url: `${config.API_URL}/set/office/createDiary`,
			name: "创建工作日志",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteDiary: {
			url: `${config.API_URL}/set/office/deleteDiary`,
			name: "删除工作日志",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateDiary: {
			url: `${config.API_URL}/set/office/updateDiary`,
			name: "工作日志更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		}
	},
	attend: {
		insertAttend: {
			url: `${config.API_URL}/set/office/insertAttend`,
			name: "添加考勤记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getTotalAttendList: {
			url: `${config.API_URL}/get/office/getTotalAttendList`,
			name: "获取考勤列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyAttendYear: {
			url: `${config.API_URL}/get/office/getMyAttendYear`,
			name: "获取个人考勤的全部年份",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getMyAttendList: {
			url: `${config.API_URL}/get/office/getMyAttendList`,
			name: "获取个人考勤",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyOutAttendList: {
			url: `${config.API_URL}/get/office/getMyOutAttendList`,
			name: "获取外出列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyLeaveList: {
			url: `${config.API_URL}/get/office/getMyLeaveList`,
			name: "获取人员请假列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyTravelList: {
			url: `${config.API_URL}/get/office/getMyLeaveList`,
			name: "获取出差列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyDutyList: {
			url: `${config.API_URL}/get/office/getMyLeaveList`,
			name: "获取值班列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyOverTimeList: {
			url: `${config.API_URL}/get/office/getMyOverTimeList`,
			name: "获取加班列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getAttendStatusDay: {
			url: `${config.API_URL}/get/office/getAttendStatusDay`,
			name: "获取个人上班考勤是否打卡",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getOffWorkAttendStatusDay: {
			url: `${config.API_URL}/get/office/getOffWorkAttendStatusDay`,
			name: "获取个人下班考勤是否打卡",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getExpAttendRecord: {
			url: `${config.API_URL}/get/office/getExpAttendRecord`,
			name: "导出打卡记录详情",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data, {
					headers: {}
				})
			}
		},
		getExpAttendRecord2: {
			url: `${config.API_URL}/get/office/getExpAttendRecord2`,
			name: "导出打卡记录汇总",
			fileDownload: async function (data) {
				return await http.fileDownload(this.url, data, {
					headers: {}
				})
			}
		},
		getMyAttendListForApp: {
			url: `${config.API_URL}/get/office/getMyAttendListForApp`,
			name: "获取移动端考勤记录",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	workPlan: {
		getMyWorkPlanListForApp: {
			url: `${config.API_URL}/get/office/getMyWorkPlanListForApp`,
			name: "移动端我参与的工作计划",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyWorkPlanList: {
			url: `${config.API_URL}/get/office/getMyWorkPlanList`,
			name: "我参与的工作计划",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyWorkPlanForDesk: {
			url: `${config.API_URL}/get/office/getMyWorkPlanForDesk`,
			name: "获取个人工作计划",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getHoldWorkPlanList: {
			url: `${config.API_URL}/get/office/getHoldWorkPlanList`,
			name: "我负责的计划列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getSupWorkPlanList: {
			url: `${config.API_URL}/get/office/getSupWorkPlanList`,
			name: "我督查的计划列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertWorkPlan: {
			url: `${config.API_URL}/set/office/insertWorkPlan`,
			name: "创建工作计划",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data, {headers: {'Content-Type': 'application/json;charset=UTF-8'}})
			}
		},
		deleteWorkPlan: {
			url: `${config.API_URL}/set/office/deleteWorkPlan`,
			name: "删除工作计划",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteWorkPlanByIds: {
			url: `${config.API_URL}/set/office/deleteWorkPlanByIds`,
			name: "批量删除工作计划",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateWorkPlan: {
			url: `${config.API_URL}/set/office/updateWorkPlan`,
			name: "更新工作计划",
			post: async function (data, config = {}) {
				return await http.bigPost(this.url, data, {headers: {'Content-Type': 'application/json;charset=UTF-8'}})
			}
		},
		getWorkPlanProcessList: {
			url: `${config.API_URL}/get/office/getWorkPlanProcessList`,
			name: "获取工作计划处理反馈列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertWorkPlanProcess: {
			url: `${config.API_URL}/set/office/insertWorkPlanProcess`,
			name: "添加计划完成情况",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteWorkPlanProcess: {
			url: `${config.API_URL}/set/office/deleteWorkPlanProcess`,
			name: "删除工作计划处理结果",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateWorkPlanProcess: {
			url: `${config.API_URL}/set/office/updateWorkPlanProcess`,
			name: "更新工作计划处理结果",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		getManageWorkPlanList: {
			url: `${config.API_URL}/get/office/getManageWorkPlanList`,
			name: "获取工作维护列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getMyWorkPlanById: {
			url: `${config.API_URL}/get/office/getMyWorkPlanById`,
			name: "获取工作计划详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	entertain:{
		getMyEntertainListForDesk:{
			url: `${config.API_URL}/get/office/getMyEntertainListForDesk`,
			name: "获取桌面待办招待事件",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getEntertainListForSearch:{
			url: `${config.API_URL}/get/office/getEntertainListForSearch`,
			name: "查询招待事件列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getEntertainListForApprove:{
			url: `${config.API_URL}/get/office/getEntertainListForApprove`,
			name: "获取待审批事件",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getEntertainListForManage:{
			url: `${config.API_URL}/get/office/getEntertainListForManage`,
			name: "获取接待事件列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		insertEntertain: {
			url: `${config.API_URL}/set/office/insertEntertain`,
			name: "创建招待记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteEntertain: {
			url: `${config.API_URL}/set/office/deleteEntertain`,
			name: "删除招待记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		deleteEntertainByIds: {
			url: `${config.API_URL}/set/office/deleteEntertainByIds`,
			name: "批量删除招待记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
		updateEntertain: {
			url: `${config.API_URL}/set/office/updateEntertain`,
			name: "更新招待记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, {
					headers: {}
				});
			}
		},
	}
}
