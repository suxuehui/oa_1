import config from "@/config"
import http from "@/utils/request"

export default {
	echarts:{
		getProCostTypePie:{
			url: `${config.API_URL}/get/project/getProCostTypePie`,
			name: "项目费用类型统计",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProCostTypeBar:{
			url: `${config.API_URL}/get/project/getProCostTypeBar`,
			name: "项目费用类型统计",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProSortPie:{
			url: `${config.API_URL}/get/project/getProSortPie`,
			name: "项目类型统计",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProSortBar:{
			url: `${config.API_URL}/get/project/getProSortBar`,
			name: "项目类型统计",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	proProblemReply:{
		insertProProblemReply:{
			url: `${config.API_URL}/set/project/insertProProblemReply`,
			name: "回复项目问题",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProProblemReply:{
			url: `${config.API_URL}/set/project/deleteProProblemReply`,
			name: "删除问题回复记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProProblemReply:{
			url: `${config.API_URL}/set/project/updateProProblemReply`,
			name: "更新问题回复",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProblemReplyList:{
			url: `${config.API_URL}/get/project/getProblemReplyList`,
			name: "更新任务问题列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProProblemReplyById:{
			url: `${config.API_URL}/get/project/getProProblemReplyById`,
			name: "获取问题回复详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	proTaskProcess:{
		createProcess:{
			url: `${config.API_URL}/set/project/createProcess`,
			name: "添加项目任务处理结果的同时更新任务的进度",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insertProTaskProcess:{
			url: `${config.API_URL}/set/project/insertProTaskProcess`,
			name: "添加任务处理过程",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProTaskProcess:{
			url: `${config.API_URL}/set/project/deleteProTaskProcess`,
			name: "删除项目任务处理过程",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProTaskProcess:{
			url: `${config.API_URL}/set/project/updateProTaskProcess`,
			name: "更新项目处理过程",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProTaskProcessList:{
			url: `${config.API_URL}/get/project/getProTaskProcessList`,
			name: "按任务ID获取处理过程列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	proBudget:{
		insertProBudget:{
			url: `${config.API_URL}/set/project/insertProBudget`,
			name: "创建项目经费",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProBudget:{
			url: `${config.API_URL}/set/project/deleteProBudget`,
			name: "删除项目经费",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProBudgetByIds:{
			url: `${config.API_URL}/set/project/deleteProBudgetByIds`,
			name: "批量删除项目费用",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProBudget:{
			url: `${config.API_URL}/set/project/updateProBudget`,
			name: "更新项目经费",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProBudgetListByProId:{
			url: `${config.API_URL}/get/project/getProBudgetListByProId`,
			name: "项目预算列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	proProblem:{
		insertProProblem:{
			url: `${config.API_URL}/set/project/insertProProblem`,
			name: "添加项目问题反馈",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProProblem:{
			url: `${config.API_URL}/set/project/deleteProProblem`,
			name: "删除项目问题反馈",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProProblemByIds:{
			url: `${config.API_URL}/set/project/deleteProProblemByIds`,
			name: "批量删除项目问题反馈",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProProblem:{
			url: `${config.API_URL}/set/project/updateProProblem`,
			name: "更新项目问题",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProProblemListByPro:{
			url: `${config.API_URL}/get/project/getProProblemListByPro`,
			name: "获取项目问题",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	proFile:{
		insertProFile:{
			url: `${config.API_URL}/set/project/insertProFile`,
			name: "创建项目文档",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProFile:{
			url: `${config.API_URL}/set/project/updateProFile`,
			name: "更新项目文档",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProFile:{
			url: `${config.API_URL}/set/project/deleteProFile`,
			name: "删除项目文档",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProFileByIds:{
			url: `${config.API_URL}/set/project/deleteProFileByIds`,
			name: "批量删除项目文档",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProFileListByProId:{
			url: `${config.API_URL}/get/project/getProFileListByProId`,
			name: "获取项目子任务列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	proApproval:{
		setApprovalProRecord:{
			url: `${config.API_URL}/set/project/setApprovalProRecord`,
			name: "添加项目审批意见",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	proLink:{
		insertProTaskLink:{
			url: `${config.API_URL}/set/project/insertProTaskLink`,
			name: "创建任务关联",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProTaskLink:{
			url: `${config.API_URL}/set/project/deleteProTaskLink`,
			name: "删除任务关联记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProTaskLink:{
			url: `${config.API_URL}/set/project/updateProTaskLink`,
			name: "更新任务关联记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	},
	proTask:{
		insertProTask:{
			url: `${config.API_URL}/set/project/insertProTask`,
			name: "创建子任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProTask:{
			url: `${config.API_URL}/set/project/deleteProTask`,
			name: "删除子任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProTask:{
			url: `${config.API_URL}/set/project/updateProTask`,
			name: "更新子任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMyTaskWorkList:{
			url: `${config.API_URL}/get/project/getMyTaskWorkList`,
			name: "获取项目子任务列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProTaskInfo:{
			url: `${config.API_URL}/get/project/getProTaskInfo`,
			name: "获取项目子任务列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getTaskAllocationList:{
			url: `${config.API_URL}/get/project/getTaskAllocationList`,
			name: "获取所有待分配子任务列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProTaskById:{
			url: `${config.API_URL}/get/project/getProTaskById`,
			name: "获取任务子任务详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	proRole:{
		setProRole:{
			url: `${config.API_URL}/set/project/setProRole`,
			name: "设置项目人员权限",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProRoleInfo:{
			url: `${config.API_URL}/get/project/getProRoleInfo`,
			name: "按状态获取项目列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getProApprovalUserList:{
			url: `${config.API_URL}/get/project/getProApprovalUserList`,
			name: "获取项项目审批人员",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	proRecord:{
		insertProRecord:{
			url: `${config.API_URL}/set/project/insertProRecord`,
			name: "创建项目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProRecord:{
			url: `${config.API_URL}/set/project/deleteProRecord`,
			name: "删除项目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProRecordByIds:{
			url: `${config.API_URL}/set/project/deleteProRecordByIds`,
			name: "批量删除项目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProRecord:{
			url: `${config.API_URL}/set/project/updateProRecord`,
			name: "更新项目信息",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		approvalProRecord:{
			url: `${config.API_URL}/set/project/approvalProRecord`,
			name: "提交审批",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProRecordListByStatus:{
			url: `${config.API_URL}/get/project/getProRecordListByStatus`,
			name: "按状态获取项目列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProRecordListByQuery:{
			url: `${config.API_URL}/get/project/getProRecordListByQuery`,
			name: "项目记录查询",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProRecordListForApproval:{
			url: `${config.API_URL}/get/project/getProRecordListForApproval`,
			name: "获取待审批项目列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProRecordById:{
			url: `${config.API_URL}/get/project/getProRecordById`,
			name: "按项目Id获取项目信息",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProRecordListForSelect:{
			url: `${config.API_URL}/get/project/getProRecordListForSelect`,
			name: "获取项目选择列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProRecordForFilter:{
			url: `${config.API_URL}/get/project/getProRecordForFilter`,
			name: "按分类获取项目",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
	},
	proSort:{
		insertProSort:{
			url: `${config.API_URL}/set/project/insertProSort`,
			name: "添加项目类型",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProSort:{
			url: `${config.API_URL}/set/project/deleteProSort`,
			name: "删除项目类型",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProSort:{
			url: `${config.API_URL}/set/project/updateProSort`,
			name: "更新项目类型",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProSortTree:{
			url: `${config.API_URL}/get/project/getProSortTree`,
			name: "获取项目分类树结构",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getProSort:{
			url: `${config.API_URL}/get/project/getProSort`,
			name: "获取项目分类",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	proDic:{
		getProDicTree: {
			url: `${config.API_URL}/get/project/getProDicTree`,
			name: "获取字典树",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getProDicList: {
			url: `${config.API_URL}/get/project/getProDicList`,
			name: "字典明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getProDicParentList: {
			url: `${config.API_URL}/get/project/getProDicParentList`,
			name: "获取字典数据",
			get: async function () {
				return await http.get(this.url);
			}
		},
		insert: {
			url: `${config.API_URL}/set/project/insertProDic`,
			name: "添加字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		delete: {
			url: `${config.API_URL}/set/project/deleteProDic`,
			name: "删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		dropProDic: {
			url: `${config.API_URL}/set/project/dropProDic`,
			name: "字典拖拽排序",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProDicByIds: {
			url: `${config.API_URL}/set/project/deleteProDicByIds`,
			name: "批量删除字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		update: {
			url: `${config.API_URL}/set/project/updateProDic`,
			name: "更新字典",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProDicByCode: {
			url: `${config.API_URL}/get/project/getProDicByCode`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	},
	proExpAccount:{
		insertProExpAccount:{
			url: `${config.API_URL}/set/project/insertProExpAccount`,
			name: "添加项目费用科目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteProExpAccount:{
			url: `${config.API_URL}/set/project/deleteProExpAccount`,
			name: "删除项目费用科目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateProExpAccount:{
			url: `${config.API_URL}/set/project/updateProExpAccount`,
			name: "更新项目费用科目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getProExpAccountTree: {
			url: `${config.API_URL}/get/project/getProExpAccountTree`,
			name: "获取字典数据",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		}
	}
}
