import config from "@/config"
import http from "@/utils/request"

export default {
	desk:{
		getAllFinishSupervisionForDesk:{
			url: `${config.API_URL}/get/supervision/getAllFinishSupervisionForDesk`,
			name: "获取所有督查事件",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getAllSupervisionForDesk:{
			url: `${config.API_URL}/get/supervision/getAllSupervisionForDesk`,
			name: "获取所有事件列表前5",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSupervisionProcessListForDesk:{
			url: `${config.API_URL}/get/supervision/getSupervisionProcessListForDesk`,
			name: "获取督查督办待办记录",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getManageSupervisionListForDesk:{
			url: `${config.API_URL}/get/supervision/getManageSupervisionListForDesk`,
			name: "获取监管事件列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getAllSupervisionProcessForDesk:{
			url: `${config.API_URL}/get/supervision/getAllSupervisionProcessForDesk`,
			name: "获取延时任务列表",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	analysis:{
		getBiSupervisionByLeadPie:{
			url: `${config.API_URL}/get/supervision/getBiSupervisionByLeadPie`,
			name: "前10位领导的人员工作量占比",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getBiSupervisionByMonthLine:{
			url: `${config.API_URL}/get/supervision/getBiSupervisionByMonthLine`,
			name: "按月份统计工作量",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getBiSupervisionTypePie:{
			url: `${config.API_URL}/get/supervision/getBiSupervisionTypePie`,
			name: "获取BPM使用分类前10的占比",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getBiSupervisionStatusTypePie:{
			url: `${config.API_URL}/get/supervision/getBiSupervisionStatusTypePie`,
			name: "获取督查督办当前状态总数",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getQuerySupervisionForDept:{
			url: `${config.API_URL}/get/supervision/getQuerySupervisionForDept`,
			name: "按部门汇总",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getQuerySupervisionForType:{
			url: `${config.API_URL}/get/supervision/getQuerySupervisionForType`,
			name: "按类型汇总",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	supervisionScore:{
		insertSupervisionScore:{
			url: `${config.API_URL}/set/supervision/insertSupervisionScore`,
			name: "创建评分",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setSupervisionScore:{
			url: `${config.API_URL}/set/supervision/setSupervisionScore`,
			name: "评分",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervisionScore:{
			url: `${config.API_URL}/set/supervision/deleteSupervisionScore`,
			name: "删除评分",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionScore:{
			url: `${config.API_URL}/set/supervision/updateSupervisionScore`,
			name: "更新评分",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSupervisionScoreList:{
			url: `${config.API_URL}/get/supervision/getSupervisionScoreList`,
			name: "督查督办积分",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	supervisionProblem:{
		insertSupervisionProblem:{
			url: `${config.API_URL}/set/supervision/insertSupervisionProblem`,
			name: "反馈问题",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervisionProblem:{
			url: `${config.API_URL}/set/supervision/deleteSupervisionProblem`,
			name: "删除反馈问题",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionProblem:{
			url: `${config.API_URL}/set/supervision/updateSupervisionProblem`,
			name: "更新反馈问题",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getMySupervisionProblemList:{
			url: `${config.API_URL}/get/supervision/getMySupervisionProblemList`,
			name: "获取问题列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeadSupervisionProblemList:{
			url: `${config.API_URL}/get/supervision/getLeadSupervisionProblemList`,
			name: "获取回复问题列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMySupervisionAnswerList:{
			url: `${config.API_URL}/get/supervision/getMySupervisionAnswerList`,
			name: "获取问题回复",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSupervisionProcessUserList:{
			url: `${config.API_URL}/get/supervision/getSupervisionProcessUserList`,
			name: "获取问题相关人员",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	supervisionResult:{
		insertSupervisionResult:{
			url: `${config.API_URL}/set/supervision/insertSupervisionResult`,
			name: "添加任务处理结果",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervisionResult:{
			url: `${config.API_URL}/set/supervision/deleteSupervisionResult`,
			name: "删除任务处理结果",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionResult:{
			url: `${config.API_URL}/set/supervision/updateSupervisionResult`,
			name: "更新任务处理结果",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSupervisionResultList:{
			url: `${config.API_URL}/get/supervision/getSupervisionResultList`,
			name: "获取任务处理结果",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	supervisionDelay:{
		insertSupervisionDelay:{
			url: `${config.API_URL}/set/supervision/insertSupervisionDelay`,
			name: "发起延期审批申请",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervisionDelay:{
			url: `${config.API_URL}/set/supervision/deleteSupervisionDelay`,
			name: "删除延期申请记录",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionDelay:{
			url: `${config.API_URL}/set/supervision/updateSupervisionDelay`,
			name: "延期更改",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionDelayApply:{
			url: `${config.API_URL}/set/supervision/updateSupervisionDelayApply`,
			name: "延期审批",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getDelayApplyList:{
			url: `${config.API_URL}/get/supervision/getDelayApplyList`,
			name: "获取延期审批列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	supervisionProcess:{
		setChangeUserByProcess:{
			url: `${config.API_URL}/set/supervision/setChangeUserByProcess`,
			name: "任务转办",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		insertSupervisionProcess:{
			url: `${config.API_URL}/set/supervision/insertSupervisionProcess`,
			name: "任务分配成功",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervisionProcess:{
			url: `${config.API_URL}/set/supervision/deleteSupervisionProcess`,
			name: "任务删除成功",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionProcess:{
			url: `${config.API_URL}/set/supervision/updateSupervisionProcess`,
			name: "任务更新成功",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		revChangeUserProcess:{
			url: `${config.API_URL}/set/supervision/revChangeUserProcess`,
			name: "接收转办任务",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSupervisionProcessList:{
			url: `${config.API_URL}/get/supervision/getSupervisionProcessList`,
			name: "获取待处理的督查列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSupervisionProcessForChangeList:{
			url: `${config.API_URL}/get/supervision/getSupervisionProcessForChangeList`,
			name: "获取转办给我的列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getControlProcessList:{
			url: `${config.API_URL}/get/supervision/getControlProcessList`,
			name: "获取我所管控的任务列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSupervisionProcessForSelect:{
			url: `${config.API_URL}/get/supervision/getSupervisionProcessForSelect`,
			name: "获取我所管控的任务列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}

	},
	supervision:{
		insertSupervision:{
			url: `${config.API_URL}/set/supervision/insertSupervision`,
			name: "创建督查项目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervision:{
			url: `${config.API_URL}/set/supervision/deleteSupervision`,
			name: "删除督查项目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervisionByIds:{
			url: `${config.API_URL}/set/supervision/deleteSupervisionByIds`,
			name: "批量删除督查事件",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervision:{
			url: `${config.API_URL}/set/supervision/updateSupervision`,
			name: "更新督查项目",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		setFinishStatus:{
			url: `${config.API_URL}/set/supervision/setFinishStatus`,
			name: "设置督查督办为完成状态",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionStatus:{
			url: `${config.API_URL}/set/supervision/updateSupervisionStatus`,
			name: "更新督查督办状态",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSupervisionList:{
			url: `${config.API_URL}/get/supervision/getSupervisionList`,
			name: "获取我创建的督查事件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getQueryOldSupervisionList:{
			url: `${config.API_URL}/get/supervision/getQueryOldSupervisionList`,
			name: "获取我创建的督查事件",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getLeadManageSupervisionList:{
			url: `${config.API_URL}/get/supervision/getLeadManageSupervisionList`,
			name: "获取当前用户管控的事件列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSupervisionTree:{
			url: `${config.API_URL}/get/supervision/getSupervisionTree`,
			name: "获取督查列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSupervisionFinishList:{
			url: `${config.API_URL}/get/supervision/getSupervisionFinishList`,
			name: "获取完成督查督办列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getMobileMySupervisionList:{
			url: `${config.API_URL}/get/supervision/getMobileMySupervisionList`,
			name: "移动端督查督办列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSupervisionById:{
			url: `${config.API_URL}/get/supervision/getSupervisionById`,
			name: "获取督查督办事件详情",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	supervisionConfig:{
		insertSupervisionConfig: {
			url: `${config.API_URL}/set/supervision/insertSupervisionConfig`,
			name: "创建类型",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		deleteSupervisionConfig: {
			url: `${config.API_URL}/set/supervision/deleteSupervisionConfig`,
			name: "删除类型",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		updateSupervisionConfig: {
			url: `${config.API_URL}/set/supervision/updateSupervisionConfig`,
			name: "更新分类",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getAllSupervisionConfig:{
			url: `${config.API_URL}/get/supervision/getAllSupervisionConfig`,
			name: "获取类型列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		getSupervisionConfigForSelect:{
			url: `${config.API_URL}/get/supervision/getSupervisionConfigForSelect`,
			name: "获取督查类型列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSupervisionLeadList:{
			url: `${config.API_URL}/get/supervision/getSupervisionLeadList`,
			name: "获取督查领导列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		getSupervisionHandedUserForSelect:{
			url: `${config.API_URL}/get/supervision/getSupervisionHandedUserForSelect`,
			name: "获取督查领导列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}
	},
	supervisionIntegral:{
		setSupervisionIntegral:{
			url: `${config.API_URL}/set/supervision/setSupervisionIntegral`,
			name: "设置积分规则",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSupervisionIntegral:{
			url: `${config.API_URL}/get/supervision/getSupervisionIntegral`,
			name: "获取积分规则",
			get: async function () {
				return await http.get(this.url);
			}
		}
	},
	bpmBySupervisionList:{
		deleteSupervisionBpmList:{
			url: `${config.API_URL}/set/supervision/deleteSupervisionBpmList`,
			name: "设置积分规则",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
		getSupervisionBpmRecordList:{
			url: `${config.API_URL}/get/supervision/getSupervisionBpmRecordList`,
			name: "删除任务处理流程",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		createBpmBySupervisionRule:{
			url: `${config.API_URL}/set/supervision/createBpmBySupervisionRule`,
			name: "发起任务处理流程",
			post: async function (data) {
				return await http.post(this.url, data, {headers: {}});
			}
		},
	}
}
