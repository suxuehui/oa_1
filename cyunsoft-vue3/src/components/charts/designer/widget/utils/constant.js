export default {
	btnList:[
		{
			key: "charts",
			select: false,
			title: '图表组件',
			icon: "el-icon-histogram"
		},
		{
			key: "layers",
			select: false,
			title: '图层控制',
			icon: "el-icon-copy-document"
		},
		{
			key: "details",
			select: false,
			title: '详情设置',
			icon: "el-icon-cold-drink"
		}
	]
}
