import {
  addContainerWidgetSchema,
  addBasicFieldSchema,
  addAdvancedFieldSchema,
  addCustomWidgetSchema
} from '@/components/designer/form-designer/widget-panel/widgetsConfig'
import {
  registerCommonProperty,
  registerAdvancedProperty,
  registerEventProperty
} from '@/components/designer/form-designer/setting-panel/propertyRegister'


export default {
  addContainerWidgetSchema,
  addBasicFieldSchema,
  addAdvancedFieldSchema,
  addCustomWidgetSchema,

  registerCommonProperty,
  registerAdvancedProperty,
  registerEventProperty,
}
