export const cardSchema = {
  type: 'card',
  category: 'container',
  icon: 'sc-icon-el-card',
  widgetList: [],
  options: {
    name: '',
    label: 'card',
    hidden: false,
    folded: false,
    showFold: true,
    cardWidth: '100%',
    shadow: 'never',
    customClass: '',
  }
}

export const alertSchema = {
  type: 'alert',
  icon: 'el-icon-bell',
  formItemFlag: false,
  options: {
    name: '',
    title: 'Good things are coming...',
    type: 'info',
    description: '',
    closable: true,
    closeText: '',
    center: true,
    showIcon: false,
    effect: 'light',
    hidden: false,
    onClose: '',
    customClass: '',
  }
}

export const tooltipSchema = {
	type: 'tooltip',
	icon: 'el-icon-chat-square',
	formItemFlag: false,
	options: {
		name: '',
		title:'说明',
		content: '关于XXX的说明',
		placement:'top-start',
		effect: 'light',
		hidden: false,
	}
}
