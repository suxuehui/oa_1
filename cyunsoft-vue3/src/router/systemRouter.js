import config from "@/config"

//系统路由
const routes = [
	{
		name: "layout",
		path: "/",
		component: () => import('@/layout'),
		redirect: config.DASHBOARD_URL || '/dashboard',
		children: []
	},
	{
		path: "/resetPasswordByEmail",
		component: () => import('@/views/login/resetPasswordByEmail'),
		meta: {
			title: "邮件找回密码"
		}
	},
	{
		path: "/app",
		component: () => import('@/layout/app'),
		meta: {
			title: "移动端主页"
		}
	},
	{
		path: "/dapp",
		component: () => import('@/layout/app/indexd'),
		meta: {
			title: "钉钉端"
		}
	},
	{
		path: "/dapp/login",
		component: () => import('@/layout/app/login/indexd'),
		meta: {
			title: "钉钉端消息跳转处理"
		}
	},
	{
		path: "/wapp",
		component: () => import('@/layout/app/indexw'),
		meta: {
			title: "企业微信端"
		}
	},
	{
		path: "/wapp/login",
		component: () => import('@/layout/app/login/indexw'),
		meta: {
			title: "企业微信端消息跳转处理"
		}
	},
	{
		path: "/login",
		component: () => import('@/views/login'),
		meta: {
			title: "登录"
		}
	},
	{
		path: "/system/info",
		component: () => import('@/views/setting/system/info'),
		meta: {
			title: "软件注册"
		}
	},
	{
		path: "/resetPassword",
		component: () => import('@/views/login/resetPassword'),
		meta: {
			title: "重置密码"
		}
	},
	{
		path: "/forget_password",
		component: () => import('@/views/login/forgetPassword'),
		meta: {
			title: "找回密码"
		}
	},
	{
		path: "/register",
		component: () => import('@/views/other/register'),
		meta: {
			title: "系统信息"
		}
	},
]

export default routes;
