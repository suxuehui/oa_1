import sysConfig from "@/config";
import tool from "@/utils/tool";
export default {
	methods: {
		openFileOnLine(extName, attachId, role) {
			if(this.$TOOL.data.get("OFFICE_TYPE")=='0')
			{
				if(sysConfig.DOC_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openword?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openword?attachId='+attachId+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}
				}else if(sysConfig.EXCEL_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openexcel?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openexcel?attachId='+attachId+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}
				}else if(sysConfig.PPT_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openppt?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openppt?attachId='+attachId+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}
				}else if(sysConfig.PDF_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openpdf?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}else{
						POBrowser.openWindowModeless(this.$TOOL.data.get("ATTACH_HOST")+'/office/openpdf?attachId='+attachId+'&extName='+extName+'&openModeType='+role, 'width=1200px;height=800px;scroll=no;');
					}
				}else if (sysConfig.IMG_FILE.includes(extName)) {
					const image = new Image()
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						image.src = sysConfig.API_URL + '/get/file/getFileDown?attachId=' + tool.crypto.createParam(attachId) + "&extName=" + extName + "&ddtab=true";
					}else{
						image.src = sysConfig.API_URL + '/get/file/getFileDown?attachId=' + attachId + "&extName=" + extName + "&ddtab=true";
					}
						image.onload = () => {
							image.style.margin = "0 auto"
							image.style.display = "block"
							const newWin = window.open("", "_blank")
							newWin.document.write(image.outerHTML)
						}
				}
			}else if (this.$TOOL.data.get("OFFICE_TYPE") == '2') {
				if(sysConfig.DOC_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openword?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType=' + role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openword?attachId='+attachId+'&extName='+extName+'&openModeType=' + role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}
				}else if(sysConfig.EXCEL_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openexcel?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType='+role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openexcel?attachId='+attachId+'&extName='+extName+'&openModeType='+role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}
				}else if(sysConfig.PPT_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openppt?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType='+role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/office/wps/openppt?attachId='+attachId+'&extName='+extName+'&openModeType='+role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}
				}else if(sysConfig.PDF_FILE.includes(extName))
				{
					this.openPdfFile(attachId,extName,role);
				}else if (sysConfig.IMG_FILE.includes(extName)) {
					const image = new Image()
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + tool.crypto.createParam(attachId) + "&extName=" + extName+"&ddtab=true";
					}else{
						image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + attachId + "&extName=" + extName+"&ddtab=true";
					}
						image.onload = () => {
							image.style.margin = "0 auto"
							image.style.display = "block"
							const newWin = window.open("", "_blank")
							newWin.document.write(image.outerHTML)
						}
				}
			}
			else if (this.$TOOL.data.get("OFFICE_TYPE") == '3') {
				if(sysConfig.DOC_FILE.includes(extName)||sysConfig.EXCEL_FILE.includes(extName)||sysConfig.PPT_FILE.includes(extName))
				{
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/v3/weboffice/index?attachId='+tool.crypto.createParam(attachId)+'&extName='+extName+'&openModeType=' + role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}else{
						window.open(this.$TOOL.data.get("ATTACH_HOST")+'/v3/weboffice/index?attachId='+attachId+'&extName='+extName+'&openModeType=' + role+'&token='+tool.crypto.createParam(this.$TOOL.cookie.get("TOKEN"))+"&ddtab=true");
					}
				}else if(sysConfig.PDF_FILE.includes(extName))
				{
					this.openPdfFile(attachId,extName,role);
				}else if (sysConfig.IMG_FILE.includes(extName)) {
					const image = new Image();
					if (sysConfig.SM4_PRIVATE_KEY != '') {
						image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + tool.crypto.createParam(attachId) + "&extName=" + extName+"&ddtab=true";
					}else{
						image.src = sysConfig.API_URL+'/get/file/getFileDown?attachId=' + attachId + "&extName=" + extName+"&ddtab=true";
					}
					image.onload = () => {
						image.style.margin = "0 auto"
						image.style.display = "block"
						const newWin = window.open("", "_blank")
						newWin.document.write(image.outerHTML)
					}
				}
			}
		},
		openPdfFile(attachId, extName, openModeType) {
			if (this.$TOOL.data.get("OFD_TYPE") === '0') {
				if(extName=='.ofd')
				{
					this.$nextTick(() => {
						window.open('/#/file/preview/ofd?attachId=' + attachId + "&extName=" + extName+"&ddtab=true", "_blank");
					})
				}else {
					this.$nextTick(() => {
						window.open('/#/file/preview/pdf?attachId=' + attachId + "&extName=" + extName+"&ddtab=true", "_blank");
					})
				}
			} else if (this.$TOOL.data.get("OFD_TYPE") === '1') {
				if (sysConfig.SM4_PRIVATE_KEY != '') {
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openfxofd?attachId=' + tool.crypto.createParam(attachId) + "&openModeType=" + openModeType + "&extName=" + extName+"&ddtab=true", "_blank");
				}else{
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openfxofd?attachId=' + attachId + "&openModeType=" + openModeType + "&extName=" + extName+"&ddtab=true", "_blank");
				}
			} else if (this.$TOOL.data.get("OFD_TYPE") === '2') {
				if (sysConfig.SM4_PRIVATE_KEY != '') {
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openswofd?attachId=' + tool.crypto.createParam(attachId) + "&openModeType=" + openModeType + "&extName=" + extName+"&ddtab=true", "_blank");
				}else{
					window.open(this.$TOOL.data.get("ATTACH_HOST") + '/office/ofd/openswofd?attachId=' + attachId + "&openModeType=" + openModeType + "&extName=" + extName+"&ddtab=true", "_blank");
				}
			}
		},
	}
}
