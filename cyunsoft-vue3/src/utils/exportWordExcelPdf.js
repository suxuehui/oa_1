import docxtemplater from 'docxtemplater';
import { saveAs } from 'file-saver';
import html2Canvas from 'html2canvas';
import JsPDF from 'jspdf';
import JSZipUtils from 'jszip-utils';
import PizZip from 'pizzip';
import * as XLSX from 'xlsx';
export default {
	/**
	 * 导出 Excel 表格
	 * @param {*} title 表头信息
	 * @param {*} data 表格体内容
	 * @param {*} outFileName 保存的文件名
	 * @param {*} orderProp 序号列的字段名
	 */
	exportExcel(title, data, outFileName, orderProp) {
		let prop = 'prop'
		let titleList = this.toolTitleList(title)
		let tableData = [
			[...titleList]//导出表头
		] // 表格表头

		data.forEach((tableItem, tableIndex) => {
			let rowData = []
			title.forEach((titleItem) => {
				if (titleItem[prop] === orderProp) {
					rowData.push(tableIndex + 1)
				} else {
					rowData.push(tableItem[titleItem[prop]])
				}
			})
			tableData.push(rowData)
		})

		// 以下部分才是生成 Excel 的重点
		let aoaToSheet = XLSX.utils.aoa_to_sheet(tableData)
		let bookNew = XLSX.utils.book_new()
		XLSX.utils.book_append_sheet(bookNew, aoaToSheet, outFileName) // 工作簿名称
		XLSX.writeFile(bookNew, outFileName + '.xlsx') // 保存的文件名
	},
	/**
	 * 获取表头信息
	 * @param {*} data 传入的表头信息数组
	 * @returns
	 */
	toolTitleList(data) {
		let result = []
		data.forEach(item => {
			result.push(item['label'])
		})
		return result
	},


	/**
	 * 导出 Word 文档
	 * @param {Object} tempDocxPath 模板文件路径
	 * @param {Object} wordData 导出数据
	 * @param {Object} fileName 导出文件名
	 **/
	exportWord(tempDocxPath, wordData, fileName) {

		// 读取并获得模板文件的二进制内容
		JSZipUtils.getBinaryContent(tempDocxPath, function (error, content) {

			if (error) {
				throw error;
			}

			// 创建一个PizZip实例，内容为模板的内容
			let zip = new PizZip(content);
			// 创建并加载docxtemplater实例对象
			let doc = new docxtemplater();
			// doc.attachModule(new ImageModule(opts));
			doc.loadZip(zip);

			doc.setData(wordData);

			try {
				// 用模板变量的值替换所有模板变量
				doc.render();
			} catch (error) {
				// 抛出异常
				let e = {
					message: error.message,
					name: error.name,
					stack: error.stack,
					properties: error.properties
				};
				console.log(JSON.stringify({
					error: e
				}));
				throw error;
			}

			// 生成一个代表docxtemplater对象的zip文件（不是一个真实的文件，而是在内存中的表示）
			let out = doc.getZip().generate({
				type: "blob",
				mimeType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
			});
			// 将目标文件对象保存为目标类型的文件，并命名
			saveAs(out, fileName);
		});
	},

	/**
	 * 导出 PDF 文件
	 * @param {*} dom 要导出的 PDF 元素
	 * @param {*} outFileName 导出的文件名
	 */
	exportPdf(dom, outFileName) {
		var element = document.querySelector(dom); // 这个dom元素是要导出pdf的div容器
		// element.style.height = param.height
		setTimeout(() => {
			html2Canvas(element).then(function (canvas) {
				var contentWidth = canvas.width;
				var contentHeight = canvas.height;

				// 一页pdf显示html页面生成的canvas高度;
				var pageHeight = contentWidth / 592.28 * 841.89;
				// 未生成pdf的html页面高度
				var leftHeight = contentHeight;
				// 页面偏移
				var position = 0;
				// a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
				var imgWidth = 595.28;
				var imgHeight = 592.28 / contentWidth * contentHeight;

				var pageData = canvas.toDataURL('image/jpeg', 1.0);

				var pdf = new JsPDF('', 'pt', 'a4');

				// 有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
				// 当内容未超过pdf一页显示的范围，无需分页
				if (leftHeight < pageHeight) {
					pdf.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight);
				} else {
					while (leftHeight > 0) {
						pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight)
						leftHeight -= pageHeight;
						position -= 841.89;
						// 避免添加空白页
						if (leftHeight > 0) {
							pdf.addPage();
						}
					}
				}
				pdf.save(outFileName + '.pdf');
			});
			// element.style.height = param.minHeight
		}, 0);
	}
}
