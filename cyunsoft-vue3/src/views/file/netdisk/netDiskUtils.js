import sysConfig from "@/config";
import {isMobile} from "@/utils/util";
import tool from "@/utils/tool";
export default {
	methods: {
		down(fileItem) {
			if (sysConfig.SM4_PRIVATE_KEY != '') {
				this.$nextTick(() => {
					window.open(sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + tool.crypto.createParam(fileItem.netDiskId) + "&fileName=" + tool.crypto.createParam(fileItem.rootPath)+"&ddtab=true", "_blank");
				})
			}else{
				this.$nextTick(() => {
					window.open(sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + fileItem.netDiskId + "&fileName=" + encodeURIComponent(fileItem.rootPath)+"&ddtab=true", "_blank");
				})
			}
		},
		preview(fileItem) {
			let url = "/app/file/preview";
			if(isMobile())
			{
				if(sysConfig.DOC_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName=='.docx')
					{
						url='/app/file/preview/word';
					}
				}else if(sysConfig.EXCEL_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName=='.xlsx')
					{
						url='/app/file/preview/excel';
					}
				}else if(sysConfig.PPT_FILE.includes(fileItem.extName))
				{
					url="/app/file/preview/ppt";
				}else if(sysConfig.PDF_FILE.includes(fileItem.extName))
				{
					if(fileItem.extName=='.ofd')
					{
						url='/app/file/preview/ofd';
					}
				}else if (sysConfig.IMG_FILE.includes(fileItem.extName)) {
					url='/app/file/preview/image';
				}
				this.$nextTick(() => {
					this.$router.push({
						path: url,
						query: {
							netDiskId:fileItem.netDiskId,
							extName:fileItem.extName,
							isNetDisk:'true',
							attachId:fileItem.rootPath
						}
					})
				})
			}else {
				if (this.$TOOL.data.get("OPEN_ON_LINE_FLAG") == '0') {
					this.$nextTick(() => {
						window.open('/#/file/preview?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
					})
				} else if (this.$TOOL.data.get("OPEN_ON_LINE_FLAG") == '2') {
					if (sysConfig.DOC_FILE.includes(fileItem.extName)) {
						if(fileItem.extName=='.docx')
						{
							this.$nextTick(() => {
								window.open('/#/file/preview/word?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}else
						{
							this.$nextTick(() => {
								window.open('/#/file/preview?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}
					} else if (sysConfig.EXCEL_FILE.includes(fileItem.extName)) {
						if(fileItem.extName==".xlsx")
						{
							this.$nextTick(() => {
								window.open('/#/file/preview/excel?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}else {
							this.$nextTick(() => {
								window.open('/#/file/preview?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}
					} else if (sysConfig.PPT_FILE.includes(fileItem.extName)) {
						if(fileItem.extName==".pptx") {
							this.$nextTick(() => {
								window.open('/#/file/preview/ppt?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}else{
							this.$nextTick(() => {
								window.open('/#/file/preview?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}
					} else if (sysConfig.PDF_FILE.includes(fileItem.extName)) {
						if(fileItem.extName==".ofd") {
							this.$nextTick(() => {
								window.open('/#/file/preview/ofd?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}else{
							this.$nextTick(() => {
								window.open('/#/file/preview/pdf?netDiskId=' + fileItem.netDiskId + "&extName=" + fileItem.extName + "&isNetDisk=true&attachId=" + fileItem.rootPath+"&ddtab=true", "_blank");
							})
						}
					}else if (sysConfig.IMG_FILE.includes(fileItem.extName)) {
							const image = new Image()
							if (sysConfig.SM4_PRIVATE_KEY != '') {
								image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + tool.crypto.createParam(fileItem.netDiskId) + "&fileName=" + tool.crypto.createParam(fileItem.rootPath)+"&ddtab=true"
							}else{
								image.src = sysConfig.API_URL+'/get/file/downNetDiskFile?netDiskId=' + fileItem.netDiskId + "&fileName=" + encodeURIComponent(fileItem.rootPath)+"&ddtab=true"
							}
						image.onload = () => {
								image.style.margin = "0 auto"
								image.style.display = "block"
								const newWin = window.open("", "_blank")
								newWin.document.write(image.outerHTML)
							}
					}
				}
			}
		},
		async createFolder(netDiskId,sourcePath, newName) {
			var res = await this.$API.file.netDisk.createNetDiskFolder.post({netDiskId: netDiskId,sourcePath:sourcePath, folderName: newName});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getNetDiskFileList(sourcePath,this.netDiskId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async paste(fileType, optType, optId) {
			if (optId == "" || optId == null) {
				this.$alert('请选择需要粘贴的文件', "提示", {type: 'error'});
				return;
			}
			var res;
			if (optType == "2") {
				res = await this.$API.file.netDisk.copyNetDiskFile.post({sourceNetDiskId: optId, sourcePath:this.optFilePath,targetNetDiskId:this.netDiskId, targetPath: this.filePath});
				if (res.code == 200) {
					this.$message.success("操作成功");
					this.getNetDiskFileList(this.filePath,this.netDiskId);
				} else {
					this.$alert(res.message, "提示", {type: 'error'})
				}
			} else if (optType == "1") {
				if (optId == "" || optId == null) {
					this.$alert('请选择需要粘贴的文件', "提示", {type: 'error'});
					return;
				}
				res = await this.$API.file.netDisk.shearNetDiskFile.post({sourceNetDiskId: optId,sourcePath:this.optFilePath, targetNetDiskId:this.netDiskId,targetPath: this.filePath, type:fileType});
				if (res.code == 200) {
					this.$message.success("操作成功");
					this.getNetDiskFileList(this.filePath,this.netDiskId);
				} else {
					this.$alert(res.message, "提示", {type: 'error'})
				}
			}
		},
		async renameFolder(fileItem, newName) {
			var res = await this.$API.file.netDisk.renameFileFolderForNetDisk.post({netDiskId: fileItem.netDiskId, filePath:fileItem.rootPath,folderName: newName});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getNetDiskFileList(this.filePath,this.netDiskId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async rename(fileItem, newName) {
			var res = await this.$API.file.netDisk.renameNetDiskFileName.post({netDiskId: fileItem.netDiskId, filePath:fileItem.rootPath,fileName: newName+fileItem.extName});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getNetDiskFileList(this.filePath,this.netDiskId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async delete(fileItem) {
			var res = await this.$API.file.netDisk.deleteNetDiskFile.post({netDiskId: fileItem.netDiskId,sourcePath:fileItem.rootPath});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getNetDiskFileList(this.filePath,this.netDiskId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
		async deleteFolder(fileItem) {
			var res = await this.$API.file.netDisk.deleteFileFolderForNetDisk.post({netDiskId: fileItem.netDiskId,filePath:fileItem.rootPath});
			if (res.code == 200) {
				this.$message.success("操作成功");
				this.getNetDiskFileList(this.filePath,this.netDiskId);
			} else {
				this.$alert(res.message, "提示", {type: 'error'})
			}
		},
	}
}
