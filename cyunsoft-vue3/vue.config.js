const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
	lintOnSave:false,
	publicPath:'',
	productionSourceMap: false,
	devServer: {
		open: false, //运行后自动打开浏览器
		port: process.env.VUE_APP_PORT, //挂载端口
		proxy: {
			'/capi': {
				target: "http://127.0.0.1:91",
				//ws: true,
				secure: false,
				changeOrigin: true,
				pathRewrite: {'^/capi/': '/'},
				overlay: {
					warnings: false,
					errors: true
				},
			}
		},
	},
	chainWebpack:(config => {
		config.plugins.delete('preload');
		config.plugins.delete('prefetch');
		config.resolve.alias.set('vue-i18n', 'vue-i18n/dist/vue-i18n.cjs.js')
	}),
	configureWebpack: {
		cache:{
			type: 'filesystem',
			buildDependencies: {
				config: [__filename],
			},
		},
		performance: {
			hints: false
		},
		optimization: {
			splitChunks: {
				chunks: "all",
				automaticNameDelimiter: '~',
				name: "cyun-soft",
				cacheGroups: {
					vendor: {
						name: "modules",
						test: /[\\/]node_modules[\\/]/,
						priority: -10
					},
					elicons: {
						name: "elicons",
						test: /[\\/]node_modules[\\/]@element-plus[\\/]icons-vue[\\/]/
					},
					tinymce: {
						name: "tinymce",
						test: /[\\/]node_modules[\\/]tinymce[\\/]/
					},
					echarts: {
						name: "echarts",
						test: /[\\/]node_modules[\\/]echarts[\\/]/
					},
					xgplayer: {
						name: "xgplayer",
						test: /[\\/]node_modules[\\/]xgplayer.*[\\/]/
					},
					codemirror: {
						name: "codemirror",
						test: /[\\/]node_modules[\\/]codemirror[\\/]/
					},
					jsplumb:{
						name: "jsplumb",
						test: /[\\/]node_modules[\\/]jsplumb[\\/]/
					},
					docxtemplater:{
						name: "docxtemplater",
						test: /[\\/]node_modules[\\/]docxtemplater[\\/]/
					},
					fullcalendar: {
						name: "fullcalendar",
						test: /[\\/]node_modules[\\/]@fullcalendar.*[\\/]/
					},
					luckyexcel: {
						name: "luckyexcel",
						test: /[\\/]node_modules[\\/]luckyexcel[\\/]/
					},
					html2canvas:{
						name: "html2canvas",
						test: /[\\/]node_modules[\\/]html2canvas[\\/]/
					},
					"vue-pdf": {
						name: "vue-pdf",
						test: /[\\/]node_modules[\\/]vue-pdf[\\/]/
					},
					"dhtmlx-gantt": {
						name: "dhtmlx-gantt",
						test: /[\\/]node_modules[\\/]dhtmlx.*[\\/]/
					},
					"vue-office": {
						name: "vue-office",
						test: /[\\/]node_modules[\\/]@vue-office.*[\\/]/
					},
					"ace-builds": {
						name: "ace-builds",
						test: /[\\/]node_modules[\\/]ace-builds[\\/]/
					},
				}
			}
		}
	}
})
